# Set the base image
FROM registry.cn-hangzhou.aliyuncs.com/guandata/oracle_jdk:8u181

# File Author / Maintainer
MAINTAINER xujiaqi@guandata.com

RUN mkdir -p /opt
COPY target/atlas-0.0.1-SNAPSHOT.jar /opt/

ENTRYPOINT ["java", "-jar", "/opt/atlas-0.0.1-SNAPSHOT.jar"]
CMD []
