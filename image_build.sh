q#!/bin/sh

# step1: build server
echo "========== Start to build server ==========="
mvn clean package -DskipTests

# step2: build docker images
echo "====== Start to prepare docker image ======="
docker build -t registry.cn-hangzhou.aliyuncs.com/guandata/atlas-assistant-backend:$1 --no-cache .
docker push registry.cn-hangzhou.aliyuncs.com/guandata/atlas-assistant-backend:$1

echo "====== Congratulations! docker image is ready! ======="
