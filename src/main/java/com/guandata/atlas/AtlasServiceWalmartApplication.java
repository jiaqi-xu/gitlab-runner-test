package com.guandata.atlas;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
@MapperScan("com.guandata.atlas.mapper")
@EnableAspectJAutoProxy
public class AtlasServiceWalmartApplication {

	public static void main(String[] args) {
		SpringApplication.run(AtlasServiceWalmartApplication.class, args);
	}
}
