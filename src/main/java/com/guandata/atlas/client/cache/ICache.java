package com.guandata.atlas.client.cache;

public interface ICache {

    boolean put(String key, Object value, long seconds);

    <T> T get(String key);

    boolean del(String key);

    /**
     * 前缀模糊删除
     * @param key
     * @return
     */
    boolean prefixFuzzyDel(String key);
}
