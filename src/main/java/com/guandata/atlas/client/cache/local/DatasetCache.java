package com.guandata.atlas.client.cache.local;

import com.google.common.cache.CacheBuilder;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

/**
 * 数据集本地缓存
 */
@Component
public class DatasetCache extends GuavaCache{

    public DatasetCache() {
        cache = CacheBuilder.newBuilder().maximumSize(1000).initialCapacity(20)
                .expireAfterWrite(7200, TimeUnit.SECONDS).build();
    }

}
