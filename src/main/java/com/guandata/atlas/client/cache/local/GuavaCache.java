package com.guandata.atlas.client.cache.local;

import com.google.common.cache.Cache;
import com.guandata.atlas.client.cache.ICache;
import org.apache.commons.collections4.MapUtils;


import java.util.Set;
import java.util.concurrent.ConcurrentMap;

public class GuavaCache implements ICache {

    protected Cache<String, Object> cache;

    @Override
    public boolean put(String key, Object value, long seconds) {

        cache.put(key, value);
        return true;
    }

    @Override
    public <T> T get(String key) {
        return (T)cache.getIfPresent(key);
    }

    @Override
    public boolean del(String key) {
        cache.invalidate(key);
        return true;
    }

    @Override
    public boolean prefixFuzzyDel(String key) {

        ConcurrentMap<String, Object> currentMap = cache.asMap();
        if (MapUtils.isEmpty(currentMap)) {
            return true;
        }

        Set<String> allKeys = currentMap.keySet();
        for (String allKey : allKeys) {
            if (allKey.startsWith(key)) {
                del(allKey);
            }
        }
        return true;
    }
}
