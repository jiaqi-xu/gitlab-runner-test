package com.guandata.atlas.client.cache.local;

import com.guandata.atlas.common.utils.CacheKeyUtil;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class GuavaCacheWrapper {

    private static DatasetCache datasetCache;

    @Resource
    public void setDatasetCache(DatasetCache datasetCache) {
        GuavaCacheWrapper.datasetCache = datasetCache;
    }

    public static void delDataSetCacheByDsId(Integer dsId) {

        if (dsId == null) {
            return;
        }
        String cardDataCachePrefixKey = CacheKeyUtil.getCardDataCachePrefixKey(dsId);

        datasetCache.prefixFuzzyDel(cardDataCachePrefixKey);


    }

}
