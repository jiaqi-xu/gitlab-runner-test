package com.guandata.atlas.client.http;

import com.alibaba.fastjson.JSON;
import com.guandata.atlas.client.http.base.BaseHttpRequest;
import com.guandata.atlas.client.http.base.BaseHttpResponse;
import com.guandata.atlas.client.http.common.HttpTemplateException;
import com.guandata.atlas.client.http.common.HttpTemplateUtils;
import com.guandata.atlas.client.http.common.Method;
import com.guandata.atlas.client.http.config.HttpTemplateConfig;
import com.guandata.atlas.client.http.config.RouteConfig;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.routing.HttpRoute;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpRequestRetryHandler;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.util.EntityUtils;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class HttpTemplate {

    private CloseableHttpClient httpClient;

    private HttpTemplateHandler httpTemplateHandler;

    private Map<String, RouteConfig> routes;

    public HttpTemplate(HttpTemplateConfig httpTemplateConfig, HttpTemplateHandler httpTemplateHandler) {
        if (httpTemplateConfig == null) {
            this.httpClient = createHttpClient(new HttpTemplateConfig(null));
            this.routes = Collections.emptyMap();
        } else {
            this.httpClient = createHttpClient(httpTemplateConfig);
            this.routes = httpTemplateConfig.getRoutes();
        }
        if (httpTemplateHandler == null) {
            this.httpTemplateHandler = new HttpTemplateHandler() {

            };
        } else {
            this.httpTemplateHandler = httpTemplateHandler;
        }
    }

    public <T extends BaseHttpResponse> T execute(BaseHttpRequest<T> baseHttpRequest) throws Exception {
        HttpGet httpGet = null;
        HttpPost httpPost = null;
        BaseHttpResponse baseHttpResponse = null;
        if (baseHttpRequest == null)
            throw new IllegalArgumentException("request is null");
        if (baseHttpRequest.getResponseClass() == null)
            throw new IllegalArgumentException("HttpTemplate ResponseClass is null");
        if (!this.httpTemplateHandler.onStart(baseHttpRequest))
            return (T)HttpTemplateUtils.newResponse(baseHttpRequest, "HttpTemplate request deny");
        if (!baseHttpRequest.check())
            return (T) HttpTemplateUtils.newResponse(baseHttpRequest, "HttpTemplate request check fail");
        RouteConfig routeConfig = this.routes.get(baseHttpRequest.getRoute());
        if (routeConfig == null)
            routeConfig = new RouteConfig(baseHttpRequest.getRoute())
                    .setConnectionRequestTimeout(1000)
                    .setConnectTimeout(3000)
                    .setSocketTimeout(3000)
                    .setMaxPerRoute(400);
        if (baseHttpRequest.getMethod() == Method.POST) {
            httpPost = buildHttpPost(baseHttpRequest, routeConfig);
        } else if (baseHttpRequest.getMethod() == Method.UPLOAD) {
            httpPost = buildHttpUpload(baseHttpRequest, routeConfig);
        } else {
            httpGet = buildHttpGet(baseHttpRequest, routeConfig);
        }
        try {
            if (httpGet != null) {
                baseHttpResponse = this.httpClient.execute(httpGet, response -> parseResponse(baseHttpRequest, response));
            } else if (httpPost != null) {
                baseHttpResponse = this.httpClient.execute(httpPost, response -> parseResponse(baseHttpRequest, response));
            }

        } catch (IOException e) {
            this.httpTemplateHandler.onFinish(baseHttpRequest, (T)HttpTemplateUtils.newResponse(baseHttpRequest, "exception occur"), e);
            throw e;
        }
        this.httpTemplateHandler.onFinish(baseHttpRequest, (T)baseHttpResponse, null);
        return (T)baseHttpResponse;
    }

    private <T extends BaseHttpResponse> T parseResponse(BaseHttpRequest<T> baseHttpRequest, HttpResponse httpResponse) throws IOException {
        HttpEntity httpEntity = httpResponse.getEntity();
        String body = EntityUtils.toString(httpEntity, "utf-8");
        try {
            BaseHttpResponse baseHttpResponse = JSON.parseObject(body, baseHttpRequest.getResponseClass());
            if (baseHttpResponse == null)
                return (T)HttpTemplateUtils.newResponse(baseHttpRequest, null);
            baseHttpResponse.addHttpInfo(httpResponse.getStatusLine(), httpResponse.getAllHeaders(), body);
            return (T)baseHttpResponse;
        } catch (Exception e) {
            return (T)HttpTemplateUtils.newResponse(baseHttpRequest, body);
        }
    }

    private HttpPost buildHttpPost(BaseHttpRequest<?> request, RouteConfig routeConfig) throws HttpTemplateException {
        HttpPost httpPost = new HttpPost(getUri(request, routeConfig));
        Map<String, Object> body = request.getBody();
        if (null != body) {
            StringEntity entity = new StringEntity(JSON.toJSONString(body), "UTF-8");
            httpPost.setEntity(entity);
        }
        setRequestHeader(httpPost, request.getHeaders());
        setTimeout(httpPost, routeConfig);
        return httpPost;
    }

    private HttpPost buildHttpUpload(BaseHttpRequest<?> request, RouteConfig routeConfig) throws HttpTemplateException {
        HttpPost httpPost = new HttpPost(getUri(request, routeConfig));
        Map<String, File> fileItems = request.getFileItems();
        Set<Map.Entry<String, File>> entries = fileItems.entrySet();
        MultipartEntityBuilder entityBuilder = MultipartEntityBuilder.create();
        entries.forEach(s -> entityBuilder.addBinaryBody(s.getKey(), s.getValue()));
        httpPost.setEntity(entityBuilder.build());
        setRequestHeader(httpPost, request.getHeaders());
        setTimeout(httpPost, routeConfig);
        return httpPost;
    }

    private HttpGet buildHttpGet(BaseHttpRequest<?> request, RouteConfig routeConfig) throws HttpTemplateException {
        HttpGet httpGet = new HttpGet(getUri(request, routeConfig));
        setRequestHeader(httpGet, request.getHeaders());
        setTimeout(httpGet, routeConfig);
        return httpGet;
    }

    private URI getUri(BaseHttpRequest<?> request, RouteConfig routeConfig) throws HttpTemplateException {
        URIBuilder uriBuilder = new URIBuilder().setScheme(routeConfig.getScheme()).setHost(routeConfig.getHostname()).setPort(routeConfig.getPort()).setPath(request.getPath());
        Map<String, String> queryParams = request.getQueryParams();
        if (null != queryParams)
            for (Map.Entry<String, String> entry : queryParams.entrySet())
                uriBuilder.setParameter(entry.getKey(), entry.getValue());
        try {
            return uriBuilder.build();
        } catch (URISyntaxException e) {
            throw new HttpTemplateException("请求路径不合法", e);
        }
    }

    private void setRequestHeader(HttpRequestBase httpRequestBase, Map<String, String> header) {
        if (header != null)
            for (Map.Entry<String, String> entry : header.entrySet())
                httpRequestBase.setHeader(entry.getKey(), entry.getValue());
    }

    private void setTimeout(HttpRequestBase httpRequestBase, RouteConfig routeConfig) {
        RequestConfig requestConfig = RequestConfig.custom().setConnectionRequestTimeout(routeConfig.getConnectionRequestTimeout()).setConnectTimeout(routeConfig.getConnectTimeout()).setSocketTimeout(routeConfig.getSocketTimeout()).build();
        httpRequestBase.setConfig(requestConfig);
    }

    private CloseableHttpClient createHttpClient(HttpTemplateConfig httpTemplateConfig) {

        RegistryBuilder<ConnectionSocketFactory> registryBuilder = RegistryBuilder.create();
        Registry<ConnectionSocketFactory> registry = registryBuilder
                .register("http", PlainConnectionSocketFactory.getSocketFactory())
                .register("https", SSLConnectionSocketFactory.getSocketFactory()).build();
        PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager(registry);
        cm.setMaxTotal(httpTemplateConfig.getMaxTotal());
        cm.setDefaultMaxPerRoute(httpTemplateConfig.getDefaultMaxPerRoute());
        Map<String, RouteConfig> routes = httpTemplateConfig.getRoutes();
        if (routes != null)
            routes.forEach((name, routeConfig) -> {
                HttpRoute route = HttpTemplateUtils.determineRoute(routeConfig.getHostname(), routeConfig.getPort(), routeConfig.getScheme());
                cm.setMaxPerRoute(route, routeConfig.getMaxPerRoute());
            });
        return HttpClients.custom()
                .setConnectionManager(cm)

                .evictIdleConnections(10L, TimeUnit.SECONDS)

                .evictExpiredConnections()

                .setRetryHandler(new DefaultHttpRequestRetryHandler(0, false))
                .build();
    }

    public CloseableHttpClient getClient() {
        return this.httpClient;
    }
}
