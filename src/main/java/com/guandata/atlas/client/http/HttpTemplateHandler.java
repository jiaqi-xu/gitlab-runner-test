package com.guandata.atlas.client.http;

import com.guandata.atlas.client.http.base.BaseHttpRequest;
import com.guandata.atlas.client.http.base.BaseHttpResponse;

public interface HttpTemplateHandler {

    default boolean onStart(BaseHttpRequest<?> baseHttpRequest) {
        return true;
    }

    default <T extends BaseHttpResponse> void onFinish(BaseHttpRequest<T> baseHttpRequest, T baseHttpResponse, Exception e) {}
}
