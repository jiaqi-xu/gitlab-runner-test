package com.guandata.atlas.client.http.base;

import com.guandata.atlas.client.http.common.Method;

import java.io.File;
import java.util.Map;

public abstract class BaseHttpRequest<T extends BaseHttpResponse> {

    public abstract Map<String, String> getQueryParams();

    public abstract Map<String, Object> getBody();

    public abstract Map<String, File> getFileItems();

    public abstract Map<String, String> getHeaders();

    public abstract Map<String, String> getBizMap();

    public abstract String getName();

    public abstract String getRoute();

    public abstract Method getMethod();

    public abstract String getPath();

    public abstract Class<T> getResponseClass();

    public abstract boolean check();

    public String getDomain(){
        return null;
    }
}
