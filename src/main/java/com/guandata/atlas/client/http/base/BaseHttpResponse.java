package com.guandata.atlas.client.http.base;

import org.apache.http.Header;
import org.apache.http.StatusLine;

public abstract class BaseHttpResponse {

    private StatusLine statusLine;

    private Header[] headers;

    private String body;

    public abstract boolean success();

    public abstract String code();

    public StatusLine statusLine() {
        return this.statusLine;
    }

    public Header[] headers() {
        return this.headers;
    }

    public String body() {
        return this.body;
    }

    public void addHttpInfo(StatusLine statusLine, Header[] headers, String body) {
        this.statusLine = statusLine;
        this.headers = headers;
        this.body = body;
    }
}
