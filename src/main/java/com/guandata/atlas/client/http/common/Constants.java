package com.guandata.atlas.client.http.common;

public class Constants {

    public static final String SECURE_SCHEME = "https";

    public static final int DEFAULT_PORT = -1;

    public static final int MAX_TOTAL = 100;

    public static final int DEFAULT_MAX_PER_ROUTE = 100;

    public static final int MAX_PER_ROUTE = 100;

    public static final int CONNECTION_REQUEST_TIMEOUT = 1000;

    public static final int CONNECTION_TIMEOUT = 3000;

    public static final int SOCKET_TIMEOUT = 6000;
}
