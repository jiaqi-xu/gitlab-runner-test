package com.guandata.atlas.client.http.common;

public class HttpTemplateException extends Exception{

    public HttpTemplateException() {}

    public HttpTemplateException(String message) {
        super(message);
    }

    public HttpTemplateException(String message, Throwable cause) {
        super(message, cause);
    }

    public HttpTemplateException(Throwable cause) {
        super(cause);
    }
}
