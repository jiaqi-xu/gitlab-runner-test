package com.guandata.atlas.client.http.common;

import com.guandata.atlas.client.http.base.BaseHttpRequest;
import com.guandata.atlas.client.http.base.BaseHttpResponse;
import org.apache.http.HttpHost;
import org.apache.http.conn.routing.HttpRoute;

public class HttpTemplateUtils {

    public static <T extends BaseHttpResponse> T newResponse(BaseHttpRequest<T> baseHttpRequest, String message) {
        if (baseHttpRequest == null)
            throw new IllegalArgumentException("Request is null");
        if (baseHttpRequest.getResponseClass() == null)
            throw new IllegalArgumentException("ResponseClass is null");
        try {
            BaseHttpResponse baseHttpResponse = baseHttpRequest.getResponseClass().newInstance();
            baseHttpResponse.addHttpInfo(null, null, message);
            return (T)baseHttpResponse;
        } catch (Exception e) {
            throw new IllegalArgumentException("ResponseClass can't be instantiated", e);
        }
    }

    public static HttpRoute determineRoute(String hostname, int port, String scheme) {
        int determinePort;
        boolean secure = "https".equalsIgnoreCase(scheme);
        if (port < 0) {
            determinePort = secure ? 443 : 80;
        } else {
            determinePort = port;
        }
        HttpHost httpHost = new HttpHost(hostname, determinePort, scheme);
        return new HttpRoute(httpHost, null, secure);
    }
}
