package com.guandata.atlas.client.http.common;

public enum Method {

    GET, POST, UPLOAD;
}
