package com.guandata.atlas.client.http.config;

import java.util.HashMap;
import java.util.Map;

public class HttpTemplateConfig {
    private int maxTotal;

    private int defaultMaxPerRoute;

    private Map<String, RouteConfig> routes;

    public HttpTemplateConfig(Map<String, RouteConfig> routes) {
        if (routes == null) {
            this.routes = new HashMap<>(0);
        } else {
            this.routes = routes;
        }
        this.maxTotal = 100;
        this.defaultMaxPerRoute = 100;
    }

    public int getMaxTotal() {
        return this.maxTotal;
    }

    public HttpTemplateConfig setMaxTotal(int maxTotal) {
        this.maxTotal = maxTotal;
        return this;
    }

    public int getDefaultMaxPerRoute() {
        return this.defaultMaxPerRoute;
    }

    public HttpTemplateConfig setDefaultMaxPerRoute(int defaultMaxPerRoute) {
        this.defaultMaxPerRoute = defaultMaxPerRoute;
        return this;
    }

    public Map<String, RouteConfig> getRoutes() {
        return this.routes;
    }

    public HttpTemplateConfig setRoutes(Map<String, RouteConfig> routes) {
        this.routes = routes;
        return this;
    }
}
