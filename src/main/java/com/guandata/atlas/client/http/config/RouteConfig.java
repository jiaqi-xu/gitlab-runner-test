package com.guandata.atlas.client.http.config;

public class RouteConfig {

    private String hostname;

    private int port;

    private String scheme;

    private int maxPerRoute;

    private int connectionRequestTimeout;

    private int connectTimeout;

    private int socketTimeout;

    public RouteConfig(String hostname) {
        this.hostname = hostname;
        this.port = -1;
        this.scheme = "https";
        this.maxPerRoute = 100;
        this.connectionRequestTimeout = 1000;
        this.connectTimeout = 3000;
        this.socketTimeout = 6000;
    }

    public RouteConfig(String hostname, int port, String scheme) {
        this.hostname = hostname;
        this.port = port;
        this.scheme = scheme;
        this.maxPerRoute = 100;
        this.connectionRequestTimeout = 1000;
        this.connectTimeout = 3000;
        this.socketTimeout = 6000;
    }

    public String getScheme() {
        return this.scheme;
    }

    public RouteConfig setScheme(String scheme) {
        this.scheme = scheme;
        return this;
    }

    public String getHostname() {
        return this.hostname;
    }

    public RouteConfig setHostname(String hostname) {
        this.hostname = hostname;
        return this;
    }

    public int getPort() {
        return this.port;
    }

    public RouteConfig setPort(int port) {
        this.port = port;
        return this;
    }

    public int getMaxPerRoute() {
        return this.maxPerRoute;
    }

    public RouteConfig setMaxPerRoute(int maxPerRoute) {
        this.maxPerRoute = maxPerRoute;
        return this;
    }

    public int getConnectionRequestTimeout() {
        return this.connectionRequestTimeout;
    }

    public RouteConfig setConnectionRequestTimeout(int connectionRequestTimeout) {
        this.connectionRequestTimeout = connectionRequestTimeout;
        return this;
    }

    public int getConnectTimeout() {
        return this.connectTimeout;
    }

    public RouteConfig setConnectTimeout(int connectTimeout) {
        this.connectTimeout = connectTimeout;
        return this;
    }

    public int getSocketTimeout() {
        return this.socketTimeout;
    }

    public RouteConfig setSocketTimeout(int socketTimeout) {
        this.socketTimeout = socketTimeout;
        return this;
    }
}
