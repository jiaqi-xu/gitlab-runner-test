package com.guandata.atlas.common.annotation;

import com.guandata.atlas.enums.TaskStatusEnum;
import com.guandata.atlas.enums.TaskTypeEnum;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface TaskCheck {
    /**
     * 哪些状态互斥
     * @return
     */
    TaskStatusEnum[] statusList() default {TaskStatusEnum.RUNNING};

    /**
     * 哪些类型
     * @return
     */
    TaskTypeEnum[] taskTypeList() default {TaskTypeEnum.BATCH_MODIFY_PT, TaskTypeEnum.SYNC_DATASET_DATA};

    /**
     * 超时时间
     * @return
     */
    long timeoutSeconds() default 600;

    /**
     *  业务id，spel表达式
     * @return
     */
    String objectId();

    /**
     * 显示任务的数量必须<=
     * @return
     */
    int taskSizeLimit() default 0;
}
