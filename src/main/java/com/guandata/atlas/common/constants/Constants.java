package com.guandata.atlas.common.constants;

public class Constants {
    /**
     * user roles
     */
    public static final String ADMIN = "Admin";
    public static final String USER = "User";

    /**
     * status
     */
    public static final String STATUS = "status";

    /**
     * response code and related msg
     */
    public static final Integer SUCCESS_CODE = 0;
    public static final Integer ERROR_CODE = -1;
    public static final String  SUCCESS_MSG = "success";
    public static final String  ERROR_MSG = "error";

    /**
     * META TYPE OF COLUMN
     */
    public static final String TEXT = "TEXT";
    public static final String DIM = "DIM";
    public static final String METRIC = "METRIC";

    /**
     * Length of Field UUID
     */
    public static final Integer FIELD_UUID_LENGTH = 24;
    public static final Integer CARD_UUID_LENGTH = 24;

    /**
     * Status of Page
     */
    public static final Integer OPEN = 0;
    public static final Integer DELETED = 1;
    public static final Integer INIT = 2;

    /**  modify type，0 for manual modification  **/
    public static final Integer MODIFY_TYPE_MANUAL = 0;

    /** Batch Size **/
    public static final Integer BATCH_SIZE = 5000;

    /** resource type **/
    public static final String PT_PAGE = "PT_PAGE";

    /** kanban item type **/
    public static final String PAGE = "PAGE";
    public static final String FORM = "FORM";



    /** kanban item url prefix per type **/
    public static final String PAGE_URL = "/page";
    public static final String FORM_URL = "/survey-engine/form-data";

    /** account type **/
    public static final String GALAXY_ACCOUNT = "GALAXY";
    public static final String UNIVERSE_ACCOUNT = "UNIVERSE";

    /** action button type, 1-workflow, 2-page jump **/
    public static final Integer ACTION_BUTTON_TYPE_WORKFLOW = 1;
    public static final Integer ACTION_BUTTON_TYPE_JUMP_ViEW = 2;

    /** Order Type **/
    public static final String ASC = "asc";
    public static final String DESC = "desc";

    /** Resource Owner: User or Group **/
    public static final String USER_OWNER = "USER";
    public static final String GROUP_OWNER = "GROUP";
}
