package com.guandata.atlas.common.constants;

public class OpenAPIConstants {
    public static final String LIST_DATASET_URL = "/public-api/dataset/{0}/sync-data?token={1}";
}
