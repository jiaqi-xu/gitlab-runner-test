package com.guandata.atlas.common.utils;

import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;

public class AtlasOperand implements Comparable<AtlasOperand>{
    private final int DEFAULT_SCALE = 6;

    private BigDecimal value;

    public static final AtlasOperand ZERO = new AtlasOperand(BigDecimal.ZERO);

    public AtlasOperand(String val) {
        if (StringUtils.isBlank(val)) {
            this.value = BigDecimal.ZERO;
            return;
        }
        this.value = new BigDecimal(val).setScale(DEFAULT_SCALE, BigDecimal.ROUND_HALF_UP);
    }

    public AtlasOperand(Object obj) {
        if (obj == null) {
            this.value = BigDecimal.ZERO;
            return;
        }
        this.value = new BigDecimal(String.valueOf(obj)).setScale(DEFAULT_SCALE, BigDecimal.ROUND_HALF_UP);
    }

    public AtlasOperand add(AtlasOperand augend) {
        BigDecimal sum = this.value.add(augend.value).setScale(DEFAULT_SCALE, BigDecimal.ROUND_HALF_UP);
        return new AtlasOperand(sum);
    }

    public AtlasOperand subtract(AtlasOperand subtrahend) {
        BigDecimal diff = this.value.subtract(subtrahend.value).setScale(DEFAULT_SCALE, BigDecimal.ROUND_HALF_UP);
        return new AtlasOperand(diff);
    }

    public AtlasOperand multiply(AtlasOperand multiplicand) {
        BigDecimal product = this.value.multiply(multiplicand.value).setScale(DEFAULT_SCALE, BigDecimal.ROUND_HALF_UP);
        return new AtlasOperand(product);
    }

    public AtlasOperand divide(AtlasOperand divisor) {
        BigDecimal quotient = this.value.divide(divisor.value, DEFAULT_SCALE, BigDecimal.ROUND_HALF_UP);
        return new AtlasOperand(quotient);
    }

    public AtlasOperand setScale(int newScale, int roundingMode) {
        BigDecimal scaledRes = this.value.setScale(newScale, roundingMode);
        return new AtlasOperand(scaledRes);
    }

    public String toPlainString() {
        return this.value.toPlainString();
    }

    public String toString() {
        return toPlainString();
    }

    @Override
    public int compareTo(AtlasOperand o) {
        return this.value.compareTo(o.value);
    }
}
