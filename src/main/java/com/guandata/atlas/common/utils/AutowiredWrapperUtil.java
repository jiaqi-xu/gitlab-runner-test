package com.guandata.atlas.common.utils;

import org.jooq.DSLContext;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class AutowiredWrapperUtil {

    private static DSLContext dslContext;

    public static DSLContext getDslContext() {
        return dslContext;
    }

    @Resource
    public void setDSLContext(DSLContext dslContext) {
        AutowiredWrapperUtil.dslContext = dslContext;
    }
}
