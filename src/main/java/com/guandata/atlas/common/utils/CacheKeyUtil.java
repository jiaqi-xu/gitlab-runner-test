package com.guandata.atlas.common.utils;

import org.apache.commons.codec.digest.DigestUtils;

public class CacheKeyUtil {

    /**
     * 获取数据缓存的key
     * @param cardId
     * @param pageNo
     * @param pageSize
     * @param condition
     * @return
     */
    public static String getCardDataCacheKey(Integer dsId,String cardId, Long pageNo,
                                             Long pageSize, String condition) {

        return "datasetcardcache_" + dsId +"_" + cardId + pageNo+pageSize+ DigestUtils.md5Hex(condition);
    }

    public static String getCardDataCachePrefixKey(Integer dsId){
        return "datasetcardcache_" + dsId;
    }

}
