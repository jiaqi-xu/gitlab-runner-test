package com.guandata.atlas.common.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.helpers.FormattingTuple;
import org.slf4j.helpers.MessageFormatter;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * @author dxc
 **/
public class ErrorLogger {

    /**
     * 核心告警日志
     */
    private static final Logger PERF_LOGGER = LoggerFactory.getLogger(ErrorLogger.class);

    /**
     * 推送核心告警日志
     *
     * @param label  告警类型，小于32字符 {@link}
     * @param format 打印的数据
     * @param args   参数
     */
    public static void core(String label, String format, Object... args) {
        try {
            FormattingTuple formattingTuple = MessageFormatter.arrayFormat(format, args);
            Throwable throwable = formattingTuple.getThrowable();
            String message = formattingTuple.getMessage();
            String stackTrace = getStackTrace(throwable);
            LogRobotHelper.pushCore(label, message, stackTrace);
        } catch (Exception ignored) {
        }
    }

    /**
     * 获取异常的堆栈信息
     *
     * @param throwable 异常
     * @return 堆栈信息
     */
    private static String getStackTrace(Throwable throwable) {
        if (throwable == null) {
            return null;
        }
        try (StringWriter sw = new StringWriter();
             PrintWriter pw = new PrintWriter(sw)) {
            throwable.printStackTrace(pw);
            return sw.toString();
        } catch (Exception e) {
            return "getStackTrace error";
        }
    }


}