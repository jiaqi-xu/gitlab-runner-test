package com.guandata.atlas.common.utils;

import com.guandata.atlas.common.constants.Constants;
import com.guandata.atlas.dto.ResultData;
import com.guandata.atlas.dto.TableResult;
import com.guandata.atlas.dto.dataset.DSColumn;
import com.guandata.atlas.pojo.FieldInfo;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ExcelUtil {
    /**
     * 导出数据至excel
     * @param fieldInfoList
     * @param res
     * @param sheetName
     * @param filePath
     * @throws Exception
     */
    public void exportToExcel(List<FieldInfo> fieldInfoList, TableResult res, String sheetName, String filePath) throws Exception {
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet(sheetName);

        Map<String, String> fdTypeHash = new HashMap<>();
        fieldInfoList.forEach( f -> {
            fdTypeHash.put(f.getColumn().getColumnId(), f.getMetaType());
        });

        Map<String, String> fdAliasHash = new HashMap<>();
        fieldInfoList.forEach( f -> {
            fdAliasHash.put(f.getColumn().getColumnId(), f.getAlias());
        });
        List<DSColumn> headers = res.getColumns();
        List<List<ResultData>> resultData = res.getResultData();

        writeExcelHeader(fdAliasHash, headers, sheet);
        writeExcelData(fdTypeHash, headers, resultData, sheet);

        FileOutputStream fos = null;

        fos = new FileOutputStream(filePath);
        workbook.write(fos);
        fos.close();
    }

    /**
     * 向sheet中写入表头字段
     * @param headers
     * @param sheet
     */
    public void writeExcelHeader(Map<String, String> fieldAliasHash, List<DSColumn> headers, XSSFSheet sheet) {
        XSSFRow headRow = sheet.createRow(0);
        for(int i = 0; i < headers.size(); i++) {
            XSSFCell cell = headRow.createCell(i);
            String fdName = headers.get(i).getName();
            String alias = fieldAliasHash.getOrDefault(fdName, null);
            cell.setCellValue(alias == null? fdName: alias);
        }
    }

    /**
     * 向sheet中逐行写入数据
     * @param headers
     * @param resultData
     * @param sheet
     */
    public void writeExcelData(Map<String, String> fieldTypeHash, List<DSColumn> headers, List<List<ResultData>> resultData, XSSFSheet sheet) {
        for (int rowIndex = 1; rowIndex <= resultData.size(); rowIndex++) {
            XSSFRow dataRow = sheet.createRow(rowIndex);

            for(int colIndex = 0; colIndex < headers.size(); colIndex++) {
                XSSFCell cell = dataRow.createCell(colIndex);
                if (fieldTypeHash.get(headers.get(colIndex).getColumnId()).equals(Constants.TEXT)) {
                    String v = resultData.get(rowIndex - 1).get(colIndex).getV();
                    cell.setCellValue(v);
                } else if (fieldTypeHash.get(headers.get(colIndex).getColumnId()).equals(Constants.METRIC)) {
                    Double v = Double.valueOf(resultData.get(rowIndex - 1).get(colIndex).getV());
                    cell.setCellValue(v);
                }
            }
        }
    }
}
