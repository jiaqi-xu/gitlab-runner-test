package com.guandata.atlas.common.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.common.base.Charsets;
import com.google.common.collect.Maps;
import com.guandata.atlas.enums.ErrorCode;
import com.guandata.atlas.exception.AtlasException;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;


public class GalaxyUtil {
    public static final Logger logger = LoggerFactory.getLogger(GalaxyUtil.class);

    public static final String SIGN_IN_URL = "/api/sso/sign-in";
    public static final String PAGE_URL = "/api/page/{0}";

    static RequestConfig defaultRequestConfig = RequestConfig.custom()
            .setCookieSpec(CookieSpecs.STANDARD)
            .setConnectTimeout(60 * 1000)
            .setConnectionRequestTimeout(10 * 1000)
            .setSocketTimeout(120 * 1000).build();

    /**
     * 获取默认的HttpClient
     * @return
     */
    private static CloseableHttpClient getDefaultHttpClient(){
        return HttpClients.custom().setDefaultRequestConfig(defaultRequestConfig).build();
    }

    /**
     * 获取有用户配置信息的HttpClient
     * @param requestConfig
     * @return
     */
    private static CloseableHttpClient getHttpClientByRequestConfig(RequestConfig requestConfig){
        return HttpClients.custom().setDefaultRequestConfig(requestConfig).build();
    }


    /**
     * 根据SSOToken获取uIdeToken
     * @param baseUrl Galaxy Base URL
     * @param ssoToken ssoToken
     * @param domainId domain Id
     * @return
     * @throws IOException
     */
    public static JSONObject getLoginTokenBySSOToken(String baseUrl, String ssoToken, String domainId) {
        // 使用较短的超时时间，防止服务不可用造成等待时间过长
        RequestConfig requestConfig = RequestConfig.custom()
                .setCookieSpec(CookieSpecs.STANDARD)
                .setConnectTimeout(500)
                .setConnectionRequestTimeout(500)
                .setSocketTimeout(500).build();

        CloseableHttpClient httpClient = getHttpClientByRequestConfig(requestConfig);

        int retryTimes = 0;
        do {
            HttpPost httpPost = new HttpPost(baseUrl + SIGN_IN_URL);
            httpPost.addHeader("Content-Type", "application/json;charset=UTF-8");
            Map<String, String> ssoTokenMap = new HashMap() {{
                put("ssoToken", ssoToken);
            }};

            Map<String, Object> body = Maps.newHashMap();
            body.put("provider", domainId);
            body.put("info", ssoTokenMap);

            httpPost.setEntity(new StringEntity(Objects.requireNonNull(JSON.toJSONString(body)), Charsets.UTF_8));
            try (CloseableHttpResponse response = httpClient.execute(httpPost)) {
                JSONObject responseObj = JSON.parseObject(EntityUtils.toString(response.getEntity(), Charsets.UTF_8));
                response.close();
                return responseObj;
            } catch (IOException ex) {
                logger.info("Retry times {{}}...", ++retryTimes);
            }
        } while (retryTimes <= 2);

        String errorMsg = "BI Service currently is not available.";
        logger.error(errorMsg);
        throw new RuntimeException(errorMsg);
    }


    /**
     * 根据SSO配置信息获取SSO Token
     * @param privateKey RSA Private Key
     * @param domainId domain Id
     * @param externalUserId username
     * @return
     * @throws AtlasException
     */
    public static String getSSOToken(String privateKey, String domainId, String externalUserId) throws AtlasException {
        try {
            JSONObject plainData = new JSONObject();
            plainData.put("domainId", domainId);
            plainData.put("externalUserId", externalUserId);
            plainData.put("timestamp", new Date().getTime());
            String cipherData = RSAUtil.privateEncrypt(plainData.toJSONString(), RSAUtil.getPrivateKey(privateKey));
            String ssoToken = RSAUtil.toHexString(cipherData);
            return ssoToken;
        } catch (Exception e) {
            logger.error("fail to get galaxy ssoToken...", e);
            throw new AtlasException("fail to get galaxy ssoToken...");
        }
    }

    public static JSONObject getBIPage(String baseUrl, String pageId, String uIdToken) {
        CloseableHttpClient httpClient = getDefaultHttpClient();
        MessageFormat pageUrlFormat = new MessageFormat(PAGE_URL);
        String[] args = new String[]{pageId};
        String pageUrl = pageUrlFormat.format(args);

        HttpGet httpGet = new HttpGet(baseUrl + pageUrl);
        httpGet.addHeader("Content-Type", "application/json;charset=UTF-8");
        httpGet.addHeader("token", uIdToken);

        try (CloseableHttpResponse response = httpClient.execute(httpGet)) {
            JSONObject responseObj = JSON.parseObject(EntityUtils.toString(response.getEntity(), Charsets.UTF_8));
            response.close();
            return responseObj;
        } catch (IOException e) {
            logger.error("获取BI平台页面失败, url: {}", httpGet.getURI(), e);
            throw new AtlasException(ErrorCode.GALAXY_FAIL_TO_GET_PAGE, e.getMessage());
        }
    }
}
