package com.guandata.atlas.common.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.common.base.Charsets;
import com.guandata.atlas.config.KeyCloakConfig;
import com.guandata.atlas.enums.ErrorCode;
import com.guandata.atlas.exception.AtlasException;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.internal.ClientResponse;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.GroupRepresentation;
import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.core.Response;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

@Component
public class KeyCloakUtil {
    private static final Logger logger = LoggerFactory.getLogger(KeyCloakUtil.class);

    private static KeyCloakConfig keyCloakConfig;
    private static Keycloak keycloak;

    private static final String SIGN_IN_URL = "/realms/{0}/protocol/openid-connect/token";

    @Autowired
    public void setKeyCloakConfig(KeyCloakConfig keyCloakConfig){
        this.keyCloakConfig = keyCloakConfig;
    }

    public static Keycloak init() {
        String authUrl = keyCloakConfig.getAuthUrl();
        String username = keyCloakConfig.getUsername();
        String password = keyCloakConfig.getPassword();
        Keycloak kc = KeycloakBuilder.builder()
                .serverUrl(authUrl).realm("master").username(username)
                .password(password).clientId("admin-cli")
                .resteasyClient(new ResteasyClientBuilder().connectionPoolSize(20).build())
                .build();
        logger.info("keycloak connection has been initialized");
        return kc;
    }

    public static Keycloak getInstance() {
        if (keycloak != null) {
            return keycloak;
        }
        keycloak = init();
        return keycloak;
    }

    /**************************  USER **************************/

    public static JSONObject loginIn(String realmId, String client, String username, String password) {
        RequestConfig requestConfig = RequestConfig.custom()
                .setCookieSpec(CookieSpecs.STANDARD)
                .setConnectTimeout(500)
                .setConnectionRequestTimeout(500)
                .setSocketTimeout(500).build();
        CloseableHttpClient httpClient = HttpClients.custom().setDefaultRequestConfig(requestConfig).build();
        int retryTimes = 0;
        do {
            MessageFormat loginUrlFormat = new MessageFormat(SIGN_IN_URL);
            String[] args = new String[]{realmId};
            String loginUrl = loginUrlFormat.format(args);

            HttpPost httpPost = new HttpPost(keyCloakConfig.getAuthUrl() + loginUrl);
            httpPost.addHeader("Content-Type", "application/x-www-form-urlencoded");

            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("grant_type", "password"));
            nameValuePairs.add(new BasicNameValuePair("username", username));
            nameValuePairs.add(new BasicNameValuePair("password", password));
            nameValuePairs.add(new BasicNameValuePair("client_id", client));

            httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs, Charsets.UTF_8));
            try (CloseableHttpResponse response = httpClient.execute(httpPost)) {
                JSONObject responseObj = JSON.parseObject(EntityUtils.toString(response.getEntity(), Charsets.UTF_8));
                response.close();
                return responseObj;
            } catch (IOException ex) {
                logger.info("Retry times {{}}...", ++retryTimes);
            }
        } while (retryTimes <= 2);
        throw new AtlasException(ErrorCode.KEYCLOAK_SERVER_NOT_AVAILABLE);
    }

    /**
     * 创建用户
     * @param realmId
     * @param user
     */
    public static String createUser(String realmId, UserRepresentation user) {
        Response response = getInstance().realm(realmId).users().create(user);
        if (response.getStatus() >= HttpStatus.SC_BAD_REQUEST) {
            if (((ClientResponse) response).getReasonPhrase().equals("Conflict")) {
                throw new AtlasException(ErrorCode.FAIL_TO_CREATE_USER, "用户已存在");
            } else {
                throw new AtlasException(ErrorCode.FAIL_TO_CREATE_USER, ((ClientResponse) response).getReasonPhrase());
            }
        } else {
            return response.getLocation().getPath().substring(
                    response.getLocation().getPath().lastIndexOf("/") + 1);
        }
    }

    public static void deleteUser(String realmId, String uId) {
        Response response = getInstance().realm(realmId).users().delete(uId);
        if (response.getStatus() >= HttpStatus.SC_BAD_REQUEST) {
            throw new AtlasException(ErrorCode.FAIL_TO_DELETE_USER);
        }
    }


    public static void updateUser(String realmId, UserRepresentation user) {
        try {
            getInstance().realm(realmId).users().get(user.getId()).update(user);
        } catch (Exception e) {
            logger.error("更新用户失败", e);
            new AtlasException(ErrorCode.FAIL_TO_UPDATE_USER);
        }
    }

    public static void resetPassword(String realmId, String uId, CredentialRepresentation credentialRepresentation) {
        getInstance().realm(realmId).users().get(uId).resetPassword(credentialRepresentation);
    }

    /**
     * 通过用户在keycloak上的Id获取用户信息
     * @param realmId keycloak 域
     * @param uId keycloak user id
     * @return 获取用户信息
     */
    public static UserRepresentation getUserById(String realmId, String uId) {
        UserRepresentation user = getInstance().realm(realmId).users().get(uId).toRepresentation();
        return user;
    }

    /**
     * get keycloak realm user list
     * @param realmId keycloak 域
     * @param search 搜索关键字符
     * @param first pagination offset
     * @param max maximum result size
     * @return keycloak realm user list
     */
    public static List<UserRepresentation> getUsers(String realmId, String search, Integer first, Integer max) {
        List<UserRepresentation> users = getInstance().realm(realmId).users().search(search, first, max);
        removeUnactivatedUsers(users);
        return users;
    }

    /**
     * get Users in the specific group
     * @param realmId keycloak 域
     * @param groupId 用户组id
     * @return
     */
    public static List<UserRepresentation> getGroupMembers(String realmId, String groupId) {
        List<UserRepresentation> users = getInstance().realm(realmId).groups().group(groupId).members();
        return users;
    }

    /**
     * 移除未激活的用户
     * @param users
     */
    private static void removeUnactivatedUsers(List<UserRepresentation> users) {
        List<UserRepresentation> unactivatedUsers = new ArrayList<>();
        for (UserRepresentation user : users) {
            if (!user.isEnabled()) {
                unactivatedUsers.add(user);
                logger.info("unactivated user, Id: {}, name: {}", user.getId(), user.getUsername());
            }
        }
        users.removeAll(unactivatedUsers);
    }


    /**************************  ROLE **************************/

    /**
     * 获取角色列表
     * @param realmId
     * @param client
     * @return
     */
    public static List<RoleRepresentation> getClientRoles(String realmId, String client) {
        String clientId = getClientId(realmId, client);
        List<RoleRepresentation> roles = getInstance().realm(realmId).clients().get(clientId).roles().list();
        return roles;
    }

    /**
     * 获取用户的角色
     * @param realmId keycloak 域
     * @param client keycloak client
     * @param uId  域用户id
     * @return 获取用户client Roles
     */
    public static List<RoleRepresentation> getClientRolesByUId(String realmId, String client, String uId) {
        String clientId = getClientId(realmId, client);
        List<RoleRepresentation> roles = getInstance().realm(realmId).users().get(uId).roles().clientLevel(clientId).listEffective();
        return roles;
    }

    public static void assignClientLevelRolesToUser(String realmId, String client, String uId, List<RoleRepresentation> roles) {
        String clientId = getClientId(realmId, client);
        getInstance().realm(realmId).users().get(uId).roles().clientLevel(clientId).add(roles);
    }

    public static void removeClientLevelRolesFromUser(String realmId, String client, String uId, List<RoleRepresentation> roles) {
        String clientId = getClientId(realmId, client);
        getInstance().realm(realmId).users().get(uId).roles().clientLevel(clientId).remove(roles);
    }


    /**************************  GROUP  **************************/

    private static String groupPath(String path) {
        return "/" + path;
    }

    public static GroupRepresentation getGroupByPath(String realmId, String path) {
        return getInstance().realm(realmId).getGroupByPath(groupPath(path));
    }

    public static List<GroupRepresentation> getSubGroups(String realmId, String path) {
        return getInstance().realm(realmId).getGroupByPath(groupPath(path)).getSubGroups();
    }

    public static void addSubGroup(String realmId, String groupId, GroupRepresentation group) {
        Response response = getInstance().realm(realmId).groups().group(groupId).subGroup(group);
        if (response.getStatus() >= HttpStatus.SC_BAD_REQUEST) {
            if (((ClientResponse) response).getReasonPhrase().equals("Conflict")) {
                throw new AtlasException(ErrorCode.FAIL_TO_CREATE_USER_GROUP, "用户组已存在");
            } else {
                throw new AtlasException(ErrorCode.FAIL_TO_CREATE_USER_GROUP, ((ClientResponse) response).getReasonPhrase());
            }
        }
    }

    public static void deleteSubGroup(String realmId, String groupId) {
        getInstance().realm(realmId).groups().group(groupId).remove();
    }

    public static void renameGroup(String realmId, String groupId, GroupRepresentation group) {
        getInstance().realm(realmId).groups().group(groupId).update(group);
    }

    public static List<GroupRepresentation> getGroupsByUId(String realmId, String uId) {
        return getInstance().realm(realmId).users().get(uId).groups();
    }

    public static void addGroupMember(String realmId, String uId, String groupId) {
        getInstance().realm(realmId).users().get(uId).joinGroup(groupId);
    }

    public static void removeGroupMember(String realmId, String uId, String groupId) {
        getInstance().realm(realmId).users().get(uId).leaveGroup(groupId);
    }

    /**
     *
     * @param realmId 域id
     * @param client Keycloak Client名称
     * @return client id
     */
    public static String getClientId(String realmId, String client) {
        String clientId = getInstance().realm(realmId).clients().findByClientId(client).get(0).getId();
        return clientId;
    }
}
