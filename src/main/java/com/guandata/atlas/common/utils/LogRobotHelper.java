package com.guandata.atlas.common.utils;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Maps;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import com.guandata.atlas.client.http.HttpTemplate;
import com.guandata.atlas.client.http.base.BaseHttpRequest;
import com.guandata.atlas.client.http.base.BaseHttpResponse;
import com.guandata.atlas.client.http.common.Method;
import com.guandata.atlas.config.EnvironmentHelper;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @author dxc
 **/
@Component
public class LogRobotHelper {

    /**
     * 订单推送测试
     */
    private static final String TEST_TOKEN = "96d4a29dd0f0dc9d810f72048c19e87273525bf65913d416c5c5835e8cc2afc8";


    private static final ThreadPoolExecutor PUSH_POOL;

    private static HttpTemplate httpTemplate;

    static {
        ThreadFactory threadFactory = new ThreadFactoryBuilder().setNameFormat("pushlog-pool-%d").build();
        PUSH_POOL = new ThreadPoolExecutor(2, 5, 0L, TimeUnit.SECONDS, new LinkedBlockingDeque<>(200), threadFactory, new ThreadPoolExecutor.AbortPolicy());
    }

    @Resource
    public void setHttpTemplate(HttpTemplate httpTemplate) {
        LogRobotHelper.httpTemplate = httpTemplate;
    }

    public static void pushCore(String label, String message, String stackTrace) {
        if(EnvironmentHelper.isPre() || EnvironmentHelper.isOnline() || EnvironmentHelper.isLocal()){
            return;
        }

        String header = EnvironmentHelper.buildEnvStr() +
                new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()) + "\n" +
                label + "\n";

        String content = message + "\n" + stackTrace;

//        String tail = ""<details><summary>展开查看</summary><pre><code></code></pre></details>";

        if (content.length() > 2500) {
            content = header + content.substring(0, 2500)+"\n ........";
        }

        postText(content);
    }

    private static void postText(String content) {
        PUSH_POOL.execute(() -> {
            try {
                RobotSendRequest request = new RobotSendRequest();
                request.setAccessToken(TEST_TOKEN);
                request.setMsgType("text");
                JSONObject text = new JSONObject();
                text.put("content", content);
                request.setText(text);
                httpTemplate.execute(request);
            } catch (Exception ignored) {

            }
        });
    }

    static void postMarkdown(String title, String text) {
        PUSH_POOL.execute(() -> {
            try {
                RobotSendRequest request = new RobotSendRequest();
                request.setAccessToken(TEST_TOKEN);
                request.setMsgType("markdown");
                JSONObject markdown = new JSONObject();
                markdown.put("title", title);
                markdown.put("text", text);
                request.setMarkdown(markdown);
                httpTemplate.execute(request);
            } catch (Exception ignored) {
            }
        });
    }

    private static class RobotSendRequest extends BaseHttpRequest<RobotSendResponse> {

        private String accessToken;

        private String msgType;

        private JSONObject text;

        private JSONObject markdown;

        @Override
        public Map<String, String> getQueryParams() {
            Map<String, String> queryParams = Maps.newHashMap();
            queryParams.put("access_token", accessToken);
            return queryParams;
        }

        @Override
        public Map<String, Object> getBody() {
            Map<String, Object> body = Maps.newHashMap();
            body.put("msgtype", msgType);
            if (text != null) {
                body.put("text", text);
            } else if (markdown != null) {
                body.put("markdown", markdown);
            }
            JSONArray atMobiles = new JSONArray();
            atMobiles.addAll(Arrays.asList("18557530481", "18657168851"));

            JSONObject at = new JSONObject();
            at.put("atMobiles", atMobiles);
            body.put("at", at);
            return body;
        }

        @Override
        public Map<String, File> getFileItems() {
            return null;
        }

        @Override
        public Map<String, String> getHeaders() {
            Map<String, String> headers = Maps.newHashMap();
            headers.put("Accept", "application/json");
            headers.put("Content-Type", "application/json");
            return headers;
        }

        @Override
        public Map<String, String> getBizMap() {
            return null;
        }

        @Override
        public String getName() {
            return "robot_send";
        }

        @Override
        public String getRoute() {
            return "dingTalk";
        }

        @Override
        public Method getMethod() {
            return Method.POST;
        }

        @Override
        public String getPath() {
            return "/robot/send";
        }

        @Override
        public Class<RobotSendResponse> getResponseClass() {
            return RobotSendResponse.class;
        }

        @Override
        public boolean check() {
            return accessToken != null;
        }

        public String getAccessToken() {
            return accessToken;
        }

        public void setAccessToken(String accessToken) {
            this.accessToken = accessToken;
        }

        public String getMsgType() {
            return msgType;
        }

        public void setMsgType(String msgType) {
            this.msgType = msgType;
        }

        public JSONObject getText() {
            return text;
        }

        public void setText(JSONObject text) {
            this.text = text;
        }

        public JSONObject getMarkdown() {
            return markdown;
        }

        public void setMarkdown(JSONObject markdown) {
            this.markdown = markdown;
        }
    }

    private static class RobotSendResponse extends BaseHttpResponse {

        @Override
        public boolean success() {
            return true;
        }

        @Override
        public String code() {
            return "-1";
        }
    }
}