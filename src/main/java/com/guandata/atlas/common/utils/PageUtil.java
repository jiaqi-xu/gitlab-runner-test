package com.guandata.atlas.common.utils;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import java.util.Map;

public class PageUtil {
    /**
     * 通过pageNo和pageSize初始化Page
     */
    public static Page<Map<String, Object>> pagination(Long pageNo, Long pageSize) {
        Page<Map<String, Object>> page;

        if (pageNo != null && pageSize != null) {
            page = new Page<>(pageNo, pageSize);
        } else {
            page = new Page<>(1, Long.MAX_VALUE);
        }

        return page;
    }

    public static Long getPages(Long total, Long pageSize) {
        if (pageSize == 0L) {
            return 0L;
        }

        return total % pageSize == 0 ? total/pageSize : total/pageSize+1;
    }
}
