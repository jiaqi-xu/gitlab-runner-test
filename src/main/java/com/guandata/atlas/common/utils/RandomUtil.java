package com.guandata.atlas.common.utils;

import java.util.Random;
import java.util.UUID;

public class RandomUtil {
    public final static char[] letters = "abcdefghijklmnopqrstuvwxyz".toCharArray();

    public static String generate(Integer size) {
        String uuid = UUID.randomUUID().toString().replaceAll("-", "");
        Random r = new Random();
        return letters[r.nextInt(letters.length)] + uuid.substring(0, size - 4) + uuid.substring(size-3, size);
    }

    public static String uuidWithPrefix(String prefix, Integer size) {
        return prefix + generate(size - prefix.length());
    }

    /**
     * 生成access token, usage: public api token
     * @return
     */
    public static String generateAccessToken() {
        String uuid = UUID.randomUUID().toString() + UUID.randomUUID().toString();
        return uuid.replace("-", "");
    }
}
