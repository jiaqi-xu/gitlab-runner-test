package com.guandata.atlas.common.utils;

import com.guandata.atlas.common.constants.Constants;
import java.io.Serializable;

/**
 * result
 * @param <T> T
 */
public class ResponseResult<T> implements Serializable {
    /** status */
    private Integer code;
    /** message */
    private String msg;
    /** data */
    private T data;

    public ResponseResult(){}

    public ResponseResult(Integer code , String msg){
        this.code = code;
        this.msg = msg;
    }

    public ResponseResult(T data){
        this.data = data;
        this.code = Constants.SUCCESS_CODE;
        this.msg = Constants.SUCCESS_MSG;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public String toString() {
        return "Status{" +
                "code='" + code + '\'' +
                ", msg='" + msg + '\'' +
                ", data=" + data +
                '}';
    }
}