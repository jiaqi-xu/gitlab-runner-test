package com.guandata.atlas.common.utils;

import org.springframework.util.CollectionUtils;

import java.util.List;


public class StringUtil {

    /**
     * 判断字符串是否为空或者空字符串
     */
    public static boolean isEmpty(String str) {
        if (null == str || "".equals(str)) {
            return true;
        }
        return false;
    }

    /**
     * 判断字符串是否为空或者空字符串
     */
    public static boolean isEmptyTrim(String str) {
        if (isEmpty(str)) {
            return true;
        }
        return "".equals(str.trim());
    }

    /**
     * 类似scala的mkString
     * @return  [{1},{2}] -> "1, 2, "
     */
    public static String mkStringList(List<String> strList) {
        if (CollectionUtils.isEmpty(strList))
            return "";
        StringBuilder stringBuilder = new StringBuilder();
        strList.forEach(str -> stringBuilder.append(str).append(", "));
        return stringBuilder.toString();
    }
}
