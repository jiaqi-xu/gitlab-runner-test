package com.guandata.atlas.common.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.common.base.Charsets;
import com.google.common.collect.Maps;
import com.guandata.atlas.exception.AtlasException;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.Map;
import java.util.Objects;

import static com.guandata.atlas.enums.ErrorCode.*;

public class UniverseUtil {
    public static final Logger logger = LoggerFactory.getLogger(UniverseUtil.class);

    public static final String SIGN_IN_URL = "/universe/public-api/token";

    public static final String LIST_PROJECT_URL = "/universe/public-api/projects";
    public static final String LIST_DATASET_URL = "/universe/public-api/projects/{0}/datasets";
    public static final String DATASET_INFO_URL = "/universe/public-api/projects/{0}/datasets/{1}";
    public static final String GET_SINGLE_DATASET_DATA = "/universe/public-api/dataset/{0}/query";
    public static final String GET_SINGLE_DATASET_DATA_PRE_INVOKE =
            "/universe/public-api/projects/{0}/datasets/{1}/export-to-avro";
    public static final String GET_SINGLE_DATASET_DATA_POST_INVOKE =
            "/universe/public-api/projects/{0}/datasets/{1}/retrieve-avro-result";

    public static final String WORKFLOW_PROCESS_URL_PREFIX = "/universe/public-api/process/";
    public static final String WORKFLOW_PROCESS_URL_SUFFIX = "/run";
    public static final String WORKFLOW_INSTANCE_URL_PREFIX = "/universe/public-api/process/instance/";

    static RequestConfig defaultRequestConfig = RequestConfig.custom()
            .setCookieSpec(CookieSpecs.STANDARD)
            .setConnectTimeout(60 * 1000)
            .setConnectionRequestTimeout(10 * 1000)
            .setSocketTimeout(120 * 1000).build();

    /**
     * 获取默认的HttpClient
     *
     * @return
     */
    private static CloseableHttpClient getDefaultHttpClient() {
        return HttpClients.custom().setDefaultRequestConfig(defaultRequestConfig).build();
    }

    /**
     * 获取有用户配置信息的HttpClient
     *
     * @param requestConfig
     * @return
     */
    private static CloseableHttpClient getHttpClientByRequestConfig(RequestConfig requestConfig) {
        return HttpClients.custom().setDefaultRequestConfig(requestConfig).build();
    }

    /**
     * 根据username, password获取uIdeToken
     *
     * @param baseUrl
     * @param userName
     * @param password
     * @return
     * @throws IOException
     */
    public static JSONObject getLoginToken(String baseUrl, String userName, String password) {
        // 使用较短的超时时间，防止服务不可用造成等待时间过长
        RequestConfig requestConfig = RequestConfig.custom()
                .setCookieSpec(CookieSpecs.STANDARD)
                .setConnectTimeout(500)
                .setConnectionRequestTimeout(500)
                .setSocketTimeout(500).build();

        CloseableHttpClient httpClient = getHttpClientByRequestConfig(requestConfig);

        String encodedPassword = Base64.encodeBase64String(password.getBytes());

        int retryTimes = 0;
        do {
            HttpPost httpPost = new HttpPost(baseUrl + SIGN_IN_URL);
            httpPost.addHeader("Content-Type", "application/json;charset=UTF-8");
            httpPost.addHeader("Cookie", "language=zh_CN");

            Map<String, Object> body = Maps.newHashMap();
            body.put("userName", userName);
            body.put("userPassword", encodedPassword);

            httpPost.setEntity(new StringEntity(Objects.requireNonNull(JSON.toJSONString(body)), Charsets.UTF_8));
            try (CloseableHttpResponse response = httpClient.execute(httpPost)) {
                JSONObject responseObj = JSON.parseObject(EntityUtils.toString(response.getEntity(), Charsets.UTF_8));
                response.close();
                return responseObj;
            } catch (IOException ex) {
                logger.info("Retry times {{}}...", ++retryTimes);
            }
        } while (retryTimes <= 2);

        String errorMsg = "Universe Service currently is not available.";
        logger.error(errorMsg);
        throw new RuntimeException(errorMsg);
    }

    /**
     * 通过token，processId运行工作流，获取工作流实例ID
     *
     * @param baseUrl
     * @param token
     * @param processId
     * @return
     */
    public static JSONObject processWorkFlow(String baseUrl, String token, String processId) {
        CloseableHttpClient httpClient = getDefaultHttpClient();

        HttpPost httpPost = new HttpPost(baseUrl + WORKFLOW_PROCESS_URL_PREFIX + processId + WORKFLOW_PROCESS_URL_SUFFIX);
        httpPost.addHeader("Content-Type", "application/json;charset=UTF-8");
        httpPost.addHeader("token", token);
        httpPost.addHeader("Cookie", "language=zh_CN");

        int retryTimes = 0;
        do {
            try (CloseableHttpResponse response = httpClient.execute(httpPost)) {
                JSONObject responseObj = JSON.parseObject(EntityUtils.toString(response.getEntity(), Charsets.UTF_8));
                response.close();
                int code = responseObj.getInteger("code");
                if (code == 0) {
                    return responseObj.getJSONObject("data");
                } else if (code == 50003 || code == 50004 || code == 10018) {
                    String errorMsg = responseObj.getString("msg");
                    throw new RuntimeException(errorMsg);
                }
            } catch (RuntimeException e) {
                logger.error(e.getMessage());
                throw new RuntimeException(e.getMessage());
            } catch (IOException e) {
                logger.info("Retry times {{}}...", ++retryTimes);
            }
        } while (retryTimes <= 2);

        String errorMsg = "Universe Service currently is not available.";
        logger.error(errorMsg);
        throw new RuntimeException(errorMsg);
    }

    /**
     * 通过token，processInstanceId获取工作流实例
     *
     * @param baseUrl
     * @param token
     * @param processInstanceId
     * @return
     */
    public static JSONObject getProcessInstanceResult(String baseUrl, String token, String processInstanceId) {
        CloseableHttpClient httpClient = getDefaultHttpClient();

        HttpGet httpGet = new HttpGet(baseUrl + WORKFLOW_INSTANCE_URL_PREFIX + processInstanceId);
        httpGet.addHeader("Content-Type", "application/json;charset=UTF-8");
        httpGet.addHeader("token", token);
        httpGet.addHeader("Cookie", "language=zh_CN");

        int retryTimes = 0;
        do {
            try (CloseableHttpResponse response = httpClient.execute(httpGet)) {
                JSONObject responseObj = JSON.parseObject(EntityUtils.toString(response.getEntity(), Charsets.UTF_8));
                response.close();
                return responseObj;
            } catch (IOException e) {
                logger.info("Retry times {{}}...", ++retryTimes);
            }
        } while (retryTimes <= 2);

        String errorMsg = "Universe Service currently is not available.";
        logger.error(errorMsg);
        throw new RuntimeException(errorMsg);
    }

    /**
     * 获取用户信息
     *
     * @param baseUrl Universe Base URL
     * @param token   Universe Login Token
     * @return
     */
    public static JSONObject getProjectList(String baseUrl, String token) {
        CloseableHttpClient httpClient = getDefaultHttpClient();
        HttpGet httpGet = new HttpGet(baseUrl + LIST_PROJECT_URL);
        httpGet.addHeader("Content-Type", "application/json;charset=UTF-8");
        httpGet.addHeader("token", token);

        try (CloseableHttpResponse response = httpClient.execute(httpGet)) {
            JSONObject responseObj = JSON.parseObject(EntityUtils.toString(response.getEntity(), Charsets.UTF_8));
            CheckUniverseResponse(responseObj);
            response.close();
            return responseObj;
        } catch (IOException e) {
            logger.error("获取Universe项目列表失败, url:{}.", httpGet.getURI().toString(), e);
            throw new AtlasException(UNIVERSE_FAIL_TO_GET_PROJECTS, e.getMessage());
        }
    }

    /**
     * 分页获取数据集列表
     *
     * @param baseUrl
     * @param token
     * @param projectId
     * @return
     */
    public static JSONObject getDataSetList(String baseUrl, String token, Integer projectId) {
        CloseableHttpClient httpClient = getDefaultHttpClient();

        MessageFormat dataSetListUrlForm = new MessageFormat(LIST_DATASET_URL);
        Integer[] args = new Integer[]{projectId};
        String dataSetListUrl = dataSetListUrlForm.format(args);

        HttpGet httpGet = new HttpGet(baseUrl + dataSetListUrl);
        httpGet.addHeader("Content-Type", "application/json;charset=UTF-8");
        httpGet.addHeader("token", token);

        try (CloseableHttpResponse response = httpClient.execute(httpGet)) {
            JSONObject responseObj = JSON.parseObject(EntityUtils.toString(response.getEntity(), Charsets.UTF_8));
            CheckUniverseResponse(responseObj);
            response.close();
            return responseObj;
        } catch (IOException e) {
            logger.error("获取Universe数据集列表失败, url:{}.", httpGet.getURI().toString(), e);
            throw new AtlasException(UNIVERSE_FAIL_TO_GET_DATASETS, e.getMessage());
        }
    }

    /**
     * 获取数据集详情
     * @param baseUrl Base Url
     * @param token Login Token
     * @param projectId  Project Id
     * @param datasetId Dataset Id
     * @return
     */
    public static JSONObject getDataSetInfo(String baseUrl, String token, Integer projectId, Integer datasetId) {
        CloseableHttpClient httpClient = getDefaultHttpClient();

        MessageFormat dataSetInfoUrlForm = new MessageFormat(DATASET_INFO_URL);
        String[] args = new String[]{projectId.toString(), datasetId.toString()};
        String dataSetInfoUrl = dataSetInfoUrlForm.format(args);

        HttpGet httpGet = new HttpGet(baseUrl + dataSetInfoUrl);
        httpGet.addHeader("Content-Type", "application/json;charset=UTF-8");
        httpGet.addHeader("token", token);

        try (CloseableHttpResponse response = httpClient.execute(httpGet)) {
            JSONObject responseObj = JSON.parseObject(EntityUtils.toString(response.getEntity(), Charsets.UTF_8));
            CheckUniverseResponse(responseObj);
            response.close();
            return responseObj;
        } catch (IOException e) {
            logger.error("获取Universe数据集详情失败, url:{}.", httpGet.getURI().toString(), e);
            throw new AtlasException(UNIVERSE_FAIL_TO_GET_DATASET_INFO, e.getMessage());
        }
    }

    /**
     * 获取数据集数据
     * @param baseUrl Base Url
     * @param token  Login Token
     * @param datasetId  dataset Id
     * @param sql query data sql
     * @return
     */
    public static JSONObject getSingleDataSetData(String baseUrl, String token, Integer datasetId, String sql) {
        CloseableHttpClient httpClient = getDefaultHttpClient();

        MessageFormat dataSetDataUrlForm = new MessageFormat(GET_SINGLE_DATASET_DATA);
        String[] args = new String[]{datasetId.toString()};
        String dataSetDataUrl = dataSetDataUrlForm.format(args);

        HttpPost httpPost = new HttpPost(baseUrl + dataSetDataUrl);
        httpPost.addHeader("Content-Type", "application/json;charset=UTF-8");
        httpPost.addHeader("token", token);

        Map<String, Object> body = Maps.newHashMap();
        body.put("sql", sql);
        httpPost.setEntity(new StringEntity(Objects.requireNonNull(JSON.toJSONString(body)), Charsets.UTF_8));

        try (CloseableHttpResponse response = httpClient.execute(httpPost)) {
            JSONObject responseObj = JSON.parseObject(EntityUtils.toString(response.getEntity(), Charsets.UTF_8));
            CheckUniverseResponse(responseObj);
            response.close();
            return responseObj;
        } catch (IOException e) {
            logger.error("获取Universe数据集数据失败, url:{}.", httpPost.getURI().toString(), e);
            throw new AtlasException(UNIVERSE_FAIL_TO_GET_DATASET_DATA, e.getMessage());
        }
    }

    /**
     * 分页获取数据集数据前置调用
     * @param baseUrl
     * @param token
     * @param projectId
     * @param datasetId
     * @param sql
     * @return
     */
    public static JSONObject getSingleDataSetDataPreInvoke(
            String baseUrl, String token, Integer projectId, Integer datasetId, String sql) {
        CloseableHttpClient httpClient = getDefaultHttpClient();

        MessageFormat dataSetDataPreInvokeUrlForm = new MessageFormat(GET_SINGLE_DATASET_DATA_PRE_INVOKE);
        String[] args = new String[]{projectId.toString(), datasetId.toString()};
        String dataSetDataPreInvokeUrl = dataSetDataPreInvokeUrlForm.format(args);

        HttpPost httpPost = new HttpPost(baseUrl + dataSetDataPreInvokeUrl);
        httpPost.addHeader("Content-Type", "application/json;charset=UTF-8");
        httpPost.addHeader("token", token);

        Map<String, Object> body = Maps.newHashMap();
        body.put("sql", sql);
        httpPost.setEntity(new StringEntity(Objects.requireNonNull(JSON.toJSONString(body)), Charsets.UTF_8));

        try (CloseableHttpResponse response = httpClient.execute(httpPost)) {
            JSONObject responseObj = JSON.parseObject(EntityUtils.toString(response.getEntity(), Charsets.UTF_8));
            CheckUniverseResponse(responseObj);
            response.close();
            return responseObj;
        } catch (IOException e) {
            logger.error("获取Universe数据集数据前置调用失败, url:{}.", httpPost.getURI().toString(), e);
            throw new AtlasException(UNIVERSE_FAIL_TO_GET_DATASET_DATA, e.getMessage());
        }
    }

    /**
     * 分页获取数据集数据后置调用
     * @param baseUrl
     * @param token
     * @param projectId
     * @param datasetId
     * @param filePath
     * @param offset
     * @param limit
     * @return
     */
    public static JSONObject getSingleDataSetDataPostInvoke(
            String baseUrl, String token, Integer projectId, Integer datasetId,
            String filePath, Integer offset, Integer limit) {
        CloseableHttpClient httpClient = getDefaultHttpClient();

        MessageFormat dataSetDataPostInvokeUrlForm = new MessageFormat(GET_SINGLE_DATASET_DATA_POST_INVOKE);
        String[] args = new String[]{projectId.toString(), datasetId.toString()};
        String dataSetDataPostInvokeUrl = dataSetDataPostInvokeUrlForm.format(args);

        HttpPost httpPost = new HttpPost(baseUrl + dataSetDataPostInvokeUrl);
        httpPost.addHeader("Content-Type", "application/json;charset=UTF-8");
        httpPost.addHeader("token", token);

        Map<String, Object> body = Maps.newHashMap();
        body.put("filePath", filePath);
        body.put("offset", offset);
        body.put("limit", limit);
        httpPost.setEntity(new StringEntity(Objects.requireNonNull(JSON.toJSONString(body)), Charsets.UTF_8));

        try (CloseableHttpResponse response = httpClient.execute(httpPost)) {
            JSONObject responseObj = JSON.parseObject(EntityUtils.toString(response.getEntity(), Charsets.UTF_8));
            CheckUniverseResponse(responseObj);
            response.close();
            return responseObj;
        } catch (IOException e) {
            logger.error("获取Universe数据集数据后置调用失败, url:{}.", httpPost.getURI().toString(), e);
            throw new AtlasException(UNIVERSE_FAIL_TO_GET_DATASET_DATA, e.getMessage());
        }
    }



    public static void CheckUniverseResponse(JSONObject responseObj) {
        if (responseObj.getInteger("code") != null && !responseObj.getInteger("code").equals(0)) {
            logger.error("Universe Inner Error, code:{}, msg:{}", responseObj.getInteger("code"), responseObj.getString("msg"));
            throw new AtlasException(UNIVERSE_INTERNAL_ERROR, responseObj.getString("msg"));
        } else if (responseObj.getInteger("status") != null && responseObj.getInteger("status").equals(404)) {
            throw new AtlasException("Universe API NOT FOUND");
        }
    }
}
