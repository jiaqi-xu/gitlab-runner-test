package com.guandata.atlas.common.utils;

import java.util.UUID;

public class UuidUtil {

    public static String getUUID() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }
}
