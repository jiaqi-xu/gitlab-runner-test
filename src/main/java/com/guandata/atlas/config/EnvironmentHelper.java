package com.guandata.atlas.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.net.InetAddress;
import java.text.SimpleDateFormat;
import java.util.Date;

@Component
public class EnvironmentHelper {

    private static String appName;

    private static String envName;

    private static String profile;

    private static String deployTime;

    private static String branchName;

    private static String ip;

    static {
        try {
            ip = InetAddress.getLocalHost().getHostAddress();
            deployTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
        } catch (Exception ex) {
        }
    }

    @Value("${environment.appName}")
    public void setAppName(String appName) {
        EnvironmentHelper.appName = appName;
    }

    @Value("${environment.envName}")
    public void setEnvName(String envName) {
        EnvironmentHelper.envName = envName;
    }

    @Value("${environment.profile}")
    public void setProfile(String profile) {
        EnvironmentHelper.profile = profile;
    }

    @Value("${environment.branchName}")
    public void setBranchName(String branchName) {
        EnvironmentHelper.branchName = branchName;
    }

    public static String getAppName() {
        return appName;
    }

    public static String getEnvName() {
        return envName;
    }

    public static String getProfile() {
        return profile;
    }

    public static String getDeployTime() {
        return deployTime;
    }

    public static String getBranchName() {
        return branchName;
    }

    public static String getIp() {
        return ip;
    }

    public static boolean isPre() {
        return "pre".equals(profile);
    }

    public static boolean isDev() {
        return "dev".equals(profile);
    }

    public static boolean isOnline() {
        return "prod".equals(profile);
    }

    public static boolean isTest() {
        return "test".equals(profile);
    }

    public static boolean isLocal() {
        return "local".equals(profile);
    }

    public static String buildEnvStr() {

        return "【应用：" + appName + "，环境名称：" + envName + "，profile信息：" + profile + "】" +
                "【" + ip + "】【部署时间：" + deployTime + "，部署分支：" + branchName + "】";
    }
}
