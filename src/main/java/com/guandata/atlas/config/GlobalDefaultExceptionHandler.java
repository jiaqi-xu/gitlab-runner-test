package com.guandata.atlas.config;


import com.alibaba.fastjson.JSON;
import com.guandata.atlas.common.utils.ErrorLogger;
import com.guandata.atlas.common.utils.ResponseResult;
import com.guandata.atlas.enums.ErrorCode;
import com.guandata.atlas.exception.AtlasException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;

@ControllerAdvice
public class GlobalDefaultExceptionHandler {
    private Logger logger = LoggerFactory.getLogger(GlobalDefaultExceptionHandler.class);

    @ExceptionHandler(value = Exception.class)
    public Object defaultErrorHandler(HttpServletRequest request, HttpServletResponse response, Exception e) throws Exception{
        if(response.isCommitted()){
            return null;
        }

        Integer code = ErrorCode.UN_KNOW.getCode();
        String msg = null;
        if (e instanceof AtlasException) {
            logger.error("AtlasException,detail=", e);
            code = ((AtlasException)e).getCode();
        }else{
            logger.error("uncatched exception,detail=", e);
        }
        msg = e.getMessage();

        code = (code== null ? ErrorCode.UN_KNOW.getCode() : code);

        ErrorLogger.core("Atlas报警", "errorCode={},msg={}",code, msg, e);
        if (isJsonRequest(request)) {
            ResponseResult responseResult = new ResponseResult();
            responseResult.setCode(code);
            responseResult.setMsg(msg);
            responseJSON(response, responseResult);
            return null;
        }

        logger.error("error uri : {}", request.getRequestURI(), e);

        throw e;
    }

    private boolean isJsonRequest(HttpServletRequest request) {
        String header = request.getHeader("content-type");
        return header == null || header.contains("json");
    }

    private void responseJSON(HttpServletResponse response, Object obj) {
        response.addHeader("Cache-Control", "no-cache");
        response.setContentType("application/json;charset=UTF-8");
        PrintWriter out = null;
        try {
            out = response.getWriter();
            response.setStatus(HttpStatus.OK.value());
            out.write(JSON.toJSONString(obj));
        } catch (Exception e) {

        } finally {
            if (out != null) {
                out.close();
            }
        }
    }
}
