package com.guandata.atlas.config;

import com.google.common.collect.Maps;
import com.guandata.atlas.client.http.HttpTemplate;
import com.guandata.atlas.client.http.HttpTemplateHandler;
import com.guandata.atlas.client.http.base.BaseHttpRequest;
import com.guandata.atlas.client.http.base.BaseHttpResponse;
import com.guandata.atlas.client.http.config.HttpTemplateConfig;
import com.guandata.atlas.client.http.config.RouteConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Map;

/**
 * @author dxc
 **/
@Configuration
public class HttpConfig {

    private Logger logger = LoggerFactory.getLogger(HttpConfig.class);

    @Bean
    public HttpTemplateConfig getHttpTemplateConfig() {
        RouteConfig dingTalkRouteConfig = new RouteConfig("oapi.dingtalk.com")
                .setConnectionRequestTimeout(1000)
                .setConnectTimeout(3000)
                .setSocketTimeout(3000)
                .setMaxPerRoute(400);
        RouteConfig dySmsRouteConfig = new RouteConfig("dysmsapi.aliyuncs.com")
                .setConnectionRequestTimeout(1000)
                .setConnectTimeout(3000)
                .setSocketTimeout(10000)
                .setMaxPerRoute(40);
        RouteConfig yuqueRouteConfig = new RouteConfig("www.yuque.com")
                .setConnectionRequestTimeout(1000)
                .setConnectTimeout(3000)
                .setSocketTimeout(5000)
                .setMaxPerRoute(40);
        Map<String, RouteConfig> routes = Maps.newHashMap();
        routes.put("dingTalk", dingTalkRouteConfig);
        routes.put("dySms", dySmsRouteConfig);
        routes.put("yuque", yuqueRouteConfig);
        return new HttpTemplateConfig(routes).setMaxTotal(500).setDefaultMaxPerRoute(40);
    }

    @Bean
    public HttpTemplate httpTemplate() {
        return new HttpTemplate(getHttpTemplateConfig(), new HttpTemplateHandler() {
            @Override
            public boolean onStart(BaseHttpRequest<?> baseHttpRequest) {
                return true;
            }

            @Override
            public <T extends BaseHttpResponse> void onFinish(BaseHttpRequest<T> baseHttpRequest, T baseHttpResponse, Exception e) {
            }
        });
    }
}