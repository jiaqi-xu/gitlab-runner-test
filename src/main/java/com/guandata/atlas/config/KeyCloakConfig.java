package com.guandata.atlas.config;


import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
public class KeyCloakConfig {
    @Value("${keycloak.auth-server-url}")
    private String authUrl;

    @Value("${keycloak-master-username}")
    private String username;

    @Value("${keycloak-master-password}")
    private String password;
}
