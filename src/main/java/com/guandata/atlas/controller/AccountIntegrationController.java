package com.guandata.atlas.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.guandata.atlas.common.utils.ResponseResult;
import com.guandata.atlas.dao.AccountEntity;
import com.guandata.atlas.dto.Account;
import com.guandata.atlas.dto.SqlParamDTO;
import com.guandata.atlas.dto.universe.UniverseLoginInDTO;
import com.guandata.atlas.service.AccountService;
import com.guandata.atlas.service.UniverseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;


@RestController
@RequestMapping("/api/account")
public class AccountIntegrationController extends BaseController {
    private static final Logger logger = LoggerFactory.getLogger(AccountIntegrationController.class);

    @Autowired
    private AccountService accountService;

    @Autowired
    private UniverseService universeService;

    /**
     * list all existing account info
     * @param type
     * @return
     */
    @GetMapping(value = "/list")
    public ResponseResult list(@RequestParam(name = "cnType") String type) {
        List<AccountEntity> accountEntityList = accountService.list(type);
        return success(accountEntityList.stream().map(item -> item.toAccount()).collect(Collectors.toList()));
    }

    /**
     * add a new account
     * @param account
     * @return
     */
    @PostMapping(value = "/add")
    public ResponseResult add(@RequestBody Account account) {
        return success(accountService.insert(account.toAccountEntity()));
    }

    /**
     * update account
     * @param id exsiting account id
     * @param account account info to update
     * @return
     */
    @PostMapping(value = "/{id}/update")
    public ResponseResult update(@PathVariable int id,
                                 @RequestBody Account account) {
        AccountEntity entity = account.toAccountEntity();
        return success(accountService.updateById(entity));
    }

    /**
     * delete account when there is no object associated with this account; otherwsie throw an exception
     * @param id account id
     * @return
     */
    @PostMapping(value = "/{id}/delete")
    public ResponseResult delete(@PathVariable int id) {
        return success(accountService.softDelete(id));
    }


    /**
     * connectivity test for existing account config
     * @param id account id
     * @return
     */
    @GetMapping(value = "/{id}/connectivity")
    public ResponseResult testConnectivity(@PathVariable int id){
        AccountEntity entity = accountService.get(id);
        return success(accountService.testConnectivity(entity));
    }

    /**
     * connectivity test for when adding new account config or updating an account
     * @param account account to be added
     * @return
     */
    @PostMapping(value = "/connectivity")
    public ResponseResult testConnectivity(@RequestBody Account account) {
        return success(accountService.testConnectivity(account.toAccountEntity()));
    }

    @GetMapping(value = "/{account-id}/projects")
    public ResponseResult getProjects(@PathVariable("account-id") Integer accountId) {
        AccountEntity accountEntity = accountService.get(accountId);
        JSONObject authConfig = JSON.parseObject(accountEntity.getAuthConfig());
        UniverseLoginInDTO universeLoginInDTO = authConfig.toJavaObject(UniverseLoginInDTO.class);
        return success(universeService.getProjects(universeLoginInDTO));
    }

    @GetMapping(value = "/{account-id}/project/{project-id}/datasets")
    public ResponseResult getDataSets(@PathVariable("account-id") Integer accountId,
                                      @PathVariable("project-id") Integer projectId) {
        AccountEntity accountEntity = accountService.get(accountId);
        JSONObject authConfig = JSON.parseObject(accountEntity.getAuthConfig());
        UniverseLoginInDTO universeLoginInDTO = authConfig.toJavaObject(UniverseLoginInDTO.class);
        return success(universeService.getDatasets(universeLoginInDTO, projectId));
    }

    @GetMapping(value = "/{account-id}/project/{project-id}/dataset/{dataset-id}/info")
    public ResponseResult getDataSetInfo(@PathVariable("account-id") Integer accountId,
                                         @PathVariable("project-id") Integer projectId,
                                         @PathVariable("dataset-id") Integer datasetId) {
        AccountEntity accountEntity = accountService.get(accountId);
        JSONObject authConfig = JSON.parseObject(accountEntity.getAuthConfig());
        UniverseLoginInDTO universeLoginInDTO = authConfig.toJavaObject(UniverseLoginInDTO.class);
        return success(universeService.getDatasetInfo(universeLoginInDTO, projectId, datasetId));
    }

    @PostMapping(value = "/{account-id}/project/{project-id}/dataset/{dataset-id}/data")
    public ResponseResult previewDataSetData(@PathVariable("account-id") Integer accountId,
                                             @PathVariable("project-id") Integer projectId,
                                             @PathVariable("dataset-id") Integer datasetId,
                                             @RequestBody SqlParamDTO params) {
        AccountEntity accountEntity = accountService.get(accountId);
        JSONObject authConfig = JSON.parseObject(accountEntity.getAuthConfig());
        UniverseLoginInDTO universeLoginInDTO = authConfig.toJavaObject(UniverseLoginInDTO.class);
        return success(universeService.exportDataSetData(universeLoginInDTO, datasetId, params));
    }
}
