package com.guandata.atlas.controller;

import com.guandata.atlas.common.utils.ResponseResult;
import com.guandata.atlas.dao.ActionButtonEntity;
import com.guandata.atlas.dto.ActionButton;
import com.guandata.atlas.dto.UserInfo;
import com.guandata.atlas.exception.AtlasException;
import com.guandata.atlas.service.ActionButtonService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/api/action-button")
public class ActionButtonController extends BaseController {
    private static final Logger logger = LoggerFactory.getLogger(ActionButtonController.class);

    @Autowired
    private ActionButtonService actionButtonService;

    /**
     * list all existing action button with state
     *
     * @return
     */
    @GetMapping(value = "/list")
    public ResponseResult list(@RequestParam(required = false, name = "pgId") String pgId) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserInfo userInfo = new UserInfo(authentication);
        List<ActionButtonEntity> actionButtonEntityList = actionButtonService.list(userInfo, pgId);
        return success(actionButtonEntityList);
    }

    /**
     * update an exiting action button item
     *
     * @param buttonList
     * @return
     */
    @PostMapping(value = "/update")
    public ResponseResult update(@RequestBody List<ActionButton> buttonList) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserInfo userInfo = new UserInfo(authentication);
        String userId = userInfo.getUserId();

        return success(actionButtonService.update(buttonList, userId));
    }

    /**
     * trigger action button
     *
     * @param buttonId
     * @return
     */
    @PostMapping(value = "/{buttonId}/trigger")
    public ResponseResult trigger(@PathVariable int buttonId) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserInfo userInfo = new UserInfo(authentication);
        String userId = userInfo.getUserId();

        return success(actionButtonService.trigger(buttonId, userId));
    }
}
