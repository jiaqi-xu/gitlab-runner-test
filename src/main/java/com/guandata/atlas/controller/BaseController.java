package com.guandata.atlas.controller;

import com.guandata.atlas.common.constants.Constants;
import com.guandata.atlas.common.utils.ResponseResult;
import com.guandata.atlas.enums.ErrorCode;

/**
 * base controller
 */
public class BaseController {

    /**
     * success response without data
     *
     * @return success response with data field NULL
     */
    public ResponseResult success() {
        ResponseResult result = new ResponseResult();
        result.setCode(Constants.SUCCESS_CODE);
        result.setMsg(Constants.SUCCESS_MSG);

        return result;
    }

    /**
     * success response with data
     *
     * @param data
     * @return success response with non-null data
     */
    public ResponseResult success(Object data) {
        ResponseResult result = new ResponseResult(data);
        return result;
    }

    /**
     * error response with error msg
     *
     * @param msg
     * @return error response with error msg
     */
    public ResponseResult error(String msg) {
        ResponseResult result = new ResponseResult();
        result.setCode(Constants.ERROR_CODE);
        result.setMsg(msg);
        return result;
    }

    /**
     * error response with specific error code
     * @param errorCode
     * @return
     */
    public ResponseResult error(ErrorCode errorCode) {
        return new ResponseResult(errorCode.getCode(), errorCode.getMsg());
    }

    /**
     * error response with specific error code and msg
     * @param code
     * @param msg
     * @return
     */
    public ResponseResult error(Integer code, String msg) {
        return new ResponseResult(code, msg);
    }
}
