package com.guandata.atlas.controller;

import com.guandata.atlas.common.utils.ExcelUtil;
import com.guandata.atlas.common.utils.ResponseResult;
import com.guandata.atlas.dao.CardInfoDO;
import com.guandata.atlas.dto.TableFilter;
import com.guandata.atlas.dto.TableResult;
import com.guandata.atlas.pojo.FieldInfo;
import com.guandata.atlas.service.CardService;
import com.guandata.atlas.service.planningtable.repository.CardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.io.FileInputStream;
import java.net.URLEncoder;
import java.util.List;

@RestController
@RequestMapping("/api/card")
public class CardController extends BaseController{
    @Autowired
    private CardService cardService;

    @Value("${export.path}")
    private String exportPath;


    @PostMapping(value="/init")
    public ResponseResult init(@RequestBody CardInfoDO cardInfo) {
        return success(cardService.init(cardInfo));
    }

    /**
     * 分页根据筛选条件获取表格数据
     * @param cdId
     * @param tableFilter
     * @param pageNo
     * @param pageSize
     * @return
     */
    @PostMapping(value="/{cdId}/data")
    public ResponseResult getResult(@PathVariable String cdId,
                                    @RequestBody TableFilter tableFilter,
                                    @RequestParam(required = false) Long pageNo,
                                    @RequestParam(required = false) Long pageSize) {
        if (tableFilter != null){
            tableFilter.setCdId(cdId);
        }
        return success(cardService.getResult(tableFilter,pageNo,pageSize));
    }

    /**
     * 分页根据筛选条件获取表格数据
     * @param cdId
     * @param tableFilter
     * @param pageNo
     * @param pageSize
     * @return
     */
    @PostMapping(value="/{cdId}/data/preview")
    public ResponseResult getPreviewResult(@PathVariable String cdId,
                                           @RequestBody TableFilter tableFilter,
                                           @RequestParam(required = false) Long pageNo,
                                           @RequestParam(required = false) Long pageSize) {
        if (tableFilter != null){
            tableFilter.setCdId(cdId);
        }
        return success(cardService.getPreviewResultDataList(tableFilter,pageNo,pageSize));
    }

    @PostMapping(value="/add")
    public ResponseResult add(@RequestBody List<CardInfoDO> cardInfoList) {
        return success(cardService.insertCards(cardInfoList));
    }

    @GetMapping(value="/{id}/get")
    public ResponseResult get(@PathVariable("id") String cdId) {
        return success(cardService.getCardById(cdId));
    }

    /**
     * 根据筛选器条件导出结果页全量数据到excel表
     * @param cdId
     * @param tableFilter
     * @return
     * @throws Exception
     */
    @PostMapping(value="/{cdId}/export")
    public Object exportCard(@PathVariable String cdId,
                             @RequestBody TableFilter tableFilter) throws Exception{
        String dataSetName = CardRepository.getCardByCardId(cdId).getPage().getDataSet().getName();
        TableResult res = cardService.getPreviewResultDataList(tableFilter, 1L, Long.MAX_VALUE);
        List<FieldInfo> fieldInfoList = tableFilter.getFieldsInfo();
        String cardName = cardService.getCardById(cdId).getName();
        String targetDSFolder = exportPath + "/" + dataSetName;
        File folderPath = new File(targetDSFolder);
        if (!folderPath.exists()) {
            folderPath.mkdirs();
        }

        String filePath = targetDSFolder + "/" + cardName + ".xlsx";
        String fileName = filePath.substring(filePath.lastIndexOf("/") + 1);
        File file = new File(filePath);
        ExcelUtil excelUtil = new ExcelUtil();
        excelUtil.exportToExcel(fieldInfoList, res, "Sheet1", filePath);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "attachment;filename=" + URLEncoder.encode(fileName, "UTF-8"));
        headers.add("Content-Length", String.valueOf(file.length()));
        headers.add("Content-Type", "application/octet-stream;charset=UTF-8");
        ResponseEntity<Object> response = ResponseEntity.ok()
                .headers(headers)
                .body(new InputStreamResource(new FileInputStream(file)));
        return response;
    }

    @GetMapping(value = "/{cdId}/filter/defaultValues")
    public ResponseResult getCardDefaultValues(@PathVariable String cdId) {

        return success(cardService.getCardFilterDefaultValues(cdId));
    }
}
