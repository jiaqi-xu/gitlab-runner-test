package com.guandata.atlas.controller;

import com.alibaba.fastjson.JSONObject;
import com.guandata.atlas.common.utils.ResponseResult;
import com.guandata.atlas.dao.AdminModule;
import com.guandata.atlas.service.DashboardService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/dashboard")
public class DashboardController extends BaseController {
    private static final Logger logger = LoggerFactory.getLogger(DashboardController.class);

    @Autowired
    private DashboardService dashboardService;


    /**
     * 获取模块信息
     * @param id
     * @return
     */
    @GetMapping(value = "/module/{id}/info")
    public ResponseResult get(@PathVariable int id) {
        return success(dashboardService.get(id));
    }

    /**
     * 新增模块
     * @param module
     * @return
     */
    @PostMapping(value = "/module/add")
    public ResponseResult add(@RequestBody AdminModule module) {
        return success(dashboardService.insert(module));
    }

    /**
     * 模块启用开关
     * @param id
     * @param enabledObj
     * @return
     */
    @PostMapping(value = "/module/{id}/switch")
    public ResponseResult enable(@PathVariable int id,
                                 @RequestBody JSONObject enabledObj) {
        Boolean isEnabled = enabledObj.getBoolean("enabled");
        return success(dashboardService.moduleSwitch(id, isEnabled));
    }

    /**
     * 设置模块别名
     * @param id 模块id
     * @param aliasObj 别名对象
     * @return
     */
    @PostMapping(value = "/module/{id}/set-alias")
    public ResponseResult rename(@PathVariable int id,
                                 @RequestBody JSONObject aliasObj) {
        String alias = aliasObj.getString("alias");
        return success(dashboardService.setAlias(id, alias));
    }

    /**
     * 获取启用模块列表
     * @return
     */
    @GetMapping(value = "/module/list")
    public ResponseResult listModules() {
        return success(dashboardService.listModules());
    }


}
