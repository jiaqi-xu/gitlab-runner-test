package com.guandata.atlas.controller;

import com.guandata.atlas.common.constants.Constants;
import com.guandata.atlas.common.utils.ResponseResult;
import com.guandata.atlas.controller.request.FilterDataGetRequest;
import com.guandata.atlas.dto.DataSetDTO;
import com.guandata.atlas.dto.UserInfo;
import com.guandata.atlas.enums.ErrorCode;
import com.guandata.atlas.exception.AtlasException;
import com.guandata.atlas.service.DataSetService;
import com.guandata.atlas.service.dataset.model.DataSet;
import com.guandata.atlas.service.dataset.repository.DataSetRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/api/dataset")
public class DataSetController extends BaseController {
    private static final Logger logger = LoggerFactory.getLogger(DataSetController.class);

    @Autowired
    private DataSetService dataSetService;

    @GetMapping(value="/list")
    public ResponseResult list(@RequestParam(required = false) String search,
                               @RequestParam(defaultValue = "update_time", required = false) String orderBy,
                               @RequestParam(defaultValue = "desc", required = false) String orderType) {
        if (!orderBy.equals("name") && !orderBy.equals("update_time")) {
            return error(ErrorCode.PARAME_ERROR_CODE);
        }

        if (!orderType.equals(Constants.DESC) && !orderType.equals(Constants.ASC)) {
            return error(ErrorCode.PARAME_ERROR_CODE);
        }

        return success(dataSetService.list(search, orderBy, orderType));
    }

    @GetMapping(value="/info")
    public ResponseResult info(@RequestParam(required = false) Integer dataSetId, @RequestParam(name = "pageId", required = false) String pgId) {
        return success(dataSetService.getDataSetSchemaInfo(dataSetId, pgId));
    }

    @GetMapping(value = "/{dataset-id}/meta")
    public ResponseResult meta(@PathVariable("dataset-id") Integer datasetId) {
        if (!dataSetService.isDataSetExisted(datasetId)) {
            throw new AtlasException(ErrorCode.ATLAS_DATASET_NOT_EXISTED);
        }
        return success(dataSetService.getDataSetMeta(datasetId));
    }

    /**
     * 更新数据集meta信息，e.g. version pk, column name, column comment, etc.
     * @param id 数据集id
     * @param dataSetDTO 数据集实体DTO
     * @return
     */
    @PostMapping(value = "/{id}/update")
    public ResponseResult update(@PathVariable int id,
                                 @RequestBody DataSetDTO dataSetDTO) {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserInfo userInfo = new UserInfo(authentication);
        String userId = userInfo.getUserId();
        return success(dataSetService.updateDataSetMeta(dataSetDTO, userId));
    }


    @GetMapping(value = "/{id}/sync-data")
    public ResponseResult syncData(@PathVariable int id) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserInfo userInfo = new UserInfo(authentication);

        DataSet dataset = DataSetRepository.getDataSetById(id);
        return success(dataSetService.syncData(id, userInfo,dataset.getTableName()));
    }

    @GetMapping(value = "/{id}/preview-data")
    public ResponseResult previewData(@PathVariable int id,
                                      @RequestParam(required = false) Integer limit) {
        if (!dataSetService.isDataSetExisted(id)) {
            throw new AtlasException(ErrorCode.ATLAS_DATASET_NOT_EXISTED);
        }
        return success(dataSetService.previewDSData(id, limit));
    }

    /**
     * 引入新数据集并新建相关数据表
     * @param dataSetDTO
     * @return
     */
    @PostMapping(value = "/import")
    public ResponseResult add(@RequestBody DataSetDTO dataSetDTO) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserInfo userInfo = new UserInfo(authentication);
        String userId = userInfo.getUserId();

        String tableName = UUID.randomUUID().toString();
        dataSetDTO.setTableName(tableName);
        Integer dsId = dataSetService.importDataSet(dataSetDTO, userId);
        dataSetService.syncData(dsId, userInfo, tableName);
        return success(tableName);
    }


    @PostMapping(value = "/{id}/delete")
    public ResponseResult softDelete(@PathVariable int id) {
        return success(dataSetService.softDelete(id));
    }

    @PostMapping(value="/filter/data")
    public ResponseResult getFilterData(@RequestBody FilterDataGetRequest request) {
        return success(dataSetService.getFilterData(request));
    }
}
