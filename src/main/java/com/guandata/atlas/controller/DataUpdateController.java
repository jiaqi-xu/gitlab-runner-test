package com.guandata.atlas.controller;

import com.guandata.atlas.common.constants.Constants;
import com.guandata.atlas.common.utils.ResponseResult;
import com.guandata.atlas.dto.BatchUpdateDataDTO;
import com.guandata.atlas.dto.DataUpdateParam;
import com.guandata.atlas.enums.ErrorCode;
import com.guandata.atlas.exception.AtlasException;
import com.guandata.atlas.service.DataSetService;
import com.guandata.atlas.service.DataUpdateService;
import com.guandata.atlas.service.ModifyHistoryService;
import com.guandata.atlas.service.dataset.model.DataSet;
import com.guandata.atlas.service.dataset.repository.DataSetRepository;
import com.guandata.atlas.service.planningtable.repository.CardRepository;
import com.guandata.atlas.service.planningtable.vo.TaskInfoVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/data")
public class DataUpdateController extends BaseController {
    private static final Logger logger = LoggerFactory.getLogger(DataUpdateController.class);

    @Autowired
    private DataUpdateService dataUpdateService;

    @Autowired
    private ModifyHistoryService modifyHistoryService;

    @PostMapping(value = "/update")
    public ResponseResult update(@RequestBody DataUpdateParam param) {
        ResponseResult result = error("");
        String message = "";

        String dsTable = CardRepository.getCardByCardId(param.getCdId()).getPage().getDataSet().getTableName();
        param.setTableName(dsTable);
        try {
            dataUpdateService.doUpdate(param, result);
            result.setCode(Constants.SUCCESS_CODE);
            result.setMsg(Constants.SUCCESS_MSG);
        } catch (AtlasException e) {
            // 自定义捕获异常
            message += e.getMessage();
            logger.error("更新预测表失败 [{}]", param,e);
            result.setMsg(e.getMessage());
        } catch (Exception e) {
            // 其他异常
            message += e.getMessage();
            logger.error("更新预测表失败 [{}]", param, e);
            result.setMsg("更新失败");
        } finally {
            if (message.length() > 0) {
                modifyHistoryService.insert(param, message, false);
            }
        }

        return result;
    }

    @PostMapping(value = "/batch/modify")
    public ResponseResult<TaskInfoVO> batchModify(@RequestBody BatchUpdateDataDTO batchUpdateData){

        if (batchUpdateData == null || !batchUpdateData.checkParams()) {
            return error(ErrorCode.PARAME_ERROR_CODE);
        }

        DataSet dataset = DataSetRepository.getDataSetById(batchUpdateData.getDsId());
        if (dataset == null) {
            throw new AtlasException(ErrorCode.PARAME_ERROR_CODE);
        }

        batchUpdateData.setTableName(dataset.getTableName());

        dataUpdateService.doBatchUpdate(batchUpdateData);

        return success();
    }

}
