package com.guandata.atlas.controller;

import com.guandata.atlas.common.utils.ResponseResult;
import com.guandata.atlas.dao.FieldInfoDO;
import com.guandata.atlas.enums.ErrorCode;
import com.guandata.atlas.service.FieldService;
import com.guandata.atlas.service.planningtable.vo.FieldInfoVO;
import org.jooq.tools.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/field")
public class FieldController extends BaseController {
    @Autowired
    private FieldService fieldService;

    @PostMapping(value="/add")
    public ResponseResult add(@RequestBody List<FieldInfoDO> fieldInfoList) {
        return success(fieldService.insertFields(fieldInfoList));
    }

    @GetMapping(value = "{cdId}/{fdId}/replaceable/get")
    public ResponseResult<List<FieldInfoVO>> getReplaceableFields(@PathVariable String cdId, @PathVariable String fdId){

        if (StringUtils.isBlank(cdId) || StringUtils.isBlank(fdId)) {
            return error(ErrorCode.PARAME_ERROR_CODE);
        }

        return success(fieldService.getReplaceableFields(cdId, fdId));
    }
}
