package com.guandata.atlas.controller;

import com.guandata.atlas.common.utils.ResponseResult;
import com.guandata.atlas.controller.request.FilterDataGetRequest;
import com.guandata.atlas.service.FilterService;
import com.guandata.atlas.service.PageService;
import com.guandata.atlas.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/filter")
public class FilterController extends BaseController{
    @Autowired
    private FilterService filterService;

    @Autowired
    private UserService userService;

    @Autowired
    private PageService pageService;

    @PostMapping(value="/data")
    public ResponseResult getFilterData(@RequestBody FilterDataGetRequest request) {
        return success(filterService.getFilterData(request));
    }

    @GetMapping(value = "/{dsId}/page-info")
    public ResponseResult getPageCardFilter(@PathVariable Integer dsId) {

        return success(pageService.getPageInfoFilter(dsId));
    }

    @GetMapping(value = "/{dsId}/user")
    public ResponseResult getUser(@PathVariable Integer dsId) {

        return success(userService.getUserFilter(dsId));
    }
}
