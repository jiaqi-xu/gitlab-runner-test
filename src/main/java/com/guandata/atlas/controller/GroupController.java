package com.guandata.atlas.controller;


import com.guandata.atlas.common.utils.ResponseResult;
import com.guandata.atlas.service.UserService;
import com.guandata.atlas.service.user.dto.GroupDTO;
import com.guandata.atlas.service.user.dto.UserDTO;
import com.guandata.atlas.service.user.model.Group;
import com.guandata.atlas.service.user.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value="/api/groups")
public class GroupController extends BaseController {
    private static final Logger logger = LoggerFactory.getLogger(GroupController.class);

    @Resource
    private UserService userService;
    /************************* Group *************************/

    @GetMapping(value = "")
    public ResponseResult listGroups(@RequestParam(name = "search", required = false) String search) {
        return success(userService.getGroups(search));
    }

    @PostMapping(value = "/create")
    public ResponseResult createGroup(@RequestBody GroupDTO groupDTO) {
        userService.createGroup(new Group(groupDTO.getName()));
        return success();
    }

    @PostMapping(value = "/{groupId}/delete")
    public ResponseResult deleteGroup(@PathVariable("groupId") String groupId) {
        userService.deleteGroup(groupId);
        return success();
    }

    @PostMapping(value = "/{groupId}/rename")
    public ResponseResult renameGroup(@PathVariable("groupId") String groupId,
                                      @RequestBody GroupDTO groupDTO) {
        userService.renameGroup(new Group(groupId, groupDTO.getName()));
        return success();
    }

    @GetMapping(value = "/{groupId}/members")
    public ResponseResult getGroupMembers(@PathVariable("groupId") String groupId) {
        List<User> users = userService.getGroupMembers(groupId);
        return success(users.stream().map(user -> user.transferToUserVO()).collect(Collectors.toList()));
    }

    @GetMapping(value = "/{groupId}/available-users")
    public ResponseResult getAvailableUsersForGroup(@PathVariable("groupId") String groupId) {
        List<User> users = userService.getAvailableUsersForGroup(groupId);
        return success(users.stream().map(user -> user.transferToUserVO()).collect(Collectors.toList()));
    }

    @PostMapping(value = "/{groupId}/members/add")
    public ResponseResult addGroupMembers(@PathVariable("groupId") String groupId,
                                          @RequestBody List<UserDTO> userDTOList) {
        userService.addGroupMembers(groupId, userDTOList);
        return success();
    }

    @PostMapping(value = "/{groupId}/members/{uId}/remove")
    public ResponseResult removeGroupMember(@PathVariable("groupId") String groupId,
                                            @PathVariable("uId") String uId) {
        userService.removeGroupMember(groupId, uId);
        return success();
    }
}
