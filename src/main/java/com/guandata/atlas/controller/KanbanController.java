package com.guandata.atlas.controller;

import com.guandata.atlas.common.utils.ResponseResult;
import com.guandata.atlas.dao.KanbanEntity;
import com.guandata.atlas.dto.UserInfo;
import com.guandata.atlas.exception.AtlasException;
import com.guandata.atlas.service.GalaxyService;
import com.guandata.atlas.service.KanbanService;
import com.guandata.atlas.service.UserService;
import com.guandata.atlas.service.user.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/kanban")
public class KanbanController extends BaseController {
    @Autowired
    private KanbanService kanbanService;

    @Autowired
    private GalaxyService galaxyService;

    @Autowired
    private UserService userService;

    /**
     * order kanban items
     * @param kanbanList list of kanban item with new order
     * @return
     */
    @PostMapping(value = "/order")
    public ResponseResult order(@RequestBody List<KanbanEntity> kanbanList) {
        return success(kanbanService.order(kanbanList));
    }

    /**
     * list kanban items by type
     * @param type
     * @return
     */
    @GetMapping(value = "/list")
    public ResponseResult list(@RequestParam(name = "type") String type) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserInfo userInfo = new UserInfo(authentication);
        String userId = userInfo.getUserId();
        return success(kanbanService.list(type, userId));
    }

    /**
     * add new kanban item
     * @param kanbanEntity
     * @return
     */
    @PostMapping(value = "/add")
    public ResponseResult add(@RequestBody KanbanEntity kanbanEntity) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserInfo userInfo = new UserInfo(authentication);
        String userId = userInfo.getUserId();
        return success(kanbanService.insert(kanbanEntity, userId));
    }

    /**
     * delete an existing kanban item
     * @param id
     * @return
     */
    @PostMapping(value = "/{id}/delete")
    public ResponseResult delete(@PathVariable int id) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserInfo userInfo = new UserInfo(authentication);
        String userId = userInfo.getUserId();
        return success(kanbanService.softDelete(id, userId));
    }

    /**
     * update an existing kanban item
     * @param id existing account id
     * @param kanbanEntity account with updating info
     * @return
     */
    @PostMapping(value ="/{id}/update")
    public ResponseResult update(@PathVariable int id,
                                 @RequestBody KanbanEntity kanbanEntity) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserInfo userInfo = new UserInfo(authentication);
        String userId = userInfo.getUserId();
        return success(kanbanService.update(kanbanEntity, userId));
    }

    /**
     * 获取类型为BI page的看板的筛选器
     * @param id
     * @return
     */
    @GetMapping(value = "/{id}/selectors")
    public ResponseResult getBIPageSelectors(@PathVariable int id) {
        return success(galaxyService.getPageSelectors(id));
    }

    /**
     * information for single sign on
     * @param id kanban item id
     * @return
     * @throws AtlasException
     */
    @GetMapping(value = "{id}/sso-info")
    public ResponseResult getSSOInfo(@PathVariable int id) throws Exception{
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserInfo userInfo = new UserInfo(authentication);

        User user = userService.getUser(userInfo.getUserId());
        String resourceId = kanbanService.getById(id).getObjectId();
        if (!user.isResourceOwner(resourceId)) {
            return error("No Resource Permission");
        }

        return success(kanbanService.getSSOInfo(id));
    }
}
