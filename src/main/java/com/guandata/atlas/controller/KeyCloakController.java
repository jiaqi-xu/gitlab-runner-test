package com.guandata.atlas.controller;

import com.guandata.atlas.common.utils.ResponseResult;
import com.guandata.atlas.dto.KeyCloackResource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/keycloak")
public class KeyCloakController extends BaseController {
    @Value("${keycloak.realm}")
    String realm;
    @Value("${keycloak.resource}")
    String resource;
    @Value("${keycloak.auth-server-url}")
    String authServerUrl;
    @Value("${keycloak.ssl-required}")
    String sslRequired;
    @Value("${keycloak.public-client}")
    String publicClient;
    @Value("${keycloak.use-resource-role-mappings}")
    String useResourceRoleMappings;

    @GetMapping(value = "/resource")
    public ResponseResult getKeyCloakResource() {
        return success(
                new KeyCloackResource(realm, resource, authServerUrl, sslRequired, Boolean.valueOf(publicClient), Boolean.valueOf(useResourceRoleMappings))
        );
    }
}
