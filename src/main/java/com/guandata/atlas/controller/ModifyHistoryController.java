package com.guandata.atlas.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.guandata.atlas.common.utils.PageUtil;
import com.guandata.atlas.common.utils.ResponseResult;
import com.guandata.atlas.dto.ModificationCheckParam;
import com.guandata.atlas.service.ModifyHistoryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/api/modify-history")
public class ModifyHistoryController extends BaseController {
    private static final Logger logger = LoggerFactory.getLogger(ModifyHistoryController.class);

    @Autowired
    private ModifyHistoryService modifyHistoryService;

    @PostMapping(value = "/{dsId}/actions")
    public ResponseResult getModifyAction(@PathVariable Integer dsId,
                                          @RequestBody ModificationCheckParam param,
                                          @RequestParam(required = false) Long pageNo,
                                          @RequestParam(required = false) Long pageSize) {
        Page<Map<String, Object>> page = PageUtil.pagination(pageNo, pageSize);
        return success(modifyHistoryService.getModifyAction(page, param, dsId));
    }

    @PostMapping(value = "/{dsId}/details")
    public ResponseResult getDetails(@PathVariable Integer dsId,
                                     @RequestBody ModificationCheckParam param,
                                     @RequestParam Long modifyId,
                                     @RequestParam boolean status,
                                     @RequestParam(required = false) Long pageNo,
                                     @RequestParam(required = false) Long pageSize) {
        Page<Map<String, Object>> page = PageUtil.pagination(pageNo, pageSize);

        return success(modifyHistoryService.getDetails(page, param, modifyId, status, dsId));
    }
}
