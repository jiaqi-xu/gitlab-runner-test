package com.guandata.atlas.controller;

import com.guandata.atlas.common.utils.ResponseResult;
import com.guandata.atlas.dto.UserInfo;
import com.guandata.atlas.enums.ErrorCode;
import com.guandata.atlas.pojo.PageInfo;
import com.guandata.atlas.service.PageService;
import com.guandata.atlas.service.UserService;
import com.guandata.atlas.service.user.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value="/api/page")
public class PageController extends BaseController {
    private static final Logger logger = LoggerFactory.getLogger(PageController.class);

    @Autowired
    private PageService pageService;

    @Autowired
    private UserService userService;

    @PostMapping(value="/init")
    public ResponseResult init(@RequestBody PageInfo pgInfo) throws Exception {
        return success(pageService.init(pgInfo));
    }

    @PostMapping(value="/save")
    public ResponseResult save(@RequestBody PageInfo pgInfo) {
        ResponseResult result = error("");
        return success(pageService.insertOrUpdatePage(pgInfo));
    }

    @GetMapping(value="/{id}/get")
    public ResponseResult<PageInfo> get(@PathVariable("id") String pageId) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserInfo userInfo = new UserInfo(authentication);
        User user = userService.getUser(userInfo.getUserId());
        if (!pageService.isPageExisted(pageId)) {
            return error(ErrorCode.PT_PAGE_NOT_EXSITED);
        }

        if (!user.isResourceOwner(pageId)) {
            return error(ErrorCode.NO_RESOURCE_PERMISSION);
        }
        return success(pageService.getPageInfoVOById(pageId));
    }

    @GetMapping(value="list")
    public ResponseResult list() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserInfo userInfo = new UserInfo(authentication);
        String userId = userInfo.getUserId();
        return success(pageService.list(userId));
    }

    @PostMapping(value="/delete")
    public ResponseResult delete(@RequestBody PageInfo pgInfo) {
        return success(pageService.deletePage(pgInfo.getPgId()));
    }
}
