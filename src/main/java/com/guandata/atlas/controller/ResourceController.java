package com.guandata.atlas.controller;

import com.guandata.atlas.common.utils.ResponseResult;
import com.guandata.atlas.dto.GrantResource;
import com.guandata.atlas.service.ResourceService;
import com.guandata.atlas.service.user.model.Group;
import com.guandata.atlas.service.user.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;


@RestController
@RequestMapping("/api/resources")
public class ResourceController extends BaseController {
    private static final Logger logger = LoggerFactory.getLogger(ResourceController.class);

    @Autowired
    private ResourceService resourceService;

    @GetMapping(value = "/list")
    public ResponseResult getResources(@RequestParam(name = "type", required = false) String type) {
        return success(resourceService.listGlobalResource(type));
    }


    @PostMapping(value = "/grant")
    public ResponseResult grantResourceTo(@RequestBody GrantResource grantResource) {
        resourceService.grantResourceToOwners(grantResource);
        return success();
    }

    @PostMapping(value = "/revoke")
    public ResponseResult revokeResourceFrom(@RequestBody GrantResource revokeResource) {
        resourceService.revokeResourceFromOwners(revokeResource);
        return success();
    }

    @GetMapping(value = "/{resource-id}/users")
    public ResponseResult getResourceOwnedUsers(@PathVariable("resource-id") String resourceId) {
        List<User> users = resourceService.getResourceUsers(resourceId);
        return success(users.stream().map(user -> user.transferToUserVO()).collect(Collectors.toList()));
    }

    @GetMapping(value = "/{resource-id}/groups")
    public ResponseResult getResourceOwnedGroups(@PathVariable("resource-id") String resourceId) {
        List<Group> groups = resourceService.getResourceGroups(resourceId);
        return success(groups);
    }
}
