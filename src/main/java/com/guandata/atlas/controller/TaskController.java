package com.guandata.atlas.controller;

import com.guandata.atlas.common.utils.ResponseResult;
import com.guandata.atlas.service.TaskService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/task")
public class TaskController extends BaseController {
    private static final Logger logger = LoggerFactory.getLogger(TaskController.class);

    @Autowired
    private TaskService taskService;

    @GetMapping(value = "/list")
    public ResponseResult getTaskList(@RequestParam String taskType,
                                      @RequestParam(required = false) String objectId,
                                      @RequestParam(required = false) Integer state) {
        return success(taskService.getTaskList(taskType, objectId, state));
    }

    @GetMapping(value = "/latest-state")
    public ResponseResult getTaskLatestState(@RequestParam String taskType,
                                             @RequestParam String objectId) {
        return success(taskService.getLatestState(taskType, objectId));
    }
}
