package com.guandata.atlas.controller;

import com.guandata.atlas.common.utils.ResponseResult;
import com.guandata.atlas.dto.UserInfo;
import com.guandata.atlas.enums.ErrorCode;
import com.guandata.atlas.exception.AtlasException;
import com.guandata.atlas.service.UserService;
import com.guandata.atlas.service.user.model.operate.ModifyPasswordDTO;
import com.guandata.atlas.service.user.dto.UserDTO;
import com.guandata.atlas.service.user.model.User;
import com.guandata.atlas.service.user.vo.UserVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;


@RestController
@RequestMapping(value="/api/users")
public class UserController extends BaseController {
    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserService userService;

    @PostMapping(value = "/create")
    public ResponseResult createUser(@RequestBody UserDTO userDTO) {
        if (userDTO.getLoginId() == null || userDTO.getPassword() == null
                || userDTO.getUsername() == null || userDTO.getTemporary() == null) {
            throw new AtlasException(ErrorCode.PARAME_ERROR_CODE);
        }
        return success(userService.createUser(userDTO).transferToUserVO());
    }

    @PostMapping(value = "/{uId}/delete")
    public ResponseResult deleteUser(@PathVariable("uId") String uId) {
        return success(userService.deleteUser(uId));
    }

    @PostMapping(value = "/{uId}/update")
    public ResponseResult updateUser(@PathVariable("uId") String uId,
                                     @RequestBody UserDTO userDTO) {
        userService.updateUser(uId, userDTO);
        return success();
    }

    @PostMapping(value = "/{uId}/reset-password")
    public ResponseResult resetPassword(@PathVariable("uId") String uId,
                                        @RequestBody UserDTO userDTO) {
        if (userDTO.getPassword() == null || userDTO.getTemporary() == null) {
            throw new AtlasException(ErrorCode.PARAME_ERROR_CODE);
        }

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserInfo userInfo = new UserInfo(authentication);
        if (!userInfo.getUserId().equals(uId) && !userService.getUser(userInfo.getUserId()).isAdminUser()) {
            throw new AtlasException(ErrorCode.FAIL_TO_RESET_PASSWORD);
        }

        userService.resetPassword(uId, userDTO);
        return success();
    }

    @PostMapping(value = "/{uId}/modify-password")
    public ResponseResult modifyPasswordByUserSelf(@PathVariable("uId") String uId,
                                                   @RequestBody ModifyPasswordDTO modifyPasswordDTO) {
        userService.modifyPasswordByUserSelf(uId, modifyPasswordDTO);
        return success();
    }

    @GetMapping(value = "/{uId}")
    public ResponseResult getUser(@PathVariable("uId") String uId) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserInfo userInfo = new UserInfo(authentication);
        if (!userInfo.getUserId().equals(uId) && !userService.getUser(userInfo.getUserId()).isAdminUser()) {
            throw new AtlasException(ErrorCode.FAIL_TO_GET_USER_INFO);
        }

        UserVO user = userService.getUser(uId).transferToUserVO();
        return success(user);
    }

    @GetMapping(value = "")
    public ResponseResult listUsers(@RequestParam(name = "search", required = false) String search,
                                    @RequestParam(name = "first", required = false) Integer first,
                                    @RequestParam(name = "max", required = false) Integer max) {
        List<User> users = userService.list(search, first, max);
        return success(users.stream().map(user -> user.transferToUserVO()).collect(Collectors.toList()));
    }

    /************************* ROLE *************************/

    @GetMapping(value = "/roles")
    public ResponseResult listRoles() {
        return success(userService.getRoles().stream().map(
                role -> role.getName()).collect(Collectors.toList()));
    }
}
