package com.guandata.atlas.controller.aspect;

import com.alibaba.fastjson.JSON;
import com.guandata.atlas.common.annotation.TaskCheck;
import com.guandata.atlas.dao.TaskDAO;
import com.guandata.atlas.enums.ErrorCode;
import com.guandata.atlas.enums.TaskStatusEnum;
import com.guandata.atlas.enums.TaskTypeEnum;
import com.guandata.atlas.exception.AtlasException;
import com.guandata.atlas.service.task.repository.TaskRepository;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Aspect
@Component
public class TaskCheckAspect {

    private static final Logger logger = LoggerFactory.getLogger(TaskCheckAspect.class);

    private final ExpressionParser parser = new SpelExpressionParser();

    /**
     * 切面配置
     */
    @Pointcut("@annotation(com.guandata.atlas.common.annotation.TaskCheck)")
    public void pointCut() {
    }

    /**
     *
     * @param pjp
     * @param taskCheck
     * @return
     * @throws Throwable
     */
    @Around("pointCut() && @annotation(taskCheck)")
    public Object interceptor(ProceedingJoinPoint pjp, TaskCheck taskCheck) throws Throwable {
        //获取被拦截的方法
        Method method = ((MethodSignature) pjp.getSignature()).getMethod();
        //获取被拦截的方法参数
        Object[] parameterValues = pjp.getArgs();

        EvaluationContext context = new StandardEvaluationContext();
        Parameter[] parameters = method.getParameters();
        for (int i = 0; i < parameters.length; i++) {
            // 方法的入参全部set进SpEL的上下文
            String name = parameters[i].getName();
            Object value = parameterValues[i];
            context.setVariable(name, value);
        }
        Object value = parser.parseExpression(taskCheck.objectId()).getValue(context);
        if (value == null) {
            logger.warn("spel表达式无法解析出数据或者不需要校验，args={}", JSON.toJSONString(parameterValues));
            return pjp.proceed();
        }

        List<Integer> statusList = Arrays.stream(taskCheck.statusList()).map(TaskStatusEnum::getStatus).collect(Collectors.toList());
        List<String> taskTypeList = Arrays.stream(taskCheck.taskTypeList()).map(TaskTypeEnum::getType).collect(Collectors.toList());
        List<TaskDAO> tasksByObjectId = TaskRepository.getTasksByObjectId(statusList, taskTypeList, String.valueOf(value));

        long nowTimeMills = new Date().getTime();
        long taskCount = tasksByObjectId.stream().filter(s ->
                (nowTimeMills - s.getStartTime().getTime()) <= taskCheck.timeoutSeconds() * 1000
        ).count();
        if (taskCount > taskCheck.taskSizeLimit()) {
            throw new AtlasException(ErrorCode.TASK_CONCURRENT_ERROR, buildErrorMsg(taskCheck));
        }

        return pjp.proceed();
    }

    private String buildErrorMsg(TaskCheck taskCheck) {

        List<String> statusNameList = Arrays.stream(taskCheck.statusList()).map(TaskStatusEnum::getStatusName).collect(Collectors.toList());
        StringBuilder errorMsg = new StringBuilder("存在状态为：");
        errorMsg.append(StringUtils.join(statusNameList, "或"));
        errorMsg.append("而且数量大于").append(taskCheck.taskSizeLimit()).append("的任务");

        return errorMsg.toString();
    }
}
