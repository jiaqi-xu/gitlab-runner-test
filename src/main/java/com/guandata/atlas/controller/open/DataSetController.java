package com.guandata.atlas.controller.open;

import com.guandata.atlas.common.utils.ResponseResult;
import com.guandata.atlas.controller.BaseController;
import com.guandata.atlas.dao.TokenDAO;
import com.guandata.atlas.dto.UserInfo;
import com.guandata.atlas.enums.TaskTypeEnum;
import com.guandata.atlas.exception.AtlasException;
import com.guandata.atlas.service.DataSetService;
import com.guandata.atlas.service.account.repository.TokenRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

import static com.guandata.atlas.enums.ErrorCode.PUBLIC_API_NO_PERMISSION;

@RestController("openDataSetController")
@RequestMapping("public-api/dataset")
public class DataSetController extends BaseController {
    private static final Logger logger = LoggerFactory.getLogger(DataSetController.class);

    @Resource
    private DataSetService dataSetService;

    @GetMapping(value = "/{dataset-uuid}/sync-data")
    public ResponseResult syncData(@PathVariable("dataset-uuid") String datasetUUID,
                                   @RequestParam String token) {
        UserInfo userInfo = new UserInfo();
        userInfo.setUserId("PUBLIC-API");
        userInfo.setUserName("PUBLIC-API");

        TokenDAO tokenDAO = TokenRepository.getToken(datasetUUID, TaskTypeEnum.SYNC_DATASET_DATA.getType());
        if (!tokenDAO.getAccessToken().equals(token)) {
            throw new AtlasException(PUBLIC_API_NO_PERMISSION);
        }
        int datasetId = dataSetService.getDataSetByTableName(datasetUUID).getId();
        return success(dataSetService.syncData(datasetId, userInfo, datasetUUID));
    }
}
