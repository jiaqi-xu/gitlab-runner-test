package com.guandata.atlas.controller.request;

import com.google.common.collect.Lists;
import com.guandata.atlas.dto.ResultCondition;
import com.guandata.atlas.pojo.FieldInfo;
import com.guandata.atlas.pojo.FilterInfo;
import com.guandata.atlas.service.planningtable.model.filter.entity.Filter;
import lombok.Data;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.stream.Collectors;

@Data
public class FilterDataGetRequest {

    /** filter base structure **/
    private List<FilterInfo> filters;
    /** page Id **/
    private String pgId;
    /** card Id; page filter if null **/
    private String cdId;
    /** card filter can be affected by page filter **/
    private List<ResultCondition> conditions;
    /** related dataset **/
    private Integer dsId;

    /** 对应planning table 表格字段模块中的字段 **/
    private List<FieldInfo> fieldsInfo;

    public void reBuildConditions(){
        if (CollectionUtils.isEmpty(conditions)) {
            return;
        }

        conditions.stream().forEach(s->s.reBuildCondition());
    }

    public List<FilterInfo> getConditionsInfo() {

        if (CollectionUtils.isEmpty(conditions)) {
            return Lists.newArrayList();
        }

        return conditions.stream().map(s->s.getFilterInfo()).collect(Collectors.toList());
    }
}
