package com.guandata.atlas.controller.test;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/test")
public class DemoController {
    @GetMapping("/demo")
    public String demo(){
        return "hello";
    }
}
