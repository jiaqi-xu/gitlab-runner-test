package com.guandata.atlas.dao;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.guandata.atlas.dto.Account;
import lombok.Data;

import java.util.Date;

@Data
@TableName("account")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AccountEntity {
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @TableField(value = "name")
    private String name;

    @TableField(value = "cn_type")
    private String cnType;

    @TableField(value = "auth_config")
    private String authConfig;

    @TableField(value = "create_time")
    private Date createTime;

    @TableField(value = "update_time")
    private Date updateTime;

    @TableField(value = "is_del")
    private boolean isDel;

    public Account toAccount() {
        Account account = new Account();
        account.setId(this.id);
        account.setName(this.name);
        account.setCnType(this.cnType);
        JSONObject authConfig = JSON.parseObject(this.getAuthConfig());

        authConfig.remove("privateKey");
        authConfig.remove("password");
        account.setAuthConfig(authConfig);
        return account;
    }
}
