package com.guandata.atlas.dao;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.Date;

@Data
@TableName("action_button")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ActionButtonEntity {
    @TableId(value = "id", type = IdType.AUTO)
    private int id;

    @TableField(value = "name")
    private String name;

    @TableField(value = "action_type")
    private int actionType;

    @TableField(value = "pg_id")
    private String pgId;

    @TableField(value = "button_setting")
    private String buttonSetting;

    @TableField(value = "target_setting")
    private String targetSetting;

    @TableField(value = "create_time")
    private Date createTime;

    @TableField(value = "update_time")
    private Date updateTime;

    @TableField(value = "creator_id")
    private String creatorId;

    @TableField(value = "editor_id")
    private String editorId;

    @TableField(value = "is_del")
    private boolean isDel;

    @TableField(exist = false)
    private WorkFlowEntity latestState;
}
