package com.guandata.atlas.dao;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("admin_module")
public class AdminModule {
    @TableId(value = "id", type = IdType.AUTO)
    private int id;

    @TableField(value = "name")
    private String name;

    @TableField(value = "alias")
    private String alias;

    @TableField(value = "type")
    private String type;

    @TableField(value = "enabled")
    private boolean enabled;
}
