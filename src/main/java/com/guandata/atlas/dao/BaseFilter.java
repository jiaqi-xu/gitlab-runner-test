package com.guandata.atlas.dao;

import com.alibaba.fastjson.JSON;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.guandata.atlas.dto.dataset.DSColumn;
import com.guandata.atlas.service.planningtable.model.filter.entity.Filter;
import com.guandata.atlas.service.planningtable.model.filter.entity.ElementFilter;
import com.guandata.atlas.service.planningtable.model.filter.valueobj.FilterExt;
import lombok.Data;

import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BaseFilter {
    private String fdId;
    private DSColumn column;
    private String alias;
    private Integer seqNo;
    private List<String> choice;
    /** search parameter **/
    private String search;
    /** null if its a page filter **/
    private String cdId;
     /** null if its a card filter **/
     private String pgId;

     private String type;

     private String ext;

     private String aggrType;

    public BaseFilter() {
    }

    public BaseFilter(BaseFilter f) {
        this.fdId = f.getFdId();
        this.column = f.getColumn();
        this.alias = f.alias;
        this.seqNo = f.seqNo;
        this.choice = f.choice;
        this.cdId = f.getCdId();
        this.pgId = f.getPgId();
        this.aggrType = f.getAggrType();
        this.type = f.getType();
        this.ext = f.getExt();
    }

    public BaseFilter(Filter filter) {
        this.column = filter.getDsColumn();
        this.seqNo = filter.getSeqNo();
        this.alias = filter.getAliasName();
        this.cdId = filter.getCdId();
        this.pgId = filter.getPageId();
        this.fdId = filter.getFdId();
        this.type = filter.getSelectType();
        if (filter.getAggrTypeEnum() != null) {
            this.aggrType = filter.getAggrTypeEnum().getType();
        }
        FilterExt filterExt = new FilterExt();
        filterExt.setSubType(filter.getSubType());
        filterExt.setDefaultType(filter.getDefaultType().getCode());
        filterExt.setDefaultValues(filter.getDefaultValues());
        if (filter instanceof ElementFilter) {
            ElementFilter elementFilter = (ElementFilter) filter;
            filterExt.setShowAll(elementFilter.getOptionalDP().getShowAll());
            filterExt.setSortType(elementFilter.getOptionalDP().getSortTypeEnum().getCode());
        }

        this.ext = JSON.toJSONString(filterExt);
    }
}
