package com.guandata.atlas.dao;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.guandata.atlas.common.utils.UuidUtil;
import com.guandata.atlas.service.planningtable.model.card.entity.Card;
import lombok.Data;
import org.jooq.tools.StringUtils;

import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CardInfoDO {
    /** card Id **/
    private String cdId;
    /** card name **/
    private String name;
    /** page Id **/
    private String pgId;
    /** card fields **/
    private List<FieldInfoDO> fields;
    /** filter Info **/
    private List<BaseFilter> cdFilters;
    /** card sequence **/
    private Integer seqNo;

    private int isDel;

    public CardInfoDO(){

    }
    public CardInfoDO(Card card) {

        this.cdId = card.getCdId();
        if (StringUtils.isBlank(this.cdId)) {
            this.cdId = "CD_"+ UuidUtil.getUUID();
        }
        this.name = card.getName();
        this.pgId = card.getPage().getPgId();
        this.seqNo = card.getSeqNo();
        this.isDel = 0;
    }
}
