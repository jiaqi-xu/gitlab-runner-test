package com.guandata.atlas.dao;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.guandata.atlas.dto.DataSetDTO;
import com.guandata.atlas.dto.dataset.DSColumn;
import com.guandata.atlas.dto.dataset.DSExt;
import lombok.Data;

import java.util.Date;

@Data
@TableName("dataset")
public class DataSetDAO {
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @TableField(value = "name")
    private String name;

    @TableField(value = "source_type")
    private String sourceType;

    @TableField(value = "source_ds")
    private String sourceDS;

    @TableField(value = "table_name")
    private String tableName;

    @TableField(value = "definition")
    private String definition;

    @TableField(value = "ext")
    private String ext;

    @TableField(value = "create_time")
    private Date createTime;

    @TableField(value = "update_time")
    private Date updateTime;

    @TableField(value = "editor_id")
    private String editorId;

    @TableField(value = "creator_id")
    private String creatorId;

    public DataSetDTO toDataSetDTO() {
        DataSetDTO dataSetDTO = new DataSetDTO();
        dataSetDTO.setId(this.id);
        dataSetDTO.setName(this.name);
        dataSetDTO.setSourceType(this.sourceType);
        dataSetDTO.setSourceDS(JSON.parseObject(this.sourceDS));
        dataSetDTO.setTableName(this.tableName);
        dataSetDTO.setDefinition(JSON.parseArray(this.definition, DSColumn.class));
        dataSetDTO.setExt(JSON.parseObject(this.ext, DSExt.class));
        dataSetDTO.setUpdateTime(this.updateTime);

        return dataSetDTO;
    }
}
