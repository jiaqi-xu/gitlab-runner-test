package com.guandata.atlas.dao;

import com.alibaba.fastjson.JSON;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.guandata.atlas.dto.dataset.DSColumn;
import com.guandata.atlas.service.planningtable.model.field.entity.BaseField;
import com.guandata.atlas.service.planningtable.model.field.entity.DimField;
import com.guandata.atlas.service.planningtable.model.field.entity.Field;
import com.guandata.atlas.service.planningtable.model.page.entity.Page;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FieldInfoDO {
    /** 字段在PT中的Id **/
    private String fdId;

    //TODO: DELETE FOLLOWING PROPERTY
    private String columnId;

    /** 数据集字段 **/
    private DSColumn column;

    /** 字段别名 **/
    private String alias;
    /** 字段在Planning Table的类型，可以是TEXT或者METRIC **/
    private String metaType;
    /** 字段在表格字段中的属性，可以是DIM或者METRIC **/
    private String fdProperty;
    /** 用来标识是否是聚合主键，用于group by **/
    private Boolean isAggregated;
    /** 聚合计算因子，e.g. SUM, MAX, AVG **/
    private String aggrType;
    /** 是否禁用筛选 **/
    private Boolean allowFiltrated;
    /** 是否启用排序 **/
    private Boolean allowSorted;
    /** 是否启用编辑 **/
    private Boolean allowEdited;
    /** 数据格式 **/
    private String dataFormat;
    /** 关联分析配置 **/
    private String correlationAnalysis;

    /** 在planning table result中是否为默认展示的field **/
    private Boolean inDefaultView;
    /** 所属card的Id **/
    private String cdId;
    /** 当保存初始可选择字段时会关联pageId **/
    private String pgId;
    /** 在表格字段中的顺序 **/
    private Integer seqNo;

    private String ext;

    public FieldInfoDO(BaseField baseField, Page page) {
        this.pgId = page.getPgId();
        this.fdId = baseField.getFdId();
        this.columnId = baseField.getColumn().getColumnId();
        this.metaType = baseField.getMetaType().getType();
        this.seqNo = baseField.getSeqNo();
        this.column = baseField.getColumn();
    }

    public FieldInfoDO() {
    }

    public FieldInfoDO(Field field) {
        this.fdId = field.getFdId();
        this.columnId = field.getColumnId();
        this.column = field.getColumn();
        this.alias = field.getAlias();
        this.metaType = field.getMetaType().getType();
        this.fdProperty = field.getFdProperty();
        this.isAggregated = field instanceof DimField;
        this.aggrType = field.getAggType();
        this.allowEdited = field.getAllowEdit();
        this.allowFiltrated = field.getAllowFilter();
        this.allowSorted = field.getAllowSort();
        this.inDefaultView = field.getInDefaultView();
        this.cdId = field.getCard().getCdId();
        this.seqNo = field.getSeqNo();
        this.dataFormat = field.getDataFormat();
        this.correlationAnalysis = JSON.toJSONString(field.getCorrelationAnalysis());
        this.ext = JSON.toJSONString(field.getFieldExt());
    }
}
