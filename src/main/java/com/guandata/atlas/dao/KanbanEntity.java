package com.guandata.atlas.dao;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.guandata.atlas.dto.Kanban;
import lombok.Data;

import java.util.Date;

@Data
@TableName("kanban")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class KanbanEntity {
    @TableId(value = "id", type = IdType.AUTO)
    private long id;

    @TableField(value = "object_name")
    private String objectName;

    @TableField(value = "account_id")
    private int accountId;

    @TableField(value = "object_type")
    private String objectType;

    @TableField(value = "object_id")
    private String objectId;

    @TableField(value = "seq_no")
    private int seqNo;

    @TableField(value = "editor_id")
    private String editorId;

    @TableField(value = "create_time")
    private Date createTime;

    @TableField(value = "update_time")
    private Date updateTime;

    @TableField(value = "is_del")
    private boolean isDel;

    public Kanban toKanban() {
        Kanban kanban = new Kanban();
        kanban.setId(this.id);
        kanban.setObjectName(this.objectName);
        kanban.setObjectType(this.objectType);
        kanban.setObjectId(this.objectId);
        kanban.setSeqNo(this.seqNo);
        return kanban;
    }
}
