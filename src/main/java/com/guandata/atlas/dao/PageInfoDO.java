package com.guandata.atlas.dao;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.guandata.atlas.service.planningtable.model.page.entity.Page;
import lombok.Data;

import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PageInfoDO {
    /** page Id **/
    private String pgId;
    /** page name **/
    private String name;
    /** dataset type id related to page **/
    private Integer dsId;
    /** page status **/
    private Integer status;
    /** cards Info **/
    private List<CardInfoDO> cards;
    /** filter Info **/
    private List<BaseFilter> pgFilters;
    /** page initial field list **/
    private List<FieldInfoDO> initialFieldList;

    private String tableName;

    public PageInfoDO(Page page) {
        this.pgId = page.getPgId();
        this.name = page.getName();
        this.dsId = page.getDataSet().getId();
        this.status = page.getPageStatusEnum().getStatus();
    }

    public PageInfoDO(){

    }
}
