package com.guandata.atlas.dao;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.guandata.atlas.enums.TaskStatusEnum;
import com.guandata.atlas.enums.TaskTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;

@Data
@AllArgsConstructor
@TableName("task")
public class TaskDAO {
    @TableId(value = "id", type = IdType.AUTO)
    private long id;

    @TableField(value = "task_id")
    private String taskId;

    @TableField(value = "task_type")
    private String taskType;

    @TableField(value = "object_id")
    private String objectId;

    @TableField(value = "status")
    private Integer status;

    private String statusName;

    @TableField(value = "failure_cause")
    private String failureCause;

    @TableField(value = "start_time")
    private Date startTime;

    @TableField(value = "end_time")
    private Date endTime;

    @TableField(value = "creator_id")
    private String creatorId;

    public TaskDAO() {

    }

    public TaskDAO(String taskId, TaskStatusEnum status, String failureCause, Date endTime) {
        this.taskId = taskId;
        this.status = status.getStatus();
        this.failureCause = failureCause;
        this.endTime = endTime;
    }

    public TaskDAO(String taskId, TaskTypeEnum taskType, String objectId, TaskStatusEnum status, String userId) {
        this.taskId = taskId;
        this.taskType = taskType.getType();
        this.objectId = objectId;
        this.status = status.getStatus();
        this.creatorId = userId;
    }

}
