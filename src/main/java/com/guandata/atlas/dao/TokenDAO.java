package com.guandata.atlas.dao;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

@Data
@TableName("token")
public class TokenDAO {
    @TableId(value = "id", type = IdType.AUTO)
    private int id;

    @TableField(value = "object_type")
    private String objectType;

    @TableField(value = "object_id")
    private String objectId;

    @TableField(value = "access_token")
    private String accessToken;

    @TableField(value = "effective_time")
    private Date effectiveTime;

    @TableField(value = "expired_time")
    private Date expiredTime;

    public TokenDAO(String objectType, String objectId, String accessToken) {
        this.objectType = objectType;
        this.objectId = objectId;
        this.accessToken = accessToken;
    }
}
