package com.guandata.atlas.dao;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.Date;

@Data
@TableName("workflow")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class WorkFlowEntity {
    private String taskId;

    private int buttonId;

    private int state;

    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date startTime;

    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date endTime;

    private String editorId;

    public WorkFlowEntity() {
    }
}
