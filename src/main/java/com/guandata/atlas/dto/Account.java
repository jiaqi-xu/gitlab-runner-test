package com.guandata.atlas.dto;


import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.guandata.atlas.dao.AccountEntity;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Account {
    private int id;

    private String name;

    private String cnType;

    private JSONObject authConfig;

    public AccountEntity toAccountEntity() {
        AccountEntity entity = new AccountEntity();
        entity.setId(this.id);
        entity.setName(this.name);
        entity.setCnType(this.cnType);
        entity.setAuthConfig(this.authConfig.toJSONString());
        return entity;
    }
}
