package com.guandata.atlas.dto;

import com.alibaba.fastjson.JSONObject;
import com.guandata.atlas.dao.ActionButtonEntity;
import lombok.Data;

@Data
public class ActionButton {
    private int id;

    private String name;

    private int actionType;

    private String pgId;

    private JSONObject buttonSetting;

    private JSONObject targetSetting;

    private boolean isDel;

    public ActionButtonEntity toActionButtonEntity() {
        ActionButtonEntity entity = new ActionButtonEntity();
        entity.setId(this.id);
        entity.setName(this.name);
        entity.setActionType(this.actionType);
        entity.setPgId(this.pgId);
        entity.setButtonSetting(this.buttonSetting.toJSONString());
        entity.setTargetSetting(this.targetSetting.toJSONString());
        entity.setDel(this.isDel);
        return entity;
    }
}
