package com.guandata.atlas.dto;

import com.guandata.atlas.dto.dataset.DSColumn;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class BaseDSDTO {
    private Integer id;

    private String name;

    private Integer rowCount;

    private Integer colCount;

    private Date createTime;

    private Date updateTime;

    private List<DSColumn> fields;
}
