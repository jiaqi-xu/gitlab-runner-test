package com.guandata.atlas.dto;

import com.alibaba.fastjson.annotation.JSONField;
import com.guandata.atlas.service.planningtable.enums.BatchModifyTypeEnum;
import com.guandata.atlas.service.planningtable.model.card.entity.Card;
import lombok.Data;
import org.jooq.tools.StringUtils;
import org.springframework.security.core.Authentication;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class BatchUpdateDataDTO {

    /** 修改参数 **/
    private List<ResultCondition> conditions;

    @NotNull(message = "dataset id不能为空")
    private Integer dsId;

    @NotBlank(message = "card id不能为空")
    private String cdId;

    @NotBlank(message = "field id不能为空")
    private String fdId;

    @NotNull(message = "type不能为空")
    private Integer type;

    @NotBlank(message = "value不能为空")
    private String value;

    private String taskId;

    @JSONField(serialize = false, deserialize = false)
    private Authentication authentication;

    private String tableName;

    @JSONField(serialize = false, deserialize = false)
    private Card card;

    public boolean checkParams() {

        if (StringUtils.isBlank(cdId) || StringUtils.isBlank(fdId) || type == null || StringUtils.isBlank(value)) {
            return false;
        }

        BatchModifyTypeEnum modifyType = BatchModifyTypeEnum.getModifyType(type);
        if (modifyType == null) {
            return false;
        }

        return true;
    }
}
