package com.guandata.atlas.dto;

import com.guandata.atlas.dto.dataset.DSColumn;
import lombok.Data;

import java.util.List;

@Data
public class DSDataPreviewDTO {
    private List<DSColumn> columns;
    private List<List<Object>> preview;
    private Boolean hasMore;

    public DSDataPreviewDTO() {

    }

    public DSDataPreviewDTO(List<DSColumn> columns, List<List<Object>> preview) {
        this.columns = columns;
        this.preview = preview;
    }
}
