package com.guandata.atlas.dto;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.guandata.atlas.dao.DataSetDAO;
import com.guandata.atlas.dto.dataset.DSColumn;
import com.guandata.atlas.dto.dataset.DSExt;
import com.guandata.atlas.service.dataset.model.DataSet;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DataSetDTO {
    /** 数据源类型Id **/
    private Integer id;
    /** 数据源展示名称 **/
    private String name;
    /** 数据集来源 **/
    private String sourceType;
    /** 数据集来源信息 **/
    private JSONObject sourceDS;
    /** 数据源所对应的数据表 **/
    private String tableName;
    /** 数据集定义 **/
    private List<DSColumn> definition;
    /** 数据集额外信息 **/
    private DSExt ext;

    /** 数据集更新时间 **/
    private Date updateTime;

    /** 数据同步public api **/
    private String dataSyncOpenAPI;
    /** 回流SQL **/
    private String backFlowSQL;

    public DataSetDAO toDataSet() {
        DataSetDAO dataSetDAO = new DataSetDAO();
        dataSetDAO.setId(this.id);
        dataSetDAO.setName(this.name);
        dataSetDAO.setSourceType(this.sourceType);
        dataSetDAO.setSourceDS(this.sourceDS != null? JSON.toJSONString(this.sourceDS): null);
        dataSetDAO.setTableName(this.tableName);
        dataSetDAO.setDefinition(this.definition != null? JSON.toJSONString(this.definition): null);
        dataSetDAO.setExt(this.ext !=null? JSON.toJSONString(this.ext): null);

        return dataSetDAO;
    }

    public DataSetDTO() {}

    public DataSetDTO(DataSet dataSet) {
        this.id = dataSet.getId();
        this.name = dataSet.getName();
        this.tableName = dataSet.getTableName();
        this.sourceType = dataSet.getSourceType().getSourceType();
        this.sourceDS = dataSet.getSourceDS();
        this.definition = dataSet.getColumnListDP().getColumnList();
        this.ext = dataSet.getExt();
        this.updateTime = dataSet.getUpdateTime();
        this.dataSyncOpenAPI = dataSet.getDataSyncOpenAPI();
        this.backFlowSQL = dataSet.getBackFlowSQL();
    }
}
