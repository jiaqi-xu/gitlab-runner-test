package com.guandata.atlas.dto;

import com.guandata.atlas.dto.dataset.DSColumn;
import lombok.Data;

import java.util.List;

@Data
public class DataUpdateParam {
    /**  聚合主键  **/
    private List<DSColumn> aggrPK;
    /** 修改参数 **/
    private List<ResultCondition> conditions;
    /**  被更改的字段名 **/
    private DSColumn field;
    /**  旧值  **/
    private String modifiedFrom;
    /**  新值  **/
    private String modifiedTo;
    /** Card Id **/
    private String cdId;

    private String tableName;
    /**
     * 需要替换的列
     */
    private DSColumn targetColumn;

    private String pgId;
}
