package com.guandata.atlas.dto;

import com.guandata.atlas.dto.dataset.DSColumn;
import lombok.Data;

import java.util.List;

@Data
public class DataUpdateParamExt extends DataUpdateParam{

    /** 修改参数 **/
    private List<List<ResultCondition>> groupByConditions;

    private List<String> modifiedFroms;

    private List<String> modifiedTos;

    private List<DSColumn> dsColumns;
}
