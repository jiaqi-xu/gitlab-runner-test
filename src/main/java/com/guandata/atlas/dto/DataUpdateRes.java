package com.guandata.atlas.dto;

import com.guandata.atlas.dto.dataset.DSColumn;
import lombok.Data;

@Data
public class DataUpdateRes {
    /** 修改对应的相关字段 **/
    private DSColumn field;
    /** 修改后字段的值 **/
    private Object value;

    public DataUpdateRes(DSColumn field, Object value) {
        this.field = field;
        this.value = value;
    }
}
