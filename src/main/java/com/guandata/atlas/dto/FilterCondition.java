package com.guandata.atlas.dto;

import lombok.Data;

import java.util.List;

@Data
public class FilterCondition {
    /** 结果条件 **/
    private String name;
    /** 结果条件值 **/
    private List<Object> values;
    /** 筛选类型 **/
    private String filterType;
    /** 数据类型 **/
    private String metaType;
}
