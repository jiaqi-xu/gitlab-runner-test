package com.guandata.atlas.dto;

import com.guandata.atlas.service.resource.dto.ResourceDTO;
import lombok.Data;

import java.util.List;

@Data
public class GrantResource {
    private ResourceDTO resource;

    private List<String> userIdList;

    private List<String> groupIdList;
}
