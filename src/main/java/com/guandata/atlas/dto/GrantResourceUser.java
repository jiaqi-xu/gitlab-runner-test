package com.guandata.atlas.dto;

import com.guandata.atlas.service.resource.dto.ResourceDTO;
import lombok.Data;

import java.util.List;

@Data
public class GrantResourceUser {
    private ResourceDTO resource;

    private List<String> uIdList;
}
