package com.guandata.atlas.dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class InitPagesDTO {

    private List<Integer> dsIdList;

    public InitPagesDTO(Integer dsId) {

        this.dsIdList = new ArrayList(){
            {
                add(dsId);
            }
        };
    }
}
