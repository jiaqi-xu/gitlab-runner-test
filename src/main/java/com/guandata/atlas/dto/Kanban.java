package com.guandata.atlas.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Kanban {
    private long id;

    private String objectName;

    private Account account;

    private String objectType;

    private String objectId;

    private int seqNo;
}
