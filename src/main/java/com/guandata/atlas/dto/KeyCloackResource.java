package com.guandata.atlas.dto;

import lombok.Data;

@Data
public class KeyCloackResource {
   private String realm;

   private String resource;

   private String authServerUrl;

   private String sslRequired;

   private Boolean publicClient;

   private Boolean useResourceRoleMappings;

   public KeyCloackResource(String realm, String resource, String authServerUrl, String sslRequired,
                            Boolean publicClient, Boolean useResourceRoleMappings) {
       this.realm = realm;
       this.resource = resource;
       this.authServerUrl = authServerUrl;
       this.sslRequired = sslRequired;
       this.publicClient = publicClient;
       this.useResourceRoleMappings = useResourceRoleMappings;
   }


}
