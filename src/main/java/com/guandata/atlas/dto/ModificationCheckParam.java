package com.guandata.atlas.dto;

import lombok.Data;

import java.util.List;

@Data
public class ModificationCheckParam {
    /** 修改行为筛选条件 **/
    private List<FilterCondition> actionFilter;
    /** 修改明细筛选条件 **/
    private List<FilterCondition> detailFilter;
}
