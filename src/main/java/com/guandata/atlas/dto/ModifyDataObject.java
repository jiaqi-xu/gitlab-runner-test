package com.guandata.atlas.dto;

import com.guandata.atlas.dto.dataset.DSColumn;
import lombok.Data;

import java.util.List;

@Data
public class ModifyDataObject {
    /**  聚合主键  **/
    private List<DSColumn> aggrPK;
    /** 修改参数 **/
    private List<ResultCondition> objects;

    public ModifyDataObject(List<DSColumn> aggrPK, List<ResultCondition> objects) {
        this.aggrPK = aggrPK;
        this.objects = objects;
    }
}
