package com.guandata.atlas.dto;

import com.guandata.atlas.dao.FieldInfoDO;
import com.guandata.atlas.pojo.FieldInfo;
import lombok.Data;

@Data
public class OrderBy {
    private FieldInfo fieldInfo;
    private String orderType;
}
