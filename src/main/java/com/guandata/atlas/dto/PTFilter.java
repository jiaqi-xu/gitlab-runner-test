package com.guandata.atlas.dto;

import com.guandata.atlas.dao.BaseFilter;
import lombok.Data;

@Data
public class PTFilter extends BaseFilter {
    /** pagination total count **/
    private Long total;
    /** pagination size **/
    private Integer size;
    /** pagination current **/
    private Long current;
    /** pagination pages **/
    private Long pages;

    public PTFilter(BaseFilter f, Long total, int size, Long current, Long pages) {
        super(f);
        this.total = total;
        this.size = size;
        this.current = current;
        this.pages = pages;
    }
}
