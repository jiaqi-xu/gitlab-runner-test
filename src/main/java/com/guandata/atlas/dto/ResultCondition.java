package com.guandata.atlas.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.guandata.atlas.dto.dataset.DSColumn;
import com.guandata.atlas.pojo.FilterInfo;
import com.guandata.atlas.pojo.PTFilterInfo;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResultCondition {
    /** 筛选器字段 **/
    private DSColumn column;
    /** 结果条件值 **/
    private List<Object> values;

    private FilterInfo filterInfo;

    public ResultCondition(){
    }

    public ResultCondition(PTFilter ptFilter) {

        this.column = ptFilter.getColumn();
        if (!CollectionUtils.isEmpty(ptFilter.getChoice())) {
            this.values = new ArrayList(){
                {
                    add(ptFilter.getChoice().get(0));
                }
            };
        }
    }

    public ResultCondition(PTFilterInfo ptFilter) {

        this.column = ptFilter.getColumn();
        if (!CollectionUtils.isEmpty(ptFilter.getChoice())) {
            this.values = new ArrayList(){
                {
                    add(ptFilter.getChoice().get(0));
                }
            };
        }
    }

    public void reBuildCondition() {
        if (filterInfo == null) {
            filterInfo = new FilterInfo();
        }
        filterInfo.setColumn(column);
        filterInfo.setChoice(values.stream().map(s->{
            return s == null ? null : String.valueOf(s);
        }).collect(Collectors.toList()));
    }

    public boolean isAllBlank() {
        if (CollectionUtils.isEmpty(values)) {
            return true;
        }

        for (Object value : values) {
            if (value != null && StringUtils.isNotBlank(String.valueOf(value))) {
                return false;
            }
        }

        return true;
    }
}
