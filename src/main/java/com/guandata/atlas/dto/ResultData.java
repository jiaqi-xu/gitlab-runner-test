package com.guandata.atlas.dto;

import lombok.Data;

@Data
public class ResultData {
    /** 数据值 **/
    private String v;

    private int m;

    public ResultData(String v) {
        this.v = v;
    }
}
