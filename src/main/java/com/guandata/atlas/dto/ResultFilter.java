package com.guandata.atlas.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.guandata.atlas.dao.BaseFilter;
import lombok.Data;
import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResultFilter {
    /** filter base structure **/
    private List<BaseFilter> filters;
    /** page Id **/
    private String pdId;
    /** card Id; page filter if null **/
    private String cdId;
    /** card filter can be affected by page filter **/
    private List<ResultCondition> conditions;
    /** related dataset **/
    private Integer dsId;

    public ResultFilter(){

    }
}
