package com.guandata.atlas.dto;

import lombok.Data;
import java.util.List;

@Data
public class SqlParamDTO {
    private List<String> select;
    private Integer limit;

    //TODO: Add where, groupBy if needed.

    public SqlParamDTO(List<String> select, Integer limit) {
        this.select = select;
        this.limit = limit;
    }
}
