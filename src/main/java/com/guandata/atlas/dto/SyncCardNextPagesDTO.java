package com.guandata.atlas.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class SyncCardNextPagesDTO {

    private Integer dsId;

    private String cardId;

    private TableFilter tableFilter;

    private Long pageNo;

    private Long pageSize;

}
