package com.guandata.atlas.dto;

import lombok.Data;

@Data
public class SyncDataSetDataDTO {
    private DataSetDTO dataSetDTO;
    private String taskId;
    private UserInfo user;

    public SyncDataSetDataDTO(DataSetDTO dataSetDTO, String taskId, UserInfo user) {
        this.dataSetDTO = dataSetDTO;
        this.taskId = taskId;
        this.user = user;
    }
}
