package com.guandata.atlas.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.common.collect.Lists;
import com.guandata.atlas.pojo.FieldInfo;
import com.guandata.atlas.pojo.FilterInfo;
import com.guandata.atlas.pojo.PTFilterInfo;
import lombok.Data;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TableFilter {
    /** 对应planning table 表格字段模块中的字段 **/
    private List<FieldInfo> fieldsInfo;
    /** 内外筛选器组成的条件 **/
    private List<ResultCondition> conditions;
    /** order by **/
    private List<OrderBy> orderBys;
    /** 数据源类型Id **/
    private Integer dsId;
    /** card Id **/
    private String cdId;

    private String pgId;

    public TableFilter(){
    }

    public TableFilter(PTFilter ptFilter, List<FieldInfo> fieldsInfo, Integer dsId, String cdId) {

        this.dsId = dsId;
        this.fieldsInfo = fieldsInfo;
        this.cdId = cdId;
        this.conditions = new ArrayList() {
            {
                add(new ResultCondition(ptFilter));
            }
        };
    }

    public TableFilter(PTFilterInfo ptFilter, List<FieldInfo> fieldsInfo, Integer dsId, String cdId) {

        this.dsId = dsId;
        this.fieldsInfo = fieldsInfo;
        this.cdId = cdId;
        this.conditions = new ArrayList() {
            {
                add(new ResultCondition(ptFilter));
            }
        };
    }

    public void reBuildConditions(){
        if (CollectionUtils.isEmpty(conditions)) {
            return;
        }
        conditions.stream().forEach(s->s.reBuildCondition());
    }

    public List<FilterInfo> getFiltersInfo() {

        if (CollectionUtils.isEmpty(conditions)) {
            return Lists.newArrayList();
        }

        return conditions.stream().map(s->s.getFilterInfo()).filter(Objects::nonNull).collect(Collectors.toList());
    }
}
