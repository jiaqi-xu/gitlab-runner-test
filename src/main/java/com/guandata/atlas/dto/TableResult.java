package com.guandata.atlas.dto;

import com.guandata.atlas.dto.dataset.DSColumn;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class TableResult {
    /** 数据结果的字段 **/
    private List<DSColumn> columns;
    /** 数据结果中的数据信息 **/
    private List<List<ResultData>> resultData;
    /** total count **/
    private Long total;
    /** size **/
    private Integer size;
    /** current **/
    private Long current;
    /** pages **/
    private Long pages;

    public TableResult() {}

    public TableResult(List<DSColumn> columns, ArrayList<List<ResultData>> resultData) {
        this.columns = columns;
        this.resultData = resultData;
    }

    public TableResult(List<DSColumn> columns, List<List<ResultData>> resultData,
                       Long total, Integer size, Long current, Long pages) {
        this.columns = columns;
        this.resultData = resultData;
        this.total = total;
        this.size = size;
        this.current = current;
        this.pages = pages;
    }
}
