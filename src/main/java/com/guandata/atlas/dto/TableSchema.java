package com.guandata.atlas.dto;

import com.guandata.atlas.dao.FieldInfoDO;
import lombok.Data;

import java.util.Date;
import java.util.List;


@Data
public class TableSchema {
    /** 数据源类型名称 **/
    private String dataSetName;
    /** 数据表中的字段；对应planning table中左侧可供选取的字段 **/
    private List<FieldInfoDO> columns;
    /** 数据表中字段个数 **/
    private Integer columnSize;
    /** 数据表中数据行数 **/
    private Integer rowSize;
    /** 数据表最近更新时间 **/
    private Date lastUpdate;

    public TableSchema(String dsName, List<FieldInfoDO> columns,
                          Integer columnSize, Integer rowSize, Date lastUpdate) {
        this.dataSetName = dsName;
        this.columns = columns;
        this.columnSize = columnSize;
        this.rowSize = rowSize;
        this.lastUpdate = lastUpdate;
    }
}
