package com.guandata.atlas.dto;

import lombok.Data;
import org.keycloak.KeycloakPrincipal;
import org.keycloak.adapters.springsecurity.account.SimpleKeycloakAccount;
import org.keycloak.representations.AccessToken;
import org.springframework.security.core.Authentication;

import java.util.Set;

@Data
public class UserInfo {
    /** userId in keycloak **/
    private String userId;
    /** user name **/
    private String userName;
    /** user role **/
    private Set roles;

    //TODO: basic properties

    public UserInfo() {}

    /**
     * get UserInfo by Authentication of Spring Security Context
     * @param auth
     */
    public UserInfo(Authentication auth) {
        KeycloakPrincipal principal = (KeycloakPrincipal) auth.getPrincipal();
        AccessToken token = principal.getKeycloakSecurityContext().getToken();
        this.userId = token.getSubject();
        this.userName = token.getName();
        this.roles = ((SimpleKeycloakAccount)auth.getDetails()).getRoles();
    }
}
