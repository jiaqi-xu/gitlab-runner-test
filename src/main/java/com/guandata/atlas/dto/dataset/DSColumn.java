package com.guandata.atlas.dto.dataset;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DSColumn {
    /** 字段在数据集表中的Id **/
    private String columnId;
    /** 字段在数据集中的名称 **/
    private String name;
    /** 字段在数据集中的类型 **/
    private String type;

    /** 字段在源数据集中的名字 **/
    private String originName;
    private String comment;
    private Integer seqNo;

    public DSColumn() {}

    public DSColumn(String columnId, String name, String columnType) {
        this.columnId = columnId;
        this.name = name;
        this.type = columnType;
    }

    public DSColumn(String columnId, String name, String columnType, Integer seqNo) {
        this.columnId = columnId;
        this.name = name;
        this.type = columnType;
        this.seqNo = seqNo;
    }
}
