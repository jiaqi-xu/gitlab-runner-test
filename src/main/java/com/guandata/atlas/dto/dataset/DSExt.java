package com.guandata.atlas.dto.dataset;

import lombok.Data;

@Data
public class DSExt {
    private Integer rowCount;
    private Integer colCount;

    //TODO: add here for more extra properties
    public DSExt(Integer rowCount, Integer colCount) {
        this.rowCount = rowCount;
        this.colCount = colCount;
    }
}
