package com.guandata.atlas.dto.galaxy;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

@Data
public class Selector {
    @JSONField(alternateNames = {"cdId"})
    private String id;
    private String name;
    private String originName;
}
