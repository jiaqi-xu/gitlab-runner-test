package com.guandata.atlas.dto.universe;

import lombok.Data;

@Data
public class UniverseLoginInDTO {
    private String address;
    private String username;
    private String password;
}
