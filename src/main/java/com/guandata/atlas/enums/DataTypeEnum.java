package com.guandata.atlas.enums;

import com.guandata.atlas.exception.AtlasException;
import org.jooq.DataType;
import org.jooq.impl.SQLDataType;

import java.util.Arrays;
import java.util.List;

public enum DataTypeEnum {
    STRING("STRING", SQLDataType.VARCHAR(255)),
    INTEGER("INTEGER", SQLDataType.DECIMAL.precision(26, 6)),
    LONG("LONG", SQLDataType.DECIMAL.precision(26, 6)),
    DOUBLE("DOUBLE", SQLDataType.DECIMAL.precision(26, 6)),
    FLOAT("FLOAT", SQLDataType.DECIMAL.precision(26, 6)),
    TIMESTAMP("TIMESTAMP", SQLDataType.TIMESTAMP),
    DATE("DATE", SQLDataType.DATE),
    DATETIME("DATETIME", SQLDataType.TIMESTAMP),
    BOOLEAN("BOOLEAN", SQLDataType.TINYINT);

    private static final List<String> numericTypeList = Arrays.asList(
            INTEGER.getType(), LONG.getType(), FLOAT.getType(), DOUBLE.getType());

    private String type;

    private DataType sqlDataType;

    DataTypeEnum(String type, DataType sqlDataType) {
        this.type = type;
        this.sqlDataType = sqlDataType;
    }

    public String getType() {
        return type;
    }

    public DataType getSqlDataType() {
        return sqlDataType;
    }

    public static DataType getSQLDataType(String type) {
        if (type == null) {
            return null;
        }

        for(DataTypeEnum dataTypeEnum: DataTypeEnum.values()) {
            if (dataTypeEnum.type.equals(type)) {
                return dataTypeEnum.sqlDataType;
            }
        }

        return null;
    }

    public static Boolean isNumericType(String type) {
        if (getSQLDataType(type) == null) {
            throw new AtlasException(ErrorCode.NOT_SUPPORT_DS_DATA_TYPE);
        }
        return numericTypeList.contains(type);
    }
}
