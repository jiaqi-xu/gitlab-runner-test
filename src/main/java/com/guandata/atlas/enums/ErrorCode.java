package com.guandata.atlas.enums;

import lombok.Getter;

@Getter
public enum ErrorCode {
    UN_KNOW(-1, "不明确的错误信息"),

    PARAME_ERROR_CODE(10000, "参数有误"),

    EXPORT_CARD_IO_EXCEPTION(10001, "根据筛选器条件导出结果页全量数据到excel表IO异常"),

    NO_RESOURCE_PERMISSION(20001, "无资源访问权限"),

    PT_PAGE_NOT_EXSITED(30001, "页面不存在"),

    NOT_SUPPORT_FIELD_TYPE(30002, "不支持的字段类型（目前只支持数值字段的修改）"),

    TASK_BATCH_MODIFY_ERROR(30003, "planningtable批量更新失败"),

    TASK_BATCH_MODIFY_NOT_ALLOW_CONCURRENT(30004, "已有批量修改任务正在进行"),

    TASK_CONCURRENT_ERROR(30005, "任务并发错误:{0}"),

    /** Universe平台相关 **/
    UNIVERSE_INTERNAL_ERROR(40000, "Universe平台内部错误: {0}"),
    UNIVERSE_FAIL_TO_GET_PROJECTS(40001, "获取Universe项目列表失败: {0}"),
    UNIVERSE_FAIL_TO_GET_DATASETS(40002, "获取Universe数据集列表失败: {0}"),
    UNIVERSE_FAIL_TO_GET_DATASET_INFO(40003, "获取Universe数据集详情失败: {0}"),
    UNIVERSE_FAIL_TO_GET_DATASET_DATA(40004, "获取Universe数据集数据失败: {0}"),
    UNIVERSE_SERVICE_UNAVAILABLE(40005, "Universe服务不可用"),

    /** 用户相关 **/
    FAIL_TO_CREATE_USER(1000, "创建用户失败: {0}"),
    FAIL_TO_DELETE_USER(1001, "删除用户失败"),
    FAIL_TO_UPDATE_USER(1002, "更新用户失败: {0}"),
    FAIL_TO_RESET_PASSWORD(1003, "更新密码失败，请确认权限"),
    FAIL_TO_GET_USER_INFO(1004, "查看用户信息失败，请确认权限"),

    FAIL_TO_CREATE_USER_GROUP(1008, "创建用户组失败: {0}"),

    /*** KeyCloak **/
    KEYCLOAK_SERVER_NOT_AVAILABLE(2000, "Keycloak服务不可用"),
    KEYCLOAK_VALIDATE_USER_CREDENTIALS(2001, "验证用户身份失败: {0}"),



    /** Atlas数据集 **/
    FAIL_TO_CREATE_ATLAS_DATASET(70001, "创建数据集失败: {0}"),

    NOT_SUPPORT_OPERATOR_TYPE(70002,"不支持的批量操作操作类型"),
    NOT_SUPPORT_ELEMENT_TYPE(70003,"不支持的选择类型"),
    NOT_SUPPORT_SORT_TYPE(70004,"不支持的排序类型"),
    NOT_SUPPORT_DEFAULT_TYPE(70005,"不支持的默认类型"),
    ATLAS_DATASET_NOT_EXISTED(70006, "数据集不存在"),
    PAGE_NAME_DUPLICATE(70007, "页面名称重复"),
    FAIL_TO_DELETE_KANBAN(70008, "删除看板资源失败: {0}"),
    NOT_SUPPORT_DS_DATA_TYPE(70009,"不支持的数据集字段类型"),

    /** BI平台相关 **/
    GALAXY_INTERNAL_ERROR(80000, "BI平台内部错误: {0}"),
    GALAXY_FAIL_TO_GET_PAGE(80002, "获取BI页面失败: {0}"),

    /*** 数据同步 **/
    DATA_SYNC_FAIL_TO_INSERT_MODIFY_RECORD(50001, "插入数据同步记录至表{0}失败"),
    VERSION_PK_CONTAINS_NULL(50002, "版本主键存在null值"),
    TASK_DATA_SYNC_NOT_ALLOW_CONCURRENT(50003, "已有数据同步任务正在进行"),

    /*** 无public api权限 ***/
    PUBLIC_API_NO_PERMISSION(60001, "PUBLIC-API调用失败，请检查相关权限")
    ;




    private Integer code;

    private String msg;

    ErrorCode(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
