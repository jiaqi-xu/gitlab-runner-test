package com.guandata.atlas.enums;

import lombok.Getter;

@Getter
public enum TaskStatusEnum {
    INIT(0, "INIT", "任务处于初始状态"),
    RUNNING(1, "RUNNING", "任务处于运行中"),
    FAILURE(2, "FAILURE", "任务运行失败"),
    SUCCESS(3, "SUCCESS", "任务运行成功"),
    TIME_OUT(4, "TIME_OUT", "超时"),
    ;

    private Integer status;

    private String statusName;

    private String desc;

    TaskStatusEnum(Integer status, String statusName,String desc) {
        this.status = status;
        this.statusName = statusName;
        this.desc = desc;
    }

    public static String getStatusName(Integer status) {
        if (status == null) {
            return null;
        }

        for(TaskStatusEnum taskStatusEnum: TaskStatusEnum.values()) {
            if (taskStatusEnum.status.equals(status)) {
                return taskStatusEnum.statusName;
            }
        }
        return null;
    }
}
