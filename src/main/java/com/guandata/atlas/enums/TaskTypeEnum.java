package com.guandata.atlas.enums;

import lombok.Getter;

@Getter
public enum TaskTypeEnum {
    SYNC_DATASET_DATA("SYNC_DATASET_DATA", "数据集数据同步任务"),

    BATCH_MODIFY_PT("BATCH_MODIFY_PT", "批量修改planningtable信息")
    ;

    private String type;

    private String desc;

    TaskTypeEnum(String type, String desc) {
        this.type = type;
        this.desc = desc;
    }
}
