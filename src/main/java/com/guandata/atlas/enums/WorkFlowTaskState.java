package com.guandata.atlas.enums;

public enum WorkFlowTaskState {
    /**
     * status：
     * 0 submit success
     * 1 running
     * 2 ready pause
     * 3 pause
     * 4 ready stop
     * 5 stop
     * 6 failure
     * 7 success
     * 8 need fault tolerance
     * 9 kill
     * 10 waiting thread
     * 11 waiting depend node complete
     * 12 validate failure
     * 13 submit failure
     * 14 not exist
     */
    SUBMITTED_SUCCESS,
    RUNNING_EXEUTION,
    READY_PAUSE,
    PAUSE,
    READY_STOP,
    STOP,
    FAILURE,
    SUCCESS,
    NEED_FAULT_TOLERANCE,
    KILL,
    WAITTING_THREAD,
    WAITTING_DEPEND,
    VALIDATE_FAILURE,
    SUBMITTED_FAILURE,
    NOT_EXIST
}
