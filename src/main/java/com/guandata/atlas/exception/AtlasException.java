package com.guandata.atlas.exception;

import com.guandata.atlas.enums.ErrorCode;

import java.text.MessageFormat;
import lombok.Getter;

@Getter
public class AtlasException extends RuntimeException {

    private Integer code;

    public AtlasException(){}

    public AtlasException(String msg){
        super(msg);
    }

    public AtlasException(Integer code, String msg){
        super(msg);
        this.code = code;
    }

    public AtlasException(ErrorCode errorCode){
        super(errorCode.getMsg());
        this.code = errorCode.getCode();
    }

    public AtlasException(ErrorCode errorCode, Object... obj) {
        super(MessageFormat.format(errorCode.getMsg(), obj));
        this.code = errorCode.getCode();
    }
}
