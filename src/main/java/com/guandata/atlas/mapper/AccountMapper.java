package com.guandata.atlas.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.guandata.atlas.dao.AccountEntity;

public interface AccountMapper extends BaseMapper<AccountEntity> {
}
