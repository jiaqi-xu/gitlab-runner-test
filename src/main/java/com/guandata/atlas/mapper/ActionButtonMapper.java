package com.guandata.atlas.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.guandata.atlas.dao.ActionButtonEntity;

public interface ActionButtonMapper extends BaseMapper<ActionButtonEntity> {
}
