package com.guandata.atlas.mapper;

import com.guandata.atlas.dao.CardInfoDO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CardMapper {
    CardInfoDO getCardById(String id);

    int insertCards(List<CardInfoDO> card);

    int deleteCards(String pgId, List<String> cdIdList);

    int deleteCardsByPgId(String pgId);

    int hardDeleteCardsByPgId(String pgId);

    String getPageIdByCard(String cardId);

    List<CardInfoDO> queryCardById(@Param("cdIdList") List<String> cdIdList);

    List<CardInfoDO> getCardByPages(@Param("pgIdList") List<String> pgIdList);

    CardInfoDO selectCardById(String id);
}
