package com.guandata.atlas.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.guandata.atlas.dao.AdminModule;

public interface DashboardMapper extends BaseMapper<AdminModule> {
}
