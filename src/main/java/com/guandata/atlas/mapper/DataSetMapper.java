package com.guandata.atlas.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.guandata.atlas.dao.DataSetDAO;
import com.guandata.atlas.dao.FieldInfoDO;
import com.guandata.atlas.dto.FilterCondition;
import com.guandata.atlas.dto.OrderBy;
import com.guandata.atlas.dto.ResultCondition;
import com.guandata.atlas.pojo.FieldInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface DataSetMapper extends BaseMapper<DataSetDAO> {
    List<DataSetDAO> list();

    DataSetDAO getById(Integer Id);

    DataSetDAO getByName(String name);

    Set<Long> getModifiedRows(List<ResultCondition> conditions, String tableName);

    List<Map<String, Object>> getModifyActions(Page<Map<String, Object>> page, @Param("filters") List<FilterCondition> modificationFilter,
                                               List<Long> modifyIdList, boolean hasDetailFilter, String dsTable);

    List<String> getColumns(String dbname, String dsTable);

    List<Map<String, Object>> getDetails(Page<Map<String, Object>> page, List<String> columns,
                                         @Param("filters") List<FilterCondition> detailFilter, Long modifyId, String dsTable);

    List<Map<String, Object>> getExceptionById(Long modifyId, String dsTable);

}
