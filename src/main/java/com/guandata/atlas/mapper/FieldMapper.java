package com.guandata.atlas.mapper;

import com.guandata.atlas.dao.FieldInfoDO;

import java.util.List;

public interface FieldMapper {
    int insertFields(List<FieldInfoDO> fields);

    int insertInitialFields(List<FieldInfoDO> fields);

    int deleteFields(String cdId, List<String> fdIdList);

    int deleteFieldsByCdId(String cdId);

    int deleteInitialFieldListByPgId(String pgId);

    List<FieldInfoDO> getFieldsById(String cardId);

    List<FieldInfoDO> getInitialFieldList(String pageId);

    FieldInfoDO selectFieldById(String cardId, String fdId);

    List<FieldInfoDO> selectAssociatedFieldsByKanban(String kanbanId);
}
