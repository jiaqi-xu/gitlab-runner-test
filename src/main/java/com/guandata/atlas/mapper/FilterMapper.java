package com.guandata.atlas.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.guandata.atlas.dao.BaseFilter;
import com.guandata.atlas.dto.ResultCondition;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface FilterMapper {
    int insertFilters(List<BaseFilter> filters);

    List<String> getFilterSource(Page<Map<String, Object>> page, String tableName, String fieldName, String search, List<ResultCondition> conditions);

    int deletePageFilters(String pgId, List<String> fdIdList);

    int deleteFiltersByPgId(String pgId);

    int deleteFiltersByCdId(String cdId);

    int deleteCardFilters(String cdId, List<String> fdIdList);

    List<BaseFilter> getPageFiltersById(String pgId);

    List<BaseFilter> getCardFiltersById(String cdId);

    List<BaseFilter> getFiltersByFilterIds(@Param("filterIds") List<String> filterIds);
}
