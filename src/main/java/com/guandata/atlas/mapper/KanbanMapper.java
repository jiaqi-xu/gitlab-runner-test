package com.guandata.atlas.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.guandata.atlas.dao.KanbanEntity;

public interface KanbanMapper extends BaseMapper<KanbanEntity> {

}
