package com.guandata.atlas.mapper;

import com.guandata.atlas.dao.PageInfoDO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PageMapper {
    int insertPage(PageInfoDO page);

    PageInfoDO getPageById(String id);

    Integer getDsIdByPage(String pageId);

    int deletePage(String pgId);

    int hardDeletePage(String pgId);

    PageInfoDO getPageByName(String name);

    List<PageInfoDO> list(@Param("pgIdList") List<String> pgIdList);

    List<PageInfoDO> getPageByDsId(int dsId);

    int isPageExisted(String pageId);

    List<PageInfoDO> getPagesByName(String name);
}
