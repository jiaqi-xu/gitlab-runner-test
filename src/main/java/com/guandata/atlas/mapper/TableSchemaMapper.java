package com.guandata.atlas.mapper;

import java.util.Date;

public interface TableSchemaMapper {
    Date getUpdateTime(String tableName, String dbName);

    Integer getRowsCount(String tableName);
}
