package com.guandata.atlas.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.guandata.atlas.dao.TokenDAO;

public interface TokenMapper extends BaseMapper<TokenDAO> {
}
