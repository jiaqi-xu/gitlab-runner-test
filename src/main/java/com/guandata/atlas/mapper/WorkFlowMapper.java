package com.guandata.atlas.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.guandata.atlas.dao.WorkFlowEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface WorkFlowMapper extends BaseMapper<WorkFlowEntity> {

    List<WorkFlowEntity> getWorkFlow(@Param("buttonIdList") List<Integer> buttonIdList);

    int updateByPK(@Param("workflow") WorkFlowEntity workflow);
}
