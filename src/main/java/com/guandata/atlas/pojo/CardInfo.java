package com.guandata.atlas.pojo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.guandata.atlas.service.planningtable.model.card.entity.Card;
import lombok.Data;

import java.util.List;
import java.util.stream.Collectors;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CardInfo {
    /** card Id **/
    private String cdId;
    /** card name **/
    private String name;
    /** page Id **/
    private String pgId;
    /** card fields **/
    private List<FieldInfo> fields;
    /** filter Info **/
    private List<FilterInfo> cdFilters;
    /** card sequence **/
    private Integer seqNo;

    public CardInfo() {

    }

    public CardInfo(Card card) {
        this.cdId = card.getCdId();
        this.name = card.getName();
        this.pgId = card.getPage().getPgId();
        this.seqNo = card.getSeqNo();
        this.fields = card.getFieldsDP().getFields()
                .stream().map(FieldInfo::new).collect(Collectors.toList());
        this.cdFilters = card.fillFiltersDP()
                .getFiltersDP().getFilters().stream().map(s-> new FilterInfo(s,card)).collect(Collectors.toList());
    }
}
