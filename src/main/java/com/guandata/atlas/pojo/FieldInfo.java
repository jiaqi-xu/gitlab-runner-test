package com.guandata.atlas.pojo;

import com.alibaba.fastjson.JSON;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.guandata.atlas.dto.dataset.DSColumn;
import com.guandata.atlas.service.planningtable.model.field.entity.Field;
import com.guandata.atlas.service.planningtable.model.field.valueobj.CorrelationAnalysis;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FieldInfo {
    /** 字段在PT中的Id **/
    private String fdId;

    private String columnId;

    /** 数据集字段 **/
    private DSColumn column;

    /** 字段别名 **/
    private String alias;
    /** 字段在Planning Table的类型，可以是TEXT或者METRIC **/
    private String metaType;
    /** 字段在表格字段中的属性，可以是DIM或者METRIC **/
    private String fdProperty;
    /** 用来标识是否是聚合主键，用于group by **/
    private Boolean isAggregated;
    /** 聚合计算因子，e.g. SUM, MAX, AVG **/
    private String aggrType;
    /** 是否禁用筛选 **/
    private Boolean allowFiltrated;
    /** 是否启用排序 **/
    private Boolean allowSorted;
    /** 是否启用编辑 **/
    private Boolean allowEdited;
    /** 关联分析配置 **/
    private CorrelationAnalysis correlationAnalysis;

    /** 在planning table result中是否为默认展示的field **/
    private Boolean inDefaultView;
    /** 所属card的Id **/
    private String cdId;
    /** 当保存初始可选择字段时会关联pageId **/
    private String pgId;
    /** 在表格字段中的顺序 **/
    private Integer seqNo;
    /** 数据格式 **/
    private String dataFormat;
    /**
     * 自定义的数据格式
     */
    private String customValuesConfig;

    private String ext;

    public FieldInfo(Field field) {
        this.fdId = field.getFdId();
        this.alias = field.getAlias();
        this.metaType = field.getMetaType().getType();
        this.cdId = field.getCard().getCdId();
        this.pgId = field.getCard().getPage().getPgId();
        this.seqNo = field.getSeqNo();
        this.dataFormat = field.getDataFormat();
        this.allowSorted = field.getAllowSort();
        this.allowFiltrated =field.getAllowFilter();
        this.allowEdited = field.getAllowEdit();
        this.correlationAnalysis = field.getCorrelationAnalysis();
        this.inDefaultView = field.getInDefaultView();
        this.isAggregated = field.isGroupByField();
        this.aggrType = field.getAggType();
        this.fdProperty = field.getFdProperty();
        this.column = field.getColumn();
        this.ext = JSON.toJSONString(field.getFieldExt());
    }

    public FieldInfo(){

    }
}
