package com.guandata.atlas.pojo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.guandata.atlas.dto.dataset.DSColumn;
import com.guandata.atlas.service.planningtable.model.card.entity.Card;
import com.guandata.atlas.service.planningtable.model.filter.entity.ElementFilter;
import com.guandata.atlas.service.planningtable.model.filter.entity.Filter;
import com.guandata.atlas.service.planningtable.model.filter.valueobj.OptionalDP;
import com.guandata.atlas.service.planningtable.model.page.entity.Page;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import org.jooq.Condition;
import org.jooq.impl.DSL;

import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FilterInfo {
    private String fdId;
    private DSColumn column;
    private String alias;
    private Integer seqNo;
    private List<String> choice;
    /** search parameter **/
    private String search;
    /** null if its a page filter **/
    private String cdId;
     /** null if its a card filter **/
     private String pgId;

     private String selectorType;

     private String aggType;

     private String subType;

     private List<String> defaultValues;

     private String defaultType;

     private Optional optional;

    public FilterInfo() {
    }

    public FilterInfo(Filter filter, Page page){
        this(filter);
        this.pgId = page.getPgId();
    }

    public FilterInfo(Filter filter, Card card){
        this(filter);
        this.cdId = card.getCdId();
        this.pgId = card.getPage().getPgId();
    }

    public FilterInfo(Filter filter) {
        this.column = filter.getDsColumn();
        this.seqNo = filter.getSeqNo();
        this.alias = filter.getAliasName();
        this.fdId = filter.getFdId();
        this.seqNo = filter.getSeqNo();
        this.cdId = filter.getCdId();
        this.pgId = filter.getPageId();

        this.selectorType = filter.getSelectType();
        if (filter.getAggrTypeEnum() != null) {
            this.aggType = filter.getAggrTypeEnum().getType();
        }
        this.subType = filter.getSubType();
        this.defaultValues = filter.getDefaultValues();
        this.defaultType = filter.getDefaultType().getCode();
        if (filter instanceof ElementFilter) {
            ElementFilter elementFilter = (ElementFilter)filter;
            OptionalDP optionalDP = elementFilter.getOptionalDP();
            this.optional = new Optional(optionalDP.getShowAll(),
                    elementFilter.getOptionalDP().getSortTypeEnum().getCode());
        }
    }

    @Getter
    @AllArgsConstructor
    public class Optional{
        private boolean showAll;

        private String sortType;
    }
}
