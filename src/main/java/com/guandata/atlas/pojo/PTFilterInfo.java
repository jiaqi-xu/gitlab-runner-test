package com.guandata.atlas.pojo;

import com.guandata.atlas.dto.dataset.DSColumn;
import com.guandata.atlas.service.dataset.model.valueobj.FilterDataDP;
import com.guandata.atlas.service.planningtable.model.filter.entity.Filter;
import com.guandata.atlas.service.planningtable.model.valueobj.LimitDP;
import lombok.Data;

import java.util.List;

@Data
public class PTFilterInfo extends PageBase{

    private String filterId;
    private String fdId;
    private DSColumn column;
    private List<String> choice;
    /** null if its a page filter **/
    private String cdId;
    /** null if its a card filter **/
    private String pgId;

    public PTFilterInfo(LimitDP limitDP, FilterInfo filter, FilterDataDP filterDataDP){
        super(limitDP.getPageNo(), limitDP.getPageSize(), filterDataDP.getTotal());
        this.fdId = filter.getFdId();
        this.column = filter.getColumn();
        this.cdId = filter.getCdId();
        this.pgId = filter.getPgId();
        this.choice = filterDataDP.getValues();
    }

    public PTFilterInfo(){

    }

}
