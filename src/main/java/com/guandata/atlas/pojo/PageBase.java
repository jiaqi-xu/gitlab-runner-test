package com.guandata.atlas.pojo;

import lombok.Data;

@Data
public class PageBase {

    /** pagination total count **/
    private Long total;
    /** pagination size **/
    private Long size;
    /** pagination current **/
    private Long current;
    /** pagination pages **/
    private Long pages;

    public PageBase(Long pageNo, Long pageSize, Long total) {
        this.total = total;
        this.size = pageSize;
        this.current = pageNo;
        this.pages = calPages(total, pageSize);
    }

    public PageBase(){

    }

    private Long calPages(Long total, Long pageSize){
        if (pageSize == 0L) {
            return 0L;
        }

        return total % pageSize == 0 ? total/pageSize : total/pageSize+1;
    }
}
