package com.guandata.atlas.pojo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.guandata.atlas.service.dataset.model.DataSet;
import com.guandata.atlas.service.planningtable.model.card.entity.Card;
import com.guandata.atlas.service.planningtable.model.page.entity.Page;
import com.guandata.atlas.service.planningtable.model.valueobj.FiltersDP;
import lombok.Data;

import java.util.List;
import java.util.stream.Collectors;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PageInfo {
    /** page Id **/
    private String pgId;
    /** page name **/
    private String name;
    /** dataset type id related to page **/
    private Integer dsId;
    /** page status **/
    private Integer status;
    /** cards Info **/
    private List<CardInfo> cards;
    /** filter Info **/
    private List<FilterInfo> pgFilters;
    /** page initial field list **/
    private List<FieldInfo> initialFieldList;
    private String tableName;

    public PageInfo(){

    }

    public PageInfo(Page page) {
        this.pgId = page.getPgId();
        this.name = page.getName();
        DataSet dataSet = page.getDataSet();
        this.dsId = dataSet.getId();
        this.status = page.getPageStatusEnum().getStatus();
        this.tableName = dataSet.getTableName();

        List<Card> cardList = page.getCardList();
        this.cards = cardList.stream().map(s-> new CardInfo(s)).collect(Collectors.toList());

        FiltersDP filtersDP = page.fillAllFilters().getFiltersDP();
        this.pgFilters = filtersDP.getFilters().stream().map(s->new FilterInfo(s, page))
                .collect(Collectors.toList());

    }
}
