package com.guandata.atlas.security;

import org.keycloak.adapters.springboot.KeycloakSpringBootConfigResolver;
import org.keycloak.adapters.springsecurity.KeycloakConfiguration;
import org.keycloak.adapters.springsecurity.authentication.KeycloakAuthenticationProvider;
import org.keycloak.adapters.springsecurity.config.KeycloakWebSecurityConfigurerAdapter;
import org.keycloak.adapters.springsecurity.management.HttpSessionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.core.authority.mapping.SimpleAuthorityMapper;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.web.authentication.session.RegisterSessionAuthenticationStrategy;
import org.springframework.security.web.authentication.session.SessionAuthenticationStrategy;
import com.guandata.atlas.common.constants.Constants;

@KeycloakConfiguration
public class WebSecurityConfig extends KeycloakWebSecurityConfigurerAdapter {
    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) {
        SimpleAuthorityMapper grantedAuthorityMapper = new SimpleAuthorityMapper();
        grantedAuthorityMapper.setPrefix("ROLE_");

        KeycloakAuthenticationProvider keycloakAuthenticationProvider = keycloakAuthenticationProvider();
        keycloakAuthenticationProvider.setGrantedAuthoritiesMapper(grantedAuthorityMapper);
        auth.authenticationProvider(keycloakAuthenticationProvider);
    }

    @Bean
    public KeycloakSpringBootConfigResolver keycloakConfigResolver() {
        return new KeycloakSpringBootConfigResolver();
    }

    @Bean
    @Override
    protected SessionAuthenticationStrategy sessionAuthenticationStrategy() {
        return new RegisterSessionAuthenticationStrategy(new SessionRegistryImpl());
    }

    @Bean
    @Override
    @ConditionalOnMissingBean(HttpSessionManager.class)
    protected HttpSessionManager httpSessionManager() {
        return new HttpSessionManager();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        super.configure(http);
        http.authorizeRequests()
                .antMatchers("/api/card/*/filter/defaultValues").hasAnyRole(Constants.USER, Constants.ADMIN)
                .antMatchers("/api/card/*/data", "/api/card/*/export").hasAnyRole(Constants.USER, Constants.ADMIN)
                .antMatchers("/api/card/**").hasRole(Constants.ADMIN)

                .antMatchers("/api/filter/**").hasAnyRole(Constants.USER, Constants.ADMIN)

                .antMatchers("/api/dataset/info").hasAnyRole(Constants.USER, Constants.ADMIN)
                .antMatchers("/api/dataset/**").hasRole(Constants.ADMIN)

                .antMatchers("/api/page/*/get", "/api/page/list").hasAnyRole(Constants.USER, Constants.ADMIN)
                .antMatchers("/api/page/**").hasRole(Constants.ADMIN)

                .antMatchers("/data/update").hasAnyRole(Constants.USER, Constants.ADMIN)

                .antMatchers("/api/kanban/list", "/api/kanban/*/sso-info", "/api/kanban/*/selectors").hasAnyRole(Constants.USER, Constants.ADMIN)
                .antMatchers("/api/kanban/**").hasRole(Constants.ADMIN)

                .antMatchers("/api/account/**").hasRole(Constants.ADMIN)

                .antMatchers("/api/dashboard/module/list").hasAnyRole(Constants.USER, Constants.ADMIN)
                .antMatchers("/api/dashboard/module/**").hasAnyRole(Constants.ADMIN)

                .antMatchers("/api/modify-history/**").hasAnyRole(Constants.USER, Constants.ADMIN)

                .antMatchers("/api/action-button/list", "/api/action-button/*/trigger").hasAnyRole(Constants.USER, Constants.ADMIN)
                .antMatchers("/api/action-button/update").hasRole(Constants.ADMIN)

                .antMatchers("/api/resources/**").hasAnyRole(Constants.ADMIN)


                .antMatchers("/api/users/*", "/api/users/*/modify-password").hasAnyRole(Constants.USER, Constants.ADMIN)
                .antMatchers("/api/users/**").hasAnyRole(Constants.ADMIN)

                .antMatchers("/api/groups/**").hasAnyRole(Constants.ADMIN)

                .antMatchers("/api/task/list").hasAnyRole(Constants.USER, Constants.ADMIN)

                .anyRequest().permitAll().and().csrf().disable();
        //http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        //http.cors().and().csrf().disable();
    }
}
