package com.guandata.atlas.service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.guandata.atlas.common.constants.Constants;
import com.guandata.atlas.common.utils.UniverseUtil;
import com.guandata.atlas.dao.AccountEntity;
import com.guandata.atlas.exception.AtlasException;
import com.guandata.atlas.mapper.AccountMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class AccountService {
    private static final Logger logger = LoggerFactory.getLogger(AccountService.class);

    @Autowired
    private AccountMapper accountMapper;

    @Autowired
    private KanbanService kanbanService;

    @Autowired
    private ActionButtonService actionButtonService;

    @Resource
    private GalaxyService galaxyService;

    /**
     * list accounts by connector type
     *
     * @param cnType connector type, e.g. GALAXY, UNIVERSE
     * @return
     */
    public List<AccountEntity> list(String cnType) {
        QueryWrapper<AccountEntity> queryWrapper = new QueryWrapper<>();
        Map<String, Object> queryParamsMap = new HashMap<>();
        queryParamsMap.put("is_del", false);
        queryParamsMap.put("cn_type", cnType);

        queryWrapper.select("id", "name", "cn_type", "auth_config").allEq(queryParamsMap).orderByDesc("update_time");
        return accountMapper.selectList(queryWrapper);
    }

    /**
     * get account config by id
     *
     * @param id account id
     * @return
     */
    public AccountEntity get(int id) {
        return accountMapper.selectById(id);
    }

    /**
     * add new account config
     *
     * @param account
     * @return
     */
    public int insert(AccountEntity account) {
        if (isNameExisted(account)) {
            throw new AtlasException("连接器名称已存在");
        }
        return accountMapper.insert(account);
    }

    /**
     * 判断是否存在相同类型相同名字的连接器
     * @param account
     * @return
     */
    public Boolean isNameExisted(AccountEntity account) {
        QueryWrapper<AccountEntity> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("cn_type", account.getCnType());
        queryWrapper.eq("name", account.getName());
        queryWrapper.eq("is_del", false);

        AccountEntity existedAccount = accountMapper.selectOne(queryWrapper);
        return (existedAccount != null && account.getId() == null)
                || (existedAccount != null && !existedAccount.getId().equals(account.getId()));
    }

    /**
     * update account config
     *
     * @param account account entity
     * @return
     */
    public int updateById(AccountEntity account) {
        if (isNameExisted(account)) {
            throw new AtlasException("连接器名称已存在");
        }
        return accountMapper.updateById(account);
    }

    /**
     * soft delete an existing account
     *
     * @param id account id
     * @return
     * @throws AtlasException
     */
    public int softDelete(int id) throws AtlasException {
        int countByKanBan = kanbanService.countByAccount(id);
        int countByActionButton = actionButtonService.countByAccount(id);
        if (countByKanBan == 0 && countByActionButton == 0) {
            UpdateWrapper<AccountEntity> updateWrapper = new UpdateWrapper<>();
            updateWrapper.set("is_del", true).eq("id", id);
            AccountEntity accountEntity = get(id);
            return accountMapper.update(accountEntity, updateWrapper);
        } else if (countByKanBan != 0 && countByActionButton != 0) {
            logger.warn("There exists kanban objects and action buttons associate with this account...");
            throw new AtlasException("Failed to delete: there exists kanban and action buttons objects associated with this account");
        } else if (countByKanBan != 0) {
            logger.warn("There exists kanban objects associate with this account...");
            throw new AtlasException("Failed to delete: there exists kanban objects associated with this account");
        } else {
            logger.warn("There exists action buttons associate with this account...");
            throw new AtlasException("Failed to delete: there exists action buttons associated with this account");
        }
    }

    /**
     * test connectivity for this account config
     *
     * @param account
     * @return
     * @throws IOException
     * @throws AtlasException
     */
    public boolean testConnectivity(AccountEntity account) {
        String cnType = account.getCnType();
        if (cnType.equals(Constants.GALAXY_ACCOUNT)) {
            galaxyService.getUIdTokenByAccount(account);
            return true;
        } else if (cnType.equals(Constants.UNIVERSE_ACCOUNT)) {
            JSONObject authConfig = JSON.parseObject(account.getAuthConfig());
            String baseUrl = authConfig.getString("address");
            String userName = authConfig.getString("username");
            String password = authConfig.getString("password");

            JSONObject responseObj = UniverseUtil.getLoginToken(baseUrl, userName, password);
            if (responseObj == null) {
                throw new AtlasException("Universe Service currently is not available.");
            } else if (responseObj.getInteger("code") == 0) {
                return true;
            } else if (responseObj.getInteger("code") == 10013) {
                throw new AtlasException(responseObj.getString("msg"));
            } else {
                throw new AtlasException("Universe Service currently is not available.");
            }
        } else {
            throw new AtlasException("Account type does not exist.");
        }
    }
}
