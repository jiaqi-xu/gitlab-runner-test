package com.guandata.atlas.service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.guandata.atlas.common.constants.Constants;
import com.guandata.atlas.common.utils.UniverseUtil;
import com.guandata.atlas.dao.AccountEntity;
import com.guandata.atlas.dao.ActionButtonEntity;
import com.guandata.atlas.dao.WorkFlowEntity;
import com.guandata.atlas.dto.ActionButton;
import com.guandata.atlas.dto.UserInfo;
import com.guandata.atlas.enums.WorkFlowTaskState;
import com.guandata.atlas.exception.AtlasException;
import com.guandata.atlas.mapper.ActionButtonMapper;
import com.guandata.atlas.mapper.WorkFlowMapper;
import org.jooq.DSLContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Service
public class ActionButtonService extends ServiceImpl<ActionButtonMapper, ActionButtonEntity> {
    private static final Logger logger = LoggerFactory.getLogger(ActionButtonService.class);

    @Autowired
    private ActionButtonMapper actionButtonMapper;

    @Autowired
    private WorkFlowMapper workFlowMapper;

    @Autowired
    private AccountService accountService;

    @Autowired
    private DSLContext dslContext;

    private static final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    /**
     * list action buttons with task state
     *
     * @return
     */
    public List<ActionButtonEntity> list(UserInfo userInfo, String pgId) {
        QueryWrapper<ActionButtonEntity> buttonQueryWrapper = new QueryWrapper<>();
        Map<String, Object> queryParamsMap = new HashMap<>();
        queryParamsMap.put("is_del", false);
        queryParamsMap.put("pg_id", pgId);

        buttonQueryWrapper.select("id", "name", "action_type", "pg_id", "button_setting", "target_setting").allEq(queryParamsMap);
        List<ActionButtonEntity> buttonList = actionButtonMapper.selectList(buttonQueryWrapper);

        if (!userInfo.getRoles().contains(Constants.ADMIN)) {
            //非管理员角色，不能看到target setting
            buttonList.stream().forEach(b -> b.setTargetSetting("{}"));
        }

        List<Integer> buttonIdList = buttonList.stream().map(ActionButtonEntity::getId).collect(Collectors.toList());
        List<WorkFlowEntity> workFlowEntityList = workFlowMapper.getWorkFlow(buttonIdList);

        buttonList.stream().map(button -> {
            int buttonId = button.getId();
            Optional<WorkFlowEntity> work = workFlowEntityList.stream().filter(w -> buttonId == w.getButtonId()).findFirst();
            if (work.isPresent()) {
                button.setLatestState(work.get());
            }
            return button;
        }).collect(Collectors.toList());

        return buttonList;
    }

    /**
     * update action button item
     *
     * @param buttonList
     * @param userId
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public boolean update(List<ActionButton> buttonList, String userId) {
        // get pgId
        String pgId = buttonList.get(0).getPgId();

        List<ActionButtonEntity> buttonEntityList = buttonList.stream().map(button -> {
            ActionButtonEntity buttonEntity = button.toActionButtonEntity();
            if (buttonEntity.getId() == 0) {
                buttonEntity.setCreatorId(userId);
            } else {
                buttonEntity.setEditorId(userId);
            }
            return buttonEntity;
        }).collect(Collectors.toList());

        // save or update
        saveOrUpdateBatch(buttonEntityList);

        QueryWrapper<ActionButtonEntity> buttonQueryWrapper = new QueryWrapper<>();
        Map<String, Object> queryParamsMap = new HashMap<>();
        queryParamsMap.put("is_del", false);
        queryParamsMap.put("pg_id", pgId);

        buttonQueryWrapper.select("id", "name", "action_type", "pg_id", "button_setting", "target_setting").allEq(queryParamsMap);
        List<ActionButtonEntity> existButtonList = actionButtonMapper.selectList(buttonQueryWrapper);

        //重名校验
        long count = existButtonList.stream().map(ActionButtonEntity::getName).distinct().count();
        if (existButtonList.size() != count) {
            throw new AtlasException("不可添加名称重复的按钮");
        }

        //平铺按钮数量校验
        AtomicInteger expandNum = new AtomicInteger();
        existButtonList.forEach(b -> {
            JSONObject buttonSetting = JSON.parseObject(b.getButtonSetting());
            boolean expand = buttonSetting.getBoolean("expend");
            if (expand) {
                expandNum.getAndIncrement();
            }
        });
        if (expandNum.get() > 3) {
            throw new AtlasException("平铺按钮数量不能超过3个");
        }

        return true;
    }

    /**
     * the number of action button items which are associated with the account
     *
     * @param accountId
     * @return
     */
    public int countByAccount(int accountId) {
        QueryWrapper<ActionButtonEntity> buttonQueryWrapper = new QueryWrapper<>();
        String accountSearch = "\"accountId\": " + accountId;
        buttonQueryWrapper.like("target_setting", accountSearch).eq("is_del", false);
        return count(buttonQueryWrapper);
    }

    /**
     * trigger action button by action type
     *
     * @param buttonId
     * @param userId
     * @return
     * @throws Exception
     */
    public int trigger(int buttonId, String userId) {
        ActionButtonEntity actionButtonEntity = getById(buttonId);
        int actionType = actionButtonEntity.getActionType();
        JSONObject targetSetting = JSON.parseObject(actionButtonEntity.getTargetSetting());

        int result = -1;
        if (actionType == Constants.ACTION_BUTTON_TYPE_WORKFLOW) {
            // action button target setting
            int accountId = targetSetting.getInteger("accountId");
            String processId = targetSetting.getString("processId");

            // account auth config
            AccountEntity accountEntity = accountService.get(accountId);
            JSONObject authConfig = JSON.parseObject(accountEntity.getAuthConfig());
            String baseUrl = authConfig.getString("address");
            String username = authConfig.getString("username");
            String password = authConfig.getString("password");

            JSONObject loginResponseObj = UniverseUtil.getLoginToken(baseUrl, username, password);
            if (loginResponseObj == null) {
                throw new AtlasException("Universe Service currently is not available.");
            } else if (loginResponseObj.getInteger("code") == 0) {
                logger.info("Login successfully");
                String token = loginResponseObj.getString("data");

                JSONObject workflowObj = UniverseUtil.processWorkFlow(baseUrl, token, processId);
                String processInstanceId = workflowObj.getString("processInstanceId");
                String name = workflowObj.getString("name");

                WorkFlowEntity workFlowEntity = new WorkFlowEntity();
                workFlowEntity.setTaskId(processInstanceId);
                workFlowEntity.setButtonId(buttonId);
                workFlowEntity.setState(1); // task start running
                workFlowEntity.setEditorId(userId);
                result = workFlowMapper.insert(workFlowEntity);
            } else if (loginResponseObj.getInteger("code") == 10013) {
                logger.error("Login failed");
                throw new AtlasException(loginResponseObj.getString("msg"));
            } else {
                logger.error("Universe Service currently is not available.");
                throw new AtlasException("Universe Service currently is not available.");
            }
        } else if (actionType == Constants.ACTION_BUTTON_TYPE_JUMP_ViEW) {
            //TODO: 跳转返回值
        }

        return result;
    }

    /**
     * get task result by task id
     *
     * @param workflow
     * @return
     * @throws Exception
     */
    @Async("threadPoolTaskExecutor")
    public void getTaskState(WorkFlowEntity workflow) throws Exception {
        int buttonId = workflow.getButtonId();
        String taskId = workflow.getTaskId();
        logger.info("Start to get process instance result; processInstanceId : {}, button_id: {}", taskId, buttonId);

        ActionButtonEntity actionButtonEntity = actionButtonMapper.selectById(buttonId);
        int actionType = actionButtonEntity.getActionType();
        JSONObject targetSetting = JSON.parseObject(actionButtonEntity.getTargetSetting());
        if (actionType != Constants.ACTION_BUTTON_TYPE_WORKFLOW) {
            logger.warn("不满足条件的actionType={}", actionType);
            return;
        }

        // action button target setting
        int accountId = targetSetting.getInteger("accountId");
        // account auth config
        AccountEntity accountEntity = accountService.get(accountId);
        JSONObject authConfig = JSON.parseObject(accountEntity.getAuthConfig());
        String baseUrl = authConfig.getString("address");
        String username = authConfig.getString("username");
        String password = authConfig.getString("password");

        JSONObject loginResponseObj = UniverseUtil.getLoginToken(baseUrl, username, password);
        if (loginResponseObj == null) {
            logger.error("Universe Service currently is not available.");
            return;
        }

        if (loginResponseObj.getInteger("code") == 10013) {
            logger.error("Login failed");
            return;
        }

        if (loginResponseObj.getInteger("code") != 0) {
            logger.error("code is not 0");
            return;
        }

        String token = loginResponseObj.getString("data");
        JSONObject processInstanceObj = UniverseUtil.getProcessInstanceResult(baseUrl, token, taskId);
        Integer code = processInstanceObj.getInteger("code");
        if (code == 0) {
            logger.info("Get process instance {} successfully", taskId);
            JSONObject resultData = processInstanceObj.getJSONObject("data");
            String state = resultData.getString("state");
            List<String> stateList = Arrays.stream(WorkFlowTaskState.values()).map(Enum::name).collect(Collectors.toList());
            int stateIndex = stateList.indexOf(state);
            if (state.equals(WorkFlowTaskState.STOP.name())
                    || state.equals(WorkFlowTaskState.FAILURE.name())
                    || state.equals(WorkFlowTaskState.SUCCESS.name())
                    || state.equals(WorkFlowTaskState.KILL.name())) {
                String finishTime = resultData.getString("endTime").replace("T", " ");
                Date endTime = formatter.parse(finishTime);
                workflow.setState(stateIndex);
                workflow.setEndTime(endTime);
                workFlowMapper.updateByPK(workflow);
                logger.info("Update process instance result successfully; processInstanceId : {}, button_id: {}", taskId, buttonId);
            } else if (!state.equals(WorkFlowTaskState.RUNNING_EXEUTION.name())) {
                workflow.setState(stateIndex);
                workFlowMapper.updateByPK(workflow);
                logger.info("Update process instance result successfully; processInstanceId : {}, button_id: {}", taskId, buttonId);
            }

            return;
        } else if (code == 50001) {
            logger.info("Process instance {} not exist", taskId);
            // 工作流实例不存在
            workflow.setState(14);
            workFlowMapper.updateByPK(workflow);
            logger.info("Update process instance result successfully; processInstanceId : {}, button_id: {}", taskId, buttonId);
            return;
        }

        logger.error("Get process instance failure; processInstanceId : {}, buttonId : {},code={}", taskId, buttonId, code);
    }
}
