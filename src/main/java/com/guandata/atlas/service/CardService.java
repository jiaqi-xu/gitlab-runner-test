package com.guandata.atlas.service;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.Lists;
import com.guandata.atlas.client.cache.local.DatasetCache;
import com.guandata.atlas.common.constants.Constants;
import com.guandata.atlas.common.utils.CacheKeyUtil;
import com.guandata.atlas.common.utils.PageUtil;
import com.guandata.atlas.common.utils.RandomUtil;
import com.guandata.atlas.dao.CardInfoDO;
import com.guandata.atlas.dto.*;
import com.guandata.atlas.dto.dataset.DSColumn;
import com.guandata.atlas.enums.ErrorCode;
import com.guandata.atlas.exception.AtlasException;
import com.guandata.atlas.mapper.CardMapper;
import com.guandata.atlas.pojo.FieldInfo;
import com.guandata.atlas.pojo.FilterInfo;
import com.guandata.atlas.service.dataset.model.DataSet;
import com.guandata.atlas.service.dataset.model.valueobj.FilterDataDP;
import com.guandata.atlas.service.disruptor.DisruptorTaskService;
import com.guandata.atlas.service.disruptor.EventTypeEnum;
import com.guandata.atlas.service.planningtable.enums.DataStatusEnum;
import com.guandata.atlas.service.planningtable.enums.DefaultTypeEnum;
import com.guandata.atlas.service.planningtable.model.card.entity.Card;
import com.guandata.atlas.service.planningtable.model.card.valueobj.DatasDP;
import com.guandata.atlas.service.planningtable.model.field.entity.DimField;
import com.guandata.atlas.service.planningtable.model.field.entity.Field;
import com.guandata.atlas.service.planningtable.model.filter.entity.ElementFilter;
import com.guandata.atlas.service.planningtable.model.filter.entity.Filter;
import com.guandata.atlas.service.planningtable.model.page.entity.Page;
import com.guandata.atlas.service.planningtable.model.valueobj.LimitDP;
import com.guandata.atlas.service.planningtable.repository.CardRepository;
import com.guandata.atlas.service.planningtable.repository.PageRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class CardService {
    private static final Logger logger = LoggerFactory.getLogger(CardService.class);

    @Autowired
    private CardMapper cardMapper;

    @Resource
    private DatasetCache datasetCache;

    @Resource
    private DisruptorTaskService disruptorTaskService;

    /**
     * 初始化card，赋予cardId后返回card实例
     * @param cdInfo
     * @return
     */
    public CardInfoDO init(CardInfoDO cdInfo) {
        cdInfo.setCdId(RandomUtil.generate(Constants.CARD_UUID_LENGTH));
        return cdInfo;
    }
    /**
     * 根据表格筛选区的信息展示临时数据结果
     * @param tableFilter
     * @return
     */
    public TableResult getResult(TableFilter tableFilter, Long pageNo, Long pageSize) {


        List<FieldInfo> fieldsInfo = tableFilter.getFieldsInfo();
        Collections.sort(fieldsInfo,Comparator.comparing(s->s.getSeqNo()));
        tableFilter.setFieldsInfo(fieldsInfo);

        List<ResultCondition> conditions = tableFilter.getConditions();
        if (!CollectionUtils.isEmpty(conditions)) {
            Collections.sort(conditions,Comparator.comparing(s->s.getColumn().getColumnId()));
            tableFilter.setConditions(conditions);
        }

        Integer dsId = tableFilter.getDsId();

        String cardDataCacheKey = CacheKeyUtil.getCardDataCacheKey(dsId, tableFilter.getCdId(), pageNo, pageSize, JSON.toJSONString(tableFilter));

        TableResult tableResult = datasetCache.get(cardDataCacheKey);
        if (tableResult != null) {
            return tableResult;
        }

//        SyncCardNextPagesDTO syncCardNextPagesDTO = new SyncCardNextPagesDTO(dsId, tableFilter.getCdId(), tableFilter, pageNo, pageSize);
//        disruptorTaskService.sendNotify(EventTypeEnum.CARD_NEXT_PAGE_SYNC, syncCardNextPagesDTO);

        tableResult = getResultDataList(tableFilter, pageNo, pageSize);
        datasetCache.put(cardDataCacheKey, tableResult, 10);
        return tableResult;
    }

    public TableResult getResultDataList(TableFilter tableFilter, Long pageNo, Long pageSize){
        tableFilter.reBuildConditions();

        Page page = PageRepository.getPageByPageId(tableFilter.getPgId(), tableFilter.getFiltersInfo());
        if (page == null) {
            throw new AtlasException(ErrorCode.PARAME_ERROR_CODE);
        }

        Card card = page.getCard(tableFilter.getCdId(), tableFilter.getFieldsInfo(), tableFilter.getFiltersInfo());
        if (card == null) {
            throw new AtlasException(ErrorCode.PARAME_ERROR_CODE);
        }

        DatasDP datasDP = card
                .fillPTSearchDP(tableFilter.getOrderBys(), pageNo, pageSize)
                .fillRecordsBySearch().getDatasDP();

        List<FieldInfo> fieldsInfo = tableFilter.getFieldsInfo();
        List<DSColumn> cols = tableFilter.getFieldsInfo().stream().filter(f -> f.getFdProperty() != null)
                .map(f -> f.getColumn()).filter(Objects::nonNull).collect(Collectors.toList());
        if (CollectionUtils.isEmpty(datasDP.getPtRecords())) {
            return new TableResult(cols, Lists.newArrayList(), datasDP.getTotal(),
                    0, pageNo, PageUtil.getPages(datasDP.getTotal(), pageSize));
        }
        DatasDP oriDatasDP = card.fillOriRecordsBySearch().getOriDatasDP();

        List<String> groupByFieldIds = fieldsInfo.stream().filter(s -> s.getIsAggregated()).map(s -> s.getColumn().getColumnId()).collect(Collectors.toList());

        Map<String, Map<String, Object>> keyToOriData = oriDatasDP.transListMap().stream().filter(Objects::nonNull).collect(Collectors.toMap(s -> {
            return buildGroupByUniqueKey(s, groupByFieldIds);
        }, v -> v, (k,v)->k));

        List<List<ResultData>> table = new ArrayList<>();

        List<Field> queryFields = card.getFieldsDP().getFields();
        List<Map<String, Object>> finalRecords = datasDP.transListMap().stream().filter(Objects::nonNull).collect(Collectors.toList());
        for (int i = 0; i < finalRecords.size(); i++) {
            List<ResultData> record = new ArrayList<>();
            Map<String, Object> oriRecord = keyToOriData.get(buildGroupByUniqueKey(finalRecords.get(i), groupByFieldIds));
            if (oriRecord == null) {
                for(Field field: queryFields) {
                    ResultData data = new ResultData(finalRecords.get(i).getOrDefault(field.getAliasName(), 0.0).toString());
                    if (field instanceof DimField) {
                        data.setM(DataStatusEnum.CHANGED.getStatus());
                    }
                    record.add(data);
                }

                table.add(record);
                continue;
            }
            Map<String, Object> finalRecord = finalRecords.get(i);

            for(Field field: queryFields) {
                Object valueObj = finalRecord.getOrDefault(field.getAliasName(), 0.0);
                String value = valueObj == null? "0.0": valueObj.toString();
                ResultData data = new ResultData(value);
                Object oriValue = oriRecord.get(field.getAliasName());
                Object finalValue = finalRecord.get(field.getAliasName());
                if (!groupByFieldIds.contains(field.getAliasName()) && !Objects.equals(oriValue, finalValue)){
                    data.setM(DataStatusEnum.CHANGED.getStatus());
                }
                record.add(data);
            }

            table.add(record);
        }

        return new TableResult(cols, table, datasDP.getTotal(), datasDP.getPtRecords().size(), pageNo, PageUtil.getPages(datasDP.getTotal(), pageSize));
    }

    public TableResult getPreviewResultDataList(TableFilter tableFilter, Long pageNo, Long pageSize){

        tableFilter.reBuildConditions();

        Page page = PageRepository.getPageByPageId(tableFilter.getPgId(), tableFilter.getFiltersInfo());
        if (page == null) {
            throw new AtlasException(ErrorCode.PARAME_ERROR_CODE);
        }
        Card card = page.getCard(tableFilter.getCdId(), tableFilter.getFieldsInfo(), tableFilter.getFiltersInfo());
        if (card == null) {
            card = new Card(tableFilter.getCdId(), page, tableFilter.getFieldsInfo(), tableFilter.getFiltersInfo());
        }
        DatasDP datasDP = card.fillPTSearchDP(tableFilter.getOrderBys(), pageNo, pageSize).fillRecordsBySearch().getDatasDP();

        List<FieldInfo> fieldsInfo = tableFilter.getFieldsInfo();
        List<DSColumn> cols = fieldsInfo.stream().filter(f -> f.getFdProperty() != null)
                .map(f -> f.getColumn()).filter(Objects::nonNull).collect(Collectors.toList());

        List<Field> queryFields = card.getFieldsDP().getFields();
        if (CollectionUtils.isEmpty(datasDP.getPtRecords())) {
            return new TableResult(cols, Lists.newArrayList(), datasDP.getTotal(),
                    0, pageNo, PageUtil.getPages(datasDP.getTotal(), pageSize));
        }
        List<List<ResultData>> table = new ArrayList<>();
        List<Map<String, Object>> finalRecords = datasDP.transListMap().stream().filter(Objects::nonNull).collect(Collectors.toList());
        for (int i = 0; i < finalRecords.size(); i++) {
            List<ResultData> record = new ArrayList<>();
            Map<String, Object> finalRecord = finalRecords.get(i);
            for(Field field: queryFields) {
                ResultData data = new ResultData(finalRecord.getOrDefault(field.getAliasName(), 0.0).toString());
                record.add(data);
            }
            table.add(record);
        }

        return new TableResult(cols, table, datasDP.getTotal(), datasDP.getPtRecords().size(), pageNo, PageUtil.getPages(datasDP.getTotal(), pageSize));
    }

    /**
     * 默认值
     * @param cardId
     * @return
     */
    public List<FilterInfo> getCardFilterDefaultValues(String cardId) {

        Card card = CardRepository.getCardByCardId(cardId);
        List<Filter> filters = card.fillFiltersDP().getFiltersDP().getFilters();
        if (CollectionUtils.isEmpty(filters)) {
            return Lists.newArrayList();
        }

        filters.stream().forEach(s->{
            if (! (s instanceof ElementFilter)) {
                return;
            }

            if (s.getDefaultType() != DefaultTypeEnum.FIRSTVALUE) {
                return;
            }

            ElementFilter elementFilter = (ElementFilter)s;
            if (s.isAggBeforeFilter()) {
                DataSet dataSet = card.getPage().getDataSet();
                FilterDataDP filterDataDP = dataSet.getFilterData(new FilterInfo(s), Lists.newArrayList(), new LimitDP(1L,1L));
                elementFilter.buildDefaultValues(filterDataDP.getValues());
                return;
            }

            elementFilter.fillFilterDefaultValue(card);
        });


        return filters.stream().map(s->new FilterInfo(s)).collect(Collectors.toList());
    }

    /**
     * 构造groupby数据的唯一键
     * @param data
     * @param groupByFieldIds
     * @return
     */
    private static String buildGroupByUniqueKey(Map<String, Object> data, List<String> groupByFieldIds) {
        StringBuilder key = new StringBuilder();
        for (String groupByFieldId : groupByFieldIds) {
            Object obj = data.get(groupByFieldId);
            if (obj != null) {
                key.append(obj).append("_");
                continue;
            }
            key.append("NULL_");
        }
        return key.toString();
    }

    public int insertCards(List<CardInfoDO> cardsInfo) {

        return cardMapper.insertCards(cardsInfo);
    }

    public CardInfoDO getCardById(String cardId) {
        return cardMapper.getCardById(cardId);
    }

}
