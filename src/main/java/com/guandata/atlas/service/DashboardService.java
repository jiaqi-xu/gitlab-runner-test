package com.guandata.atlas.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.guandata.atlas.dao.AdminModule;
import com.guandata.atlas.mapper.DashboardMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DashboardService {
    private static final Logger logger = LoggerFactory.getLogger(DashboardService.class);

    @Autowired
    private DashboardMapper dashboardMapper;


    /**
     * 新增模块
     * @param module 模块对象
     * @return
     */
    public int insert(AdminModule module) {
        return dashboardMapper.insert(module);
    }

    /**
     * 获取模块信息
     * @param id 模块id
     * @return
     */
    public AdminModule get(int id) {
        return dashboardMapper.selectById(id);
    }

    /**
     * 模块启用开关
     * @param id 模块id
     * @param enabled 是否启用
     * @return
     */
    public int moduleSwitch(int id, Boolean enabled) {
        UpdateWrapper<AdminModule> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set("enabled", enabled).eq("id", id);
        return dashboardMapper.update(null, updateWrapper);
    }

    /**
     * 设置模块别名
     * @param id  模块id
     * @param alias 别名
     * @return
     */
    public int setAlias(int id, String alias) {
        UpdateWrapper<AdminModule> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set("alias", alias).eq("id", id);
        return dashboardMapper.update(null, updateWrapper);
    }

    /**
     * 获取已启用模块列表
     * @return
     */
    public List<AdminModule> listModules() {
        QueryWrapper<AdminModule> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("enabled", true);
        return dashboardMapper.selectList(queryWrapper);
    }
}
