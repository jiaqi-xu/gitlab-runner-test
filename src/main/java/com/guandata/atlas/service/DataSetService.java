package com.guandata.atlas.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.common.collect.Lists;
import com.guandata.atlas.common.annotation.TaskCheck;
import com.guandata.atlas.common.constants.Constants;
import com.guandata.atlas.common.utils.RandomUtil;
import com.guandata.atlas.common.utils.StringUtil;
import com.guandata.atlas.controller.request.FilterDataGetRequest;
import com.guandata.atlas.dao.DataSetDAO;
import com.guandata.atlas.dao.FieldInfoDO;
import com.guandata.atlas.dao.TaskDAO;
import com.guandata.atlas.dao.TokenDAO;
import com.guandata.atlas.dto.*;
import com.guandata.atlas.dto.dataset.DSColumn;
import com.guandata.atlas.dto.dataset.DSExt;
import com.guandata.atlas.enums.DataTypeEnum;
import com.guandata.atlas.enums.ErrorCode;
import com.guandata.atlas.enums.TaskStatusEnum;
import com.guandata.atlas.enums.TaskTypeEnum;
import com.guandata.atlas.exception.AtlasException;
import com.guandata.atlas.mapper.DataSetMapper;
import com.guandata.atlas.mapper.FieldMapper;
import com.guandata.atlas.mapper.TableSchemaMapper;
import com.guandata.atlas.pojo.FilterInfo;
import com.guandata.atlas.pojo.PTFilterInfo;
import com.guandata.atlas.service.account.repository.TokenRepository;
import com.guandata.atlas.service.dataset.model.DataSet;
import com.guandata.atlas.service.dataset.model.valueobj.FilterDataDP;
import com.guandata.atlas.service.dataset.repository.DataSetRepository;
import com.guandata.atlas.service.disruptor.DisruptorTaskService;
import com.guandata.atlas.service.disruptor.EventTypeEnum;
import com.guandata.atlas.service.planningtable.model.valueobj.LimitDP;
import com.guandata.atlas.service.planningtable.repository.PageRepository;
import com.guandata.atlas.sqltransform.SQLOperator;
import org.jooq.*;
import org.jooq.impl.DSL;
import org.jooq.impl.SQLDataType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

import static com.guandata.atlas.enums.ErrorCode.FAIL_TO_CREATE_ATLAS_DATASET;

@Service
public class DataSetService {
    private static final Logger logger = LoggerFactory.getLogger(DataSetService.class);

    @Value("${spring.datasource.dbname}")
    private String dbName;

    @Autowired
    private TableSchemaMapper tableSchemaMapper;

    @Autowired
    private DataSetMapper dataSetMapper;

    @Resource
    private FieldMapper fieldMapper;

    @Autowired
    private TaskService taskService;

    @Resource
    private DisruptorTaskService disruptorTaskService;

    @Autowired
    private DSLContext dslContext;

    /**
     * 数据源类型列表展示
     *
     * @return
     */
    public List<DataSetDTO> list(String search, String orderBy, String orderType) {
        SelectJoinStep selectSelectStep = dslContext.select(DSL.field("id"), DSL.field("name"), DSL.field("table_name"), DSL.field("ext"), DSL.field("update_time"))
                .from(DSL.table("dataset"));
        if (!StringUtil.isEmpty(search)) {
            selectSelectStep.where(DSL.field("name").containsIgnoreCase(search));
        }
        selectSelectStep.where(DSL.condition("is_del = 0"));

        OrderField orderByField = null;
        if (orderType.equals(Constants.DESC)) {
            orderByField = DSL.field(DSL.name(orderBy)).desc();
        } else {
            orderByField = DSL.field(DSL.name(orderBy)).asc();
        }

        List<DataSetDAO> datasets = selectSelectStep.orderBy(orderByField).fetch(item -> item.into(DataSetDAO.class));
        return datasets.stream().map(item -> item.toDataSetDTO()).collect(Collectors.toList());
    }

    /**
     * 根据数据源类型id获取单数据源信息
     *
     * @param dataSetId
     * @return
     */
    public DataSetDTO getDataSetInfo(Integer dataSetId) {
        return dataSetMapper.getById(dataSetId).toDataSetDTO();
    }

    /**
     * 获取数据集meta信息
     * @param datasetId 数据集id
     * @return
     */
    public DataSetDTO getDataSetMeta(Integer datasetId) {
        DataSetDAO dataSetDO = dslContext.select(
                DSL.field("id"), DSL.field("name"), DSL.field("source_type"), DSL.field("source_ds").as("sourceDS"),
                DSL.field("table_name"), DSL.field("definition"), DSL.field("ext"), DSL.field("update_time"))
                .from(DSL.table("dataset")).where(DSL.condition("id = {0} and is_del = 0", datasetId))
                .fetchOneInto(DataSetDAO.class);

        DataSet dataSet = new DataSet(dataSetDO);
        int rowCount = dslContext.fetchCount(DSL.table(DSL.name(dataSet.getTableName())));
        int colCount = dataSet.getColumnListDP().getColumnList().size();

        dataSet.setExt(new DSExt(rowCount, colCount));
        dataSet.fillDataSyncOpenAPI().fillBackFlowSQL();

        return new DataSetDTO(dataSet);
    }

    /**
     * 更新数据集meta信息，e.g. version pk, column name, column comment, etc.
     * @param dataSetDTO
     * @param userId
     * @return
     */
    public boolean updateDataSetMeta(DataSetDTO dataSetDTO, String userId) {
        if (isNameExisted(dataSetDTO)) {
            throw new AtlasException("存在相同名称的数据集");
        }

        DataSetDAO dataSetDAO = dataSetDTO.toDataSet();
        dataSetDAO.setCreatorId(userId);

        return dataSetMapper.updateById(dataSetDAO) > 0;
    }

    /**
     * 同步数据集数据
     * @param dsId 数据集id
     * @return
     */
    @TaskCheck(objectId = "#tableName", timeoutSeconds = 1800)
    public String syncData(int dsId, UserInfo user, String tableName) {
        DataSetDTO dataSetDTO = getDataSetMeta(dsId);

        String taskId = UUID.randomUUID().toString().replace("-", "");
        TaskDAO taskDAO = new TaskDAO(taskId, TaskTypeEnum.SYNC_DATASET_DATA, dataSetDTO.getTableName(), TaskStatusEnum.RUNNING, user.getUserId());
        switch (dataSetDTO.getSourceType()) {
            case Constants.UNIVERSE_ACCOUNT:
                taskService.initTask(taskDAO);
                SyncDataSetDataDTO syncDataSetDataDTO = new SyncDataSetDataDTO(dataSetDTO, taskId, user);
                disruptorTaskService.sendNotify(EventTypeEnum.DATASET_DATA_SYNC, syncDataSetDataDTO);

                return taskId;

            default:
                throw new AtlasException("Unsupported dataset source type");
        }
    }


    /**
     * 获取预览数据集数据
     * @param datasetId
     * @return
     */
    public DSDataPreviewDTO previewDSData(int datasetId, Integer limit) {
        DataSet dataSet = DataSetRepository.getDataSet(datasetId);

        Result<Record> result = (limit != null)?
                DataSetRepository.concatGetDataSetDataSQL(dataSet).limit(limit).fetch():
                DataSetRepository.concatGetDataSetDataSQL(dataSet).fetch();

        List<DSColumn> previewColumns = dataSet.getColumnListDP().getColumnList().stream().map(col ->
                new DSColumn(col.getColumnId(), col.getName(), col.getType(), col.getSeqNo())
        ).collect(Collectors.toList());

        List<List<Object>> preview = new ArrayList<>();
        result.stream().forEach(record -> preview.add(record.intoList()));

        return new DSDataPreviewDTO(previewColumns, preview);
    }


    /**
     * 得到数据源的初始可供选择的字段。如果page已经存在，则从之前保存的initial fields获取；对于新page的化重新生成数据和字段信息。
     *
     * @param dataSetId
     * @return
     */
    public TableSchema getDataSetSchemaInfo(Integer dataSetId, String pgId) {
        if (pgId != null) {
            return getDataSetSchemaInfoByExistingPage(pgId);
        } else {
            return getDataSetSchemaInfoByDsId(dataSetId);
        }
    }

    /**
     * 根据数据源类型id获取数据源table schema以及数据信息，作为planning table的选择字段
     *
     * @param dataSetId
     * @return
     */
    public TableSchema getDataSetSchemaInfoByDsId(Integer dataSetId) {
        Set<String> MetricDataType = new HashSet<String>() {{
            add("DOUBLE");
            add("FLOAT");
            add("LONG");
            add("INTEGER");
        }};
        DataSetDTO ds = getDataSetInfo(dataSetId);
        String tableName = ds.getTableName();

        List<FieldInfoDO> fields = new ArrayList<>();

        for (DSColumn col : ds.getDefinition()) {
            FieldInfoDO field = new FieldInfoDO();
            field.setFdId(RandomUtil.generate(Constants.FIELD_UUID_LENGTH));
            field.setColumn(col);
            field.setSeqNo(col.getSeqNo());

            if (MetricDataType.contains(field.getColumn().getType().toUpperCase())) {
                field.setMetaType(Constants.METRIC);
            } else {
                field.setMetaType(Constants.TEXT);
            }
            fields.add(field);
        }

        Date lastUpdate = tableSchemaMapper.getUpdateTime(tableName, dbName);
        Integer rowsCount = tableSchemaMapper.getRowsCount(tableName);
        return new TableSchema(ds.getName(), fields, ds.getDefinition().size(), rowsCount, lastUpdate);
    }


    /**
     * 当page已经存在的时候，获取之前保存下来的字段信息以及对应数据表中的数据信息
     *
     * @param pgId
     * @return
     */
    public TableSchema getDataSetSchemaInfoByExistingPage(String pgId) {
        Integer dsId = PageRepository.getPageByPageId(pgId).getDataSet().getId();
        DataSetDTO ds = getDataSetInfo(dsId);
        String tableName = ds.getTableName();

        Date lastUpdate = tableSchemaMapper.getUpdateTime(tableName, dbName);
        Integer rowsCount = tableSchemaMapper.getRowsCount(tableName);
        List<FieldInfoDO> fields = fieldMapper.getInitialFieldList(pgId);

        Map<String, DSColumn> columnsMap = ds.getDefinition().stream().collect(Collectors.toMap(s->s.getColumnId(),v->v, (k,v)->k));
        fields.stream().forEach(field -> {
            field.setColumn(columnsMap.get(field.getColumnId()));
            field.setColumnId(null);
        });

        return new TableSchema(ds.getName(), fields, fields.size(), rowsCount, lastUpdate);
    }

    /**
     * 获取数据集版本主键
     * @param dataSetDTO
     * @return
     */
    public List<DSColumn> getDSVersionPK(DataSetDTO dataSetDTO) {
        List<Integer> indexes = dataSetDTO.getSourceDS().getJSONArray("versionPK").toJavaList(Integer.class);
        return indexes.stream().map(index ->  dataSetDTO.getDefinition().get(index - 1)).collect(Collectors.toList());
    }

    /**
     * 引入数据集
     * @param dataSetDTO 数据集数据传输对象
     * @param userId 创建数据集用户
     */
    @Transactional(rollbackFor = Exception.class)
    public Integer importDataSet(DataSetDTO dataSetDTO, String userId) {
        if (isNameExisted(dataSetDTO)) {
            throw new AtlasException("存在相同名称的数据集");
        }

        dataSetDTO.getDefinition().stream().forEach(column -> {
            column.setColumnId(UUID.randomUUID().toString());
        });
        Integer colCount = dataSetDTO.getDefinition().size();
        dataSetDTO.setExt(new DSExt(0, colCount));
        Integer dsId = save(dataSetDTO.toDataSet(), userId);
        createAssociatedTable(dataSetDTO.getTableName(), dataSetDTO.getDefinition());
        createAssociatedRecord(dataSetDTO);
        return dsId;
    }


    /**
     * 判断是否相同名字的数据集
     * @param dataSetDTO
     * @return
     */
    public boolean isNameExisted(DataSetDTO dataSetDTO) {
        DataSetDAO dataSetDAO = dataSetMapper.getByName(dataSetDTO.getName());
        return dataSetDAO == null ||
                (dataSetDAO != null && dataSetDAO.getId().equals(dataSetDTO.getId())) ? false: true;
    }

    /**
     * 判断数据集是否存在
     * @param dsId
     * @return
     */
    public Boolean isDataSetExisted(Integer dsId) {
        QueryWrapper<DataSetDAO> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("id", dsId);
        queryWrapper.eq("is_del", false);
        return dataSetMapper.selectCount(queryWrapper) > 0;
    }

    /**
     * 插入数据集记录
     * @param dataSet 数据集对象
     * @param userId 创建数据集用户
     * @return
     */
    public int save(DataSetDAO dataSet, String userId) {
       dslContext.insertInto(DSL.table("dataset")).columns(DSL.field("name"), DSL.field("source_type"), DSL.field("source_ds"), DSL.field("table_name"), DSL.field("definition"), DSL.field("ext"), DSL.field("editor_id"), DSL.field("creator_id"))
                .values(dataSet.getName(), dataSet.getSourceType(), dataSet.getSourceDS(), dataSet.getTableName(), dataSet.getDefinition(), dataSet.getExt(), userId, userId).execute();
       return dslContext.lastID().intValue();
    }

    /**
     * 创建数据集数据表，更新表, modify_history表, modify_history_detail表
     * @param tableName 数据集对应数据表名称
     * @param definition 数据集字段schema定义
     */
    public void createAssociatedTable(String tableName, List<DSColumn> definition) {
        CreateTableColumnStep createOriTableColumnStep = (CreateTableColumnStep) dslContext.createTable(tableName)
                .column("id", SQLDataType.BIGINT.identity(true))
                .constraint(DSL.constraint("PK_TABLE").primaryKey("id"));

        CreateTableColumnStep createUpdateTableColumnStep = (CreateTableColumnStep) dslContext.createTable(tableName + "_update")
                .column("id", SQLDataType.BIGINT.nullable(false))
                .constraint(DSL.constraint("PK_TABLE").primaryKey("id"));

        CreateTableColumnStep createModifyHistoryTableColumnStep = (CreateTableColumnStep) dslContext.createTable(tableName + "_modify_history")
                .column("id", SQLDataType.BIGINTUNSIGNED.identity(true))
                .column("cd_id", SQLDataType.VARCHAR(100))
                .column("pg_id", SQLDataType.VARCHAR(100))
                .column("ds_id", SQLDataType.INTEGER)
                .column("user_id", SQLDataType.VARCHAR(150))
                .column("user_name", SQLDataType.VARCHAR(100))
                .column("modify_time", SQLDataType.TIMESTAMP.nullable(false).defaultValue(DSL.now()))
                .column("apply_status", SQLDataType.TINYINT)
                .column("data_object", SQLDataType.JSON)
                .column("column_id", SQLDataType.VARCHAR(100))
                .column("modified_from", SQLDataType.CLOB)
                .column("modified_to", SQLDataType.CLOB)
                .column("message", SQLDataType.VARCHAR(5000))
                .constraint(DSL.constraint("PK_TABLE").primaryKey("id"));

        CreateTableColumnStep createModifyHistoryDetailColumnStep = (CreateTableColumnStep) dslContext.createTable(tableName + "_modify_history_detail")
                .column("id", SQLDataType.BIGINT.nullable(false))
                .column("modify_id", SQLDataType.BIGINTUNSIGNED.nullable(false))
                .column("column_id", SQLDataType.VARCHAR(100))
                .column("modified_from", SQLDataType.CLOB)
                .column("modified_to", SQLDataType.CLOB);

        definition.stream().forEach(item -> {
            DataType type = DataTypeEnum.getSQLDataType(item.getType());
            if (type == null) {
                throw new AtlasException(FAIL_TO_CREATE_ATLAS_DATASET, "不支持字段{" + item.getName() + "}的数据类型: " + item.getType());
            }
            createOriTableColumnStep.column(DSL.field(DSL.name(item.getColumnId()), type));
            createUpdateTableColumnStep.column(DSL.field(DSL.name(item.getColumnId()), type));
            createModifyHistoryDetailColumnStep.column(DSL.field(DSL.name(item.getColumnId()), type));
        });

        createOriTableColumnStep.execute();
        createUpdateTableColumnStep.execute();
        createModifyHistoryTableColumnStep.execute();
        createModifyHistoryDetailColumnStep.execute();
    }

    /**
     * 新建/插入数据集相关记录至相关表
     * @param dataSetDTO
     */
    public void createAssociatedRecord(DataSetDTO dataSetDTO) {
        TokenDAO tokenDAO = new TokenDAO(
                TaskTypeEnum.SYNC_DATASET_DATA.getType(), dataSetDTO.getTableName(), RandomUtil.generateAccessToken());
        TokenRepository.saveToken(tokenDAO);
    }

    /**
     * is any page using the dataset
     * @param id datasetId
     * @return true if there is some existing page using the dataset
     */
    public boolean anyPageInUse(int id) {
        return dslContext.fetchCount(DSL.table("page"),
                DSL.condition("status = 0").and("ds_id = {0}", id)) > 0;
    }

    /**
     * (软)删除数据集
     * @param id 数据集id
     * @return
     */
    public int softDelete(int id) {
        if (anyPageInUse(id)) {
            throw new AtlasException("删除失败，有页面在使用该数据集");
        }
        
        Table<Record> table = DSL.table("dataset");
        UpdateQuery<Record> updateQuery = dslContext.updateQuery(table);
        updateQuery.addValue(DSL.field("is_del"), true);
        Condition condition = DSL.field("id").eq(id);
        updateQuery.addConditions(condition);
        return updateQuery.execute();
    }

    /**
     * 通过数据集tableName(uuid)获取数据集信息
     * @param tableName
     * @return
     */
    public DataSetDTO getDataSetByTableName(String tableName) {
        QueryWrapper<DataSetDAO> queryWrapper = new QueryWrapper<>();
        DataSetDAO dataSetDAO = dataSetMapper.selectOne(queryWrapper.eq("table_name", tableName));
        return dataSetDAO.toDataSetDTO();
    }

    /**
     * 根据筛选器条件从modify_history_detail表中获取modify_id
     *
     * @param detailFilter
     * @param dsTable
     * @return
     */
    public List<Long> getModifyIDs(List<FilterCondition> detailFilter, String dsTable) {
        if (detailFilter.size() > 0) {
            return dslContext.selectDistinct(DSL.field("modify_id", SQLDataType.BIGINT))
                    .from(DSL.table(DSL.name(dsTable + "_modify_history_detail")))
                    .where(SQLOperator.filterConditionsTransForm(detailFilter)).fetch("modify_id", Long.class);
        } else {
            return dslContext.selectDistinct(DSL.field("modify_id", SQLDataType.BIGINT))
                    .from(DSL.table(DSL.name(dsTable + "_modify_history_detail"))).fetch("modify_id", Long.class);
        }
    }

    /**
     * 通过modify_history获取user info
     *
     * @param dsTable
     * @return
     */
    public List<UserInfo> getUserFilters(String dsTable) {
        return dslContext.selectDistinct(DSL.field("user_id"), DSL.field("user_name"))
                .from(DSL.table(DSL.name(dsTable + "_modify_history")))
                .fetch().map(item -> {
                    UserInfo user = item.into(UserInfo.class);
                    return user;
                });
    }

    /**
     * 获取filter的数据源
     * @param request
     * @return
     */
    public List<PTFilterInfo> getFilterData(FilterDataGetRequest request) {
        request.reBuildConditions();

        List<FilterInfo> filters = request.getFilters();
        if (CollectionUtils.isEmpty(filters) || filters.size() >1 ) {
            throw new AtlasException(ErrorCode.PARAME_ERROR_CODE);
        }

        LimitDP limitDP = new LimitDP(1L, Long.MAX_VALUE);
        DataSet dataSet = DataSetRepository.getDataSetById(request.getDsId());
        FilterDataDP filterData = dataSet.getFilterData(filters.get(0), request.getConditionsInfo(), new LimitDP(1L,Long.MAX_VALUE));
        return Lists.newArrayList(new PTFilterInfo(limitDP, filters.get(0), filterData));
    }
}
