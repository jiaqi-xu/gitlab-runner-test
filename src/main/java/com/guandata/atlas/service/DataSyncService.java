package com.guandata.atlas.service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.guandata.atlas.client.cache.local.GuavaCacheWrapper;
import com.guandata.atlas.common.constants.Constants;
import com.guandata.atlas.common.utils.ExceptionUtil;
import com.guandata.atlas.common.utils.SpringBeanUtil;
import com.guandata.atlas.common.utils.StringUtil;
import com.guandata.atlas.dao.AccountEntity;
import com.guandata.atlas.dao.TaskDAO;
import com.guandata.atlas.dto.DSDataPreviewDTO;
import com.guandata.atlas.dto.DataSetDTO;
import com.guandata.atlas.dto.SqlParamDTO;
import com.guandata.atlas.dto.UserInfo;
import com.guandata.atlas.dto.dataset.DSColumn;
import com.guandata.atlas.dto.universe.UniverseLoginInDTO;
import com.guandata.atlas.enums.DataTypeEnum;
import com.guandata.atlas.enums.ErrorCode;
import com.guandata.atlas.enums.TaskStatusEnum;
import com.guandata.atlas.exception.AtlasException;
import org.apache.commons.collections4.CollectionUtils;
import org.jooq.*;
import org.jooq.impl.DSL;
import org.jooq.impl.SQLDataType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static com.guandata.atlas.enums.ErrorCode.DATA_SYNC_FAIL_TO_INSERT_MODIFY_RECORD;

@Service
public class DataSyncService {
    private static final Logger logger = LoggerFactory.getLogger(DataSyncService.class);

    @Autowired
    private UniverseService universeService;

    @Autowired
    private AccountService accountService;

    @Autowired
    private TaskService taskService;

    @Autowired
    private DataSetService dataSetService;

    @Autowired
    private DSLContext dslContext;

    /**
     * 同步Universe数据集数据
     *
     * @param dataSetDTO 数据集实体DTO
     * @return
     */
    public void syncUniverseData(DataSetDTO dataSetDTO, String taskId, UserInfo user) {
        String errorMsg = null;
        List<DSColumn> versionPKs = null;
        Result<Record> pkRecords = null;
        try {
            versionPKs = dataSetService.getDSVersionPK(dataSetDTO);
            String tempTable = createTempTable(dataSetDTO);
            exportDataAndDumpToTempTable(dataSetDTO, tempTable);
            pkRecords = getVersionPKValuesFromTempTable(dataSetDTO, versionPKs, tempTable);
            DataSyncService dataSyncService = SpringBeanUtil.getBean(DataSyncService.class);
            dataSyncService.deleteAndDumpDataByVPK(dataSetDTO, pkRecords, tempTable);

        } catch (AtlasException e) {
            errorMsg = e.getMessage();
            logger.error("同步数据失败, 任务ID: {}: {}", taskId, e);
        } catch (Exception e) {
            errorMsg = e.getMessage();
            if (errorMsg == null) {
                errorMsg = ExceptionUtil.getStackTrace(e);
            }
            logger.error("同步数据失败, 任务ID: {}: {}", taskId, e);
        } finally {
            dropTempTable(dataSetDTO);

            JSONObject dataObject = processPKRecords(versionPKs, pkRecords);
            if (StringUtil.isEmpty(errorMsg)) {
                insertDataSyncRecord(dataSetDTO, dataObject, true, null, user);
                taskService.updateTask(new TaskDAO(taskId, TaskStatusEnum.SUCCESS, null, new Date()));
            } else {
                errorMsg = (errorMsg.length() >= 5000) ? errorMsg.substring(0, 5000) : errorMsg;
                insertDataSyncRecord(dataSetDTO, dataObject, false, errorMsg, user);
                taskService.updateTask(new TaskDAO(taskId, TaskStatusEnum.FAILURE, errorMsg, new Date()));
            }
            GuavaCacheWrapper.delDataSetCacheByDsId(dataSetDTO.getId());
            //disruptorTaskService.sendNotify(EventTypeEnum.INIT_PAGE_SYNC, new InitPagesDTO(dataSetDTO.getId()));
        }
    }

    /**
     * 创建数据同步临时表
     *
     * @param dataSetDTO
     */
    public String createTempTable(DataSetDTO dataSetDTO) {
        // Create Temp Table
        String tempTableName = dataSetDTO.getTableName() + "_temp";
        logger.info("开始创建临时表:{}.", tempTableName);
        CreateTableColumnStep createTempTableColumnStep = (CreateTableColumnStep) dslContext.createTable(DSL.name(tempTableName)).column("id", SQLDataType.BIGINT.identity(true))
                .constraint(DSL.constraint("PK_TABLE").primaryKey("id"));
        dataSetDTO.getDefinition().stream().forEach(item -> {
            createTempTableColumnStep.column(DSL.field(DSL.name(item.getColumnId()), DataTypeEnum.getSQLDataType(item.getType())));
        });
        createTempTableColumnStep.execute();

        logger.info("临时表创建完毕:{}.", tempTableName);
        return tempTableName;
    }

    /**
     * 分页获取数据并导入数据至临时表
     *
     * @param dataSetDTO
     * @return
     */
    public Boolean exportDataAndDumpToTempTable(DataSetDTO dataSetDTO, String tempTable) {
        Integer accountId = dataSetDTO.getSourceDS().getInteger("accountId");
        AccountEntity accountEntity = accountService.get(accountId);
        JSONObject authConfig = JSON.parseObject(accountEntity.getAuthConfig());
        UniverseLoginInDTO universeLoginInDTO = authConfig.toJavaObject(UniverseLoginInDTO.class);

        Integer datasetId = dataSetDTO.getSourceDS().getInteger("dataSetId");
        Integer projectId = dataSetDTO.getSourceDS().getInteger("projectId");
        List<String> select = dataSetDTO.getDefinition().stream().map(col -> col.getOriginName()).collect(Collectors.toList());

        String dsSnapShotPath = universeService.getDataSetDataSnapShotPath(universeLoginInDTO, projectId, datasetId, new SqlParamDTO(select, null));

        Integer rowsCount = 0;

        List<Field<Object>> columns = dataSetDTO.getDefinition().stream().map(f -> DSL.field(DSL.name(f.getColumnId()))).collect(Collectors.toList());
        List<Integer> booleanIndexes = dataSetDTO.getDefinition().stream().filter(
                f -> f.getType().equals("BOOLEAN")).map(c -> (c.getSeqNo() - 1)).collect(Collectors.toList());

        DSDataPreviewDTO dataPreview;
        do {
            dataPreview = universeService.exportDataSetDataByPagination(universeLoginInDTO, dsSnapShotPath, projectId, datasetId, rowsCount, Constants.BATCH_SIZE);
            // Dump Data
            List<List<Object>> data = dataPreview.getPreview();
            if (!CollectionUtils.isEmpty(data)) {
                logger.info("开始导入{}行数据至临时表.", data.size());

                InsertValuesStepN insertValuesStepN = dslContext.insertInto(DSL.table(DSL.name(tempTable))).columns(columns);
                for (List<Object> rowData : data) {
                    universeService.processBooleanColumn(booleanIndexes, rowData);
                    insertValuesStepN.values(rowData);
                }
                insertValuesStepN.execute();
                rowsCount += data.size();
                logger.info("导入完毕，目前临时表{}数据量为:{}.", tempTable, rowsCount);
            }
        } while (dataPreview.getHasMore());

        logger.info("临时表数据导入完毕:{}, 共{}行.", tempTable, rowsCount);

        Integer rowsCountInTemp = dslContext.fetchCount(DSL.table(DSL.name(tempTable)));

        if (!rowsCountInTemp.equals(rowsCount)) {
            throw new AtlasException("数据集同步失败: 临时表导入数据量与源数据量不一致");
        }
        return true;
    }

    /**
     * 获取数据集数据中版本主键的值
     *
     * @param dataSetDTO
     * @param versionPKs
     * @return
     */
    public Result<Record> getVersionPKValuesFromTempTable(DataSetDTO dataSetDTO, List<DSColumn> versionPKs, String tempTable) {
        List<Field> fields = versionPKs.stream().map(
                col -> DSL.field(DSL.name(col.getColumnId()))
        ).collect(Collectors.toList());
        Result<Record> pkRecords = dslContext.selectDistinct(fields).from(DSL.table(DSL.name(tempTable))).fetch();
        return pkRecords;
    }

    /**
     * 检测版本主键是否存在空值
     * @param pkRecord
     * @return
     */
    public Boolean validateVersionPK(Record pkRecord) {
        if (pkRecord.intoList().contains(null)) {
            throw new AtlasException(ErrorCode.VERSION_PK_CONTAINS_NULL);
        }
        return true;
    }

    /**
     * 删除数据集表和从临时表中导入数据至数据集表中
     *
     * @param dataSetDTO
     * @param pkRecords
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public Boolean deleteAndDumpDataByVPK(DataSetDTO dataSetDTO, Result<Record> pkRecords, String tempTable) {
        String oriTableName = dataSetDTO.getTableName();
        String updateTableName = oriTableName + "_update";

        List<Field<Object>> columns = dataSetDTO.getDefinition().stream().map(f -> DSL.field(DSL.name(f.getColumnId()))).collect(Collectors.toList());
        List<Field<Object>> columnsPlusId = new ArrayList<>(columns);
        columnsPlusId.add(DSL.field("id"));

        Integer totalRowCount = 0;
        for (int i = 0; i < pkRecords.size(); i++) {
            Record pkRecord = pkRecords.get(i);
            validateVersionPK(pkRecord);

            logger.info("正在处理版本主键为{}, 值为{}:", pkRecord.fieldsRow().fields(), pkRecord.valuesRow().fields());

            logger.info("   正在删除ori数据表数据");
            dslContext.deleteFrom(DSL.table(DSL.name(oriTableName))).where(DSL.condition(pkRecord)).execute();
            logger.info("   正在删除update数据表数据");
            dslContext.deleteFrom(DSL.table(DSL.name(updateTableName))).where(DSL.condition(pkRecord)).execute();

            // Batch insert
            Integer rowsCount = dslContext.fetchCount(DSL.table(DSL.name(tempTable)), DSL.condition(pkRecord));
            Integer insertTimes = (rowsCount - 1) / Constants.BATCH_SIZE + 1;

            for (int j = 0; j < insertTimes; ++j) {
                dslContext.insertInto(DSL.table(DSL.name(oriTableName)), columns)
                        .select(
                                dslContext.select(columns).from(DSL.table(DSL.name(tempTable)))
                                        .where(DSL.condition(pkRecord)).orderBy(DSL.field("id").asc())
                                        .limit(j * Constants.BATCH_SIZE,
                                                Constants.BATCH_SIZE.intValue()
                                        )
                        ).execute();

                dslContext.insertInto(DSL.table(DSL.name(updateTableName)), columnsPlusId)
                        .select(
                                dslContext.select(columnsPlusId).from(DSL.table(DSL.name(oriTableName)))
                                        .where(DSL.condition(pkRecord)).orderBy(DSL.field("id").asc())
                                        .limit(j * Constants.BATCH_SIZE,
                                                Constants.BATCH_SIZE.intValue()
                                        )
                        ).execute();
            }

            logger.info("   分别成功导入{}行数据至ori和update数据表", rowsCount);
            totalRowCount += rowsCount;
        }

        logger.info("共分别导入{}行数据至ori和update数据表", totalRowCount);
        return true;
    }

    /**
     * 删除临时表
     *
     * @param dataSetDTO
     * @return
     */
    public Boolean dropTempTable(DataSetDTO dataSetDTO) {
        String tempTable = dataSetDTO.getTableName() + "_temp";
        return dslContext.dropTableIfExists(DSL.table(DSL.name(tempTable))).execute() > 0;
    }

    /**
     * 处理版本主键记录，构造数据对象，用于历史记录表
     *
     * @param versionPKs
     * @param pkRecords
     * @return
     */
    public JSONObject processPKRecords(List<DSColumn> versionPKs, Result<Record> pkRecords) {
        JSONObject dataObject = new JSONObject();
        dataObject.put("versionPK", versionPKs);

        List<List<Object>> versionPKValues = new ArrayList<>();
        if (!CollectionUtils.isEmpty(pkRecords)) {
            for (Record pkRecord : pkRecords) {
                List<Object> value = new ArrayList<>();
                List<Field> fields = Arrays.asList(pkRecord.fieldsRow().fields());
                fields.stream().forEach(field -> value.add(pkRecord.getValue(field)));
                versionPKValues.add(value);
            }
            dataObject.put("values", versionPKValues);
        }

        return dataObject;
    }

    /**
     * 插入数据同步记录至数据修改历史记录表
     *
     * @param dataSetDTO
     * @param dataObject
     * @param status
     * @param message
     * @param user
     */
    public void insertDataSyncRecord(DataSetDTO dataSetDTO, JSONObject dataObject, boolean status, String message, UserInfo user) {
        String dsTable = dataSetDTO.getTableName();
        String modifyHistoryTable = dsTable + "_modify_history";

        try {
            dslContext.insertInto(DSL.table(DSL.name(modifyHistoryTable)))
                    .columns(DSL.field("ds_id"), DSL.field("user_id"), DSL.field("user_name"),
                            DSL.field("apply_status"), DSL.field("data_object"), DSL.field("message")
                    ).values(
                    dataSetDTO.getId(), user.getUserId(), user.getUserName(),
                    status, dataObject.toJSONString(), message
            ).execute();
        } catch (Exception e) {
            logger.error("插入数据同步记录至表{}失败", modifyHistoryTable, e);
            throw new AtlasException(DATA_SYNC_FAIL_TO_INSERT_MODIFY_RECORD, modifyHistoryTable);
        }
    }

    /**
     * 全量导出Universe数据集数据
     *
     * @param dataSetDTO
     * @return
     */
    public DSDataPreviewDTO exportDataSetData(DataSetDTO dataSetDTO) {
        Integer accountId = dataSetDTO.getSourceDS().getInteger("accountId");
        AccountEntity accountEntity = accountService.get(accountId);
        JSONObject authConfig = JSON.parseObject(accountEntity.getAuthConfig());
        UniverseLoginInDTO universeLoginInDTO = authConfig.toJavaObject(UniverseLoginInDTO.class);

        Integer datasetId = dataSetDTO.getSourceDS().getInteger("dataSetId");
        List<String> select = dataSetDTO.getDefinition().stream().map(col -> col.getOriginName()).collect(Collectors.toList());

        DSDataPreviewDTO dataPreview = universeService.exportDataSetData(universeLoginInDTO, datasetId, new SqlParamDTO(select, null));
        return dataPreview;
    }

    /**
     * 创建临时表，导入数据并校验数据量一致性
     *
     * @param dataSetDTO
     * @param dataPreview
     * @return
     */
    public Boolean createTempTableAndDumpData(DataSetDTO dataSetDTO, DSDataPreviewDTO dataPreview) {
        String tempTableName = createTempTable(dataSetDTO);
        // Dump Data
        List<Field<Object>> columns = dataSetDTO.getDefinition().stream().map(f -> DSL.field(DSL.name(f.getColumnId()))).collect(Collectors.toList());
        List<Integer> booleanIndexes = dataSetDTO.getDefinition().stream().filter(
                f -> f.getType().equals("BOOLEAN")).map(c -> (c.getSeqNo() - 1)).collect(Collectors.toList());

        List<List<Object>> data = dataPreview.getPreview();
        logger.info("即将导入{}行数据.", data.size());

        Integer count = 0;
        logger.info("开始导入数据至临时表:{}.", tempTableName);
        List<List<List<Object>>> partitionDatas = Lists.partition(data, Constants.BATCH_SIZE);
        for (List<List<Object>> partitionData : partitionDatas) {
            InsertValuesStepN insertValuesStepN = dslContext.insertInto(DSL.table(DSL.name(tempTableName))).columns(columns);
            for (List<Object> rowData : partitionData) {
                universeService.processBooleanColumn(booleanIndexes, rowData);
                insertValuesStepN.values(rowData);
            }
            count += partitionData.size();
            insertValuesStepN.execute();
            logger.info("导入完毕，目前临时表{}数据量为:{}.", tempTableName, count);
        }

        logger.info("临时表数据导入完毕:{}.", tempTableName);

        Integer rowsCount = dslContext.fetchCount(DSL.table(DSL.name(tempTableName)));

        if (!rowsCount.equals(data.size())) {
            throw new AtlasException("数据集同步失败: 临时表导入数据量不一致");
        }
        return true;
    }
}


