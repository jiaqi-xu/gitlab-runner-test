package com.guandata.atlas.service;

import com.google.common.collect.Lists;
import com.guandata.atlas.client.cache.local.GuavaCacheWrapper;
import com.guandata.atlas.common.annotation.TaskCheck;
import com.guandata.atlas.common.constants.Constants;
import com.guandata.atlas.common.utils.ResponseResult;
import com.guandata.atlas.dao.TaskDAO;
import com.guandata.atlas.dto.*;
import com.guandata.atlas.dto.dataset.DSColumn;
import com.guandata.atlas.enums.TaskStatusEnum;
import com.guandata.atlas.enums.TaskTypeEnum;
import com.guandata.atlas.pojo.FilterInfo;
import com.guandata.atlas.service.disruptor.DisruptorTaskService;
import com.guandata.atlas.service.disruptor.EventTypeEnum;
import com.guandata.atlas.service.planningtable.enums.ElementsTypeEnum;
import com.guandata.atlas.service.planningtable.enums.SelectTypeEnum;
import com.guandata.atlas.service.planningtable.model.card.entity.Card;
import com.guandata.atlas.service.planningtable.model.page.entity.Page;
import com.guandata.atlas.service.planningtable.repository.CardRepository;
import com.guandata.atlas.service.planningtable.repository.PageRepository;
import com.guandata.atlas.service.task.repository.TaskRepository;
import org.jooq.DSLContext;
import org.jooq.Field;
import org.jooq.InsertValuesStepN;
import org.jooq.Record;
import org.jooq.impl.DSL;
import org.jooq.tools.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class DataUpdateService {
    private static final Logger logger = LoggerFactory.getLogger(DataUpdateService.class);

    @Autowired
    private DSLContext dslContext;

    @Resource
    private DisruptorTaskService disruptorTaskService;

    /**
     * 事务控制, 字段预测值修改
     * 字段预测值修改
     *
     * @param param
     */
    @Transactional(rollbackFor = Exception.class)
    @TaskCheck(objectId = "#param.tableName")
    public void doUpdate(DataUpdateParam param, ResponseResult result) {

        List<ResultCondition> conditions = param.getConditions();
        conditions.stream().forEach(s->s.reBuildCondition());

        List<FilterInfo> filters
                = conditions.stream().map(s -> s.getFilterInfo()).filter(Objects::nonNull)
                .collect(Collectors.toList());

        filters.forEach(s->{
            if (StringUtils.isBlank(s.getSelectorType())) {
                s.setSelectorType(SelectTypeEnum.DS_ELEMENTS.getType());
                s.setSubType(ElementsTypeEnum.MULTI.getCode());
            }
        });

        Card card = CardRepository.getCardByCardId(param.getCdId());
        Page page = PageRepository.getPageByPageId(card.getPage().getPgId(), filters);
        card = page.getCard(card.getCdId(), null, filters);

        card.singleModify(param);

        DSColumn modifiedField = param.getField();

        List<DataUpdateRes> updateRes = new ArrayList<>();
        updateRes.add(new DataUpdateRes(modifiedField, param.getModifiedTo()));
        Map<String, Object> dataMap = new HashMap<>();
        dataMap.put("affectedFields", updateRes);
        result.setData(dataMap);

        GuavaCacheWrapper.delDataSetCacheByDsId(page.getDataSet().getId());
    }

    public boolean updateList(String dsTable, String modifiedField, List<Record> resToUpdate){

        Field<?>[] fields = resToUpdate.get(0).fields();
        List<Field<Object>> transFields = Lists.newArrayList();
        for (Field<?> field : fields) {
            transFields.add(DSL.field(DSL.name(field.getName())));
        }

        try {
            logger.info("Total {} rows to update: ", resToUpdate.size());
            List<List<Record>> partitionRecords = Lists.partition(resToUpdate, Constants.BATCH_SIZE);
            for (List<Record> partitionRecord : partitionRecords) {
                InsertValuesStepN insertValuesStepN = dslContext.insertInto(DSL.table(DSL.name(dsTable + "_update"))).columns(transFields);
                for (Record record : partitionRecord) {
                    insertValuesStepN = insertValuesStepN.values(record.intoList());
                }

                insertValuesStepN.onDuplicateKeyUpdate().set(DSL.field(DSL.name(modifiedField)), (Object) DSL.field("VALUES({0})", DSL.field(DSL.name(modifiedField)))).execute();
            }
        } catch (Exception e) {
            logger.error("Fail to update dataset table {0}, ", dsTable, e);
            throw new RuntimeException("Fail to update dataset table " + dsTable);
        }

        logger.info("Update dataset table {0} successfully", dsTable);
        return true;
    }

    /**
     * 批量修改
     * @param batchUpdateData
     */
    @TaskCheck(objectId = "#batchUpdateData.tableName")
    public void doBatchUpdate(BatchUpdateDataDTO batchUpdateData) {

        String tableName = batchUpdateData.getTableName();

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserInfo userInfo = new UserInfo(authentication);

        String taskId = UUID.randomUUID().toString().replace("-", "");
        TaskDAO taskDAO = new TaskDAO(taskId, TaskTypeEnum.BATCH_MODIFY_PT, tableName, TaskStatusEnum.RUNNING, userInfo.getUserId());
        TaskRepository.insertTask(taskDAO);

        batchUpdateData.setTaskId(taskId);

        batchUpdateData.setAuthentication(authentication);

        disruptorTaskService.sendNotify(EventTypeEnum.BATCH_UPDATE_TABLE, batchUpdateData);
    }

}
