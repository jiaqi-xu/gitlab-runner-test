package com.guandata.atlas.service;

import com.google.common.collect.Lists;
import com.guandata.atlas.dao.FieldInfoDO;
import com.guandata.atlas.enums.ErrorCode;
import com.guandata.atlas.exception.AtlasException;
import com.guandata.atlas.mapper.FieldMapper;
import com.guandata.atlas.service.planningtable.model.card.entity.Card;
import com.guandata.atlas.service.planningtable.model.field.entity.Field;
import com.guandata.atlas.service.planningtable.model.page.entity.Page;
import com.guandata.atlas.service.planningtable.repository.CardRepository;
import com.guandata.atlas.service.planningtable.repository.FieldRepository;
import com.guandata.atlas.service.planningtable.repository.PageRepository;
import com.guandata.atlas.service.planningtable.vo.FieldInfoVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class FieldService {
    private static final Logger logger = LoggerFactory.getLogger(FieldService.class);

    @Autowired
    private FieldMapper fieldMapper;

    public int insertFields(List<FieldInfoDO> fieldsInfo) {
        return fieldMapper.insertFields(fieldsInfo);
    }

    public int deleteFields(String cdId, List<FieldInfoDO> fieldInfos) {
        List<String> fdIdList = fieldInfos.stream().map(f -> f.getFdId()).collect(Collectors.toList());
        return fieldMapper.deleteFields(cdId, fdIdList);
    }

    /**
     * 批量修改时选择满足条件的列
     * @param cdId
     * @param fieldId
     * @return
     */
    public List<FieldInfoVO> getReplaceableFields(String cdId, String fieldId) {

        Card card = CardRepository.getCardByCardId(cdId);
        if (card == null) {
            throw new AtlasException(ErrorCode.PARAME_ERROR_CODE);
        }

        Field field = FieldRepository.getFieldByCardAndFdId(card, fieldId);
        if (field == null) {
            throw new AtlasException(ErrorCode.PARAME_ERROR_CODE);
        }

        Page page = PageRepository.getPageByPageId(card.getPage().getPgId());
        if (page == null) {
            throw new AtlasException(ErrorCode.PARAME_ERROR_CODE);
        }

        List<Field> replaceableFields = card.getReplaceableFields(field);
        if (CollectionUtils.isEmpty(replaceableFields)) {
            return Lists.newArrayList();
        }

        return replaceableFields.stream().map(s->new FieldInfoVO(s)).collect(Collectors.toList());
    }
}
