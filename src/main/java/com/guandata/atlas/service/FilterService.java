package com.guandata.atlas.service;

import com.google.common.collect.Lists;
import com.guandata.atlas.controller.request.FilterDataGetRequest;
import com.guandata.atlas.enums.ErrorCode;
import com.guandata.atlas.exception.AtlasException;
import com.guandata.atlas.pojo.FilterInfo;
import com.guandata.atlas.pojo.PTFilterInfo;
import com.guandata.atlas.service.dataset.model.valueobj.FilterDataDP;
import com.guandata.atlas.service.planningtable.model.card.entity.Card;
import com.guandata.atlas.service.planningtable.model.page.entity.Page;
import com.guandata.atlas.service.planningtable.model.valueobj.LimitDP;
import com.guandata.atlas.service.planningtable.repository.CardRepository;
import com.guandata.atlas.service.planningtable.repository.PageRepository;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;

@Service
public class FilterService {
    private static final Logger logger = LoggerFactory.getLogger(FilterService.class);

    /**
     * 获取filter的数据源
     * @param request
     * @return
     */
    public List<PTFilterInfo> getFilterData(FilterDataGetRequest request) {
        request.reBuildConditions();

        List<FilterInfo> filterInfos = request.getConditionsInfo();

        if (StringUtils.isBlank(request.getPgId())) {
            request.setPgId(request.getFilters().get(0).getPgId());
        }

        if (StringUtils.isBlank(request.getPgId())){
            String cdId = request.getCdId();
            Card card = CardRepository.getCardByCardId(cdId);
            request.setPgId(card.getPage().getPgId());
        }

        Page page = PageRepository.getPageByPageId(request.getPgId(), filterInfos);
        if (page == null) {
            throw new AtlasException(ErrorCode.PARAME_ERROR_CODE);
        }

        List<FilterInfo> filters = request.getFilters();
        if (CollectionUtils.isEmpty(filters) || filters.size() >1 ) {
            throw new AtlasException(ErrorCode.PARAME_ERROR_CODE);
        }

        LimitDP limitDP = new LimitDP(1L, Long.MAX_VALUE);
        FilterDataDP filterData = page.getFilterData(request);
        return Lists.newArrayList(new PTFilterInfo(limitDP, filters.get(0), filterData));
    }
}
