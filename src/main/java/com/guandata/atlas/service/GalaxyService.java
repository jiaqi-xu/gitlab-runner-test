package com.guandata.atlas.service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.guandata.atlas.common.utils.GalaxyUtil;
import com.guandata.atlas.dao.AccountEntity;
import com.guandata.atlas.dao.KanbanEntity;
import com.guandata.atlas.dto.galaxy.Selector;
import com.guandata.atlas.exception.AtlasException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class GalaxyService {
    private static final Logger logger = LoggerFactory.getLogger(GalaxyService.class);

    @Resource
    private KanbanService kanbanService;

    @Resource
    private AccountService accountService;

    /**
     * 获取BI user token
     * @param accountEntity
     * @return
     */
    public String getUIdTokenByAccount(AccountEntity accountEntity) {
        JSONObject authConfig = JSON.parseObject(accountEntity.getAuthConfig());
        String privateKey = authConfig.getString("privateKey");
        String baseUrl = authConfig.getString("address");
        String domainId = authConfig.getString("domainId");
        String ssoToken = GalaxyUtil.getSSOToken(
                privateKey, domainId, authConfig.getString("username"));
        JSONObject responseObj = GalaxyUtil.getLoginTokenBySSOToken(baseUrl, ssoToken, domainId);
        if (responseObj.containsKey("token")) {
            return responseObj.getString("token");
        } else if (responseObj.containsKey("error_message")) {
            throw new AtlasException(responseObj.getString("error_message"));
        } else {
            throw new AtlasException("BI Service currently is not available.");
        }
    }

    /**
     * 获取BI页面筛选器
     * @param id kanban id
     * @return
     */
    public List<Selector> getPageSelectors(int id) {
        KanbanEntity kanban = kanbanService.getById(id);
        AccountEntity accountEntity = accountService.get(kanban.getAccountId());
        JSONObject authConfig = JSON.parseObject(accountEntity.getAuthConfig());
        String uIdToken = getUIdTokenByAccount(accountEntity);
        JSONObject pageObj = GalaxyUtil.getBIPage(authConfig.getString("address"), kanban.getObjectId(), uIdToken);

        return pageObj.getJSONArray("cards").toJavaList(JSONObject.class)
                .stream().filter(card -> card.getString("cdType").equals("SELECTOR"))
                .map(selector -> selector.toJavaObject(Selector.class)).collect(Collectors.toList());
    }
}
