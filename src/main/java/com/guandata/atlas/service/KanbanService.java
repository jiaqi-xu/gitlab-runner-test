package com.guandata.atlas.service;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.service.IService;
import com.guandata.atlas.dao.KanbanEntity;
import com.guandata.atlas.dto.Kanban;
import com.guandata.atlas.exception.AtlasException;

import java.io.IOException;
import java.util.List;

public interface KanbanService extends IService<KanbanEntity> {

    /**
     * order items in kanban
     * @param kanbanList item order map
     * @return true if order finished successfully; otherwise false
     */
    Boolean order(List<KanbanEntity> kanbanList);

    /**
     * add a new kanban item
     * @param kanban
     * @param userId
     * @return
     */
    Boolean insert(KanbanEntity kanban, String userId);

    /**
     * list kanban items by type
     * @param type
     * @return list of kanban items
     */
    List<Kanban> list(String type, String userId);

    /**
     * the number of kanban items which are associated with the account
     * @param accountId
     * @return
     */
    int countByAccount(int accountId);


    /**
     * delete kanban item
     * @param id item id
     * @param userId user id
     * @return true if deleted; otherwise return false
     */
    Boolean softDelete(long id, String userId);

    /**
     * update kanban item
     * @param kanbanEntity modified item used for updated
     * @param userId user id
     * @return  true if updated; otherwise return false
     */
    Boolean update(KanbanEntity kanbanEntity, String userId);

    /**
     * infomation for single sign on
     * @param id kanban item id
     * @return
     * @throws AtlasException
     */
    JSONObject getSSOInfo(int id) throws AtlasException, IOException;
}
