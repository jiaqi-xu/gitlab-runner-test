package com.guandata.atlas.service;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.common.collect.Lists;
import com.guandata.atlas.common.constants.Constants;
import com.guandata.atlas.dao.CardInfoDO;
import com.guandata.atlas.dto.*;
import com.guandata.atlas.dto.dataset.DSColumn;
import com.guandata.atlas.mapper.CardMapper;
import com.guandata.atlas.mapper.DataSetMapper;
import com.guandata.atlas.service.planningtable.model.card.entity.Card;
import com.guandata.atlas.service.planningtable.repository.CardRepository;
import org.apache.commons.lang3.StringUtils;
import org.jooq.*;
import org.jooq.impl.DSL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class ModifyHistoryService {
    private static final Logger logger = LoggerFactory.getLogger(ModifyHistoryService.class);

    @Autowired
    private CardService cardService;

    @Autowired
    private DataSetService dataSetService;

    @Autowired
    private DataSetMapper dataSetMapper;

    @Autowired
    private CardMapper cardMapper;

    @Autowired
    private DSLContext dslContext;

    @Value("${spring.datasource.dbname}")
    private String dbname;

    /**
     * 修改成功，记录修改行为和明细，错误信息为null
     *
     * @param dsTable
     * @param param
     * @param resultMapList
     * @param status
     * @throws Exception
     */
    public void insert(String dsTable, DataUpdateParam param, List<Map<String, Object>> resultMapList, boolean status) {
        Long modifyId = processModifyHistory(dsTable, param, status, null);

        for (Map<String, Object> map : resultMapList) {
            map.put("modify_id", modifyId);
            map.put("column_id", param.getField().getColumnId());
        }

        try {
            // insert into modify history detail
            logger.info("Total {} rows to insert: ", resultMapList.size());
            List<Field<Object>> fields = resultMapList.get(0).keySet().stream().map(f -> DSL.field(DSL.name(f))).collect(Collectors.toList());
            List<List<Map<String, Object>>> partitionResults = Lists.partition(resultMapList, Constants.BATCH_SIZE);
            for (List<Map<String, Object>> partitionResult : partitionResults) {
                InsertValuesStepN insertValuesStepN = dslContext.insertInto(DSL.table(DSL.name(dsTable + "_modify_history_detail"))).columns(fields);
                for (Map<String, Object> stringObjectMap : partitionResult) {
                    insertValuesStepN = insertValuesStepN.values(stringObjectMap.values());
                }
                insertValuesStepN.execute();
            }
        } catch (Exception e) {
            logger.error("Fail to insert modify detail table {0}_modify_history_detail", dsTable);
            throw new RuntimeException("Fail to insert modify detail table " + dsTable + "_modify_history_detail");
        }

        logger.info("Insert into modify detail table {0}_modify_history_detail successfully", dsTable);
    }

    /**
     * 修改失败，在修改记录中记录错误信息
     *
     * @param param
     * @param message
     * @param status
     * @throws Exception
     */
    @Transactional(rollbackFor = Exception.class)
    public void insert(DataUpdateParam param, String message, boolean status) {
        String dsTable = CardRepository.getCardByCardId(param.getCdId()).getPage().getDataSet().getTableName();

        processModifyHistory(dsTable, param, status, message);
    }

    /**
     * 记录修改行为
     *
     * @param dsTable
     * @param param
     * @param status
     * @param message
     * @return
     * @throws Exception
     */
    public Long processModifyHistory(String dsTable, DataUpdateParam param, boolean status, String message) {
        String pgId = param.getPgId();
        if (StringUtils.isBlank(pgId)) {
            Card card = CardRepository.getCardByCardId(param.getCdId());
            pgId = card.getPage().getPgId();
        }

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserInfo userInfo = new UserInfo(authentication);
        String userId = userInfo.getUserId();
        String userName = userInfo.getUserName();

        // convert object to json
        ModifyDataObject dataObject = new ModifyDataObject(param.getAggrPK(), param.getConditions());
        String object = JSONObject.toJSONString(dataObject);

        try {
            dslContext.insertInto(DSL.table(DSL.name(dsTable + "_modify_history")))
                    .columns(DSL.field("cd_id"), DSL.field("pg_id"), DSL.field("user_id"),
                            DSL.field("user_name"), DSL.field("apply_status"), DSL.field("data_object"),
                            DSL.field("column_id"), DSL.field("modified_from"), DSL.field("modified_to"), DSL.field("message"))
                    .values(param.getCdId(), pgId, userId, userName, status, object, param.getField().getColumnId(),
                            param.getModifiedFrom(), param.getModifiedTo(), message).execute();
        } catch (Exception e) {
            logger.error("Fail to insert into modify history table {0}_modify_history", dsTable, e);
            throw new RuntimeException("Fail to insert into modify history table " + dsTable + "_modify_history");
        }

        logger.info("Insert into modify history table {0}_modify_history successfully", dsTable);
        return dslContext.lastID().longValue();
    }

    /**
     * 记录修改行为
     *
     * @param dsTable
     * @param param
     * @param records
     * @return
     * @throws Exception
     */
    public Long batchProcessModifyHistory(String dsTable, DataUpdateParamExt param,
                                          List<List<Record>> records, List<List<Map<String, Object>>> modifyFieldRecords) {
        String cdId = param.getCdId();

        String pgId = param.getPgId();
        if (StringUtils.isBlank(pgId)) {
            Card card = CardRepository.getCardByCardId(param.getCdId());
            pgId = card.getPage().getPgId();
        }

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserInfo userInfo = new UserInfo(authentication);
        String userId = userInfo.getUserId();
        String userName = userInfo.getUserName();

        // convert object to json
        ModifyDataObject dataObject = new ModifyDataObject(param.getAggrPK(), param.getConditions());
        String object = JSONObject.toJSONString(dataObject);

        try {
            for (int i = 0; i < param.getModifiedFroms().size(); i++) {
                InsertValuesStep10 insertValuesStep10 = dslContext.insertInto(DSL.table(DSL.name(dsTable + "_modify_history")))
                        .columns(DSL.field("cd_id"), DSL.field("pg_id"), DSL.field("user_id"),
                                DSL.field("user_name"), DSL.field("apply_status"), DSL.field("data_object"),
                                DSL.field("column_id"), DSL.field("modified_from"), DSL.field("modified_to"), DSL.field("message"));
                insertValuesStep10.values(cdId, pgId, userId, userName, true, object, param.getField().getColumnId(),
                        param.getModifiedFroms().get(i), param.getModifiedTos().get(i), null);
                insertValuesStep10.execute();
                long modifyId = dslContext.lastID().longValue();

                // insert into modify history detail
                List<String> columnIds = Lists.newArrayList();
                Field<?>[] fields = records.get(i).get(0).fields();
                for (Field<?> field : fields) {
                    columnIds.add(field.getName());
                }
                columnIds.addAll(Arrays.asList(new String[]{"modify_id", "column_id", "modified_from","modified_to"}));

                List<Field<Object>> allFields = Lists.newArrayList();
                for (String columnId : columnIds) {
                    allFields.add(DSL.field(DSL.name(columnId)));
                }
                List<List<Record>> partitionRecords = Lists.partition(records.get(i), Constants.BATCH_SIZE);
                List<List<Map<String, Object>>> modifyFieldRecord = Lists.partition(modifyFieldRecords.get(i), Constants.BATCH_SIZE);
                for (int ii = 0; ii < partitionRecords.size(); ii++) {
                    List<Record> partitionResult = partitionRecords.get(ii);
                    List<Map<String, Object>> fieldModifyRecord = modifyFieldRecord.get(ii);
                    InsertValuesStepN insertValuesStepN = dslContext.insertInto(DSL.table(DSL.name(dsTable + "_modify_history_detail")))
                            .columns(allFields);
                    for (int jj = 0; jj < partitionResult.size(); jj++) {
                        List<Object> values = new ArrayList<>(partitionResult.get(jj).intoList());
                        values.add(modifyId);
                        values.add(param.getField().getColumnId());
                        values.add(fieldModifyRecord.get(jj).get("modified_from"));
                        values.add(fieldModifyRecord.get(jj).get("modified_to"));
                        insertValuesStepN.values(values);
                    }
                    insertValuesStepN.execute();
                }
            }

        } catch (Exception e) {
            logger.error("Fail to insert into modify history table {0}_modify_history", dsTable, e);
            throw new RuntimeException("Fail to insert into modify history table " + dsTable + "_modify_history");
        }

        logger.info("Insert into modify history table {0}_modify_history successfully", dsTable);
        return dslContext.lastID().longValue();
    }

    /**
     * 查看修改行为记录
     *
     * @param page
     * @param param
     * @param dsId
     * @return
     */
    public TableResult getModifyAction(Page<Map<String, Object>> page, ModificationCheckParam param, int dsId) {
        String dsTable = dataSetService.getDataSetInfo(dsId).getTableName();
        List<FilterCondition> modifyActionFilter = param.getActionFilter();
        List<FilterCondition> detailFilter = param.getDetailFilter();
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        boolean hasDetailFilter = true;
        if (detailFilter.size() == 0) hasDetailFilter = false;

        List<Long> modifyIDs = dataSetService.getModifyIDs(detailFilter, dsTable);
        List<Map<String, Object>> resultList = dataSetMapper.getModifyActions(page, modifyActionFilter, modifyIDs, hasDetailFilter, dsTable);
        List<String> cardIdList = resultList.stream().filter(r -> r.get("cd_id") != null).map(e -> e.get("cd_id").toString()).distinct().collect(Collectors.toList());
        Map<String, String> cardHash = getCardName(cardIdList);

        Page<Map<String, Object>> res = page.setRecords(resultList);

        List<DSColumn> cols = Arrays.asList(new DSColumn(null, "id", null),
                                            new DSColumn(null, "cd_id", null),
                                            new DSColumn(null, "card_name", null),
                                            new DSColumn(null, "user_id", null),
                                            new DSColumn(null, "user_name", null),
                                            new DSColumn(null, "modify_time", null),
                                            new DSColumn(null, "apply_status", null),
                                            new DSColumn(null, "data_object", null),
                                            new DSColumn(null, "column_id", null),
                                            new DSColumn(null, "modified_from", null),
                                            new DSColumn(null, "modified_to", null));

        ArrayList<List<ResultData>> table = new ArrayList<>();

        for (Map entry : res.getRecords()) {
            List<ResultData> record = new ArrayList<>();
            String cardId = entry.get("cd_id") != null? entry.get("cd_id").toString(): null;
            for (DSColumn col : cols) {
                ResultData data;
                if (col.getName().equals("card_name")) {
                    data = new ResultData(cardHash.getOrDefault(cardId, null));
                } else if (col.getName().equals("modify_time")) {
                    data = new ResultData(df.format(entry.get(col.getName())));
                } else {
                    data = new ResultData(String.valueOf(entry.get(col.getName())));
                }
                record.add(data);
            }
            table.add(record);
        }

        return new TableResult(cols, table, res.getTotal(), table.size(), res.getCurrent(), res.getPages());
    }

    /**
     * 查看修改记录明细
     *
     * @param page
     * @param param
     * @param modifyId
     * @param status
     * @param dsId
     * @return
     */
    public TableResult getDetails(Page<Map<String, Object>> page, ModificationCheckParam param, Long modifyId, boolean status, int dsId) {
        String dsTable = dataSetService.getDataSetInfo(dsId).getTableName();
        List<FilterCondition> detailFilter = param.getDetailFilter();

        Page<Map<String, Object>> res;
        List<DSColumn> cols = new ArrayList<>();

        if (status) {
            // 修改成功的明细记录
            List<String> columns = dataSetMapper.getColumns(dbname, dsTable);
            res = page.setRecords(dataSetMapper.getDetails(page, columns, detailFilter, modifyId, dsTable));

            for (String column : columns) {
                cols.add(new DSColumn(null, column, null));
            }
        } else {
            // 修改失败的错误信息
            cols = Arrays.asList(new DSColumn(null, "modify_id", null),
                                 new DSColumn(null, "message", null));
            res = page.setRecords(dataSetMapper.getExceptionById(modifyId, dsTable));
        }

        ArrayList<List<ResultData>> table = new ArrayList<>();

        for (Map entry : res.getRecords()) {
            List<ResultData> record = new ArrayList<>();
            for (DSColumn col : cols) {
                ResultData data = new ResultData(String.valueOf(entry.get(col.getName())));
                record.add(data);
            }
            table.add(record);
        }

        return new TableResult(cols, table, res.getTotal(), table.size(), res.getCurrent(), res.getPages());

    }

    public Map<String, String> getCardName(List<String> cardIdList) {
        Map<String, String> cardHash = new HashMap<>();

        if (!cardIdList.isEmpty()) {
            // cardIdList is not null, get card info
            List<CardInfoDO> cardInfoList = cardMapper.queryCardById(cardIdList);

            for (CardInfoDO card : cardInfoList) {
                cardHash.put(card.getCdId(), card.getName());
            }
        }

        return cardHash;
    }
}
