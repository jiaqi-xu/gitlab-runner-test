package com.guandata.atlas.service;

import com.google.common.collect.Lists;
import com.guandata.atlas.common.constants.Constants;
import com.guandata.atlas.dao.CardInfoDO;
import com.guandata.atlas.dao.PageInfoDO;
import com.guandata.atlas.enums.ErrorCode;
import com.guandata.atlas.exception.AtlasException;
import com.guandata.atlas.mapper.CardMapper;
import com.guandata.atlas.mapper.PageMapper;
import com.guandata.atlas.pojo.PageInfo;
import com.guandata.atlas.service.planningtable.enums.PageStatusEnum;
import com.guandata.atlas.service.planningtable.model.page.entity.Page;
import com.guandata.atlas.service.planningtable.repository.PageRepository;
import com.guandata.atlas.service.user.model.User;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class PageService {
    private static final Logger logger = LoggerFactory.getLogger(PageService.class);

    @Autowired
    private PageMapper pageMapper;

    @Autowired
    private CardMapper cardMapper;

    @Autowired
    private UserService userService;

    /**
     * 初始化page，赋予pgId后返回page实例
     * @param pgInfo
     * @return
     */
    public PageInfo init(PageInfo pgInfo) {
        if (isNameExisted(pgInfo.getName(), pgInfo.getPgId())) {
            throw new AtlasException("页面名称已存在");
        }
        pgInfo.setStatus(Constants.INIT);

        Page page = new Page(pgInfo);
        page.savePage();

        return new PageInfo(page);
    }

    @Transactional(rollbackFor = Exception.class)
    public int insertOrUpdatePage(PageInfo pageInfo){
        pageInfo.setStatus(PageStatusEnum.NORMAL.getStatus());
        Page page = new Page(pageInfo);
        page.storeMySelf();

        return 1;
    }

    public PageInfo getPageInfoVOById(String pageId) {
        Page page = PageRepository.getPageByPageId(pageId);
        if (page == null) {
            throw new AtlasException(ErrorCode.PT_PAGE_NOT_EXSITED);
        }

        page.fillAllFilters().fillAllFiltersDefaultValues().fillAllCards(true, true);

        return new PageInfo(page);
    }

    public List<PageInfoDO> list(String userId) {
        User user = userService.getUser(userId);
        List<String> pageIdList = user.fillUserOwnedResources().getAllResources(Constants.PT_PAGE)
                .stream().map(resource -> resource.getId()).collect(Collectors.toList());
        if (CollectionUtils.isEmpty(pageIdList)) {
            return Lists.newArrayList();
        }
        return pageMapper.list(pageIdList);
    }

    public int deletePage(String pageId) {
        return pageMapper.deletePage(pageId);
    }

    /**
     * 判断是否有页面存在相同的名称
     *
     * @param name
     * @param pgId
     * @return
     */
    public Boolean isNameExisted(String name, String pgId) {
        PageInfoDO page = pageMapper.getPageByName(name.trim());
        return (pgId == null && page != null) || (page != null && !page.getPgId().equals(pgId));
    }

    /**
     * 获取修改记录页面page/card筛选器
     *
     * @param dsId
     * @return
     */
    public List<PageInfoDO> getPageInfoFilter(int dsId) {
        List<PageInfoDO> pgList = pageMapper.getPageByDsId(dsId);
        if(CollectionUtils.isEmpty(pgList)) {
            return Lists.newArrayList();
        }

        List<String> pgIdList = pgList.stream().map(PageInfoDO::getPgId).collect(Collectors.toList());

        List<CardInfoDO> cdList = cardMapper.getCardByPages(pgIdList);
        Map<String, List<CardInfoDO>> cdHash = new HashMap<>();
        for (CardInfoDO card : cdList) {
            String pgId = card.getPgId();
            List<CardInfoDO> cardList = cdHash.getOrDefault(pgId, new ArrayList<>());
            cardList.add(card);
            cdHash.put(pgId, cardList);
        }

        pgList.forEach(p -> p.setCards(cdHash.get(p.getPgId())));
        return pgList;
    }

    public Boolean isPageExisted(String pgId) {
        return pageMapper.isPageExisted(pgId) > 0;
    }
}
