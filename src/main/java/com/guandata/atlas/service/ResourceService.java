package com.guandata.atlas.service;

import com.guandata.atlas.dto.GrantResource;
import com.guandata.atlas.service.resource.dto.ResourceDTO;
import com.guandata.atlas.service.resource.model.ATResource;
import com.guandata.atlas.service.resource.repository.ResourceRepository;
import com.guandata.atlas.service.user.model.Group;
import com.guandata.atlas.service.user.model.User;
import org.apache.commons.collections4.CollectionUtils;
import org.jooq.DSLContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ResourceService {
    private static final Logger logger = LoggerFactory.getLogger(ResourceService.class);

    @Autowired
    private UserService userService;

    @Autowired
    private DSLContext dslContext;


    /**
     * 资源列表
     * @param type 资源类型， e.g. PAGE, FORM, PT_PAGE, NULL
     * @return
     */
    public List<ATResource> listGlobalResource(String type) {
       return ResourceRepository.listGlobalResource(type);
    }

    /**
     * 将资源授权给用户或者用户组
     * @param grantResource
     */
    @Transactional(rollbackFor = Exception.class)
    public void grantResourceToOwners(GrantResource grantResource) {
        ResourceDTO resource = grantResource.getResource();
        if (!CollectionUtils.isEmpty(grantResource.getUserIdList())) {
            ResourceRepository.grantResourceToUsers(resource, grantResource.getUserIdList());
        }
        if (!CollectionUtils.isEmpty(grantResource.getGroupIdList())) {
            ResourceRepository.grantResourceToGroups(resource, grantResource.getGroupIdList());
        }
    }

    /**
     * 移除用户或者用户组所拥有的资源
     * @param revokeResource
     */
    @Transactional(rollbackFor = Exception.class)
    public void revokeResourceFromOwners(GrantResource revokeResource) {
        ResourceDTO resource = revokeResource.getResource();
        if (!CollectionUtils.isEmpty(revokeResource.getUserIdList())) {
            ResourceRepository.revokeResourceFromUsers(resource, revokeResource.getUserIdList());
        }
        if (!CollectionUtils.isEmpty(revokeResource.getGroupIdList())) {
            ResourceRepository.revokeResourceFromGroups(resource, revokeResource.getGroupIdList());
        }
    }

    /**
     * 获取拥有某资源权限的用户列表
     * @param resourceId 资源id
     * @return
     */
    public List<User> getResourceUsers(String resourceId) {
        List<User> users = userService.list(null, null, null);
        List<String> uIdList = ResourceRepository.getResourceUsers(resourceId);
        return users.stream().filter(
                user -> uIdList.contains(user.getUId()) || user.isAdminUser()
        ).collect(Collectors.toList());
    }

    /**
     * 获取拥有某资源权限的用户列表
     * @param resourceId 资源id
     * @return
     */
    public List<Group> getResourceGroups(String resourceId) {
        List<Group> groups = userService.getGroups(null);
        List<String> groupIdList = ResourceRepository.getResourceGroups(resourceId);
        return groups.stream().filter(
                group -> groupIdList.contains((group.getGroupId()))
        ).collect(Collectors.toList());
    }

    /**
     * 获取未激活的资源列表，e.g. init状态的PT page
     * @param type 资源类型
     * @return
     */
    public List<ATResource> listGlobalUnactivatedResource(String type) {
       return ResourceRepository.listGlobalUnactivatedResource(type);
    }
}
