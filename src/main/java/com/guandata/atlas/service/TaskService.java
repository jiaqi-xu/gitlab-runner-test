package com.guandata.atlas.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.guandata.atlas.dao.TaskDAO;
import com.guandata.atlas.enums.TaskStatusEnum;
import com.guandata.atlas.mapper.TaskMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TaskService {
    private static final Logger logger = LoggerFactory.getLogger(TaskService.class);

    @Autowired
    private TaskMapper taskMapper;

    /**
     * 新建初始化任务
     * @param taskDAO
     * @return
     */
    public int initTask(TaskDAO taskDAO) {
        return taskMapper.insert(taskDAO);
    }

    /**
     * 更新任务信息, 备注：根据要更新的信息, e.g. status, failure_message, end_time, etc.建立task对象
     * @param taskDAO
     * @return
     */
    public int updateTask(TaskDAO taskDAO) {
        UpdateWrapper<TaskDAO> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("task_id", taskDAO.getTaskId());
        return taskMapper.update(taskDAO, updateWrapper);
    }

    /**
     * 获取task状态
     * @param taskType 任务类型
     * @param objectId 任务业务对象id
     * @param status 任务运行状态
     * @return
     */
    public List<TaskDAO> getTaskList(String taskType, String objectId, Integer status) {
        QueryWrapper<TaskDAO> queryWrapper = new QueryWrapper<>();
        queryWrapper.select(
                "id", "task_id", "task_type", "object_id", "status", "failure_cause", "start_time", "end_time", "creator_id")
        .orderByDesc("id");

        if (objectId != null) {
            queryWrapper.eq("object_id", objectId);
        }
        queryWrapper.eq("task_type", taskType);
        if(status != null) {
            queryWrapper.eq("status", status);
        }

        List<TaskDAO> taskDAOS = taskMapper.selectList(queryWrapper);
        taskDAOS.stream().forEach(taskDAO -> {
            taskDAO.setStatusName(TaskStatusEnum.getStatusName(taskDAO.getStatus()));
        });
        return taskDAOS;
    }

    /**
     * 获取特定类型的任务的最近一次运行状态
     * @param taskType
     * @param objectId
     * @return
     */
    public TaskDAO getLatestState(String taskType, String objectId) {
        QueryWrapper<TaskDAO> queryWrapper = new QueryWrapper<>();
        queryWrapper.select(
                "id", "task_id", "task_type", "object_id", "status", "failure_cause", "start_time", "end_time", "creator_id")
                .orderByDesc("id");
        queryWrapper.eq("object_id", objectId);
        queryWrapper.eq("task_type", taskType);
        queryWrapper.orderByDesc("id");
        Optional<TaskDAO> optTaskDAOS = taskMapper.selectList(queryWrapper).stream().findFirst();

        if (optTaskDAOS.isPresent()) {
            TaskDAO taskDAO = optTaskDAOS.get();
            taskDAO.setStatusName(TaskStatusEnum.getStatusName(taskDAO.getStatus()));
            return taskDAO;
        } else {
            return null;
        }
    }
}
