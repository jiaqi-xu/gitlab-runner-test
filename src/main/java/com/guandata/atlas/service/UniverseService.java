package com.guandata.atlas.service;

import com.alibaba.fastjson.JSONObject;
import com.guandata.atlas.common.utils.UniverseUtil;
import com.guandata.atlas.dto.BaseDSDTO;
import com.guandata.atlas.dto.DSDataPreviewDTO;
import com.guandata.atlas.dto.SqlParamDTO;
import com.guandata.atlas.dto.universe.UniverseLoginInDTO;
import com.guandata.atlas.dto.universe.UniverseProjectDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.text.MessageFormat;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UniverseService {
    private static final Logger logger = LoggerFactory.getLogger(UniverseService.class);

    /**
     * 获取universe平台登陆token
     * @param universeLoginInDTO
     * @return
     */
    public String getLoginToken(UniverseLoginInDTO universeLoginInDTO) {
        JSONObject response = UniverseUtil.getLoginToken(
                universeLoginInDTO.getAddress(), universeLoginInDTO.getUsername(), universeLoginInDTO.getPassword());
        return response.getString("data");
    }

    /**
     * 获取universe用户项目列表
     * @param universeLoginInDTO
     * @return
     */
    public List<UniverseProjectDTO> getProjects(UniverseLoginInDTO universeLoginInDTO) {
        String token = getLoginToken(universeLoginInDTO);
        JSONObject response = UniverseUtil.getProjectList(universeLoginInDTO.getAddress(), token);
        return response.getJSONArray("data").toJavaList(UniverseProjectDTO.class);
    }

    /**
     * 获取universe数据集列表
     * @param universeLoginInDTO
     * @param projectId
     * @return
     */
    public List<BaseDSDTO> getDatasets(UniverseLoginInDTO universeLoginInDTO, Integer projectId) {
        String token = getLoginToken(universeLoginInDTO);
        JSONObject response = UniverseUtil.getDataSetList(universeLoginInDTO.getAddress(), token, projectId);
        return response.getJSONArray("data").toJavaList(BaseDSDTO.class);
    }

    /**
     * 获取universe数据集信息
     * @param universeLoginInDTO
     * @param projectId
     * @param datasetId
     * @return
     */
    public BaseDSDTO getDatasetInfo(UniverseLoginInDTO universeLoginInDTO, Integer projectId, Integer datasetId) {
        String token = getLoginToken(universeLoginInDTO);
        JSONObject response = UniverseUtil.getDataSetInfo(universeLoginInDTO.getAddress(), token, projectId, datasetId);
        return response.getJSONObject("data").toJavaObject(BaseDSDTO.class);
    }

    /**
     * 全量导出unicerse数据集数据
     * @param universeLoginInDTO
     * @param datasetId
     * @param sqlParam
     * @return
     */
    public DSDataPreviewDTO exportDataSetData(UniverseLoginInDTO universeLoginInDTO, Integer datasetId, SqlParamDTO sqlParam) {
        String token = getLoginToken(universeLoginInDTO);
        String sql = generateSql(sqlParam);
        JSONObject response = UniverseUtil.getSingleDataSetData(universeLoginInDTO.getAddress(), token, datasetId, sql);
        return response.getJSONObject("data").toJavaObject(DSDataPreviewDTO.class);
    }

    /**
     * 获取Universe数据集快照文件路径
     * @param universeLoginInDTO
     * @param projectId
     * @param datasetId
     * @param sqlParam
     * @return
     */
    public String getDataSetDataSnapShotPath(UniverseLoginInDTO universeLoginInDTO, Integer projectId, Integer datasetId, SqlParamDTO sqlParam) {
        String token = getLoginToken(universeLoginInDTO);
        String sql = generateSql(sqlParam);
        JSONObject response = UniverseUtil.getSingleDataSetDataPreInvoke(
                universeLoginInDTO.getAddress(), token, projectId, datasetId, sql);
        return response.getString("data");
    }


    /**
     * 分页获取universe数据集数据
     * @param universeLoginInDTO
     * @param filePath
     * @param projectId
     * @param datasetId
     * @param offset
     * @param limit
     * @return
     */
    public DSDataPreviewDTO exportDataSetDataByPagination(UniverseLoginInDTO universeLoginInDTO, String filePath,
                                                    Integer projectId, Integer datasetId,
                                                    Integer offset, Integer limit) {
        String token = getLoginToken(universeLoginInDTO);
        JSONObject response = UniverseUtil.getSingleDataSetDataPostInvoke(
                universeLoginInDTO.getAddress(), token, projectId, datasetId, filePath, offset, limit);
        return response.getJSONObject("data").toJavaObject(DSDataPreviewDTO.class);
    }

    /**
     * 生成获取universe数据集的sql
     * @param params
     * @return
     */
    public String generateSql(SqlParamDTO params) {
        List<String> cols = params.getSelect().stream().map(col -> {
            if (!col.equals("*")) {
                col = "`" + col + "`";
            }
            return col;
        }).collect(Collectors.toList());
        String select = String.join(", ", cols);
        String limit = params.getLimit() != null? "limit " + params.getLimit().toString(): "";
        MessageFormat sqlForm = new MessageFormat("select {0} from input {1}");
        String[] args = new String[]{select, limit};
        return sqlForm.format(args);
    }

    /**
     * 将universe数据集数据中Boolean类型字段进行转换
     * @param indexes 字段索引列表
     * @param preview 字段值列表
     * @return
     */
    public List<Object> processBooleanColumn(List<Integer> indexes, List<Object> preview) {
        indexes.stream().forEach(index -> {
            if (preview.get(index).toString().equalsIgnoreCase("TRUE")) {
                preview.set(index, true);
            } else {
                preview.set(index, false);
            }
        });
        return preview;
    }
}
