package com.guandata.atlas.service;

import com.guandata.atlas.dto.UserInfo;
import com.guandata.atlas.service.user.model.operate.ModifyPasswordDTO;
import com.guandata.atlas.service.user.dto.UserDTO;
import com.guandata.atlas.service.user.model.Group;
import com.guandata.atlas.service.user.model.Role;
import com.guandata.atlas.service.user.model.User;
import com.guandata.atlas.service.user.repository.GroupRepository;
import com.guandata.atlas.service.user.repository.RoleRepository;
import com.guandata.atlas.service.user.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;


@Service
public class UserService {
    private static final Logger logger = LoggerFactory.getLogger(UserService.class);

    @Autowired
    private DataSetService dataSetService;

    @Autowired
    private ResourceService resourceService;


    /********************************** USER **********************************/

    /**
     * 创建并返回用户
     * @param userDTO
     */
    public User createUser(UserDTO userDTO) {
        String uId = UserRepository.createUser(new User(userDTO));
        RoleRepository.addRolesToUser(uId, userDTO.getRoles());
        GroupRepository.addGroupsToUser(uId, userDTO.getGroups());

        return getUser(uId);
    }

    /**
     * 删除用户(非软删除)
     * @param uId
     * @return
     */
    public Boolean deleteUser(String uId) {
        UserRepository.deleteUser(uId);
        return true;
    }

    /**
     * 更新用户，包括用户角色，用户所属用户组
     * @param uId
     * @param userDTO
     */
    public void updateUser(String uId, UserDTO userDTO) {
        UserRepository.updateUser(uId, new User(userDTO));
        RoleRepository.updateRolesForUser(uId, userDTO.getRoles());
        GroupRepository.updateGroupsForUser(uId, userDTO.getGroups());
    }

    /**
     * 重置用户密码
     * @param uId 用户id
     * @param userDTO 必须包含用户password和是否为临时密码
     */
    public void resetPassword(String uId, UserDTO userDTO) {
        UserRepository.resetPassword(uId, new User(userDTO).generateCredential());
    }

    /**
     * 修改个人密码
     * @param uId 用户user id
     * @param modifyPasswordDTO 修改密码对象实体
     */
    public void modifyPasswordByUserSelf(String uId, ModifyPasswordDTO modifyPasswordDTO) {
        UserRepository.validateUserCredential(modifyPasswordDTO.getLoginId(), modifyPasswordDTO.getOldPassword());
        UserRepository.resetPassword(uId, modifyPasswordDTO.generateCredentialRep());
    }

    /**
     * 获取keycloak用户对象
     * @param uId keycloak user id
     * @return
     */
    public User getUser(String uId) {
        User user = UserRepository.getUserById(uId);
        user.fillUserRoles();
        user.fillUserGroups();
        return user;
    }

    /**
     * 获取keycloak 某个域下的用户列表
     * @param search 关键字搜索
     * @param first pagination offset
     * @param max maximum result size
     * @return
     */
    public List<User> list(String search, Integer first, Integer max) {
        List<User> users = UserRepository.getUserList(search, first, max);
        return users.parallelStream().map(user -> user.fillUserRoles().fillUserGroups())
                .collect(Collectors.toList());
    }

    /********************************** ROLE **********************************/

    public List<Role> getRoles() {
        return RoleRepository.list();
    }

    /********************************** GROUP **********************************/

    public List<Group> getGroups(String search) {
        return GroupRepository.list(search);
    }

    public void createGroup(Group group) {
        GroupRepository.createGroup(group);
    }

    public void deleteGroup(String groupId) {
        GroupRepository.deleteGroup(groupId);
    }

    public void renameGroup(Group group) {
        GroupRepository.renameGroup(group);
    }

    public List<User> getGroupMembers(String groupId) {
        List<User> users = GroupRepository.getGroupMembers(groupId);
        return users.parallelStream().map(user -> user.fillUserRoles().fillUserGroups())
                .collect(Collectors.toList());
    }

    public List<User> getAvailableUsersForGroup(String groupId) {
        List<User> users = UserRepository.getUserList(null, null, null);
        List<String> memberIdList = GroupRepository.getGroupMembers(groupId).stream()
                .map(member -> member.getUId()).collect(Collectors.toList());
        return users.parallelStream().filter(user -> !memberIdList.contains(user.getUId()))
                .map(user -> user.fillUserRoles().fillUserGroups()).collect(Collectors.toList());
    }

    public void addGroupMembers(String groupId, List<UserDTO> userDTOList) {
        List<User> members = userDTOList.stream().map(userDTO -> new User(userDTO)).collect(Collectors.toList());
        GroupRepository.addGroupMembers(groupId, members);
    }

    public void removeGroupMember(String groupId, String uId) {
        GroupRepository.removeGroupMember(groupId, uId);
    }


    /**********************************      **********************************/

    /**
     * 通过modify_history获取user info
     * TODO: 通过keycloak获取user
     *
     * @param dsId
     * @return
     */
    public List<UserInfo> getUserFilter(int dsId) {
        String dsTable = dataSetService.getDataSetInfo(dsId).getTableName();
        return dataSetService.getUserFilters(dsTable);
    }

}
