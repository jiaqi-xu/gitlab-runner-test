package com.guandata.atlas.service.account.repository;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.guandata.atlas.dao.TokenDAO;
import com.guandata.atlas.mapper.TokenMapper;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;

@Repository
public class TokenRepository {

    private static TokenMapper tokenMapper;

    @Resource
    public void setTokenMapper(TokenMapper tokenMapper) {
        TokenRepository.tokenMapper = tokenMapper;
    }

    public static int saveToken(TokenDAO tokenDAO) {
        return tokenMapper.insert(tokenDAO);
    }

    public static TokenDAO getToken(String objectId, String objectType) {
        QueryWrapper<TokenDAO> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("object_id", objectId);
        queryWrapper.eq("object_type", objectType);

        return tokenMapper.selectOne(queryWrapper);
    }
}
