package com.guandata.atlas.service.dataset.enums;

public enum DataSetStatusEnum {

    NORMAL(0, "正常状态"),

    DELETED(1, "正常状态"),

    ;

    private Integer status;

    private String desc;

    DataSetStatusEnum(Integer status, String desc){

        this.status = status;
        this.desc = desc;
    }

    public Integer getStatus() {
        return status;
    }

    public String getDesc() {
        return desc;
    }
}
