package com.guandata.atlas.service.dataset.enums;

public enum SourceTypeEnum {

    UNIVERSE("UNIVERSE", "数开平台"),

    GALAXY("GALAXY", "GALAXY平台"),

    ;

    private String sourceType;

    private String desc;

    SourceTypeEnum(String sourceType, String desc){

        this.sourceType = sourceType;
        this.desc = desc;
    }

    public String getSourceType() {
        return sourceType;
    }

    public String getDesc() {
        return desc;
    }

    public static SourceTypeEnum getBySourceType(String sourceType) {
        if (sourceType == null) {
            return null;
        }

        for(SourceTypeEnum sourceTypeEnum: SourceTypeEnum.values()) {
            if (sourceType.equals(sourceTypeEnum.getSourceType())) {
                return sourceTypeEnum;
            }
        }
        return null;
    }
}
