package com.guandata.atlas.service.dataset.model;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.guandata.atlas.common.utils.AutowiredWrapperUtil;
import com.guandata.atlas.dao.DataSetDAO;
import com.guandata.atlas.dto.dataset.DSColumn;
import com.guandata.atlas.dto.dataset.DSExt;
import com.guandata.atlas.pojo.FilterInfo;
import com.guandata.atlas.service.dataset.enums.DataSetStatusEnum;
import com.guandata.atlas.service.dataset.enums.SourceTypeEnum;
import com.guandata.atlas.service.dataset.model.valueobj.ColumnListDP;
import com.guandata.atlas.service.dataset.model.valueobj.FilterDataDP;
import com.guandata.atlas.service.dataset.repository.DataSetRepository;
import com.guandata.atlas.service.planningtable.model.filter.entity.Filter;
import com.guandata.atlas.service.planningtable.model.valueobj.LimitDP;
import com.guandata.atlas.service.planningtable.repository.FilterRepository;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.jooq.Condition;
import org.jooq.DSLContext;
import org.jooq.Result;
import org.jooq.impl.DSL;
import org.springframework.util.CollectionUtils;

import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
public class DataSet {
    /** 数据源类型Id **/
    private Integer id;
    /** 数据源展示名称 **/
    private String name;
    /** 数据源所对应的数据表 **/
    private String tableName;

    private DataSetStatusEnum status;

    private SourceTypeEnum sourceType;
    private JSONObject sourceDS;

    private DSExt ext;

    private List<Record> records;

    private List<Record> updateRecords;

    private ColumnListDP columnListDP;

    /** 数据同步public api **/
    private String dataSyncOpenAPI;
    /** 回流SQL **/
    private String backFlowSQL;

    private Date updateTime;

    public DataSet(DataSetDAO dataSetDAO) {
        this.id = dataSetDAO.getId();
        this.name = dataSetDAO.getName();
        this.tableName = dataSetDAO.getTableName();
        this.sourceType = SourceTypeEnum.getBySourceType(dataSetDAO.getSourceType());
        this.sourceDS = JSON.parseObject(dataSetDAO.getSourceDS());

        this.ext = JSON.parseObject(dataSetDAO.getExt(), DSExt.class);

        String definition = dataSetDAO.getDefinition();
        List<DSColumn> dsColumns = JSONArray.parseArray(definition, DSColumn.class);
        dsColumns.sort(Comparator.comparing(DSColumn::getSeqNo));
        this.columnListDP = new ColumnListDP(dsColumns);
        this.updateTime = dataSetDAO.getUpdateTime();
    }

    public FilterDataDP getFilterData(FilterInfo filterInfo,
                                      List<FilterInfo> conditions,
                                      LimitDP limitDP) {
        DSLContext dslContext = AutowiredWrapperUtil.getDslContext();
        Filter filter = FilterRepository.transSingleFilter(filterInfo, null);
        Condition condition = DSL.trueCondition();

        String search = filterInfo.getSearch();
        if (StringUtils.isNotBlank(search)) {
            condition = condition.and(filter.buildLikeCondition(search));
        }

        if (!CollectionUtils.isEmpty(conditions)) {
            List<Filter> conFilters = FilterRepository.transFilters(conditions, null);
            for (Filter filterTmp : conFilters) {
                condition = condition.and(filterTmp.buildAndCondition());
            }
        }

        String sql = dslContext
                .selectDistinct(DSL.field(DSL.name(filter.getDsColumn().getColumnId())).as(DSL.name(filter.getDsColumn().getColumnId())))
                .from(DSL.table(DSL.name(tableName+"_update")))
                .where(condition)
                .limit(limitDP.getOffset(), limitDP.getPageSize())
                .toString();

        Result<org.jooq.Record> result = dslContext.fetch(sql);
        List<String> datas = result.stream().map(s -> {
            Object obj = s.get(filter.getDsColumn().getColumnId());
            return obj == null ? null : String.valueOf(obj);
        }).collect(Collectors.toList());

        return new FilterDataDP(datas, Long.valueOf(datas.size()), datas.size());
    }

    public DataSet fillBackFlowSQL() {
        String backFlowSQL = DataSetRepository.concatGetDataSetDataSQL(this).toString().replace("\"", "`");
        this.setBackFlowSQL(backFlowSQL);
        return this;
    }

    public DataSet fillDataSyncOpenAPI() {
        this.setDataSyncOpenAPI(DataSetRepository.getDataSyncOpenAPI(this.tableName));
        return this;
    }
}
