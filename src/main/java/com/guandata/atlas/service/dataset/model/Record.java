package com.guandata.atlas.service.dataset.model;

import com.guandata.atlas.service.dataset.model.valueobj.Cell;

import java.util.List;
import java.util.Map;

public class Record {

    private DataSet dataSet;

    /**
     * 记录id
     */
    private String recordId;

    /**
     * 每一格的字段和原始值
     */
    private List<Cell> cells;

    /**
     * fieldId:转换好的value数据
     */
    private Map<String, String> values;
}
