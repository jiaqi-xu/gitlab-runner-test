package com.guandata.atlas.service.dataset.model.valueobj;

import com.guandata.atlas.dto.dataset.DSColumn;

/**
 * dxc
 */
public class Cell {

    /**
     * 字段
     */
    private DSColumn column;

    /**
     * 原始值
     */
    private Object value;

}
