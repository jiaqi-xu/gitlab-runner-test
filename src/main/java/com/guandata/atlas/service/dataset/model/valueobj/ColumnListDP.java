package com.guandata.atlas.service.dataset.model.valueobj;

import com.guandata.atlas.dto.dataset.DSColumn;
import lombok.Getter;

import java.util.List;

@Getter
public class ColumnListDP {

    private List<DSColumn> columnList;

    public ColumnListDP(List<DSColumn> columnList){
        this.columnList = columnList;
    }
}
