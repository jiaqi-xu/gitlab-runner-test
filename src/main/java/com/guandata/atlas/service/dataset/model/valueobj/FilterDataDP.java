package com.guandata.atlas.service.dataset.model.valueobj;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class FilterDataDP {

    private List<String> values;

    private Long total;

    private Integer size;
}
