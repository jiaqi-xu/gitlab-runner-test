package com.guandata.atlas.service.dataset.repository;

import com.guandata.atlas.common.constants.OpenAPIConstants;
import com.guandata.atlas.common.utils.AutowiredWrapperUtil;
import com.guandata.atlas.dao.DataSetDAO;
import com.guandata.atlas.dao.TokenDAO;
import com.guandata.atlas.enums.TaskTypeEnum;
import com.guandata.atlas.mapper.DataSetMapper;
import com.guandata.atlas.service.account.repository.TokenRepository;
import com.guandata.atlas.service.dataset.model.DataSet;
import com.guandata.atlas.service.planningtable.utils.FieldUtils;
import org.jooq.Field;
import org.jooq.SelectJoinStep;
import org.jooq.impl.DSL;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

@Repository
public class DataSetRepository {

    private static DataSetMapper dataSetMapper;

    @Resource
    public void setDataSetMapper(DataSetMapper dataSetMapper) {
        DataSetRepository.dataSetMapper = dataSetMapper;
    }

    public static DataSet getDataSetById(Integer dsId) {

        if (dsId == null) {
            return null;
        }

        DataSetDAO dataSet = dataSetMapper.getById(dsId);
        if (dataSet == null) {
            return null;
        }

        return new DataSet(dataSet);
    }

    /**
     * 获取数据同步public api接口字符串
     * @param tableName
     * @return
     */
    public static String getDataSyncOpenAPI(String tableName) {
        TokenDAO tokenDAO = TokenRepository.getToken(tableName, TaskTypeEnum.SYNC_DATASET_DATA.getType());
        MessageFormat dataSyncOpenAPI = new MessageFormat(OpenAPIConstants.LIST_DATASET_URL);
        String[] args = new String[]{tokenDAO.getObjectId(), tokenDAO.getAccessToken()};
        return dataSyncOpenAPI.format(args);
    }

    /**
     * 拼接获取数据集数据SQL by JOOQ
     * @param dataSet
     * @return
     */
    public static SelectJoinStep concatGetDataSetDataSQL(DataSet dataSet) {
        String tableName = dataSet.getTableName();
        List<Field<Object>> aliasFields = new ArrayList<>();

        dataSet.getColumnListDP().getColumnList().forEach(column -> {
            aliasFields.add(FieldUtils.transferByDataType(column));
        });

        return AutowiredWrapperUtil.getDslContext().select(aliasFields).from(DSL.table(DSL.name(tableName + "_update")));
    }

    public static DataSet getDataSet(Integer dataSetId) {
        return new DataSet(dataSetMapper.getById(dataSetId));
    }

}
