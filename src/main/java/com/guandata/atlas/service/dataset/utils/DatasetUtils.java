package com.guandata.atlas.service.dataset.utils;


import com.guandata.atlas.dto.ResultData;
import com.guandata.atlas.dto.dataset.DSColumn;

import java.util.List;
import java.util.Map;

public class DatasetUtils {

    public static ResultData getValue(String columnId, List<DSColumn> columns, List<ResultData> resultData) {

        for (int i=0; i< columns.size(); i++) {

            if (columns.get(i).getColumnId().equals(columnId)) {
                return resultData.get(i);
            }
        }

        return new ResultData("0.0");
    }

    public static String getValue(String columnId, List<DSColumn> columns, Map<String, Object> resultData) {

        for (int i=0; i< columns.size(); i++) {

            if (columns.get(i).getColumnId().equals(columnId)) {
                return String.valueOf(resultData.get(columnId));
            }
        }

        return "0.0";
    }
}
