package com.guandata.atlas.service.disruptor;

import lombok.Data;

@Data
public class AtlasEvent {

    private EventTypeEnum type;

    private Object data;

}
