package com.guandata.atlas.service.disruptor;

import com.lmax.disruptor.EventFactory;

public class AtlasEventFactory implements EventFactory<AtlasEvent> {
    @Override
    public AtlasEvent newInstance() {
        return new AtlasEvent();
    }
}
