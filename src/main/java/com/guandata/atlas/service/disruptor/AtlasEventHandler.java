package com.guandata.atlas.service.disruptor;

import com.alibaba.fastjson.JSON;
import com.guandata.atlas.common.utils.SpringBeanUtil;
import com.guandata.atlas.service.disruptor.handle.IHandler;
import com.lmax.disruptor.WorkHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Map;

@Component
public class AtlasEventHandler implements WorkHandler<AtlasEvent> {

    private static final Logger logger = LoggerFactory.getLogger(AtlasEventHandler.class);

    @Resource
    private Map<String, IHandler> handlesMap;

    @Override
    public void onEvent(AtlasEvent atlasEvent) {
        if (atlasEvent == null || atlasEvent.getType() == null) {
            logger.error("非法的请求，atlasEvent==null or atlasEvent.getType == null，sequence={}");
            return;
        }

        if (handlesMap == null) {
            handlesMap = SpringBeanUtil.getBeansOfType(IHandler.class);
        }

        String eventType = atlasEvent.getType().getType();
        IHandler handle = handlesMap.get(eventType);
        if (handle == null) {
            logger.error("找不到对应的处理器，atlasEvent={}", JSON.toJSONString(atlasEvent));
            return;
        }

        Object data = atlasEvent.getData();

        try {
            logger.info("start event execute,atlasEvent={}", JSON.toJSONString(atlasEvent));
            Boolean result = handle.handle(data);
            if (result == null || !result) {
                logger.error("disruptor event execute failed, atlasEvent={}", JSON.toJSONString(atlasEvent));
            }
        } catch (Exception ex){

            logger.error("disruptor event execute error, atlasEvent={}", JSON.toJSONString(atlasEvent),ex);
        }
    }
}
