package com.guandata.atlas.service.disruptor;

import com.lmax.disruptor.BlockingWaitStrategy;
import com.lmax.disruptor.RingBuffer;
import com.lmax.disruptor.dsl.Disruptor;
import com.lmax.disruptor.dsl.ProducerType;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.concurrent.Executor;

@Component
public class DisruptorTaskService{

    private Disruptor<AtlasEvent>  disruptor;

    private RingBuffer<AtlasEvent> ringBuffer;

    @Resource
    private AtlasEventHandler atlasEventHandler;

    @Resource
    private Executor disruptorConsumerThreadPool;

    @PostConstruct
    public void init() {

        this.disruptor = new Disruptor(new AtlasEventFactory(),1024*1024,
                disruptorConsumerThreadPool,ProducerType.MULTI, new BlockingWaitStrategy());


        // 创建10个消费者来处理同一个生产者发的消息(这10个消费者不重复消费消息)
        AtlasEventHandler[] consumers = new AtlasEventHandler[5];
        for (int i = 0; i < consumers.length; i++) {
            consumers[i] = atlasEventHandler;
        }

        this.disruptor.handleEventsWithWorkerPool(consumers);
        this.disruptor.start();

        ringBuffer = disruptor.getRingBuffer();

        Runtime.getRuntime().addShutdownHook(new Thread(()->{
            disruptor.shutdown();
            try {
                Thread.sleep(500);
            }catch (Exception ex){
            }
        }));
    }


    public<T> void sendNotify(EventTypeEnum eventType, T message) {

        long sequence = ringBuffer.next();
        try {
            AtlasEvent atlasEvent = ringBuffer.get(sequence);
            atlasEvent.setType(eventType);
            atlasEvent.setData(message);
        } finally {
            ringBuffer.publish(sequence);
        }
    }
}
