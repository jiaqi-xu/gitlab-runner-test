package com.guandata.atlas.service.disruptor;

public enum EventTypeEnum {

    BATCH_UPDATE_TABLE("batch_update_table", "批量修改planningtable数据"),

    DATASET_DATA_SYNC("dataset_data_sync", "同步Universe数据集至Atlas数据集"),

    CARD_NEXT_PAGE_SYNC("card_next_pages_sync", "同步下几页的数据"),

    INIT_PAGE_SYNC("int_pages_sync", "初始化开始的数据"),
     ;

     private String type;

     private String desc;

    EventTypeEnum(String type, String desc) {

        this.type = type;
        this.desc = desc;
    }

    public String getType() {
        return type;
    }

    public String getDesc() {
        return desc;
    }
}
