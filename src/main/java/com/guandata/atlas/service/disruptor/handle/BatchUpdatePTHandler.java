package com.guandata.atlas.service.disruptor.handle;

import com.alibaba.fastjson.JSON;
import com.guandata.atlas.client.cache.local.GuavaCacheWrapper;
import com.guandata.atlas.dao.TaskDAO;
import com.guandata.atlas.dto.BatchUpdateDataDTO;
import com.guandata.atlas.dto.TableFilter;
import com.guandata.atlas.enums.TaskStatusEnum;
import com.guandata.atlas.service.TaskService;
import com.guandata.atlas.service.planningtable.enums.BatchModifyTypeEnum;
import com.guandata.atlas.service.planningtable.model.card.entity.Card;
import com.guandata.atlas.service.planningtable.model.page.entity.Page;
import com.guandata.atlas.service.planningtable.repository.CardRepository;
import com.guandata.atlas.service.planningtable.repository.PageRepository;
import com.guandata.atlas.service.task.repository.TaskRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Date;

/**
 * 批量更新planningtable
 */
@Component("batch_update_table")
public class BatchUpdatePTHandler implements IHandler<BatchUpdateDataDTO>{

    private static final Logger logger = LoggerFactory.getLogger(BatchUpdatePTHandler.class);

    @Resource
    private TaskService taskService;

    @Override
    public Boolean handle(BatchUpdateDataDTO batchUpdateDataDTO) {

        logger.info("进行planningtable批量更新，params={}", JSON.toJSONString(batchUpdateDataDTO));

        long now = System.currentTimeMillis();
        TableFilter tableFilter = new TableFilter();

        tableFilter.setDsId(batchUpdateDataDTO.getDsId());
        tableFilter.setCdId(batchUpdateDataDTO.getCdId());
        tableFilter.setConditions(batchUpdateDataDTO.getConditions());

        tableFilter.reBuildConditions();

        BatchModifyTypeEnum modifyType = BatchModifyTypeEnum.getModifyType(batchUpdateDataDTO.getType());
        if (modifyType == null){
            taskService.updateTask(new TaskDAO(batchUpdateDataDTO.getTaskId(), TaskStatusEnum.FAILURE, "不支持的批量修改类型,type="+modifyType.getType(), new Date()));
            logger.error("不支持的批量修改类型，type={}", batchUpdateDataDTO.getType());
            return false;
        }

        Authentication authentication = batchUpdateDataDTO.getAuthentication();
        SecurityContextHolder.getContext().setAuthentication(authentication);

        Card card = CardRepository.getCardByCardId(batchUpdateDataDTO.getCdId());

        if (card == null) {
            taskService.updateTask(new TaskDAO(batchUpdateDataDTO.getTaskId(), TaskStatusEnum.FAILURE, "card信息不存在", new Date()));
            logger.error("任务执行失败，card不存在，id={}", card.getCdId());
            return false;
        }

        Page page = PageRepository.getPageByPageId(card.getPage().getPgId(), tableFilter.getFiltersInfo());
        card = page.getCard(card.getCdId(), tableFilter.getFieldsInfo(), tableFilter.getFiltersInfo());

        try {
            card.batchModify(batchUpdateDataDTO);
        } catch (Exception ex){
            TaskRepository.updateTask(new TaskDAO(batchUpdateDataDTO.getTaskId(), TaskStatusEnum.FAILURE, ex.getMessage(), new Date()));
            logger.error("批量更新失败，info={}", JSON.toJSONString(batchUpdateDataDTO), ex);
            return false;
        }


        GuavaCacheWrapper.delDataSetCacheByDsId(tableFilter.getDsId());
        taskService.updateTask(new TaskDAO(batchUpdateDataDTO.getTaskId(), TaskStatusEnum.SUCCESS, "", new Date()));

        logger.info("共耗时：{}",System.currentTimeMillis()-now);
        logger.info("进行planningtable批量更新成功，params={}", JSON.toJSONString(batchUpdateDataDTO));
        return true;
    }
}
