package com.guandata.atlas.service.disruptor.handle;

public interface IHandler<T> {

    Boolean handle(T a);
}
