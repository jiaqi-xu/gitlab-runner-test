package com.guandata.atlas.service.disruptor.handle;

import com.guandata.atlas.controller.request.FilterDataGetRequest;
import com.guandata.atlas.dto.DataSetDTO;
import com.guandata.atlas.dto.InitPagesDTO;
import com.guandata.atlas.dto.TableFilter;
import com.guandata.atlas.dto.dataset.DSColumn;
import com.guandata.atlas.pojo.FieldInfo;
import com.guandata.atlas.pojo.PTFilterInfo;
import com.guandata.atlas.service.CardService;
import com.guandata.atlas.service.DataSetService;
import com.guandata.atlas.service.FilterService;
import com.guandata.atlas.service.planningtable.model.card.entity.Card;
import com.guandata.atlas.service.planningtable.model.field.entity.Field;
import com.guandata.atlas.service.planningtable.model.filter.entity.Filter;
import com.guandata.atlas.service.planningtable.model.page.entity.Page;
import com.guandata.atlas.service.planningtable.repository.PageRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component("int_pages_sync")
public class InitPagesHandler implements IHandler<InitPagesDTO> {
    private static final Logger logger = LoggerFactory.getLogger(InitPagesHandler.class);

    @Resource
    private CardService cardService;

    @Resource
    private FilterService filterService;

    @Resource
    private DataSetService dataSetService;

    @Override
    public Boolean handle(InitPagesDTO initPagesDTO) {

//        List<Page> allPages = PageRepository.getAllPages();
//        if (CollectionUtils.isEmpty(allPages)) {
//            return true;
//        }
//
//        /**
//         * 如果传入的是非空集合，只重建传入的ds
//         */
//        allPages = allPages.stream().filter(s->
//            initPagesDTO == null
//                    || CollectionUtils.isEmpty(initPagesDTO.getDsIdList())
//                    || initPagesDTO.getDsIdList().contains(s.getDataSet().getId())
//        ).collect(Collectors.toList());
//
//        for (Page page : allPages) {
//
//            List<Filter> filters = page.fillAllFilters().getFiltersDP().getFilters();
//            if (CollectionUtils.isEmpty(filters)) {
//                continue;
//            }
//            List<Card> cardList = page.fillAllCards(false, false).getCardList();
//            if (CollectionUtils.isEmpty(cardList)) {
//                continue;
//            }
//
//            Filter filter = filters.get(0);
//
//            List<PTFilterInfo> filterData = filterService.getFilterData(1L, Long.MAX_VALUE,
//                    new FilterDataGetRequest(page.getDataSet().getId(), filter));
//            if (CollectionUtils.isEmpty(filterData)) {
//                continue;
//            }
//
//            DataSetDTO dataSetDTO = dataSetService.getDataSetInfo(page.getDataSet().getId());
//            Map<String, DSColumn> columnsMap = dataSetDTO.getDefinition().stream().collect(Collectors.toMap(s->s.getColumnId(), v->v));
//
//            for (Card card : cardList) {
//
//                List<Field> ptFields = card.fillFieldsDP().getFieldsDP().getFields();
//
//                if (CollectionUtils.isEmpty(ptFields)) {
//                    continue;
//                }
//
//                List<FieldInfo> fields = ptFields.stream().map(s -> new FieldInfo(s)).collect(Collectors.toList());
//
//                fields.stream().forEach(field -> {
//                    field.setColumn(columnsMap.get(field.getColumnId()));
//                    field.setColumnId(null);
//                });
//
//                TableFilter tableFilter = new TableFilter(filterData.get(0), fields, page.getDataSet().getId(), card.getCdId());
//                cardService.getResult(tableFilter, 1L,100L);
//                cardService.getResult(tableFilter, 2L,100L);
//            }
//        }

        return true;
    }

}
