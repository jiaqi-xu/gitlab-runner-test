package com.guandata.atlas.service.disruptor.handle;

import com.alibaba.fastjson.JSON;
import com.guandata.atlas.client.cache.local.DatasetCache;
import com.guandata.atlas.common.utils.CacheKeyUtil;
import com.guandata.atlas.dto.SyncCardNextPagesDTO;
import com.guandata.atlas.dto.TableFilter;
import com.guandata.atlas.dto.TableResult;
import com.guandata.atlas.service.CardService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;

@Component("card_next_pages_sync")
public class SyncCardNextPagesHandler implements IHandler<SyncCardNextPagesDTO> {
    private static final Logger logger = LoggerFactory.getLogger(SyncCardNextPagesHandler.class);

    @Resource
    private CardService cardService;

    @Resource
    private DatasetCache datasetCache;

    @Override
    public Boolean handle(SyncCardNextPagesDTO syncDataSetDataDTO) {

        TableFilter tableFilter = syncDataSetDataDTO.getTableFilter();

        for (int i=0; i<2;i++){
            Long pageNo = syncDataSetDataDTO.getPageNo()+i+1;
            String cardDataCacheKey = null;

            cardDataCacheKey = CacheKeyUtil.getCardDataCacheKey(syncDataSetDataDTO.getDsId(),
                    tableFilter.getCdId(), pageNo, syncDataSetDataDTO.getPageSize(), JSON.toJSONString(tableFilter));

            TableResult tableResult = datasetCache.get(cardDataCacheKey);
            if (tableResult != null) {
               continue;
            }

            tableResult
                    = cardService.getResultDataList(tableFilter, pageNo, syncDataSetDataDTO.getPageSize());
            if (CollectionUtils.isEmpty(tableResult.getResultData())) {
                break;
            }

            datasetCache.put(cardDataCacheKey, tableResult, 10);
        }

        return true;
    }
}
