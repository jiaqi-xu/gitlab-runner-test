package com.guandata.atlas.service.disruptor.handle;

import com.guandata.atlas.dto.SyncDataSetDataDTO;
import com.guandata.atlas.service.DataSyncService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("dataset_data_sync")
public class SyncDataSetDataHandler implements IHandler<SyncDataSetDataDTO> {
    private static final Logger logger = LoggerFactory.getLogger(SyncDataSetDataHandler.class);

    @Autowired
    private DataSyncService dataSyncService;

    @Override
    public Boolean handle(SyncDataSetDataDTO syncDataSetDataDTO) {
        dataSyncService.syncUniverseData(syncDataSetDataDTO.getDataSetDTO(), syncDataSetDataDTO.getTaskId(), syncDataSetDataDTO.getUser());
        return true;
    }
}
