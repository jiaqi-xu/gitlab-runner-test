package com.guandata.atlas.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.guandata.atlas.common.utils.GalaxyUtil;
import com.guandata.atlas.dao.AccountEntity;
import com.guandata.atlas.dao.KanbanEntity;
import com.guandata.atlas.dto.Account;
import com.guandata.atlas.dto.Kanban;
import com.guandata.atlas.enums.ErrorCode;
import com.guandata.atlas.exception.AtlasException;
import com.guandata.atlas.mapper.KanbanMapper;
import com.guandata.atlas.service.AccountService;
import com.guandata.atlas.service.KanbanService;
import com.guandata.atlas.service.ResourceService;
import com.guandata.atlas.service.UserService;
import com.guandata.atlas.service.planningtable.repository.FieldRepository;
import com.guandata.atlas.service.user.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class KanbanServiceImpl extends ServiceImpl<KanbanMapper, KanbanEntity> implements
        KanbanService {

    @Autowired
    private AccountService accountService;

    @Autowired
    private ResourceService resourceService;

    @Autowired
    private UserService userService;


    /**
     * order items in kanban
     * @param orderList item order map
     * @return true if order finished successfully; otherwise false
     */
    @Override
    public Boolean order(List<KanbanEntity> orderList) {
        List<KanbanEntity> kanbanList = new ArrayList<>();
        orderList.forEach( item -> {
            KanbanEntity kanban = getById(item.getId());
            kanban.setSeqNo(item.getSeqNo());
            kanbanList.add(kanban);
        });
        return updateBatchById(kanbanList);
    }

    /**
     * add a new kanban item
     * @param kanban
     * @param userId
     * @return
     */
    @Override
    public Boolean insert(KanbanEntity kanban, String userId) {
        kanban.setEditorId(userId);
        return save(kanban);
    }

    /**
     * list kanban items by type
     * @param type
     * @return list of kanban items
     */
    @Override
    public List<Kanban> list(String type, String userId) {
        List<Kanban> kanbanList = new ArrayList<>();

        User user = userService.getUser(userId);
        List<String> resourceIdList = user.getAllResources(type).stream()
                .map(resource -> resource.getId()).collect(Collectors.toList());
        if(resourceIdList.isEmpty()) {
            return kanbanList;
        }

        QueryWrapper<KanbanEntity> queryWrapper = new QueryWrapper<>();
        Map<String, Object> queryParamsMap = new HashMap<>();
        queryParamsMap.put("object_type", type);
        queryParamsMap.put("is_del", false);
        queryWrapper.select("id", "object_name", "account_id", "object_type", "object_id", "seq_no")
                .allEq(queryParamsMap);
        queryWrapper.in("object_id", resourceIdList).orderByDesc("seq_no");
        List<KanbanEntity> kanbanEntityList = list(queryWrapper);

        for (KanbanEntity kanbanEntity : kanbanEntityList) {
            Kanban kanban = kanbanEntity.toKanban();
            Account account = accountService.get(kanbanEntity.getAccountId()).toAccount();
            account.setAuthConfig(null);
            kanban.setAccount(account);
            kanbanList.add(kanban);
        }
        return kanbanList;
    }

    /**
     * the number of kanban items which are associated with the account
     * @param accountId
     * @return
     */
    @Override
    public int countByAccount(int accountId) {
        QueryWrapper<KanbanEntity> queryWrapper = new QueryWrapper<>();
        Map<String, Object> queryParamsMap = new HashMap<>();
        queryParamsMap.put("account_id", accountId);
        queryParamsMap.put("is_del", false);
        queryWrapper.allEq(queryParamsMap);
        return count(queryWrapper);
    }

    /**
     * delete kanban item
     * @param id item id
     * @param userId user id
     * @return true if deleted; otherwise return false
     */
    @Override
    public Boolean softDelete(long id, String userId) {
        if (FieldRepository.hasAssociatedFieldsWithKanban(String.valueOf(id))) {
            throw new AtlasException(ErrorCode.FAIL_TO_DELETE_KANBAN, "存在与看板关联的字段");
        }

        UpdateWrapper<KanbanEntity> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set("is_del", true).set("editor_id", userId).eq("id", id);
        return update(updateWrapper);
    }

    /**
     * update kanban item
     * @param kanbanEntity modified item used for updated
     * @param userId user id
     * @return  true if updated; otherwise return false
     */
    @Override
    public Boolean update(KanbanEntity kanbanEntity, String userId) {
        KanbanEntity kanbanToUpdate = getById(kanbanEntity.getId());
        kanbanToUpdate.setObjectName(kanbanEntity.getObjectName());
        kanbanToUpdate.setAccountId(kanbanEntity.getAccountId());
        kanbanToUpdate.setObjectId(kanbanEntity.getObjectId());
        kanbanToUpdate.setEditorId(userId);
        return updateById(kanbanToUpdate);
    }

    /**
     * necessary information for single sign on
     * @param id kanban item id
     * @return
     * @throws AtlasException
     */
    public JSONObject getSSOInfo(int id) throws AtlasException {
        KanbanEntity kanbanEntity = getById(id);
        AccountEntity accountEntity = accountService.get(kanbanEntity.getAccountId());
        JSONObject authConfig = JSON.parseObject(accountEntity.getAuthConfig());
        String privateKey = authConfig.getString("privateKey");
        String baseUrl = authConfig.getString("address");
        String domainId= authConfig.getString("domainId");
        String ssoToken = GalaxyUtil.getSSOToken(privateKey, domainId, authConfig.getString("username"));

        JSONObject ssoInfo = new JSONObject();
        ssoInfo.put("address", baseUrl);
        ssoInfo.put("domainId", domainId);
        ssoInfo.put("objectType", kanbanEntity.getObjectType());
        ssoInfo.put("objectId", kanbanEntity.getObjectId());
        ssoInfo.put("ssoToken", ssoToken);
        return ssoInfo;
    }
}
