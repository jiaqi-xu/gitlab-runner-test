package com.guandata.atlas.service.planningtable.enums;

import org.apache.commons.lang3.StringUtils;

public enum AggrTypeEnum {

    SUM("SUM", "累加", "SUM"),

    MAX("MAX", "最大值","MAX"),

    MIN("MIN", "最小值", "MIN"),

    AVG("AVG", "平均值", "AVG"),

    COUNT("COUNT", "计数", "COUNT"),

    ;

    private String type;

    private String desc;

    private String operator;

    AggrTypeEnum(String type, String desc, String operator){

        this.type = type;
        this.desc = desc;
        this.operator = operator;
    }

    public String getType() {
        return type;
    }

    public String getDesc() {
        return desc;
    }

    public String getOperator() {
        return operator;
    }

    public static AggrTypeEnum getAggrType(String type) {

        if (StringUtils.isBlank(type)) {
            return null;
        }

        for (AggrTypeEnum aggrTypeEnum : AggrTypeEnum.values()){
            if(aggrTypeEnum.getType().equalsIgnoreCase(type)) {
                return aggrTypeEnum;
            }
        }

        return null;
    }
}
