package com.guandata.atlas.service.planningtable.enums;

public enum BatchModifyTypeEnum {

    REPLACE(0, "replaceModify", "替换数据"),
    TIMES(1, "timesModify","倍数缩放"),
    FIX_VALUE(2, "fixedValueModify","固定值"),
    IMPORT_PADDING(3, "importPaddingModify","引入填充")
    ;

    private Integer type;

    private String beanName;

    private String desc;

    BatchModifyTypeEnum(Integer type, String beanName, String desc){

        this.beanName = beanName;
        this.type = type;
        this.desc = desc;
    }

    public Integer getType() {
        return type;
    }

    public String getDesc() {
        return desc;
    }

    public String getBeanName() {
        return beanName;
    }

    public static BatchModifyTypeEnum getModifyType(Integer type) {

        if (type == null) {
            return null;
        }

        for (BatchModifyTypeEnum batchModifyTypeEnum : BatchModifyTypeEnum.values()){
            if(batchModifyTypeEnum.type.equals(type)) {
                return batchModifyTypeEnum;
            }
        }

        return null;
    }
}
