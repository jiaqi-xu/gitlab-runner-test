package com.guandata.atlas.service.planningtable.enums;

public enum CardStatusEnum {

    NORMAL(0, "正常状态"),

    DELETED(1, "正常状态"),

    ;

    private Integer status;

    private String desc;

    CardStatusEnum(Integer status, String desc){

        this.status = status;
        this.desc = desc;
    }

    public Integer getStatus() {
        return status;
    }

    public String getDesc() {
        return desc;
    }

    public static CardStatusEnum getCardStatus(Integer status) {

        if (status == null) {
            return null;
        }

        for (CardStatusEnum statusEnum : CardStatusEnum.values()){
            if(statusEnum.status.equals(status)) {
                return statusEnum;
            }
        }

        return null;
    }
}
