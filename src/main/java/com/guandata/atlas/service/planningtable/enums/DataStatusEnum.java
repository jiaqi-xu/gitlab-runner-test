package com.guandata.atlas.service.planningtable.enums;

public enum DataStatusEnum {

    NO_CHANGED(0, "未修改过"),

    CHANGED(1, "修改过"),

    ;

    private Integer status;

    private String desc;

    DataStatusEnum(Integer status, String desc){

        this.status = status;
        this.desc = desc;
    }

    public Integer getStatus() {
        return status;
    }

    public String getDesc() {
        return desc;
    }

}
