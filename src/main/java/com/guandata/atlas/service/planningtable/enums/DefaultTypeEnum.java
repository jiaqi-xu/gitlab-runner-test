package com.guandata.atlas.service.planningtable.enums;


import org.apache.commons.lang3.StringUtils;

public enum DefaultTypeEnum {

    NONE("NONE", "无"),
    FIXVALUE("FIXVALUE", "固定值"),
    FIRSTVALUE("FIRSTVALUE", "列表第一个")
    ;

    private String code;

    private String name;

    DefaultTypeEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public static DefaultTypeEnum getDefaultType(String code) {

        if (StringUtils.isBlank(code)) {
            return NONE;
        }

        for (DefaultTypeEnum defaultTypeEnum : DefaultTypeEnum.values()){
            if(defaultTypeEnum.code.equals(code)) {
                return defaultTypeEnum;
            }
        }

        return NONE;
    }
}
