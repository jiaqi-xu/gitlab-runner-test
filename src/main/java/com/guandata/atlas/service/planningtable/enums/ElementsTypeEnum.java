package com.guandata.atlas.service.planningtable.enums;

import org.jooq.tools.StringUtils;

public enum ElementsTypeEnum {

    SINGLE("SINGLE", "单选"),
    MULTI("MULTI", "多选"),
    ;

    private String code;

    private String name;

    ElementsTypeEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public static ElementsTypeEnum getElementsType(String code) {

        if (StringUtils.isBlank(code)) {
            return MULTI;
        }

        for (ElementsTypeEnum elementsType : ElementsTypeEnum.values()){
            if(elementsType.code.equals(code)) {
                return elementsType;
            }
        }

        return MULTI;
    }
}
