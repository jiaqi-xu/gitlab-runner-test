package com.guandata.atlas.service.planningtable.enums;

import org.jooq.tools.StringUtils;

public enum MetaTypeEnum {

    TEXT("TEXT", "文本"),

    METRIC("METRIC", "数值"),

    DATE("DATE", "日期"),

    TIMESTAMP("TIMESTAMP", "时间"),

    ;

    private String type;

    private String desc;

    MetaTypeEnum(String type, String desc){

        this.type = type;
        this.desc = desc;
    }

    public String getType() {
        return type;
    }

    public String getDesc() {
        return desc;
    }


    public static MetaTypeEnum getMetaType(String type) {

        if (StringUtils.isBlank(type)) {
            return null;
        }

        for (MetaTypeEnum metaTypeEnum : MetaTypeEnum.values()){
            if(metaTypeEnum.type.equals(type)) {
                return metaTypeEnum;
            }
        }

        return null;
    }
}
