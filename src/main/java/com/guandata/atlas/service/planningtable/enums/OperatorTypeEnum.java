package com.guandata.atlas.service.planningtable.enums;

import org.jooq.tools.StringUtils;

public enum OperatorTypeEnum {

    GT("GT", "大于"),
    GE("GE", "大于等于"),
    LT("LT", "小于"),
    LE("LE", "小于等于"),
    BT("BT", "区间"),
    EQ("EQ", "等于"),
    NE("NE", "不等于"),
    IS_NULL("IS_NULL", "为空"),
    NOT_NULL("NOT_NULL", "不为空"),
    ;

    private String code;

    private String name;

    OperatorTypeEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public static OperatorTypeEnum getOperatorType(String code) {

        if (StringUtils.isBlank(code)) {
            return null;
        }

        for (OperatorTypeEnum operatorType : OperatorTypeEnum.values()){
            if(operatorType.code.equals(code)) {
                return operatorType;
            }
        }

        return null;
    }
}
