package com.guandata.atlas.service.planningtable.enums;

public enum PageStatusEnum {

    NORMAL(0, "正常状态"),

    DELETED(1, "删除状态"),

    INIT(2, "初始化"),

    ;

    private Integer status;

    private String desc;

    PageStatusEnum(Integer status, String desc){

        this.status = status;
        this.desc = desc;
    }

    public Integer getStatus() {
        return status;
    }

    public String getDesc() {
        return desc;
    }


    public static PageStatusEnum getPageStatus(Integer status) {

        for (PageStatusEnum statusEnum : PageStatusEnum.values()){
            if(statusEnum.status.equals(status)) {
                return statusEnum;
            }
        }

        return null;
    }
}
