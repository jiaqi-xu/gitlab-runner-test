package com.guandata.atlas.service.planningtable.enums;

public enum SelectTypeEnum {

    DS_ELEMENTS("DS_ELEMENTS", "选择"),
    DS_INTERVAL("DS_INTERVAL", "范围"),
    CALENDAR("CALENDAR", "日期")
    ;

    private String type;

    private String name;

    SelectTypeEnum(String type, String name) {
        this.type = type;
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public String getName() {
        return name;
    }
}
