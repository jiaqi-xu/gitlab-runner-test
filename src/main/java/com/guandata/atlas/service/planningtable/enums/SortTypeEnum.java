package com.guandata.atlas.service.planningtable.enums;

import org.jooq.tools.StringUtils;

public enum SortTypeEnum {

    ASC("ASC", "升序"),
    DESC("DESC", "降序"),
    DEFAULT("DEFAULT","默认")
    ;

    private String code;

    private String name;

    SortTypeEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public static SortTypeEnum getSortType(String code) {

        if (StringUtils.isBlank(code)) {
            return null;
        }

        for (SortTypeEnum sortType : SortTypeEnum.values()){
            if(sortType.code.equals(code.toUpperCase())) {
                return sortType;
            }
        }

        return null;
    }
}
