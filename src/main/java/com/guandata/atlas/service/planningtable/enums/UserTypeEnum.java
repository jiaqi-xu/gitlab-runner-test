package com.guandata.atlas.service.planningtable.enums;

public enum UserTypeEnum {

    DIM("DIM", "维度，groupby字段"),

    AGGR("AGGR", "聚合字段"),

    ;

    private String type;

    private String desc;

    UserTypeEnum(String type, String desc){

        this.type = type;
        this.desc = desc;
    }

    public String getType() {
        return type;
    }

    public String getDesc() {
        return desc;
    }
}
