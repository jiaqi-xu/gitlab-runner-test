package com.guandata.atlas.service.planningtable.factory;

import com.guandata.atlas.dto.BatchUpdateDataDTO;
import com.guandata.atlas.enums.ErrorCode;
import com.guandata.atlas.exception.AtlasException;
import com.guandata.atlas.service.planningtable.enums.BatchModifyTypeEnum;
import com.guandata.atlas.service.planningtable.model.field.entity.Field;
import com.guandata.atlas.service.planningtable.model.operate.*;

import java.util.List;
import java.util.Map;

public class BatchModifyFactory {

    public static IModify createBatchModify(BatchModifyTypeEnum modifyType,
                                            Field field,
                                            BatchUpdateDataDTO batchUpdateDataDTO,
                                            List<Map<String, Object>> records) {

        if (modifyType == null) {
            throw new AtlasException(ErrorCode.PARAME_ERROR_CODE);
        }

        switch (modifyType){
            case REPLACE:
                return new ReplaceModify(field, batchUpdateDataDTO, records);
            case IMPORT_PADDING:
                return new ImportPaddingModify(field, batchUpdateDataDTO, records);
            case TIMES:
                return new TimesModify(field, batchUpdateDataDTO, records);
            case FIX_VALUE:
                return new FixedValueModify(field, batchUpdateDataDTO, records);
            default:
                throw new AtlasException(ErrorCode.NOT_SUPPORT_OPERATOR_TYPE);
        }
    }
}
