package com.guandata.atlas.service.planningtable.model.card.entity;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.Lists;
import com.guandata.atlas.common.utils.AutowiredWrapperUtil;
import com.guandata.atlas.common.utils.UuidUtil;
import com.guandata.atlas.dao.CardInfoDO;
import com.guandata.atlas.dto.BatchUpdateDataDTO;
import com.guandata.atlas.dto.DataUpdateParam;
import com.guandata.atlas.dto.OrderBy;
import com.guandata.atlas.dto.dataset.DSColumn;
import com.guandata.atlas.enums.ErrorCode;
import com.guandata.atlas.exception.AtlasException;
import com.guandata.atlas.pojo.CardInfo;
import com.guandata.atlas.pojo.FieldInfo;
import com.guandata.atlas.pojo.FilterInfo;
import com.guandata.atlas.service.planningtable.enums.BatchModifyTypeEnum;
import com.guandata.atlas.service.planningtable.enums.CardStatusEnum;
import com.guandata.atlas.service.planningtable.factory.BatchModifyFactory;
import com.guandata.atlas.service.planningtable.model.card.valueobj.DatasDP;
import com.guandata.atlas.service.planningtable.model.card.valueobj.FieldsDP;
import com.guandata.atlas.service.planningtable.model.card.valueobj.PTSearchDP;
import com.guandata.atlas.service.planningtable.model.field.entity.Field;
import com.guandata.atlas.service.planningtable.model.filter.entity.Filter;
import com.guandata.atlas.service.planningtable.model.operate.IModify;
import com.guandata.atlas.service.planningtable.model.page.entity.Page;
import com.guandata.atlas.service.planningtable.model.valueobj.FiltersDP;
import com.guandata.atlas.service.planningtable.model.valueobj.LimitDP;
import com.guandata.atlas.service.planningtable.model.valueobj.OrderBysDP;
import com.guandata.atlas.service.planningtable.repository.FieldRepository;
import com.guandata.atlas.service.planningtable.repository.FilterRepository;
import com.guandata.atlas.service.planningtable.repository.PageRepository;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.jooq.*;
import org.jooq.impl.DSL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

@Getter
@NoArgsConstructor
public class Card {

    private static final Logger logger = LoggerFactory.getLogger(Card.class);

    private String cdId;

    private String name;

    private Page page;

    private Integer seqNo;

    private CardStatusEnum status;

    private FieldsDP fieldsDP = new FieldsDP();

    private FiltersDP filtersDP = new FiltersDP();

    private PTSearchDP ptSearchDP = new PTSearchDP();

    private DatasDP  datasDP = new DatasDP();

    private DatasDP oriDatasDP = new DatasDP();

    public Card(Page page, CardInfoDO cardInfoDO, boolean fillFilter, boolean fillFields){
        this(cardInfoDO);
        this.page = page;
        if (fillFields){
            fillFieldsDP();
        }
        if (fillFilter){
            fillFiltersDP();
        }
    }

    public Card(CardInfoDO cardInfoDO, Page page, List<FieldInfo> fieldInfos, List<FilterInfo> filterInfos) {
        this.cdId = cardInfoDO.getCdId();
        this.name = cardInfoDO.getName();
        this.seqNo = cardInfoDO.getSeqNo();
        this.page = page;
        this.status = CardStatusEnum.NORMAL;
        this.fieldsDP.fillByFieldsInfo(fieldInfos, this);

        List<Filter> filters = FilterRepository.transFilters(filterInfos, this).stream()
                .filter(s->s.isCardFilter() && s.isValidFilter()).collect(Collectors.toList());
        filtersDP.fillByFilters(filters);
    }

    public Card(CardInfoDO cardInfoDO) {
        this.cdId = cardInfoDO.getCdId();
        this.name = cardInfoDO.getName();
        this.seqNo = cardInfoDO.getSeqNo();
        this.page = PageRepository.getPageByPageId(cardInfoDO.getPgId());
        this.status = CardStatusEnum.NORMAL;
    }

    public Card(CardInfo cardInfo, Page page) {
        this.page = page;
        this.cdId = cardInfo.getCdId();
        this.name = cardInfo.getName();
        this.seqNo = cardInfo.getSeqNo();
        this.status = CardStatusEnum.NORMAL;
        this.fieldsDP.fillByFieldsInfo(cardInfo.getFields(), this);
        this.filtersDP.fillByFilters(FilterRepository.transFilters(cardInfo.getCdFilters(), this));
    }

    public Card(String cdId, Page page,List<FieldInfo> fieldInfos, List<FilterInfo> filterInfos) {
        this.cdId = cdId;
        if (StringUtils.isBlank(this.cdId)) {
            this.cdId = "CD_"+ UuidUtil.getUUID();
        }
        this.page = page;
        this.status = CardStatusEnum.NORMAL;
        this.fieldsDP.fillByFieldsInfo(fieldInfos, this);
        fillFieldsDP();
        List<Filter> filters = FilterRepository.transFilters(filterInfos, this).stream()
                .filter(s->s.isCardFilter() && s.isValidFilter()).collect(Collectors.toList());
        filtersDP.fillByFilters(filters);
    }

    /**
     * 获取可以批量修改的字段
     * @param field
     * @return
     */
    public List<Field> getReplaceableFields(Field field) {

        /**
         * 字段不允许被修改
         */
        if (!field.isAllowBatchModify()) {
            throw new AtlasException(ErrorCode.NOT_SUPPORT_FIELD_TYPE);
        }

        List<Field> fields = fillFieldsDP().getFieldsDP().getFields();
        if (CollectionUtils.isEmpty(fields)) {
            return Lists.newArrayList();
        }

        if ("STRING".equalsIgnoreCase(field.getColumn().getType())) {
            return fields.stream().filter(s-> !s.getFdId().equals(field.getFdId())
                            && s.isAllowBatchModify()
                            && Objects.equals(s.getAggType(), field.getAggType()))
                    .collect(Collectors.toList());
        }

        return fields.stream().filter(s-> !s.getFdId().equals(field.getFdId())
                        && s.isAllowBatchModify()
                        && s.getColumn().getType().equals(field.getColumn().getType())
                        && Objects.equals(s.getAggType(), field.getAggType()))
                .collect(Collectors.toList());
    }

    /**
     * 字段填充
     * @return
     */
    public Card fillFieldsDP() {
        if (!CollectionUtils.isEmpty(fieldsDP.getFields())) {
            return this;
        }

        this.fieldsDP.fillByFields(FieldRepository.getAllFields(this));

        fillDataSetColumns();
        return this;
    }

    /**
     * 填充过滤器
     * @return
     */
    public Card fillFiltersDP() {

        if (!CollectionUtils.isEmpty(filtersDP.getFilters())) {
            return this;
        }

        List<Filter> cardFilters = FilterRepository.getCardFilter(this);
        this.filtersDP.fillByFilters(cardFilters);

        return this;
    }

    private Card fillDataSetColumns() {

        if (CollectionUtils.isEmpty(fieldsDP.getFields())) {
            return this;
        }

        List<DSColumn> columnList = this.getPage().getDataSet().getColumnListDP().getColumnList();
        fieldsDP.fillDatasetColumns(columnList);

        return this;
    }

    public Card fillPTSearchDP(List<OrderBy> orderBys, Long pageNo, Long pageSize){
        if (orderBys == null) {
            orderBys = Lists.newArrayList();
        }

        this.ptSearchDP.fillSearch(orderBys, this, pageNo, pageSize);

        return this;
    }
    /**
     * 获取planningtable数据
     * @return
     */
    public Card fillRecordsBySearch() {

        LimitDP limitDP = ptSearchDP.getLimitDP();
        OrderBysDP orderBysDP = ptSearchDP.getOrderBysDP();

        String tableName = page.getDataSet().getTableName();
        DSLContext dslContext = AutowiredWrapperUtil.getDslContext();

        List<Condition> allConditions = new ArrayList(){{
            addAll(filtersDP.getWhereConditions());
            addAll(page.fillAllFilters().getFiltersDP().getWhereConditions());
        }};

        List<Condition> havingConditions = filtersDP.getHavingConditions();

        List<OrderField> orderByFields = orderBysDP.getOrderByFields();
        String sql = dslContext
                .select(fieldsDP.buildAggJooqFields())
                .from(DSL.table(DSL.name(tableName+ "_update")))
                .where(allConditions)
                .groupBy(fieldsDP.buildGroupByFields())
                .having(havingConditions)
                .orderBy(orderByFields.toArray(new OrderField[orderByFields.size()]))
                .limit(limitDP.getOffset(), limitDP.getPageSize()).toString();

        //增加SQL_CALC_FOUND_ROWS
        Result<Record> result = dslContext.fetch(sql);

        SelectHavingConditionStep<Record> having = dslContext
                .select(fieldsDP.buildAggJooqFields())
                .from(DSL.table(DSL.name(tableName + "_update")))
                .where(allConditions)
                .groupBy(fieldsDP.buildGroupByFields())
                .having(havingConditions);
        Long total = dslContext.selectCount().from(having).fetchOne().into(Long.class);

        this.datasDP.fillDatasDP(this.fieldsDP, result, total);

        return this;
    }

    public Card fillOriRecordsBySearch() {

        List<Field> groupByFields = fieldsDP.getGroupByFields();
        List<Map<String, Object>> groupDatas = datasDP.transListMap();

        List<Condition> orConditions = Lists.newArrayList();
        for (Map<String, Object> groupData : groupDatas) {
            List<Condition> subAndConditions = Lists.newArrayList();
            for (Field groupByField : groupByFields) {
                if (groupData.get(groupByField.getColumnId()) != null){
                    subAndConditions.add(
                            DSL.field(DSL.name(groupByField.getColumnId())).eq(groupData.get(groupByField.getColumnId())));
                } else {
                    subAndConditions.add(
                            DSL.field(DSL.name(groupByField.getColumnId())).isNull());
                }
            }

            orConditions.add(DSL.and(subAndConditions));
        }

        List<Condition> allConditions = new ArrayList(){{
            addAll(filtersDP.getWhereConditions());
            addAll(page.getFiltersDP().getWhereConditions());
            add(DSL.or(orConditions));
        }};

        List<Condition> havingConditions = filtersDP.getHavingConditions();

        String tableName = page.getDataSet().getTableName();
        DSLContext dslContext = AutowiredWrapperUtil.getDslContext();
        String sql = dslContext
                .select(fieldsDP.buildAggJooqFields())
                .from(DSL.table(DSL.name(tableName)))
                .where(allConditions)
                .groupBy(fieldsDP.buildGroupByFields())
                .having(havingConditions)
                .toString();
        //增加SQL_CALC_FOUND_ROWS
        Result<Record> result = dslContext.fetch(sql);

        this.oriDatasDP.fillDatasDP(this.fieldsDP, result, new Long(result.size()));

        return this;
    }


    public boolean storeMySelf(){

        updateFields();

        updateFilters();

        return true;
    }

    /**
     * 批量修改
     * @param batchUpdateDataDTO
     * @return
     */
    public boolean batchModify(BatchUpdateDataDTO batchUpdateDataDTO) {

        DatasDP datasDP = fillFieldsDP().fillPTSearchDP(Lists.newArrayList(), 1L, Long.MAX_VALUE).fillRecordsBySearch().getDatasDP();
        if (CollectionUtils.isEmpty(datasDP.getPtRecords())){
            logger.warn("查询的数据为空，批量修改结束，data={}", JSON.toJSONString(batchUpdateDataDTO));
            return true;
        }

        List<Map<String, Object>> finalRecords = datasDP.transListMap().stream().filter(Objects::nonNull).collect(Collectors.toList());

        BatchModifyTypeEnum modifyType = BatchModifyTypeEnum.getModifyType(batchUpdateDataDTO.getType());
        List<Field> ptFields = fillFieldsDP().getFieldsDP().getFields();
        Field field = ptFields.stream().filter(s -> s.getFdId().equals(batchUpdateDataDTO.getFdId())).findFirst().get();
        IModify batchModify = BatchModifyFactory.createBatchModify(modifyType, field, batchUpdateDataDTO,  finalRecords);

        batchUpdateDataDTO.setCard(this);
        batchModify.handleBatchModify();

        return true;
    }

    /**
     * 单个修改
     * @param dataUpdateParam
     * @return
     */
    public boolean singleModify(DataUpdateParam dataUpdateParam) {

        DatasDP datasDP = fillFieldsDP().fillPTSearchDP(Lists.newArrayList(), 1L, Long.MAX_VALUE).fillRecordsBySearch().getDatasDP();
        if (CollectionUtils.isEmpty(datasDP.getPtRecords())){
            logger.error("查询的数据为空，批量修改结束，data={}", JSON.toJSONString(dataUpdateParam));
            throw new AtlasException(ErrorCode.PARAME_ERROR_CODE);
        }

        List<Field> fields = fillFieldsDP().getFieldsDP().getFields();
        Field field = fields.stream().filter(s -> s.getColumnId().equals(dataUpdateParam.getField().getColumnId())).findAny().orElse(null);

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        List<Map<String, Object>> finalRecords = datasDP.transListMap().stream().filter(Objects::nonNull).collect(Collectors.toList());

        BatchUpdateDataDTO batchUpdateDataDTO = new BatchUpdateDataDTO();
        batchUpdateDataDTO.setConditions(dataUpdateParam.getConditions());
        batchUpdateDataDTO.setTableName(page.getDataSet().getTableName());
        batchUpdateDataDTO.setCard(this);
        batchUpdateDataDTO.setAuthentication(authentication);
        batchUpdateDataDTO.setCdId(this.getCdId());
        batchUpdateDataDTO.setDsId(page.getDataSet().getId());
        batchUpdateDataDTO.setFdId(field.getFdId());
        batchUpdateDataDTO.setType(BatchModifyTypeEnum.FIX_VALUE.getType());
        batchUpdateDataDTO.setValue(dataUpdateParam.getModifiedTo());

        BatchModifyTypeEnum modifyType = BatchModifyTypeEnum.getModifyType(batchUpdateDataDTO.getType());
        IModify batchModify = BatchModifyFactory.createBatchModify(modifyType, field, batchUpdateDataDTO, finalRecords);

        dataUpdateParam.setModifiedTo(batchModify.handleSingleModify());
        return true;
    }

    private boolean updateFields(){
        FieldRepository.deleteFieldsByCdId(this.getCdId());

        List<Field> fields = this.fieldsDP.getFields();
        if (CollectionUtils.isEmpty(fields)) {
            return true;
        }
        FieldRepository.insertFields(fields);
        return true;
    }

    private boolean updateFilters(){
        FilterRepository.deleteCardFilters(this);
        List<Filter> filters = this.filtersDP.getFilters();
        if (CollectionUtils.isEmpty(filters)) {
            return true;
        }

        FilterRepository.updateCardFilters(this);

        return true;
    }
}
