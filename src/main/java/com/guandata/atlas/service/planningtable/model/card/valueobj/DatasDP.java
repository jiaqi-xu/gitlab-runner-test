package com.guandata.atlas.service.planningtable.model.card.valueobj;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.guandata.atlas.service.planningtable.model.field.entity.Field;
import com.guandata.atlas.service.planningtable.model.valueobj.PTRecord;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.jooq.Record;
import org.jooq.Result;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
public class DatasDP {
    
    /**
     * 查询的数据
     */
    private List<PTRecord> ptRecords = Lists.newArrayList();

    private Long total;

    public DatasDP fillDatasDP(FieldsDP fieldsDP, Result<Record> result, Long total) {

        this.ptRecords = Lists.newArrayList();
        this.total = total;
        for (Record record : result) {
            Map<String, Object> singleRecordMap = Maps.newHashMap();
            for (Field atlasField : fieldsDP.getFields()) {
                singleRecordMap.put(atlasField.getAliasName(), record.getValue(atlasField.getAliasName()));
            }

            this.ptRecords.add(new PTRecord(singleRecordMap));
        }

        return this;
    }

    public List<Map<String, Object>> transListMap() {
        return ptRecords.stream().map(s->s.getValues()).collect(Collectors.toList());
    }
}
