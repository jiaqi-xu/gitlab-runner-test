package com.guandata.atlas.service.planningtable.model.card.valueobj;

import com.google.common.collect.Lists;
import com.guandata.atlas.dto.dataset.DSColumn;
import com.guandata.atlas.pojo.FieldInfo;
import com.guandata.atlas.service.planningtable.model.card.entity.Card;
import com.guandata.atlas.service.planningtable.model.field.entity.AggrField;
import com.guandata.atlas.service.planningtable.model.field.entity.DimField;
import com.guandata.atlas.service.planningtable.model.field.entity.Field;
import com.guandata.atlas.service.planningtable.utils.FieldUtils;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jooq.impl.DSL;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Getter
@NoArgsConstructor
public class FieldsDP {

    private List<Field> fields = Lists.newArrayList();

    public FieldsDP fillByFields(List<Field> fields) {
        this.fields = fields;
        return this;
    }

    public FieldsDP fillByFieldsInfo(List<FieldInfo> fieldsInfo, Card card){

        if (CollectionUtils.isEmpty(fieldsInfo)) {
            return this;
        }
        this.fields = fieldsInfo.stream().map(s-> FieldUtils.transField(s, card)).collect(Collectors.toList());
        return this;
    }

    public List<org.jooq.Field> buildAggJooqFields() {
        return fields.stream().map(s->s.getAggJooqField().as(s.getAliasName())).collect(Collectors.toList());
    }

    public List<org.jooq.Field> buildGroupByFields() {
        return fields.stream().filter(s-> s instanceof DimField).map(s-> DSL.field(DSL.name(s.getColumn().getColumnId())))
                .collect(Collectors.toList());
    }

    public List<Field> getGroupByFields(){
        return fields.stream().filter(s-> s instanceof DimField).collect(Collectors.toList());
    }

    public List<Field> getAggFields(){
        return fields.stream().filter(s-> s instanceof AggrField).collect(Collectors.toList());
    }

    public void fillDatasetColumns(List<DSColumn> columnList) {

        Map<String, DSColumn> colIdToDSColumn = columnList.stream().collect(Collectors.toMap(s -> s.getColumnId(), v -> v));
        fields.stream().forEach(s->{
            s.setColumn(colIdToDSColumn.get(s.getColumnId()));
        });
    }
}
