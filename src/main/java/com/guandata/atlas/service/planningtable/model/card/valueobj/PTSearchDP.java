package com.guandata.atlas.service.planningtable.model.card.valueobj;

import com.guandata.atlas.dto.OrderBy;
import com.guandata.atlas.service.planningtable.model.card.entity.Card;
import com.guandata.atlas.service.planningtable.model.valueobj.LimitDP;
import com.guandata.atlas.service.planningtable.model.valueobj.OrderBysDP;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@Getter
@NoArgsConstructor
public class PTSearchDP {

    private OrderBysDP orderBysDP = new OrderBysDP();

    private LimitDP limitDP;

    public PTSearchDP fillSearch(List<OrderBy> orderBys, Card card, Long pageNo, Long pageSize) {
        this.orderBysDP.fillOrderByDP(orderBys, card);
        this.limitDP = new LimitDP(pageNo, pageSize);

        return this;
    }
}
