package com.guandata.atlas.service.planningtable.model.field.entity;

import com.guandata.atlas.dao.FieldInfoDO;
import com.guandata.atlas.enums.ErrorCode;
import com.guandata.atlas.exception.AtlasException;
import com.guandata.atlas.pojo.FieldInfo;
import com.guandata.atlas.service.planningtable.enums.AggrTypeEnum;
import com.guandata.atlas.service.planningtable.model.card.entity.Card;
import lombok.Getter;
import org.jooq.impl.DSL;

import java.math.BigDecimal;

@Getter
public class AggrField extends Field {

    private AggrTypeEnum aggrType;

    public AggrField(Card card, FieldInfoDO fieldInfo){
        super(card, fieldInfo);
        this.aggrType = AggrTypeEnum.getAggrType(fieldInfo.getAggrType());
    }

    public AggrField(Card card, FieldInfo fieldInfo){
        super(card, fieldInfo);
        this.aggrType = AggrTypeEnum.getAggrType(fieldInfo.getAggrType());
    }

    @Override
    public org.jooq.Field getAggJooqField() {

        org.jooq.Field<Object> jooField = DSL.field(DSL.name(columnId));
        switch (aggrType){
            case SUM:
                return DSL.sum(DSL.field(DSL.name(columnId), BigDecimal.class));
            case MAX:
                return DSL.max(jooField);
            case MIN:
                return DSL.min(jooField);
            case AVG:
                return DSL.avg(DSL.field(DSL.name(columnId), BigDecimal.class));
            case COUNT:
                return DSL.count(jooField);
            default:
                throw new AtlasException(ErrorCode.PARAME_ERROR_CODE);
        }
    }

    //TODO 待后续切换field时整改
    @Override
    public String getAliasName() {
        //return columnId+"_"+ aggrType.getOperator();
        return columnId;
    }

    @Override
    public String getAggType() {
        return aggrType.getType();
    }
}
