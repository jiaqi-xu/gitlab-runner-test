package com.guandata.atlas.service.planningtable.model.field.entity;

import com.guandata.atlas.dao.FieldInfoDO;
import com.guandata.atlas.dto.dataset.DSColumn;
import com.guandata.atlas.service.planningtable.enums.MetaTypeEnum;
import lombok.Getter;
import lombok.Setter;

@Getter
public class BaseField {

    protected String fdId;

    /** 数据集字段 **/
    protected String columnId;

    /** 在表格字段中的顺序 **/
    private Integer seqNo;

    private MetaTypeEnum metaType;

    /** 数据集字段 **/
    @Setter
    private DSColumn column;

    public BaseField(){}

    public BaseField(String fdId, DSColumn column, Integer seqNo, String type) {
        this.fdId = fdId;
        this.column = column;
        this.columnId = column.getColumnId();
        this.seqNo = seqNo;
        this.metaType = MetaTypeEnum.getMetaType(type);
    }

    public BaseField(FieldInfoDO fieldInfoDO) {
        this.fdId = fieldInfoDO.getFdId();
        this.columnId = fieldInfoDO.getColumnId();
        this.seqNo = fieldInfoDO.getSeqNo();
        this.metaType = MetaTypeEnum.getMetaType(fieldInfoDO.getMetaType());
        this.column = fieldInfoDO.getColumn();
    }
}
