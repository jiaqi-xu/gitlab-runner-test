package com.guandata.atlas.service.planningtable.model.field.entity;

import com.guandata.atlas.dao.FieldInfoDO;
import com.guandata.atlas.pojo.FieldInfo;
import com.guandata.atlas.service.planningtable.model.card.entity.Card;
import com.guandata.atlas.service.planningtable.model.filter.entity.Filter;
import lombok.Getter;

@Getter
public class DimField extends Field {

    @Override
    public boolean isAllowBatchModify() {
        return false;
    }

    @Override
    public boolean isGroupByField() {
        return true;
    }

    public DimField(Card card, FieldInfoDO fieldInfo){
        super(card, fieldInfo);
    }

    public DimField(Card card, FieldInfo fieldInfo){
        super(card, fieldInfo);
    }

    public DimField(Card card, Filter filter) {
        super(card, filter);
    }
}
