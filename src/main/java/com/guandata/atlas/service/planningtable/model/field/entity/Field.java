package com.guandata.atlas.service.planningtable.model.field.entity;

import com.alibaba.fastjson.JSON;
import com.guandata.atlas.dao.FieldInfoDO;
import com.guandata.atlas.pojo.FieldInfo;
import com.guandata.atlas.service.planningtable.enums.AggrTypeEnum;
import com.guandata.atlas.service.planningtable.model.card.entity.Card;
import com.guandata.atlas.service.planningtable.model.field.valueobj.CorrelationAnalysis;
import com.guandata.atlas.service.planningtable.model.field.valueobj.FieldExt;
import com.guandata.atlas.service.planningtable.model.filter.entity.Filter;
import lombok.Getter;
import org.jooq.impl.DSL;

@Getter
public abstract class Field extends BaseField {
    /** 所属card **/
    private Card card;

    /** 字段别名 **/
    private String alias;

    /** 是否禁用筛选 **/
    private Boolean allowFilter;
    /** 是否启用排序 **/
    private Boolean allowSort;
    /** 是否启用编辑 **/
    private Boolean allowEdit;
    /** 关联分析配置 **/
    private CorrelationAnalysis correlationAnalysis;

    /** 在planning table result中是否为默认展示的field **/
    private Boolean inDefaultView;
    /** 数据格式 **/
    private String dataFormat;

    private String fdProperty;

    private FieldExt fieldExt;

    public Field(Card card, FieldInfoDO fieldInfo) {

        super(fieldInfo);
        this.card = card;
        this.fdId = fieldInfo.getFdId();

        this.alias = fieldInfo.getAlias();
        this.allowFilter = fieldInfo.getAllowFiltrated();
        this.allowSort = fieldInfo.getAllowSorted();
        this.allowEdit = fieldInfo.getAllowEdited();
        this.dataFormat = fieldInfo.getDataFormat();
        this.correlationAnalysis = JSON.parseObject(fieldInfo.getCorrelationAnalysis(), CorrelationAnalysis.class);
        this.inDefaultView = fieldInfo.getInDefaultView();
        this.fdProperty = fieldInfo.getFdProperty();
        this.fieldExt = JSON.parseObject(fieldInfo.getExt(), FieldExt.class);
    }

    public Field(Card card, FieldInfo fieldInfo) {

        super(fieldInfo.getFdId(), fieldInfo.getColumn(), fieldInfo.getSeqNo(), fieldInfo.getMetaType());
        this.card = card;
        this.fdId = fieldInfo.getFdId();

        this.alias = fieldInfo.getAlias();
        this.allowFilter = fieldInfo.getAllowFiltrated();
        this.allowSort = fieldInfo.getAllowSorted();
        this.allowEdit = fieldInfo.getAllowEdited();
        this.inDefaultView = fieldInfo.getInDefaultView();
        this.dataFormat = fieldInfo.getDataFormat();
        this.correlationAnalysis = fieldInfo.getCorrelationAnalysis();
        this.fdProperty = fieldInfo.getFdProperty();
        this.fieldExt = JSON.parseObject(fieldInfo.getExt(), FieldExt.class);
    }

    public Field(Card card, Filter filter){
        super(filter.getFdId(), filter.getDsColumn(), null, null);
        this.card = card;
    }

    /**
     * 是否允许批量修改
     * @return
     */
    public boolean isAllowBatchModify() {
        return true;
    }

    public org.jooq.Field getAggJooqField() {
        return DSL.field(DSL.name(columnId));
    }

    public String getAliasName(){
        return columnId;
    }

    public boolean isGroupByField() {
        return false;
    }

    public String getAggType() {
        return AggrTypeEnum.MAX.getType();
    }
}
