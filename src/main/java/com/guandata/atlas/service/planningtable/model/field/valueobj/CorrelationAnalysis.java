package com.guandata.atlas.service.planningtable.model.field.valueobj;

import lombok.Data;

import java.util.List;

@Data
public class CorrelationAnalysis {
    /** 是否启用 **/
    private Boolean isEnabled;

    /** 关联对象的id, 目前是看板 **/
    private String objectId;

    /** 关联关系  **/
    private List<CorrelationMapping> correlationMappings;

    @Data
    public static class CorrelationMapping {
        /** BI看板筛选器字段名称 **/
        private String filterName;
        /** card视图字段名称 **/
        private String fieldName;
    }
}
