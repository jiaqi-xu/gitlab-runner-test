package com.guandata.atlas.service.planningtable.model.field.valueobj;

import lombok.Data;

import java.util.List;

@Data
public class CustomValuesConfig {

    private String elementType;

    private List<String> optionalValues;

    private boolean enable;
}
