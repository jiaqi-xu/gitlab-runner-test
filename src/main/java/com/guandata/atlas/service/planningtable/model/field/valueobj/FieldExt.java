package com.guandata.atlas.service.planningtable.model.field.valueobj;

import lombok.Data;

import java.util.List;

@Data
public class FieldExt {

    private FieldTextSelect fieldFormat;

    private FieldRemark fieldRemark;



    @Data
    public static class FieldTextSelect{

        private String elementType;

        private List<String> optionalValues;

        private boolean enable;
    }

    @Data
    public static class FieldRemark{

        private String remark;
    }
}
