package com.guandata.atlas.service.planningtable.model.filter.entity;

import com.guandata.atlas.dao.BaseFilter;
import com.guandata.atlas.dto.dataset.DSColumn;
import com.guandata.atlas.pojo.FilterInfo;
import com.guandata.atlas.service.planningtable.enums.SelectTypeEnum;
import com.guandata.atlas.service.planningtable.model.card.entity.Card;
import lombok.Getter;

/**
 * 日期筛选器
 */
@Getter
public class CalendarFilter extends IntervalFilter {

    public CalendarFilter(BaseFilter baseFilter, DSColumn dsColumn, Card card){
        super(baseFilter, dsColumn, card);
    }

    public CalendarFilter(FilterInfo filterInfo, Card card){
        super(filterInfo, card);
    }

    @Override
    public String getSelectType() {
        return SelectTypeEnum.CALENDAR.getType();
    }
}
