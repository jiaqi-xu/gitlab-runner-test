package com.guandata.atlas.service.planningtable.model.filter.entity;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.Lists;
import com.guandata.atlas.common.utils.AutowiredWrapperUtil;
import com.guandata.atlas.dao.BaseFilter;
import com.guandata.atlas.dto.dataset.DSColumn;
import com.guandata.atlas.enums.ErrorCode;
import com.guandata.atlas.exception.AtlasException;
import com.guandata.atlas.pojo.FilterInfo;
import com.guandata.atlas.service.dataset.model.DataSet;
import com.guandata.atlas.service.dataset.model.valueobj.FilterDataDP;
import com.guandata.atlas.service.planningtable.enums.DefaultTypeEnum;
import com.guandata.atlas.service.planningtable.enums.ElementsTypeEnum;
import com.guandata.atlas.service.planningtable.enums.SelectTypeEnum;
import com.guandata.atlas.service.planningtable.enums.SortTypeEnum;
import com.guandata.atlas.service.planningtable.model.card.entity.Card;
import com.guandata.atlas.service.planningtable.model.filter.valueobj.FilterExt;
import com.guandata.atlas.service.planningtable.model.filter.valueobj.OptionalDP;
import com.guandata.atlas.service.planningtable.model.page.entity.Page;
import com.guandata.atlas.service.planningtable.model.valueobj.LimitDP;
import com.guandata.atlas.service.planningtable.repository.FilterRepository;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;
import org.jooq.*;
import org.jooq.impl.DSL;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 选择筛选器
 */
@Getter
public class ElementFilter extends Filter {

    private ElementsTypeEnum elementsType;

    private OptionalDP optionalDP;

    public ElementFilter(BaseFilter baseFilter, DSColumn dsColumn, Card card){
        super(baseFilter, dsColumn, card);
        this.elementsType = ElementsTypeEnum.MULTI;
        this.optionalDP = new OptionalDP(false, SortTypeEnum.DEFAULT.getCode());
        this.defaultType = DefaultTypeEnum.NONE;
        FilterExt filterExt = JSON.parseObject(baseFilter.getExt(), FilterExt.class);
        if (filterExt != null){
            this.elementsType = ElementsTypeEnum.getElementsType(filterExt.getSubType());
            this.optionalDP = new OptionalDP(filterExt.isShowAll(), filterExt.getSortType());
            this.defaultType = DefaultTypeEnum.getDefaultType(filterExt.getDefaultType());
        }
    }

    public ElementFilter(FilterInfo filterInfo, Card card){
        super(filterInfo, card);
        this.elementsType = ElementsTypeEnum.getElementsType(filterInfo.getSubType());
        this.optionalDP = new OptionalDP(false, SortTypeEnum.DEFAULT.getCode());
        FilterInfo.Optional optional = filterInfo.getOptional();
        if (optional != null) {
            this.optionalDP = new OptionalDP(optional.isShowAll(), optional.getSortType());
        }
    }

    @Override
    public String getSelectType() {
        return SelectTypeEnum.DS_ELEMENTS.getType();
    }

    @Override
    public String getSubType() {
        return elementsType.getCode();
    }

    @Override
    public Condition buildAndCondition() {

        Field<Object> field = getJooqField();

        switch (elementsType){
            case MULTI:
                return field.in(selectValues);
            case SINGLE:
                return field.eq(selectValues.get(0));
            default:
                throw new AtlasException(ErrorCode.PARAME_ERROR_CODE);
        }
    }

    @Override
    public OrderField getOrderField() {

        Field field = DSL.field("convert(`"+getDsColumn().getColumnId()+"` using gbk)");
        SortTypeEnum sortType = optionalDP.getSortTypeEnum();
        switch (sortType){
            case ASC:
                return field.asc();
            case DESC:
                return field.desc();
            default:
                return null;
        }
    }

    /**
     * 获取列表中第一个值的
     * @param page
     * @return
     */
    public void fillFilterDefaultValue(Page page) {
        if (this.defaultType != DefaultTypeEnum.FIRSTVALUE) {
            return;
        }

        DataSet dataSet = page.getDataSet();
        FilterDataDP filterData = dataSet.getFilterData(new FilterInfo(this), Lists.newArrayList(), new LimitDP(1L,1L));
        this.defaultValues = filterData.getValues();
    }

    /**
     * 获取列表中第一个值的
     * @param card
     * @return
     */
    public void fillFilterDefaultValue(Card card) {

        if (this.defaultType != DefaultTypeEnum.FIRSTVALUE) {
            return;
        }

        FilterDataDP filterData
                = getFilterData(card, Lists.newArrayList(), null, new LimitDP(1L, 1L));
        this.defaultValues = filterData.getValues();
    }

    @Override
    public FilterDataDP getFilterData(Card card,
                                      List<FilterInfo> conditionsInfo,
                                      String search,
                                      LimitDP limitDP) {
        DataSet dataSet = card.getPage().getDataSet();
        String tableName = dataSet.getTableName();
        DSLContext dslContext = AutowiredWrapperUtil.getDslContext();

        Condition condition = DSL.trueCondition();
        if (StringUtils.isNotBlank(search)) {
            condition = condition.and(this.buildLikeCondition(search));
        }

        List<Filter> filters = FilterRepository.transFilters(conditionsInfo, card);
        List<Condition> whereConditions
                = filters.stream().filter(s -> s.isAggBeforeFilter()).map(s -> s.buildAndCondition()).collect(Collectors.toList());
        List<Condition> havingConditions
                = filters.stream().filter(s -> s.isAggAfterFilter()).map(s -> s.buildAndCondition()).collect(Collectors.toList());
        if (isAggAfterFilter()) {
            havingConditions.add(condition);
        } else {
            whereConditions.add(condition);
        }

        List<OrderField> orderFields = Lists.newArrayList();
        OrderField orderField = this.getOrderField();
        if (orderField != null){
            orderFields.add(orderField);
        }
        SelectSeekStepN selectSeekStepN = dslContext.selectDistinct(getField().getAggJooqField().as(DSL.name(this.getDsColumn().getColumnId())))
                .from(DSL.table(DSL.name(tableName+"_update")))
                .where(whereConditions)
                .groupBy(card.getFieldsDP().buildGroupByFields())
                .having(havingConditions)
                .orderBy(orderFields);
        String sql = selectSeekStepN.limit(limitDP.getOffset(), limitDP.getPageSize()).toString();
        Result<Record> result = dslContext.fetch(sql, limitDP.getOffset(), limitDP.getPageSize());
        List<String> datas = result.stream().map(s -> {
            Object obj = s.get(this.getDsColumn().getColumnId());
            return obj == null ? null : String.valueOf(obj);
        }).collect(Collectors.toList());

        return new FilterDataDP(datas, Long.valueOf(datas.size()), datas.size());
    }
}
