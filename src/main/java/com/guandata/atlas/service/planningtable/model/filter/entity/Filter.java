package com.guandata.atlas.service.planningtable.model.filter.entity;

import com.alibaba.fastjson.JSON;
import com.guandata.atlas.dao.BaseFilter;
import com.guandata.atlas.dto.dataset.DSColumn;
import com.guandata.atlas.pojo.FilterInfo;
import com.guandata.atlas.service.dataset.model.valueobj.FilterDataDP;
import com.guandata.atlas.service.planningtable.enums.AggrTypeEnum;
import com.guandata.atlas.service.planningtable.enums.DefaultTypeEnum;
import com.guandata.atlas.service.planningtable.enums.ElementsTypeEnum;
import com.guandata.atlas.service.planningtable.enums.SelectTypeEnum;
import com.guandata.atlas.service.planningtable.model.card.entity.Card;
import com.guandata.atlas.service.planningtable.model.field.entity.DimField;
import com.guandata.atlas.service.planningtable.model.field.entity.Field;
import com.guandata.atlas.service.planningtable.model.filter.valueobj.FilterExt;
import com.guandata.atlas.service.planningtable.model.valueobj.LimitDP;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;
import org.jooq.Condition;
import org.jooq.OrderField;
import org.jooq.impl.DSL;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Getter
public abstract class Filter {
    /**
     * 过滤参数
     */
    protected List<String> selectValues;

    private String aliasName;

    private Integer seqNo;

    protected List<String> defaultValues;

    protected DefaultTypeEnum defaultType;

    private AggrTypeEnum aggrTypeEnum;

    private String fdId;

    private Field field;

    private DSColumn dsColumn;

    private String pageId;

    private String cdId;

    private String search;

    public Filter(BaseFilter baseFilter, DSColumn dsColumn, Card card) {
        this.aliasName = baseFilter.getAlias();
        this.seqNo = baseFilter.getSeqNo();
        this.aggrTypeEnum = AggrTypeEnum.getAggrType(baseFilter.getAggrType());
        this.fdId = baseFilter.getFdId();
        this.dsColumn = dsColumn;
        this.pageId = baseFilter.getPgId();
        this.cdId = baseFilter.getCdId();
        this.search = baseFilter.getSearch();
        FilterExt filterExt = JSON.parseObject(baseFilter.getExt(), FilterExt.class);
        if (filterExt != null) {
            this.defaultValues = filterExt.getDefaultValues();
        }

        if (card != null) {
            Map<String, Field> colIdToFieldMap = card.fillFieldsDP().getFieldsDP().getAggFields().stream()
                    .collect(Collectors.toMap(s->s.getColumn().getColumnId(), v->v));
            this.field = colIdToFieldMap.get(dsColumn.getColumnId());
        }

        if (this.field == null) {
            this.field = new DimField(card, this);
        }
    }

    public Filter(FilterInfo filterInfo, Card card) {
        this.aliasName = filterInfo.getAlias();
        this.seqNo = filterInfo.getSeqNo();
        this.defaultValues = filterInfo.getDefaultValues();
        this.defaultType = DefaultTypeEnum.getDefaultType(filterInfo.getDefaultType());
        this.aggrTypeEnum = AggrTypeEnum.getAggrType(filterInfo.getAggType());
        this.fdId = filterInfo.getFdId();
        this.dsColumn = filterInfo.getColumn();
        this.pageId = filterInfo.getPgId();
        this.cdId = filterInfo.getCdId();
        this.selectValues = filterInfo.getChoice();
        this.search = filterInfo.getSearch();

        if (card != null) {
            Map<String, Field> colIdToFieldMap = card.fillFieldsDP().getFieldsDP().getAggFields().stream()
                    .collect(Collectors.toMap(s->s.getColumn().getColumnId(), v->v));
            this.field = colIdToFieldMap.get(dsColumn.getColumnId());
        }

        if (this.field == null) {
            this.field = new DimField(card, this);
        }
    }

    public String getSelectType() {
        return SelectTypeEnum.DS_ELEMENTS.getType();
    }

    public String getSubType() {
        return ElementsTypeEnum.MULTI.getCode();
    }

    public abstract Condition buildAndCondition();

    public Boolean isAggBeforeFilter() {
        return field == null || field instanceof DimField || isPageFilter();
    }

    public Boolean isAggAfterFilter() {
        return !isAggBeforeFilter();
    }


    protected org.jooq.Field getJooqField() {
        if (field == null || field instanceof DimField) {
            return DSL.field(DSL.name(dsColumn.getColumnId()));
        }

        return this.field.getAggJooqField();
    }

    public Condition buildLikeCondition(String search) {
        return DSL.field(field.getAggJooqField()).like(DSL.concat("%", search, "%"));
    }

    public abstract OrderField getOrderField();

    public boolean isPageFilter() {
        return StringUtils.isBlank(cdId);
    }

    public boolean isCardFilter() {
        return StringUtils.isNotBlank(cdId);
    }

    public void buildDefaultValues(List<String> defaultValues) {
        this.defaultValues = defaultValues;
    }

    public abstract FilterDataDP getFilterData(Card card,
                               List<FilterInfo> conditionsInfo,
                               String search, LimitDP limitDP);

    /**
     * 是否是有效的筛选器
     * @return
     */
    public boolean isValidFilter() {

        if (CollectionUtils.isEmpty(selectValues)) {
            return false;
        }

        for (String selectValue : selectValues) {
            if (StringUtils.isNotBlank(selectValue)) {
                return true;
            }
        }

        return false;
    }
}
