package com.guandata.atlas.service.planningtable.model.filter.entity;

import com.alibaba.fastjson.JSON;
import com.guandata.atlas.dao.BaseFilter;
import com.guandata.atlas.dto.dataset.DSColumn;
import com.guandata.atlas.enums.ErrorCode;
import com.guandata.atlas.exception.AtlasException;
import com.guandata.atlas.pojo.FilterInfo;
import com.guandata.atlas.service.dataset.model.valueobj.FilterDataDP;
import com.guandata.atlas.service.planningtable.enums.DefaultTypeEnum;
import com.guandata.atlas.service.planningtable.enums.OperatorTypeEnum;
import com.guandata.atlas.service.planningtable.enums.SelectTypeEnum;
import com.guandata.atlas.service.planningtable.model.card.entity.Card;
import com.guandata.atlas.service.planningtable.model.filter.valueobj.FilterExt;
import com.guandata.atlas.service.planningtable.model.filter.valueobj.OperatorDP;
import com.guandata.atlas.service.planningtable.model.valueobj.LimitDP;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;
import org.jooq.Condition;
import org.jooq.Field;
import org.jooq.OrderField;
import org.jooq.impl.DSL;

import java.util.List;

/**
 * 日期筛选器
 */
@Getter
public class IntervalFilter extends Filter {

    private OperatorDP operateDP;

    public IntervalFilter(BaseFilter baseFilter, DSColumn dsColumn, Card card){
        super(baseFilter, dsColumn, card);
        this.operateDP = new OperatorDP(OperatorTypeEnum.BT.getCode());
        this.defaultType = DefaultTypeEnum.getDefaultType(DefaultTypeEnum.FIXVALUE.getCode());
        FilterExt filterExt = JSON.parseObject(baseFilter.getExt(), FilterExt.class);
        if (filterExt != null){
            this.operateDP = new OperatorDP(filterExt.getSubType());
        }
    }

    public IntervalFilter(FilterInfo filterInfo, Card card){
        super(filterInfo, card);
        this.operateDP = new OperatorDP(filterInfo.getSubType());
    }

    @Override
    public String getSelectType() {
        return SelectTypeEnum.DS_INTERVAL.getType();
    }

    @Override
    public String getSubType() {
        return operateDP.getOperatorType().getCode();
    }

    @Override
    public Condition buildAndCondition() {

        Condition whereCondition = DSL.trueCondition();
        Field<Object> field = getJooqField();
        switch (operateDP.getOperatorType()){
            case BT:
            {
                int size = selectValues.size();
                if (size == 1) {
                    return whereCondition.and(field.ge(selectValues.get(0)));
                }
                String first = selectValues.get(0);
                String second = selectValues.get(1);
                if (StringUtils.isNotBlank(first) && StringUtils.isNotBlank(second)) {
                    return whereCondition.and(field.between(selectValues.get(0), selectValues.get(1)));
                }

                if (StringUtils.isNotBlank(first)){
                    return whereCondition.and(field.ge(selectValues.get(0)));
                }

                return whereCondition.and(field.le(selectValues.get(1)));
            }
            case GT:
                return whereCondition.and(field.gt(selectValues.get(0)));
            case GE:
                return whereCondition.and(field.ge(selectValues.get(0)));
            case LT:
                return whereCondition.and(field.lt(selectValues.get(0)));
            case LE:
                return whereCondition.and(field.le(selectValues.get(0)));
            case EQ:
                return whereCondition.and(field.eq(selectValues.get(0)));
            case NE:
                return whereCondition.and(field.ne(selectValues.get(0)));
            case IS_NULL:
                return whereCondition.and(field.isNull().or(field.length().eq(0)));
            case NOT_NULL:
                return whereCondition.and(field.isNotNull().and(field.length().gt(0)));
            default:
                throw new AtlasException(ErrorCode.PARAME_ERROR_CODE);
        }
    }

    @Override
    public OrderField getOrderField() {
        throw new AtlasException(ErrorCode.NOT_SUPPORT_ELEMENT_TYPE);
    }

    @Override
    public FilterDataDP getFilterData(Card card, List<FilterInfo> conditionsInfo, String search, LimitDP limitDP) {
        throw new AtlasException(ErrorCode.PARAME_ERROR_CODE);
    }

    @Override
    public boolean isValidFilter() {

        OperatorTypeEnum operatorType = operateDP.getOperatorType();
        if (operatorType == OperatorTypeEnum.IS_NULL
                || operatorType == OperatorTypeEnum.NOT_NULL) {
            return true;
        }

        return super.isValidFilter();
    }


}
