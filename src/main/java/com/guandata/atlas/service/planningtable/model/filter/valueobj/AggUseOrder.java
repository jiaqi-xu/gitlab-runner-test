package com.guandata.atlas.service.planningtable.model.filter.valueobj;

import com.guandata.atlas.service.planningtable.model.filter.entity.Filter;
import org.jooq.Condition;

public interface AggUseOrder {

    Condition buildCondition(Filter filter);
}
