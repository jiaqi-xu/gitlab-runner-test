package com.guandata.atlas.service.planningtable.model.filter.valueobj;

import lombok.Data;

import java.util.List;

@Data
public class FilterExt {

    private String subType;

    private List<String> defaultValues;

    private String defaultType;

    private boolean showAll;

    private String sortType;
}
