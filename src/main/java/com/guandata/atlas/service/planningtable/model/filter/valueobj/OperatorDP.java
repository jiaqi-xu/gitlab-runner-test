package com.guandata.atlas.service.planningtable.model.filter.valueobj;

import com.guandata.atlas.enums.ErrorCode;
import com.guandata.atlas.exception.AtlasException;
import com.guandata.atlas.service.planningtable.enums.OperatorTypeEnum;
import lombok.Getter;

@Getter
public class OperatorDP {

    private OperatorTypeEnum operatorType;

    public OperatorDP(String code){
        OperatorTypeEnum operatorType = OperatorTypeEnum.getOperatorType(code);
        if (operatorType == null) {
           throw new AtlasException(ErrorCode.NOT_SUPPORT_OPERATOR_TYPE);
        }
        this.operatorType = operatorType;
    }
}
