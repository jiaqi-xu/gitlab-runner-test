package com.guandata.atlas.service.planningtable.model.filter.valueobj;

import com.guandata.atlas.enums.ErrorCode;
import com.guandata.atlas.exception.AtlasException;
import com.guandata.atlas.service.planningtable.enums.SortTypeEnum;
import lombok.Getter;

@Getter
public class OptionalDP {

    private Boolean showAll;

    private SortTypeEnum sortTypeEnum;

    public OptionalDP(Boolean showAll, String code) {
        SortTypeEnum sortTypeEnum = SortTypeEnum.getSortType(code);
        if (sortTypeEnum == null) {
            throw new AtlasException(ErrorCode.NOT_SUPPORT_SORT_TYPE);
        }
        this.sortTypeEnum = sortTypeEnum;

        this.showAll = showAll;
    }
}
