package com.guandata.atlas.service.planningtable.model.operate;

import com.guandata.atlas.dto.ResultData;
import com.guandata.atlas.dto.dataset.DSColumn;
import com.guandata.atlas.service.planningtable.model.field.entity.Field;
import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class BatchModifyBO {

    private List<DSColumn> columns;

    private Map<String, Object> resultData;

    private String originalValue;

    private String value;

    private List<Field> allFields;
}
