package com.guandata.atlas.service.planningtable.model.operate;

import com.google.common.collect.Lists;
import com.guandata.atlas.common.utils.AutowiredWrapperUtil;
import com.guandata.atlas.common.utils.SpringBeanUtil;
import com.guandata.atlas.dto.BatchUpdateDataDTO;
import com.guandata.atlas.dto.DataUpdateParamExt;
import com.guandata.atlas.dto.ResultCondition;
import com.guandata.atlas.dto.dataset.DSColumn;
import com.guandata.atlas.enums.ErrorCode;
import com.guandata.atlas.exception.AtlasException;
import com.guandata.atlas.pojo.FieldInfo;
import com.guandata.atlas.service.DataUpdateService;
import com.guandata.atlas.service.ModifyHistoryService;
import com.guandata.atlas.service.dataset.model.DataSet;
import com.guandata.atlas.service.dataset.utils.DatasetUtils;
import com.guandata.atlas.service.planningtable.enums.AggrTypeEnum;
import com.guandata.atlas.service.planningtable.model.card.entity.Card;
import com.guandata.atlas.service.planningtable.model.field.entity.DimField;
import com.guandata.atlas.service.planningtable.model.field.entity.Field;
import com.guandata.atlas.service.planningtable.model.filter.entity.Filter;
import com.guandata.atlas.service.planningtable.repository.FilterRepository;
import org.jooq.Condition;
import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.Result;
import org.jooq.impl.DSL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import static org.jooq.impl.DSL.asterisk;

public abstract class DataRatioModify implements IModify{

    protected static final Logger logger = LoggerFactory.getLogger(DataRatioModify.class);

    protected IAggType iAggType;

    protected ModifyAfterDP modifyAfterDP = new ModifyAfterDP();

    protected BatchUpdateDataDTO batchUpdateDataDTO;

    protected List<Map<String, Object>> records;

    protected Field field;

    public DataRatioModify(Field field,
                           BatchUpdateDataDTO batchUpdateDataDTO,
                           List<Map<String, Object>> records) {

        this.batchUpdateDataDTO = batchUpdateDataDTO;
        this.records = records;
        this.field = field;
        AggrTypeEnum aggrType = AggrTypeEnum.getAggrType(field.getAggType());
        switch (aggrType){
            case SUM:
                iAggType = new SumAggType();
                break;
            case MAX:
                iAggType = new MaxAggType();
                break;
            default:
                throw new AtlasException(ErrorCode.PARAME_ERROR_CODE);
        }
    }

    /**
     * 获取需要修改成的目标值
     * @param batchModifyBO
     * @return
     */
    protected abstract String getTargetValue(BatchModifyBO batchModifyBO);

    /**
     * 后置处理器
     * @param dataUpdateParam
     */
    protected abstract void fillPostProcessDP(DataUpdateParamExt dataUpdateParam);

    @Override
    public void handleBatchModify() {

        String value = batchUpdateDataDTO.getValue();
        Card card = batchUpdateDataDTO.getCard();
        DataUpdateParamExt dataUpdateParam = new DataUpdateParamExt();
        dataUpdateParam.setCdId(batchUpdateDataDTO.getCdId());
        dataUpdateParam.setTableName(batchUpdateDataDTO.getTableName());
        dataUpdateParam.setPgId(card.getPage().getPgId());

        List<Field> allFields = card.fillFieldsDP().getFieldsDP().getFields();
        List<FieldInfo> fields = allFields.stream().map(s -> new FieldInfo(s)).collect(Collectors.toList());
        List<DSColumn> columns = fields.stream().filter(f -> f.getFdProperty() != null)
                .map(f -> f.getColumn()).filter(Objects::nonNull).collect(Collectors.toList());
        dataUpdateParam.setAggrPK(
                allFields.stream().filter(s-> s instanceof DimField).map(s->s.getColumn()).collect(Collectors.toList()));
        dataUpdateParam.setField(
                allFields.stream().filter(s-> s.getFdId().equals(batchUpdateDataDTO.getFdId())).map(s->s.getColumn()).findFirst().get());
        dataUpdateParam.setTargetColumn(
                allFields.stream().filter(s-> s.getFdId().equals(batchUpdateDataDTO.getValue())).map(s->s.getColumn()).findAny().orElse(null));
        dataUpdateParam.setConditions(batchUpdateDataDTO.getConditions());

        DataSet dataSet = card.getPage().getDataSet();
        dataUpdateParam.setDsColumns(dataSet.getColumnListDP().getColumnList());

        List<DSColumn> aggrPKs = dataUpdateParam.getAggrPK();
        for (List<Map<String, Object>> resultData : Lists.partition(records, 100)) {
                List<List<ResultCondition>> groupByConditions = new ArrayList<>();

                List<String> modifiedFroms = Lists.newArrayList();
                List<String> modifiedTos = Lists.newArrayList();
                for (Map<String, Object> resultDatum : resultData) {
                    List<ResultCondition> groupByCond = Lists.newArrayList();
                    for (DSColumn aggrPK : aggrPKs) {
                        ResultCondition condition = new ResultCondition();
                        condition.setColumn(aggrPK);
                        condition.setValues(Lists.newArrayList(DatasetUtils.getValue(aggrPK.getColumnId(),columns,resultDatum)));
                        groupByCond.add(condition);
                    }
                    groupByConditions.add(groupByCond);

                    String originalValue = DatasetUtils.getValue(dataUpdateParam.getField().getColumnId(), columns, resultDatum);
                    modifiedFroms.add(originalValue);

                    BatchModifyBO batchModifyBO = buildBatchModifyBO(columns, resultDatum, allFields, value, originalValue);
                    modifiedTos.add(getTargetValue(batchModifyBO));
                }

                dataUpdateParam.setModifiedFroms(modifiedFroms);
                dataUpdateParam.setModifiedTos(modifiedTos);
                dataUpdateParam.setGroupByConditions(groupByConditions);

                handle(dataUpdateParam);
        }

        fillPostProcessDP(dataUpdateParam);
    }

    @Override
    public String handleSingleModify() {
        handleBatchModify();
        return modifyAfterDP.getValue();
    }

    private void handle(DataUpdateParamExt param){
        String dsTable = param.getTableName();
        Result<Record> allResultRecords = getAllResultRecords(param);

        List<List<ResultCondition>> groupByConditions = param.getGroupByConditions();

        Map<String, List<Record>> keyToRecords = allResultRecords.stream().collect(Collectors.groupingBy(s ->
                buildGroupKey(param.getAggrPK(), s)));

        List<List<Record>> parAffectedRecords = Lists.newArrayList();
        List<List<Map<String, Object>>> modifyFieldRecords = Lists.newArrayList();

        for (int i = 0; i < groupByConditions.size(); i++) {
            List<Record> affectedRecords = keyToRecords.get(buildGroupKeyV2(groupByConditions.get(i), param.getAggrPK()));
            if (CollectionUtils.isEmpty(affectedRecords)) {
                logger.warn("No matched data in table {0}.", dsTable);
                continue;
            }

            param.setModifiedFrom(param.getModifiedFroms().get(i));
            param.setModifiedTo(param.getModifiedTos().get(i));

            List<Map<String, Object>> modifyFieldRecord = Lists.newArrayList();
            buildAffectedRecords(affectedRecords,param, modifyFieldRecord);
            logger.info("affected row count of dataset is {0}", affectedRecords.size());

            parAffectedRecords.add(affectedRecords);
            modifyFieldRecords.add(modifyFieldRecord);
        }

        String modifiedFieldId = param.getField().getColumnId();
        DataUpdateService dataUpdateService = SpringBeanUtil.getBean(DataUpdateService.class);
        ModifyHistoryService modifyHistoryService = SpringBeanUtil.getBean(ModifyHistoryService.class);

        List<Record> flatRecords = parAffectedRecords.stream().flatMap(s->s.stream()).collect(Collectors.toList());
        if (dataUpdateService.updateList(dsTable, modifiedFieldId, flatRecords)) {
            modifyHistoryService.batchProcessModifyHistory(dsTable, param,  parAffectedRecords, modifyFieldRecords);
        }
    }

    protected void buildAffectedRecords(List<Record> affectedRecords,
                                        DataUpdateParamExt param,
                                        List<Map<String, Object>> modifyFieldRecord) {

        iAggType.buildAffectedRecords(affectedRecords, param, modifyFieldRecord);
    }

    private String buildGroupKey(List<DSColumn> aggPks, Record record) {

        StringBuilder key = new StringBuilder();
        for (DSColumn aggPk : aggPks) {
            Object o = record.get(aggPk.getColumnId());
            key.append(o == null ? "" : String.valueOf(o));
            key.append("_");
        }

        return key.substring(0, key.length()-1);
    }

    private String buildGroupKeyV2(List<ResultCondition> groupByCondition, List<DSColumn> aggPks) {

        Map<String, ResultCondition> idToResultConditionMap
                = groupByCondition.stream().collect(Collectors.toMap(s -> s.getColumn().getColumnId(), v -> v, (k,v)->k));
        StringBuilder key = new StringBuilder();
        for (DSColumn aggPk : aggPks) {
            ResultCondition o = idToResultConditionMap.get(aggPk.getColumnId());
            key.append(o == null && !CollectionUtils.isEmpty(o.getValues()) ? "" : String.valueOf(o.getValues().get(0)));
            key.append("_");
        }

        return key.substring(0, key.length()-1);
    }

    private Result<Record> getAllResultRecords(DataUpdateParamExt dataUpdateParam) {
        String tableName = dataUpdateParam.getTableName();
        DSLContext dslContext = AutowiredWrapperUtil.getDslContext();

        Condition whereCondition = DSL.trueCondition();
        List<ResultCondition> conditions = dataUpdateParam.getConditions();
        Card card = batchUpdateDataDTO.getCard();
        if (!CollectionUtils.isEmpty(conditions)){
            for (ResultCondition condition : conditions) {
                if (CollectionUtils.isEmpty(condition.getValues())) {
                    continue;
                }

                Filter tmpFilter = FilterRepository.transSingleFilter(condition.getFilterInfo(), card);
                if (tmpFilter.isAggAfterFilter()) {
                    continue;
                }
                whereCondition = whereCondition.and(DSL.field(DSL.name(condition.getColumn().getColumnId())).in(condition.getValues()));
            }
        }

        List<List<ResultCondition>> groupByConditions = dataUpdateParam.getGroupByConditions();
        if (!CollectionUtils.isEmpty(groupByConditions)) {
            List<Condition> orConditions = Lists.newArrayList();
            for (List<ResultCondition> groupByCondition : groupByConditions) {
                List<Condition> andConditions = Lists.newArrayList();
                for (ResultCondition resultCondition : groupByCondition) {
                    if (!CollectionUtils.isEmpty(resultCondition.getValues())) {
                        andConditions.add(DSL.field(DSL.name(resultCondition.getColumn().getColumnId())).eq(resultCondition.getValues().get(0)));
                    } else {
                        andConditions.add(DSL.field(DSL.name(resultCondition.getColumn().getColumnId())).isNull());
                    }
                }

                orConditions.add(DSL.and(andConditions));
            }

            whereCondition = whereCondition.and(DSL.or(orConditions));
        }
        return dslContext.select(asterisk()).from(DSL.table(DSL.name(tableName+"_update"))).where(whereCondition).fetch();
    }


    private BatchModifyBO buildBatchModifyBO(
            List<DSColumn> columns, Map<String, Object> resultData, List<Field> allFields,
            String value, String originalValue){

        BatchModifyBO batchModifyBO = new BatchModifyBO();
        batchModifyBO.setColumns(columns);
        batchModifyBO.setResultData(resultData);
        batchModifyBO.setValue(value);
        batchModifyBO.setOriginalValue(originalValue);
        batchModifyBO.setAllFields(allFields);

        return batchModifyBO;
    }
}
