package com.guandata.atlas.service.planningtable.model.operate;

import com.guandata.atlas.common.utils.AtlasOperand;
import com.guandata.atlas.dto.BatchUpdateDataDTO;
import com.guandata.atlas.dto.DataUpdateParamExt;
import com.guandata.atlas.service.planningtable.enums.MetaTypeEnum;
import com.guandata.atlas.service.planningtable.model.field.entity.AggrField;
import com.guandata.atlas.service.planningtable.model.field.entity.Field;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.Map;

public class FixedValueModify extends DataRatioModify{

    public FixedValueModify(Field field, BatchUpdateDataDTO batchUpdateDataDTO,
                            List<Map<String, Object>> records) {
        super(field, batchUpdateDataDTO,records);
    }

    @Override
    protected String getTargetValue(BatchModifyBO batchModifyBO) {

        String targetValue = batchModifyBO.getValue();
        if (field.getMetaType() == MetaTypeEnum.METRIC) {
            return new AtlasOperand(targetValue).toPlainString();
        }
        return batchModifyBO.getValue();
    }

    @Override
    protected void fillPostProcessDP(DataUpdateParamExt dataUpdateParam) {
        modifyAfterDP.setValue(dataUpdateParam.getModifiedTo());
    }
}
