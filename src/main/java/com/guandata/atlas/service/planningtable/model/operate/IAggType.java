package com.guandata.atlas.service.planningtable.model.operate;

import com.guandata.atlas.dto.DataUpdateParamExt;
import org.jooq.Record;

import java.util.List;
import java.util.Map;

public interface IAggType {

    void buildAffectedRecords(List<Record> affectedRecords,
                              DataUpdateParamExt param,
                              List<Map<String, Object>> modifyFieldRecord);

    void buildReplaceAffectedRecords(List<Record> affectedRecords,
                              DataUpdateParamExt param,
                              List<Map<String, Object>> modifyFieldRecord);
}
