package com.guandata.atlas.service.planningtable.model.operate;

import com.guandata.atlas.dto.BatchUpdateDataDTO;
import com.guandata.atlas.dto.TableResult;

import java.util.List;
import java.util.Map;

public interface IModify {

    /**
     * 处理批量修改
     */
    void handleBatchModify();

    /**
     * 单个修改
     */
    String handleSingleModify();
}
