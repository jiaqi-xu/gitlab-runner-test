package com.guandata.atlas.service.planningtable.model.operate;

import com.guandata.atlas.common.utils.AtlasOperand;
import com.guandata.atlas.dto.BatchUpdateDataDTO;
import com.guandata.atlas.dto.DataUpdateParamExt;
import com.guandata.atlas.enums.ErrorCode;
import com.guandata.atlas.exception.AtlasException;
import com.guandata.atlas.service.dataset.utils.DatasetUtils;
import com.guandata.atlas.service.planningtable.enums.AggrTypeEnum;
import com.guandata.atlas.service.planningtable.enums.MetaTypeEnum;
import com.guandata.atlas.service.planningtable.model.field.entity.AggrField;
import com.guandata.atlas.service.planningtable.model.field.entity.Field;
import org.apache.commons.lang3.StringUtils;
import org.jooq.Record;

import java.util.List;
import java.util.Map;

public class
ImportPaddingModify extends DataRatioModify{

    public ImportPaddingModify(Field field, BatchUpdateDataDTO batchUpdateDataDTO,
                               List<Map<String, Object>> records) {
        super(field, batchUpdateDataDTO, records);
        if (AggrTypeEnum.MAX.getType().equals(field.getAggType())) {
            throw new AtlasException(ErrorCode.PARAME_ERROR_CODE);
        }
    }

    @Override
    protected String getTargetValue(BatchModifyBO batchModifyBO) {

        String value = batchModifyBO.getValue();
        List<Field> allFields = batchModifyBO.getAllFields();
        String dsColumnId = allFields.stream().filter(s->s.getFdId().equals(value)).map(s->s.getColumnId()).findAny().get();
        String targetValue = DatasetUtils.getValue(dsColumnId, batchModifyBO.getColumns(), batchModifyBO.getResultData());

        if (field.getMetaType() == MetaTypeEnum.METRIC) {
            return new AtlasOperand(targetValue).toPlainString();
        }

        return targetValue;
    }

    @Override
    public void buildAffectedRecords(List<Record> affectedRecords,
                                     DataUpdateParamExt dataUpdateParam,
                                     List<Map<String, Object>> modifyFieldRecord){
        iAggType.buildReplaceAffectedRecords(affectedRecords, dataUpdateParam, modifyFieldRecord);
    }

    @Override
    protected void fillPostProcessDP(DataUpdateParamExt dataUpdateParam) {
    }
}
