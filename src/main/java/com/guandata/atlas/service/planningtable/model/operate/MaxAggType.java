package com.guandata.atlas.service.planningtable.model.operate;

import com.google.common.collect.Maps;
import com.guandata.atlas.dto.DataUpdateParamExt;
import org.jooq.Record;
import org.jooq.impl.DSL;

import java.util.List;
import java.util.Map;

public class MaxAggType implements IAggType{
    @Override
    public void buildAffectedRecords(List<Record> affectedRecords, DataUpdateParamExt param, List<Map<String, Object>> modifyFieldRecord) {

        String modifiedFieldId = param.getField().getColumnId();
        String modifiedTo = param.getModifiedTo();

        for (Record f : affectedRecords) {
            Map<String, Object> modifyR = Maps.newHashMap();
            modifyR.put("modified_from",String.valueOf(f.getValue(DSL.field(modifiedFieldId))));
            modifyR.put("modified_to",modifiedTo);
            f.set(DSL.field(modifiedFieldId), modifiedTo);
            modifyFieldRecord.add(modifyR);
        }
    }

    @Override
    public void buildReplaceAffectedRecords(List<Record> affectedRecords, DataUpdateParamExt param, List<Map<String, Object>> modifyFieldRecord) {
        buildAffectedRecords(affectedRecords, param, modifyFieldRecord);
    }
}
