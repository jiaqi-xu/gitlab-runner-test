package com.guandata.atlas.service.planningtable.model.operate;

import lombok.Data;

@Data
public class ModifyAfterDP {

    private String value;
}
