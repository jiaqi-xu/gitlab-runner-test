package com.guandata.atlas.service.planningtable.model.operate;

import com.guandata.atlas.common.utils.AtlasOperand;
import com.guandata.atlas.dto.BatchUpdateDataDTO;
import com.guandata.atlas.dto.DataUpdateParamExt;
import com.guandata.atlas.enums.ErrorCode;
import com.guandata.atlas.exception.AtlasException;
import com.guandata.atlas.service.dataset.utils.DatasetUtils;
import com.guandata.atlas.service.planningtable.enums.AggrTypeEnum;
import com.guandata.atlas.service.planningtable.enums.MetaTypeEnum;
import com.guandata.atlas.service.planningtable.model.field.entity.AggrField;
import com.guandata.atlas.service.planningtable.model.field.entity.Field;
import org.apache.commons.lang3.StringUtils;
import org.jooq.Record;

import java.util.List;
import java.util.Map;

public class ReplaceModify extends DataRatioModify{

    public ReplaceModify(Field field, BatchUpdateDataDTO batchUpdateDataDTO,
                         List<Map<String, Object>> records) {
        super(field, batchUpdateDataDTO,records);
    }

    @Override
    protected String getTargetValue(BatchModifyBO batchModifyBO) {
        List<Field> allFields = batchModifyBO.getAllFields();
        String dsColumnId = allFields.stream().filter(s->s.getFdId().equals(batchModifyBO.getValue())).map(s->s.getColumnId()).findAny().get();
        String targetValue = DatasetUtils.getValue(dsColumnId, batchModifyBO.getColumns(), batchModifyBO.getResultData());

        if (field.getMetaType() == MetaTypeEnum.METRIC) {
            return new AtlasOperand(targetValue).toPlainString();
        }

        return targetValue;
    }

    @Override
    protected void fillPostProcessDP(DataUpdateParamExt dataUpdateParam) {
    }
}
