package com.guandata.atlas.service.planningtable.model.operate;

import com.google.common.collect.Maps;
import com.guandata.atlas.common.utils.AtlasOperand;
import com.guandata.atlas.dto.DataUpdateParamExt;
import org.jooq.Record;
import org.jooq.impl.DSL;

import java.util.List;
import java.util.Map;

public class SumAggType implements IAggType{
    @Override
    public void buildAffectedRecords(List<Record> affectedRecords, DataUpdateParamExt param, List<Map<String, Object>> modifyFieldRecord) {
        String modifiedFieldId = param.getField().getColumnId();
        AtlasOperand modifiedTo = new AtlasOperand(param.getModifiedTo());
        AtlasOperand modifiedFrom = new AtlasOperand(param.getModifiedFrom());

        if (modifiedFrom.compareTo(AtlasOperand.ZERO) == 0) {
            for (Record f : affectedRecords) {
                Map<String, Object> modifyR = Maps.newHashMap();
                modifyR.put("modified_from",String.valueOf(f.getValue(DSL.field(modifiedFieldId))));
                AtlasOperand modifiedToDetail = modifiedTo.divide(new AtlasOperand(affectedRecords.size()));
                modifyR.put("modified_to",modifiedToDetail.toPlainString());
                f.set(DSL.field(modifiedFieldId), modifiedToDetail.toPlainString());
                modifyFieldRecord.add(modifyR);
            }
        } else {
            for (Record f : affectedRecords) {
                Map<String, Object> modifyR = Maps.newHashMap();
                Object v = f.getValue(DSL.field(modifiedFieldId));
                modifyR.put("modified_from",String.valueOf(v));
                AtlasOperand modifiedFromDetail = new AtlasOperand(v);
                AtlasOperand modifiedToDetail = modifiedFromDetail.multiply(modifiedTo).divide(modifiedFrom);
                modifyR.put("modified_to", modifiedToDetail.toPlainString());
                f.set(DSL.field(modifiedFieldId), modifiedToDetail.toPlainString());
                modifyFieldRecord.add(modifyR);
            }
        }
    }

    @Override
    public void buildReplaceAffectedRecords(List<Record> affectedRecords, DataUpdateParamExt dataUpdateParam, List<Map<String, Object>> modifyFieldRecord) {

        String modifiedFieldId = dataUpdateParam.getField().getColumnId();

        affectedRecords.stream().forEach(f -> {

            Map<String, Object> modifyR = Maps.newHashMap();
            modifyR.put("modified_from",String.valueOf(f.getValue(DSL.field(modifiedFieldId))));
            Object value = f.getValue(DSL.field(dataUpdateParam.getTargetColumn().getColumnId()));
            AtlasOperand modifiedToDetail = new AtlasOperand(value);
            modifyR.put("modified_to", modifiedToDetail.toPlainString());
            f.set(DSL.field(modifiedFieldId), modifiedToDetail.toPlainString());
            modifyFieldRecord.add(modifyR);
        });
    }
}
