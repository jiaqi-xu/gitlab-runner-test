package com.guandata.atlas.service.planningtable.model.operate;

import com.guandata.atlas.common.utils.AtlasOperand;
import com.guandata.atlas.dto.BatchUpdateDataDTO;
import com.guandata.atlas.dto.DataUpdateParamExt;
import com.guandata.atlas.service.planningtable.model.field.entity.Field;

import java.util.List;
import java.util.Map;

public class TimesModify extends DataRatioModify{

    public TimesModify(Field field, BatchUpdateDataDTO batchUpdateDataDTO, List<Map<String, Object>> records) {
        super(field, batchUpdateDataDTO, records);
    }

    @Override
    protected String getTargetValue(BatchModifyBO batchModifyBO) {
        AtlasOperand targetValue = new AtlasOperand(batchModifyBO.getOriginalValue()).multiply(new AtlasOperand(batchModifyBO.getValue()));
        return targetValue.toPlainString();
    }

    @Override
    protected void fillPostProcessDP(DataUpdateParamExt dataUpdateParam) {
    }
}
