package com.guandata.atlas.service.planningtable.model.page.entity;

import com.google.common.collect.Lists;
import com.guandata.atlas.client.cache.local.GuavaCacheWrapper;
import com.guandata.atlas.common.utils.UuidUtil;
import com.guandata.atlas.controller.request.FilterDataGetRequest;
import com.guandata.atlas.dao.PageInfoDO;
import com.guandata.atlas.enums.ErrorCode;
import com.guandata.atlas.exception.AtlasException;
import com.guandata.atlas.pojo.FieldInfo;
import com.guandata.atlas.pojo.FilterInfo;
import com.guandata.atlas.pojo.PageInfo;
import com.guandata.atlas.service.dataset.model.DataSet;
import com.guandata.atlas.service.dataset.model.valueobj.FilterDataDP;
import com.guandata.atlas.service.dataset.repository.DataSetRepository;
import com.guandata.atlas.service.planningtable.enums.PageStatusEnum;
import com.guandata.atlas.service.planningtable.model.card.entity.Card;
import com.guandata.atlas.service.planningtable.model.field.entity.BaseField;
import com.guandata.atlas.service.planningtable.model.filter.entity.Filter;
import com.guandata.atlas.service.planningtable.model.valueobj.FiltersDP;
import com.guandata.atlas.service.planningtable.model.valueobj.LimitDP;
import com.guandata.atlas.service.planningtable.repository.CardRepository;
import com.guandata.atlas.service.planningtable.repository.FieldRepository;
import com.guandata.atlas.service.planningtable.repository.FilterRepository;
import com.guandata.atlas.service.planningtable.repository.PageRepository;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.stream.Collectors;

@Getter
@NoArgsConstructor
public class Page {

    private String pgId;

    private String name;

    private DataSet dataSet;

    private PageStatusEnum pageStatusEnum;

    private List<Card> cardList = Lists.newArrayList();

    private FiltersDP filtersDP = new FiltersDP();

    private List<BaseField> initialFields = Lists.newArrayList();

    public Page(String pgId){
        this.pgId = pgId;
    }

    public Page(PageInfoDO pageInfo, List<FilterInfo> filterInfos) {
        this.pgId = pageInfo.getPgId();
        this.name = pageInfo.getName();
        this.dataSet = DataSetRepository.getDataSetById(pageInfo.getDsId());
        this.pageStatusEnum = PageStatusEnum.getPageStatus(pageInfo.getStatus());
        List<Filter> filters = FilterRepository.transFilters(filterInfos, null).stream()
                .filter(s->s.isPageFilter() && s.isValidFilter())
                .collect(Collectors.toList());
        filtersDP.fillByFilters(filters);
    }

    public Page(PageInfo pageInfo) {
        this.pgId = pageInfo.getPgId();
        if (StringUtils.isBlank(this.pgId)) {
            this.pgId = "PG_"+ UuidUtil.getUUID();
        }
        this.name = pageInfo.getName();
        this.dataSet = DataSetRepository.getDataSetById(pageInfo.getDsId());
        this.pageStatusEnum = PageStatusEnum.getPageStatus(pageInfo.getStatus());
        if (!CollectionUtils.isEmpty(pageInfo.getCards())) {
            this.cardList = pageInfo.getCards().stream().map(s->new Card(s, this)).collect(Collectors.toList());
        }

        filtersDP.fillByFilters(FilterRepository.transFilters(pageInfo.getPgFilters(), null));

        if (!CollectionUtils.isEmpty(pageInfo.getInitialFieldList())){
            this.initialFields = pageInfo.getInitialFieldList().stream()
                    .map(s-> new BaseField(s.getFdId(), s.getColumn(), s.getSeqNo(), s.getMetaType())).collect(Collectors.toList());
        }
    }

    public Page fillAllFilters() {

        if (!CollectionUtils.isEmpty(filtersDP.getFilters())) {
            return this;
        }
        List<Filter> pageFilter = FilterRepository.getPageFilter(this);
        filtersDP.fillByFilters(pageFilter);

        return this;
    }

    public Page fillAllFiltersDefaultValues() {
        fillAllFilters().getFiltersDP().fillFilterDefaultValues(this);
        cardList.clear();
        return this;
    }

    public Page fillAllCards(boolean fillFilter, boolean fillFields) {

        if (!CollectionUtils.isEmpty(cardList)) {
            return this;
        }

        this.cardList = CardRepository.getCards(this, fillFilter, fillFields);

        return this;
    }

    public List<Card> getCardList() {
        fillAllCards(true, true);
        return this.cardList;
    }

    public Page fillInitialFieldsDP(){
        if (!CollectionUtils.isEmpty(initialFields)) {
            return this;
        }

        this.initialFields = FieldRepository.getInitialFields(this);

        return this;
    }

    public boolean storeMySelf() {

        /**
         * 1.校验页面名称
         */
        if (checkPageNameDuplicate()){
            throw new AtlasException(ErrorCode.PAGE_NAME_DUPLICATE);
        }

        /**
         * 2.更新或者插入initialFields
         */
        FieldRepository.insertInitialFields(this.initialFields, this);

        /**
         * 3.更新筛选器
         */
        updatePgFilters();

        /**
         * 4.更新cards
         */
        updateCards();

        /**
         * 5.保存page
         */
        savePage();

        /**
         * 6.删除缓存
         */
        GuavaCacheWrapper.delDataSetCacheByDsId(this.dataSet.getId());

        return true;
    }

    /**
     * 校验名称是否重复
     * @return  true 有重复  false 无重复
     */
    private boolean checkPageNameDuplicate() {

        List<Page> pages = PageRepository.getPagesByName(this.getName());
        if (CollectionUtils.isEmpty(pages)) {
            return false;
        }

        if (this.pgId == null && pages.size() > 0) {
            return true;
        }

        return pages.stream().filter(s->!s.getPgId().equals(this.pgId)).count() > 0L;
    }

    /**
     * 更新页面筛选器
     * @return
     */
    private boolean updatePgFilters() {
        FilterRepository.deletePageFilters(this);
        FilterRepository.updatePageFilters(this);

        return true;
    }

    /**
     * 更新cards
     * @return
     */
    private boolean updateCards() {

        List<Card> cardList = this.cardList;
        if (CollectionUtils.isEmpty(cardList)) {
            return true;
        }

        CardRepository.deleteCards(this);
        CardRepository.updateCards(this.cardList);

        cardList.stream().forEach(s->{
            s.storeMySelf();
        });

        return true;
    }

    public Page savePage() {
        PageRepository.insertPage(this);
        return this;
    }

    public Card getCard(String cdId, List<FieldInfo> fieldInfos,
                        List<FilterInfo> filterInfos) {
        Card card = CardRepository.getCardByCardId(cdId, this, fieldInfos, filterInfos);
        return card;
    }

    public FilterDataDP getFilterData(FilterDataGetRequest request) {

        if (StringUtils.isBlank(request.getCdId())) {
            Card card = this.fillAllCards(false, false).getCardList().get(0);
            request.setCdId(card.getCdId());
        }
        Card card = getCard(request.getCdId(), request.getFieldsInfo(), request.getConditionsInfo());
        if (card == null) {
            card = new Card(request.getCdId(), this, null,null);
        }

        Filter filter = FilterRepository.transSingleFilter(request.getFilters().get(0), card);
        if (filter.isAggBeforeFilter()) {
            DataSet dataSet = card.getPage().getDataSet();
            return dataSet.getFilterData(request.getFilters().get(0), request.getConditionsInfo(),
                    new LimitDP(1L,Long.MAX_VALUE));
        }

        FilterDataDP filterData = filter.getFilterData(
                card, request.getConditionsInfo(), filter.getSearch(), new LimitDP(1L,Long.MAX_VALUE));

        return new FilterDataDP(filterData.getValues(), filterData.getTotal(), filterData.getValues().size());
    }

}
