package com.guandata.atlas.service.planningtable.model.valueobj;

import com.google.common.collect.Lists;
import com.guandata.atlas.service.planningtable.model.filter.entity.ElementFilter;
import com.guandata.atlas.service.planningtable.model.filter.entity.Filter;
import com.guandata.atlas.service.planningtable.model.page.entity.Page;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jooq.Condition;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.stream.Collectors;

@Getter
@NoArgsConstructor
public class FiltersDP {

    private List<Filter> filters = Lists.newArrayList();

    public FiltersDP fillByFilters(List<Filter> filters) {
        this.filters = filters;
        return this;
    }

    public List<Condition> getWhereConditions() {
        return filters.stream()
                .filter(s-> s.isAggBeforeFilter() && s.isValidFilter())
                .map(Filter::buildAndCondition).collect(Collectors.toList());
    }

    public List<Condition> getHavingConditions() {
        return filters.stream()
                .filter(s-> s.isAggAfterFilter() && s.isValidFilter())
                .map(Filter::buildAndCondition).collect(Collectors.toList());
    }

    public void fillFilterDefaultValues(Page page) {

        filters.stream().forEach(s->{
            if (s instanceof ElementFilter) {
                ElementFilter elementFilter = (ElementFilter) s;
                elementFilter.fillFilterDefaultValue(page);
            }
        });
    }

}
