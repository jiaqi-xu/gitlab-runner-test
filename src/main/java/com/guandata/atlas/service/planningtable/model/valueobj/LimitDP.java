package com.guandata.atlas.service.planningtable.model.valueobj;

import lombok.Getter;

@Getter
public class LimitDP {

    private Long pageNo;

    private Long pageSize;

    public LimitDP(Long pageNo, Long pageSize) {
        this.pageNo = (pageNo == null ? 1 : pageNo);
        this.pageSize = (pageSize == null ? Long.MAX_VALUE : pageSize);
    }

    public Long getOffset() {
        return (pageNo-1)*pageSize;
    }
}
