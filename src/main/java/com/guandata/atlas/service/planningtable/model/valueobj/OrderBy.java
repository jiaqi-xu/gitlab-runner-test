package com.guandata.atlas.service.planningtable.model.valueobj;

import com.guandata.atlas.service.planningtable.enums.SortTypeEnum;
import com.guandata.atlas.service.planningtable.model.card.entity.Card;
import com.guandata.atlas.service.planningtable.model.field.entity.Field;
import com.guandata.atlas.service.planningtable.utils.FieldUtils;
import lombok.Data;
import org.jooq.OrderField;
import org.jooq.impl.DSL;

@Data
public class OrderBy {
    private Field field;
    private SortTypeEnum orderType;

    public OrderField getOrderField() {

        org.jooq.Field<Object> fd = DSL.field(DSL.name(this.field.getAliasName()));
        switch (orderType){
            case ASC:
                return fd.asc();
            case DESC:
                return fd.desc();
            default:
                return null;
        }
    }

    public OrderBy(com.guandata.atlas.dto.OrderBy order, Card card) {
        this.field = FieldUtils.transField(order.getFieldInfo(), card);
        this.orderType = SortTypeEnum.getSortType(order.getOrderType());
    }
}
