package com.guandata.atlas.service.planningtable.model.valueobj;

import com.google.common.collect.Lists;
import com.guandata.atlas.service.planningtable.model.card.entity.Card;
import lombok.Getter;
import org.jooq.OrderField;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Getter
public class OrderBysDP {

    private List<OrderBy> orderBys = Lists.newArrayList();

    public OrderBysDP(){
    }

    public OrderBysDP fillOrderByDP(List<com.guandata.atlas.dto.OrderBy> orderBys, Card card) {
        this.orderBys = orderBys.stream().map(s->new OrderBy(s, card)).collect(Collectors.toList());
        return this;
    }

    public List<OrderField> getOrderByFields() {
        return orderBys.stream().map(OrderBy::getOrderField).filter(Objects::nonNull).collect(Collectors.toList());
    }
}
