package com.guandata.atlas.service.planningtable.model.valueobj;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Map;

@Getter
@AllArgsConstructor
public class PTRecord {
    /**
     * fieldId:转换好的value数据
     */
    private Map<String, Object> values;
}
