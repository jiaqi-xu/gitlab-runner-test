package com.guandata.atlas.service.planningtable.repository;

import com.google.common.collect.Lists;
import com.guandata.atlas.dao.CardInfoDO;
import com.guandata.atlas.mapper.CardMapper;
import com.guandata.atlas.pojo.FieldInfo;
import com.guandata.atlas.pojo.FilterInfo;
import com.guandata.atlas.service.planningtable.model.card.entity.Card;
import com.guandata.atlas.service.planningtable.model.page.entity.Page;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class CardRepository {

    private static CardMapper cardMapper;

    @Resource
    public void setCardMapper(CardMapper cardMapper) {
        CardRepository.cardMapper = cardMapper;
    }

    public static Card getCardByCardId(String cdId) {

        CardInfoDO cardInfo = cardMapper.selectCardById(cdId);
        if (cardInfo == null) {
            return null;
        }

        return new Card(cardInfo);
    }

    public static Card getCardByCardId(String cdId, Page page,
                                       List<FieldInfo> fieldInfos,
                                       List<FilterInfo> filterInfos) {

        CardInfoDO cardInfo = cardMapper.selectCardById(cdId);
        if (cardInfo == null) {
            return null;
        }

        return new Card(cardInfo, page, fieldInfos, filterInfos);
    }

    public static int updateCards(List<Card> cards) {
        if (CollectionUtils.isEmpty(cards)) {
            return 0;
        }

        List<CardInfoDO> cardDOS = cards.stream().map(s -> new CardInfoDO(s)).collect(Collectors.toList());
        return cardMapper.insertCards(cardDOS);
    }

    public static int deleteCards(Page page) {
        return cardMapper.deleteCardsByPgId(page.getPgId());
    }

    public static List<Card> getCards(Page page, boolean fillFilter, boolean fillFields) {

        List<CardInfoDO> cardByPages = cardMapper.getCardByPages(Lists.newArrayList(page.getPgId()));

        return cardByPages.stream().map(s->new Card(page, s, fillFilter, fillFields)).collect(Collectors.toList());
    }

}
