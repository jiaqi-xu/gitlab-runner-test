package com.guandata.atlas.service.planningtable.repository;

import com.google.common.collect.Lists;
import com.guandata.atlas.dao.FieldInfoDO;
import com.guandata.atlas.dto.dataset.DSColumn;
import com.guandata.atlas.mapper.FieldMapper;
import com.guandata.atlas.service.planningtable.model.card.entity.Card;
import com.guandata.atlas.service.planningtable.model.field.entity.AggrField;
import com.guandata.atlas.service.planningtable.model.field.entity.BaseField;
import com.guandata.atlas.service.planningtable.model.field.entity.DimField;
import com.guandata.atlas.service.planningtable.model.field.entity.Field;
import com.guandata.atlas.service.planningtable.model.page.entity.Page;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class FieldRepository {

    private static FieldMapper fieldMapper;

    @Resource
    public void setFieldMapper(FieldMapper fieldMapper) {
        FieldRepository.fieldMapper = fieldMapper;
    }

    public static Field getFieldByCardAndFdId(Card card, String fdId) {

        FieldInfoDO fieldInfo = fieldMapper.selectFieldById(card.getCdId(), fdId);
        if (fieldInfo == null){
            return null;
        }

        Map<String, DSColumn> colIdToDSColumnMap = card.getPage().getDataSet().getColumnListDP().getColumnList().stream().collect(Collectors.toMap(s -> s.getColumnId(), v -> v));
        fieldInfo.setColumn(colIdToDSColumnMap.get(fieldInfo.getColumnId()));

       return transDOToModel(card, fieldInfo);
    }

    public static List<Field> getAllFields(Card card) {

        List<FieldInfoDO> fields = fieldMapper.getFieldsById(card.getCdId());
        if (CollectionUtils.isEmpty(fields)) {
            return Lists.newArrayList();
        }

        return fields.stream().map(s->transDOToModel(card,s)).collect(Collectors.toList());
    }

    public static List<BaseField> getInitialFields(Page page) {
        List<FieldInfoDO> initialFieldList = fieldMapper.getInitialFieldList(page.getPgId());
        return initialFieldList.stream().map(
                s-> new BaseField(s.getFdId(), s.getColumn(), s.getSeqNo(), s.getMetaType())
        ).collect(Collectors.toList());
    }

    public static int insertInitialFields(List<BaseField> initialFields, Page page) {
        if (CollectionUtils.isEmpty(initialFields)) {
            return 0;
        }
        return fieldMapper.insertInitialFields(initialFields.stream().map(s->new FieldInfoDO(s, page)).collect(Collectors.toList()));
    }

    public static int deleteFieldsByCdId(String cdId) {
        return fieldMapper.deleteFieldsByCdId(cdId);
    }

    public static int insertFields(List<Field> fields) {
        if (CollectionUtils.isEmpty(fields)) {
            return 0;
        }
        List<FieldInfoDO> fieldDOS = fields.stream().map(s -> new FieldInfoDO(s)).collect(Collectors.toList());
        return fieldMapper.insertFields(fieldDOS);
    }

    private static Field transDOToModel(Card card, FieldInfoDO fieldInfo) {

        if (fieldInfo.getIsAggregated()) {
            return new DimField(card,fieldInfo);
        }

        return new AggrField(card, fieldInfo);
    }

    public static Boolean hasAssociatedFieldsWithKanban(String kanbanId) {
        List<FieldInfoDO> fieldInfoDOS = fieldMapper.selectAssociatedFieldsByKanban("\"" + kanbanId + "\"");
        return !CollectionUtils.isEmpty(fieldInfoDOS);
    }
}
