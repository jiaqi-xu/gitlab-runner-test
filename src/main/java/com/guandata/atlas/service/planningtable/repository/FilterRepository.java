package com.guandata.atlas.service.planningtable.repository;

import com.google.common.collect.Lists;
import com.guandata.atlas.dao.BaseFilter;
import com.guandata.atlas.dto.dataset.DSColumn;
import com.guandata.atlas.enums.ErrorCode;
import com.guandata.atlas.exception.AtlasException;
import com.guandata.atlas.mapper.FilterMapper;
import com.guandata.atlas.pojo.FilterInfo;
import com.guandata.atlas.service.dataset.model.DataSet;
import com.guandata.atlas.service.dataset.repository.DataSetRepository;
import com.guandata.atlas.service.planningtable.enums.SelectTypeEnum;
import com.guandata.atlas.service.planningtable.model.card.entity.Card;
import com.guandata.atlas.service.planningtable.model.filter.entity.CalendarFilter;
import com.guandata.atlas.service.planningtable.model.filter.entity.ElementFilter;
import com.guandata.atlas.service.planningtable.model.filter.entity.Filter;
import com.guandata.atlas.service.planningtable.model.filter.entity.IntervalFilter;
import com.guandata.atlas.service.planningtable.model.page.entity.Page;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
public class FilterRepository {

    private static FilterMapper filterMapper;

    @Resource
    public void setFilterMapper(FilterMapper filterMapper) {
        FilterRepository.filterMapper = filterMapper;
    }

    public static List<Filter> getPageFilter(Page page) {

        List<BaseFilter> pageFilters = filterMapper.getPageFiltersById(page.getPgId());
        if (CollectionUtils.isEmpty(pageFilters)) {
            return Lists.newArrayList();
        }
        return pageFilters.stream().filter(Objects::nonNull).map(s->buildFilter(s, getColumnIdToColMap(page.getDataSet().getId()), null)).collect(Collectors.toList());
    }

    public static List<Filter> getCardFilter(Card card) {

        List<BaseFilter> cardFilters = filterMapper.getCardFiltersById(card.getCdId());

        return cardFilters.stream().map(s->buildFilter(s, getColumnIdToColMap(card.getPage().getDataSet().getId()), card)).collect(Collectors.toList());
    }

    private static Map<String, DSColumn> getColumnIdToColMap(Integer dsId) {
        DataSet dataSet = DataSetRepository.getDataSetById(dsId);
        List<DSColumn> columnList = dataSet.getColumnListDP().getColumnList();
        Map<String, DSColumn> columnIdToColMap = columnList.stream().collect(Collectors.toMap(s -> s.getColumnId(), v -> v));
        return columnIdToColMap;
    }

    public static int deletePageFilters(Page page) {
        return filterMapper.deleteFiltersByPgId(page.getPgId());
    }

    public static int updatePageFilters(Page page) {
        List<Filter> filters = page.getFiltersDP().getFilters();
        if (CollectionUtils.isEmpty(filters)) {
            return 0;
        }
        List<BaseFilter> filterDOS = filters.stream().map(s -> new BaseFilter(s)).collect(Collectors.toList());
        return filterMapper.insertFilters(filterDOS);
    }

    public static int deleteCardFilters(Card card) {
        return filterMapper.deleteFiltersByCdId(card.getCdId());
    }

    public static int updateCardFilters(Card card) {
        List<Filter> filters = card.getFiltersDP().getFilters();
        if (CollectionUtils.isEmpty(filters)) {
            return 0;
        }
        List<BaseFilter> filterDOS = filters.stream().map(s -> new BaseFilter(s)).collect(Collectors.toList());
        return filterMapper.insertFilters(filterDOS);
    }

    private static Filter buildFilter(BaseFilter baseFilter, Map<String, DSColumn> columnIdToColMap, Card card) {

        String type = baseFilter.getType();
        DSColumn dsColumn = columnIdToColMap.get(baseFilter.getColumn().getColumnId());
        if (SelectTypeEnum.DS_ELEMENTS.getType().equals(type)) {
            return new ElementFilter(baseFilter, dsColumn, card);
        } else if (SelectTypeEnum.DS_INTERVAL.getType().equals(type)) {
            return new IntervalFilter(baseFilter, dsColumn, card);
        } else if (SelectTypeEnum.CALENDAR.getType().equals(type)) {
            return new CalendarFilter(baseFilter, dsColumn, card);
        }

        throw new AtlasException(ErrorCode.PARAME_ERROR_CODE);
    }

    public static List<Filter> transFilters(List<FilterInfo> filterInfos, Card card) {

        if (CollectionUtils.isEmpty(filterInfos)) {
            return Lists.newArrayList();
        }
        return filterInfos.stream().map(s -> {
            return transSingleFilter(s, card);
        }).collect(Collectors.toList());
    }

    public static Filter transSingleFilter(FilterInfo filterInfo, Card card) {

            String type = filterInfo.getSelectorType();
            if (SelectTypeEnum.DS_ELEMENTS.getType().equals(type)) {
                return new ElementFilter(filterInfo, card);
            } else if (SelectTypeEnum.DS_INTERVAL.getType().equals(type)) {
                return new IntervalFilter(filterInfo, card);
            } else if (SelectTypeEnum.CALENDAR.getType().equals(type)) {
                return new CalendarFilter(filterInfo, card);
            }

            return new ElementFilter(filterInfo, card);
    }
}
