package com.guandata.atlas.service.planningtable.repository;

import com.google.common.collect.Lists;
import com.guandata.atlas.dao.PageInfoDO;
import com.guandata.atlas.mapper.PageMapper;
import com.guandata.atlas.pojo.FilterInfo;
import com.guandata.atlas.service.planningtable.model.filter.entity.Filter;
import com.guandata.atlas.service.planningtable.model.page.entity.Page;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class PageRepository {

    private static PageMapper pageMapper;

    @Resource
    public void setPageMapper(PageMapper pageMapper) {
        PageRepository.pageMapper = pageMapper;
    }

    /**
     * 单个页面获取
     * @param pageId
     * @return
     */
    public static Page getPageByPageId(String pageId) {

        PageInfoDO pageInfo = pageMapper.getPageById(pageId);
        if (pageInfo == null) {
            return null;
        }

        return new Page(pageInfo, Lists.newArrayList());
    }

    public static Page getPageByPageId(String pageId, List<FilterInfo> filterInfos) {

        PageInfoDO pageInfo = pageMapper.getPageById(pageId);
        if (pageInfo == null) {
            return null;
        }

        return new Page(pageInfo, filterInfos);
    }

    /**
     * 获取所有的页面
     * @return
     */
    public static List<Page> getAllPages() {
        List<PageInfoDO> list = pageMapper.list(null);
        return list.stream().map(s->new Page(s,Lists.newArrayList())).collect(Collectors.toList());
    }


    public static List<Page> getPagesByName(String name) {
        List<PageInfoDO> list = pageMapper.getPagesByName(name);
        return list.stream().map(s->new Page(s, Lists.newArrayList())).collect(Collectors.toList());
    }

    public static int insertPage(Page page) {
        PageInfoDO pageInfoDO = new PageInfoDO(page);
        return pageMapper.insertPage(pageInfoDO);
    }
 }
