package com.guandata.atlas.service.planningtable.utils;

import com.guandata.atlas.dto.dataset.DSColumn;
import com.guandata.atlas.enums.DataTypeEnum;
import com.guandata.atlas.pojo.FieldInfo;
import com.guandata.atlas.service.planningtable.model.card.entity.Card;
import com.guandata.atlas.service.planningtable.model.field.entity.AggrField;
import com.guandata.atlas.service.planningtable.model.field.entity.DimField;
import com.guandata.atlas.service.planningtable.model.field.entity.Field;
import org.jooq.impl.DSL;

import java.math.BigDecimal;

public class FieldUtils {

    public static Field transField(FieldInfo fieldInfo, Card card) {

        if (fieldInfo.getIsAggregated()) {
            return new DimField(card,fieldInfo);
        }

        return new AggrField(card, fieldInfo);
    }

    public static org.jooq.Field transferByDataType(DSColumn column) {
        if (DataTypeEnum.INTEGER.getType().equals(column.getType()) || DataTypeEnum.LONG.getType().equals(column.getType())) {
            return DSL.round(
                    DSL.field(DSL.name(column.getColumnId()), BigDecimal.class)
            ).as(DSL.field(DSL.name(column.getName())));
        } else if (DataTypeEnum.FLOAT.getType().equals(column.getType()) || DataTypeEnum.DOUBLE.getType().equals(column.getType())) {
            return DSL.round(
                    DSL.field(DSL.name(column.getColumnId()), BigDecimal.class), 2
            ).as(DSL.field(DSL.name(column.getName())));
        } else {
            return DSL.field(DSL.name(column.getColumnId())).as(DSL.field(DSL.name(column.getName())));
        }
    }
}
