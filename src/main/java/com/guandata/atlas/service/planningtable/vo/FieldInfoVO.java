package com.guandata.atlas.service.planningtable.vo;


import com.guandata.atlas.pojo.FieldInfo;
import com.guandata.atlas.service.planningtable.model.field.entity.Field;
import lombok.Data;

@Data
public class FieldInfoVO extends FieldInfo {

    /** 是否禁用筛选 **/
    private Boolean allowFilter;
    /** 是否启用排序 **/
    private Boolean allowSort;
    /** 是否启用编辑 **/
    private Boolean allowEdit;

    public FieldInfoVO(Field field) {
        super(field);
        this.allowEdit = field.getAllowEdit();
        this.allowFilter = field.getAllowFilter();
        this.allowSort = field.getAllowSort();
    }
}
