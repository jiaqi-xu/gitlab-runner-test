package com.guandata.atlas.service.planningtable.vo;

import lombok.Data;

@Data
public class TaskInfoVO {

    private String taskId;
}
