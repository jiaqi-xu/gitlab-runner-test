package com.guandata.atlas.service.resource.dto;

import lombok.Data;

@Data
public class ResourceDTO {
    private String id;
    private String name;
    private String type;

    public ResourceDTO(String id, String name, String type) {
        this.id = id;
        this.name = name;
        this.type =type;
    }
}
