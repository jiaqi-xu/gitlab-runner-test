package com.guandata.atlas.service.resource.model;

import lombok.Data;

import java.util.Objects;

@Data
public class ATResource {
    private String id;
    private String name;
    private String type;

    public ATResource(String id, String name, String type) {
        this.id = id;
        this.name = name;
        this.type =type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ATResource that = (ATResource) o;
        return Objects.equals(id, that.id) && Objects.equals(name, that.name) && Objects.equals(type, that.type);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, type);
    }
}
