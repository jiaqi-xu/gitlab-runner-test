package com.guandata.atlas.service.resource.repository;

import com.guandata.atlas.common.constants.Constants;
import com.guandata.atlas.service.resource.dto.ResourceDTO;
import com.guandata.atlas.service.resource.model.ATResource;
import org.jooq.DSLContext;
import org.jooq.InsertValuesStep4;
import org.jooq.SelectConditionStep;
import org.jooq.SelectJoinStep;
import org.jooq.impl.DSL;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Repository
public class ResourceRepository {

    private static DSLContext dslContext;

    @Resource
    public void setDslContext(DSLContext dslContext) {
        ResourceRepository.dslContext = dslContext;
    }

    /**
     * 资源列表
     * @param type 资源类型， e.g. PAGE, FORM, PT_PAGE, NULL
     * @return
     */
    public static List<ATResource> listGlobalResource(String type) {
        List<ATResource> resource = new ArrayList<>();
        List<ATResource> ptResource = listPTPageResource(Constants.OPEN);
        if (type != null && type.equals(Constants.PT_PAGE)) {
            return ptResource;
        }

        if (type != null && (type.equals(Constants.PAGE) || type.equals(Constants.FORM))) {
            return listKanbanResource(type);
        }
        List<ATResource> kanbanResource = listKanbanResource(type);

        resource.addAll(ptResource);
        resource.addAll(kanbanResource);
        return resource;
    }

    /**
     * 获取未激活的资源列表，e.g. init状态的PT page
     * @param type 资源类型
     * @return
     */
    public static List<ATResource> listGlobalUnactivatedResource(String type) {
        List<ATResource> resource = new ArrayList<>();
        List<ATResource> ptResource = ResourceRepository.listPTPageResource(Constants.INIT);
        if (type != null && type.equals(Constants.PT_PAGE)) {
            return ptResource;
        }

        resource.addAll(ptResource);
        return resource;
    }


    /**
     * 获取特定状态的PT page资源
     * @param status
     * @return
     */
    public static List<ATResource> listPTPageResource(Integer status) {
        return dslContext.select(DSL.field("pg_id"), DSL.field("name")).from(DSL.table("page"))
                .where(DSL.condition("status = {0}", status))
                .fetch(record -> new ATResource(record.getValue("pg_id").toString(), record.getValue("name").toString(), Constants.PT_PAGE));
    }

    /**
     * 获取特定类型的kanban资源
     * @param type kanban资源类型
     * @return
     */
    public static List<ATResource> listKanbanResource(String type) {
        SelectJoinStep kanbanResourceSelect = dslContext.select(DSL.field("object_id"), DSL.field("object_name"), DSL.field("object_type"))
                .from(DSL.table("kanban"));
        if (type != null && (type.equals(Constants.PAGE) || type.equals(Constants.FORM))) {
            return kanbanResourceSelect.where(DSL.condition("object_type = {0} and is_del = false", type)).fetch(record -> new ATResource(record.getValue("object_id").toString(), record.getValue("object_name").toString(), record.getValue("object_type").toString()));
        }
        List<ATResource> kanbanResource = kanbanResourceSelect.where("is_del = false")
                .fetch(record -> new ATResource(record.getValue("object_id").toString(), record.getValue("object_name").toString(), record.getValue("object_type").toString()));
        return kanbanResource;
    }

    /***********************************  User Resource  ***********************************/

    public static Boolean grantResourceToUsers(ResourceDTO resourceDTO, List<String> userIdList) {
        InsertValuesStep4 insertValuesStep4 =  dslContext.insertInto(DSL.table("grant_users_resources")).columns(DSL.field("resource_id"), DSL.field("resource_name"), DSL.field("resource_type"), DSL.field("user_id"));
        userIdList.stream().forEach(uId -> {
            insertValuesStep4.values(resourceDTO.getId(), resourceDTO.getName(), resourceDTO.getType(), uId);
        });
        insertValuesStep4.onDuplicateKeyUpdate().set(DSL.field("resource_id"), (Object) DSL.field("VALUES({0})", DSL.field("resource_id")))
                .set(DSL.field("resource_name"), (Object) DSL.field("VALUES({0})", DSL.field("resource_name")))
                .set(DSL.field("resource_type"), (Object) DSL.field("VALUES({0})", DSL.field("resource_type")))
                .set(DSL.field("user_id"), (Object) DSL.field("VALUES({0})", DSL.field("user_id")));
        return insertValuesStep4.execute() > 0;
    }

    public static Boolean revokeResourceFromUsers(ResourceDTO resourceDTO, List<String> userIdList) {
        return dslContext.deleteFrom(DSL.table("grant_users_resources"))
                .where(DSL.condition("resource_id = {0}", resourceDTO.getId()).and(DSL.field("user_id").in(userIdList)))
                .execute() > 0;
    }

    public static List<String> getResourceUsers(String resourceId) {
        List<String> uIdList = dslContext.select(DSL.field("user_id")).from(DSL.table("grant_users_resources"))
                .where(DSL.condition("resource_id = {0}", resourceId)).fetch("user_id", String.class);
        return uIdList;
    }

    /**
     * 获取用户直接关联的资源 note: exclude admin user
     * @param userId
     * @param type
     * @return
     */
    public static List<ATResource> getUserOwnedResources(String userId, String type) {
        SelectConditionStep resourcesSelect = dslContext.select(DSL.field("resource_id"), DSL.field("resource_name"), DSL.field("resource_type")).from(DSL.table("grant_users_resources"))
                .where(DSL.condition("user_id = {0}", userId));

        if (type != null) {
            resourcesSelect.and(DSL.condition("resource_type = {0}", type));
        }
        return resourcesSelect.fetch(record -> new ATResource(record.getValue("resource_id").toString(), record.getValue("resource_name").toString(), record.getValue("resource_type").toString()));
    }

    /***********************************  Group Resource  ***********************************/

    public static Boolean grantResourceToGroups(ResourceDTO resourceDTO, List<String> groupIdList) {
        InsertValuesStep4 insertValuesStep4 =  dslContext.insertInto(DSL.table("grant_groups_resources")).columns(DSL.field("resource_id"), DSL.field("resource_name"), DSL.field("resource_type"), DSL.field("group_id"));
        groupIdList.stream().forEach(uId -> {
            insertValuesStep4.values(resourceDTO.getId(), resourceDTO.getName(), resourceDTO.getType(), uId);
        });
        insertValuesStep4.onDuplicateKeyUpdate().set(DSL.field("resource_id"), (Object) DSL.field("VALUES({0})", DSL.field("resource_id")))
                .set(DSL.field("resource_name"), (Object) DSL.field("VALUES({0})", DSL.field("resource_name")))
                .set(DSL.field("resource_type"), (Object) DSL.field("VALUES({0})", DSL.field("resource_type")))
                .set(DSL.field("group_id"), (Object) DSL.field("VALUES({0})", DSL.field("group_id")));
        return insertValuesStep4.execute() > 0;
    }

    public static Boolean revokeResourceFromGroups(ResourceDTO resourceDTO, List<String> groupIdList) {
        return dslContext.deleteFrom(DSL.table("grant_groups_resources"))
                .where(DSL.condition("resource_id = {0}", resourceDTO.getId()).and(DSL.field("group_id").in(groupIdList)))
                .execute() > 0;
    }

    public static List<String> getResourceGroups(String resourceId) {
        List<String> groupIdList = dslContext.select(DSL.field("group_id")).from(DSL.table("grant_groups_resources"))
                .where(DSL.condition("resource_id = {0}", resourceId)).fetch("group_id", String.class);
        return groupIdList;
    }

    /**
     * 获取用户组直接关联的资源
     * @param groupId
     * @param type
     * @return
     */
    public static List<ATResource> getGroupOwnedResources(String groupId, String type) {
        SelectConditionStep resourcesSelect = dslContext.select(DSL.field("resource_id"), DSL.field("resource_name"), DSL.field("resource_type")).from(DSL.table("grant_groups_resources"))
                .where(DSL.condition("group_id = {0}", groupId));

        if (type != null) {
            resourcesSelect.and(DSL.condition("resource_type = {0}", type));
        }
        return resourcesSelect.fetch(record -> new ATResource(record.getValue("resource_id").toString(), record.getValue("resource_name").toString(), record.getValue("resource_type").toString()));
    }

}
