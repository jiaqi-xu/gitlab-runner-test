package com.guandata.atlas.service.task.repository;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.guandata.atlas.dao.TaskDAO;
import com.guandata.atlas.mapper.TaskMapper;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

@Component
public class TaskRepository {

    private static TaskMapper taskMapper;

    @Resource
    public void setTaskMapper(TaskMapper taskMapper) {
        TaskRepository.taskMapper = taskMapper;
    }

    public static Integer insertTask(TaskDAO taskDAO) {

        return taskMapper.insert(taskDAO);
    }


    public static List<TaskDAO> getTasksByObjectId(List<Integer> statusList, List<String> taskTypeList, String objectId) {
        QueryWrapper<TaskDAO> queryWrapper = new QueryWrapper<>();
        queryWrapper.select(
                        "id", "task_id", "task_type", "object_id", "status", "failure_cause", "start_time", "end_time", "creator_id");
        queryWrapper.eq("object_id", objectId);
        queryWrapper.in("task_type", taskTypeList);
        queryWrapper.in("status", statusList);
        queryWrapper.orderByDesc("id");
        List<TaskDAO> optTaskDAOS = taskMapper.selectList(queryWrapper);

        return optTaskDAOS;
    }

    public static List<TaskDAO> getTasksByStatus(List<Integer> statusList) {
        QueryWrapper<TaskDAO> queryWrapper = new QueryWrapper<>();
        queryWrapper.select(
                "id", "task_id", "task_type", "object_id", "status", "failure_cause", "start_time", "end_time", "creator_id");
        queryWrapper.in("status", statusList);
        queryWrapper.orderByDesc("id");
        List<TaskDAO> optTaskDAOS = taskMapper.selectList(queryWrapper);

        return optTaskDAOS;
    }

    public static int updateTask(TaskDAO taskDAO) {
        UpdateWrapper<TaskDAO> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("task_id", taskDAO.getTaskId());
        return taskMapper.update(taskDAO, updateWrapper);
    }
}
