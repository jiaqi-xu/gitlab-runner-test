package com.guandata.atlas.service.user.dto;

import lombok.Data;

@Data
public class GroupDTO {
    private String groupId;

    private String name;
}
