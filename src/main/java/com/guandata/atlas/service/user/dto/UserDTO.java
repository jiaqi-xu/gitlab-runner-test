package com.guandata.atlas.service.user.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class UserDTO {
    /** 用户Id **/
    private String uId;

    /** 账号 **/
    @NotBlank(message = "登陆id不能为空")
    private String loginId;

    /** 用户名称 **/
    @NotBlank(message = "用户姓名不能为空")
    private String username;

    /** 密码 **/
    @NotBlank(message = "密码不能为空")
    private String password;
    /** 用于表示是否为临时密码；如果是，下次登陆需要重新设置 **/
    @NotNull(message = "请设置是否为临时密码")
    private Boolean temporary;

    /** 邮箱 **/
    private String email;

    /** 账号角色 **/
    private List<String> roles;

    /** 用户组 **/
    private List<String> groups;

}
