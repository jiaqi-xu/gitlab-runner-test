package com.guandata.atlas.service.user.model;

import lombok.Data;
import org.keycloak.representations.idm.GroupRepresentation;

@Data
public class Group {
    /** 用户组Id **/
    private String groupId;

    /** 用户组名称 **/
    private String name;

    public Group(String name) {
        this.name = name;
    }

    public Group(String groupId, String name) {
        this.groupId = groupId;
        this.name =name;
    }

    public Group(GroupRepresentation groupRep) {
        this.groupId = groupRep.getId();
        this.name = groupRep.getName();
    }

    public GroupRepresentation transferToGroupRep() {
        GroupRepresentation groupRepresentation = new GroupRepresentation();
        groupRepresentation.setId(this.groupId);
        groupRepresentation.setName(this.name);
        return groupRepresentation;
    }
}
