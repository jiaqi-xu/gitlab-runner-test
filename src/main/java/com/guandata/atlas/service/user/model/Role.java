package com.guandata.atlas.service.user.model;

import lombok.Data;
import org.keycloak.representations.idm.RoleRepresentation;

@Data
public class Role {
    /** 角色Id **/
    private String roleId;

    /** 角色名称 **/
    private String name;

    /** 是否为clientRole **/
    private Boolean clientRole;

    /**  是否为复合角色 备注: 复合角色可以包含其他的角色，用户拥有了复合角色就相当于拥有了它下面的所有子角色**/
    private Boolean composite;

    /** 容器Id **/
    private String containerId;

    public Role(RoleRepresentation roleRepresentation) {
        this.roleId = roleRepresentation.getId();
        this.name = roleRepresentation.getName();

        this.clientRole = roleRepresentation.getClientRole();
        this.composite = roleRepresentation.isComposite();
        this.containerId = roleRepresentation.getContainerId();
    }

}
