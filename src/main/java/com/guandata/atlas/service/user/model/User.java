package com.guandata.atlas.service.user.model;

import com.guandata.atlas.common.constants.Constants;
import com.guandata.atlas.service.resource.model.ATResource;
import com.guandata.atlas.service.resource.repository.ResourceRepository;
import com.guandata.atlas.service.user.dto.UserDTO;
import com.guandata.atlas.service.user.repository.GroupRepository;
import com.guandata.atlas.service.user.repository.RoleRepository;
import com.guandata.atlas.service.user.vo.UserVO;
import lombok.Data;
import org.apache.commons.collections4.CollectionUtils;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.UserRepresentation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Data
public class User {
    /** 用户Id **/
    private String uId;

    /** 账号 **/
    private String loginId;

    /** 用户名称 **/
    private String username;

    /** 密码 **/
    private String password;
    /** 用于表示是否为临时密码；如果是，下次登陆需要重新设置 **/
    private Boolean temporary;

    /** 邮箱 **/
    private String email;

    /** 账号角色 **/
    private List<Role> roles;

    /** 用户组 **/
    private List<Group> groups;

    /** 用户直接关联的资源 **/
    private List<ATResource> resources;


    public User(UserDTO userDTO) {
        this.uId = userDTO.getUId();
        this.loginId = userDTO.getLoginId();
        this.username = userDTO.getUsername();
        this.password = userDTO.getPassword();
        this.temporary = userDTO.getTemporary();
        this.email = userDTO.getEmail();
    }

    public User(UserRepresentation userRepresentation) {
        this.uId = userRepresentation.getId();
        this.loginId = userRepresentation.getUsername();
        this.username = userRepresentation.getFirstName();
        if (userRepresentation.getCredentials() != null) {
            this.password = userRepresentation.getCredentials().get(0).getValue();
            this.temporary = userRepresentation.getCredentials().get(0).isTemporary();
        }
        this.email = userRepresentation.getEmail();
    }


    public UserVO transferToUserVO() {
        UserVO userVO = new UserVO();
        userVO.setUId(this.uId);
        userVO.setLoginId(this.loginId);
        userVO.setUsername(this.username);
        userVO.setEmail(this.email);

        if (!CollectionUtils.isEmpty(this.roles)) {
            userVO.setRoles(
                    this.roles.stream().map(r -> r.getName()).collect(Collectors.toList())
            );
        }

        if (!CollectionUtils.isEmpty(this.groups)) {
            userVO.setGroups(
                    this.groups.stream().map(g -> g.getName()).collect(Collectors.toList())
            );
        }
        return userVO;
    }

    public UserRepresentation transferToUserRep() {
        UserRepresentation userRepresentation = new UserRepresentation();

        userRepresentation.setEnabled(true);
        userRepresentation.setUsername(this.loginId);
        userRepresentation.setFirstName(this.username);
        userRepresentation.setCredentials(Collections.singletonList(generateCredential()));
        userRepresentation.setEmail(this.email);

        return userRepresentation;
    }

    public CredentialRepresentation generateCredential() {
        CredentialRepresentation credentialRepresentation = new CredentialRepresentation();

        credentialRepresentation.setTemporary(this.temporary);
        credentialRepresentation.setType("password");
        credentialRepresentation.setValue(this.password);
        return credentialRepresentation;
    }

    public User fillUserRoles() {
        List<Role> roles = RoleRepository.getRolesByUId(this.uId);
        this.roles = roles;
        return this;
    }

    public User fillUserGroups() {
        List<Group> groups = GroupRepository.getGroupsByUId(this.uId);
        this.groups = groups;
        return this;
    }

    public User fillUserOwnedResources() {
        List<ATResource> resources = new ArrayList<>();
        if (this.isAdminUser()) {
            resources.addAll(ResourceRepository.listGlobalResource(null));
            resources.addAll(ResourceRepository.listGlobalUnactivatedResource(null));
            this.resources = resources;
        } else {
            this.resources = ResourceRepository.getUserOwnedResources(this.getUId(), null);
        }
        return this;
    }

    /**
     * 判断当前用户是否为admin
     * @return
     */
    public boolean isAdminUser() {
        return this.getRoles().stream().map(role -> role.getName()).collect(Collectors.toList())
                .contains(Constants.ADMIN);
    }

    /**
     * 判断用户是否为某资源的拥有者
     * @param resourceId resource id
     * @return true if user is the owner of the resource; otherwise return false
     */
    public boolean isResourceOwner(String resourceId) {
        return this.getAllResources(null).stream()
                .map(resource -> resource.getId()).collect(Collectors.toSet()).contains(resourceId);
    }


    /**
     * 获取该用户以及其所在的用户组关联的资源
     * @param type 资源类型
     * @return
     */
    public List<ATResource> getAllResources(String type) {
        List<ATResource> resources = new ArrayList<>();
        // 1. 判断当前用户是否已经含有直接关联的资源
        if (this.resources == null) {
            this.fillUserOwnedResources();
        }

        // 2. 获取该用户直接关联的资源
        if (type != null) {
            resources.addAll(this.resources.stream().filter(resource -> resource.getType().equals(type)).collect(Collectors.toList()));
        } else {
            resources.addAll(this.resources);
        }
        // 3. 获取该用户所在用户组关联的资源
        this.groups.stream().forEach(group -> resources.addAll(ResourceRepository.getGroupOwnedResources(group.getGroupId(), type)));


        return resources.stream().distinct().collect(Collectors.toList());
    }
}
