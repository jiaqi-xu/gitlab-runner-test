package com.guandata.atlas.service.user.model.operate;

import lombok.Data;
import org.keycloak.representations.idm.CredentialRepresentation;

@Data
public class ModifyPasswordDTO {
    private String loginId;
    private String oldPassword;
    private String newPassword;

    public CredentialRepresentation generateCredentialRep() {
        CredentialRepresentation credentialRepresentation = new CredentialRepresentation();

        credentialRepresentation.setTemporary(false);
        credentialRepresentation.setType("password");
        credentialRepresentation.setValue(newPassword);
        return credentialRepresentation;
    }
}
