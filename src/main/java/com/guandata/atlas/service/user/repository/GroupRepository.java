package com.guandata.atlas.service.user.repository;

import com.guandata.atlas.common.utils.KeyCloakUtil;
import com.guandata.atlas.service.user.model.Group;
import com.guandata.atlas.service.user.model.User;
import org.keycloak.representations.idm.GroupRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

@Repository
public class GroupRepository {
    private static String realmId;

    private static String client;

    @Value("${keycloak.realm}")
    public void setRealmId(String realm) {
        GroupRepository.realmId = realm;
    }

    @Value("${keycloak.resource}")
    public void setClient(String client) {
        GroupRepository.client = client;
    }

    public static List<Group> list(String search) {
        return KeyCloakUtil.getSubGroups(realmId, client).stream().filter(groupRep -> search == null || groupRep.getName().contains(search))
                .map(groupRep -> new Group(groupRep)).collect(Collectors.toList());
    }

    public static void createGroup(Group group) {
        GroupRepresentation clientGroup = KeyCloakUtil.getGroupByPath(realmId, client);
        KeyCloakUtil.addSubGroup(realmId, clientGroup.getId(), group.transferToGroupRep());
    }


    public static List<Group> getGroupsByUId(String uId) {
        return KeyCloakUtil.getGroupsByUId(realmId, uId).stream()
                .map(groupRep -> new Group(groupRep)).collect(Collectors.toList());
    }

    public static void deleteGroup(String groupId) {
        KeyCloakUtil.deleteSubGroup(realmId, groupId);
    }

    public static void renameGroup(Group group) {
        KeyCloakUtil.renameGroup(realmId, group.getGroupId(), group.transferToGroupRep());
    }

    public static List<User> getGroupMembers(String groupId) {
        List<UserRepresentation> userRepList = KeyCloakUtil.getGroupMembers(realmId, groupId);
        return userRepList.stream().map(userRep -> new User(userRep)).collect(Collectors.toList());
    }

    public static void addGroupMembers(String groupId, List<User> members) {
        List<String> userIds = members.stream().map(member -> member.getUId()).collect(Collectors.toList());
        userIds.stream().forEach(userId -> KeyCloakUtil.addGroupMember(realmId, userId, groupId));
    }

    public static void removeGroupMember(String groupId, String uId) {
        KeyCloakUtil.removeGroupMember(realmId, uId, groupId);
    }

    /**
     * 将某个用户加入多个用户组当中
     * @param uId 用户id
     * @param groups 用户组名称列表
     */
    public static void addGroupsToUser(String uId, List<String> groups) {
        List<GroupRepresentation> allGroups = KeyCloakUtil.getSubGroups(realmId, client);
        List<String> groupToJoin = allGroups.stream().filter(groupRep -> groups.contains(groupRep.getName()))
                .map(groupRep -> groupRep.getId()).collect(Collectors.toList());
        groupToJoin.forEach(groupId -> KeyCloakUtil.addGroupMember(realmId, uId, groupId));
    }

    /**
     * 更新用户所属用户组
     * @param uId 用户id
     * @param groups 用户组名称列表
     */
    public static void updateGroupsForUser(String uId, List<String> groups) {
        List<GroupRepresentation> allGroups = KeyCloakUtil.getSubGroups(realmId, client);

        List<String> joinedGroups =  KeyCloakUtil.getGroupsByUId(realmId, uId).stream()
                .map(groupRep -> groupRep.getName()).collect(Collectors.toList());

        List<String> groupToJoin = allGroups.stream()
                .filter(groupRep -> groups.contains(groupRep.getName()) && !joinedGroups.contains(groupRep.getName()))
                .map(groupRep -> groupRep.getId()).collect(Collectors.toList());
        List<String> groupToLeave = allGroups.stream()
                .filter(groupRep -> !groups.contains(groupRep.getName()) && joinedGroups.contains(groupRep.getName()))
                .map(groupRep -> groupRep.getId()).collect(Collectors.toList());

        groupToJoin.forEach(groupId -> KeyCloakUtil.addGroupMember(realmId, uId, groupId));
        groupToLeave.forEach(groupId -> KeyCloakUtil.removeGroupMember(realmId, uId, groupId));
    }
}
