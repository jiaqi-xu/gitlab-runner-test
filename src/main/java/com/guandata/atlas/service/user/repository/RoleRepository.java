package com.guandata.atlas.service.user.repository;

import com.guandata.atlas.common.utils.KeyCloakUtil;
import com.guandata.atlas.service.user.model.Role;
import org.keycloak.representations.idm.RoleRepresentation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

@Repository
public class RoleRepository {

    private static String realmId;

    private static String client;

    @Value("${keycloak.realm}")
    public void setRealmId(String realm) {
        RoleRepository.realmId = realm;
    }

    @Value("${keycloak.resource}")
    public void setClient(String client) {
        RoleRepository.client = client;
    }

    public static List<Role> list() {
        return KeyCloakUtil.getClientRoles(realmId, client).stream().map(roleRep -> new Role(roleRep))
                .collect(Collectors.toList());
    }

    public static List<Role> getRolesByUId(String uId) {
        return KeyCloakUtil.getClientRolesByUId(realmId, client, uId).stream().map(roleRep -> new Role(roleRep))
                .collect(Collectors.toList());
    }

    public static void addRolesToUser(String uId, List<String> roles) {
        List<RoleRepresentation> clientRoles = KeyCloakUtil.getClientRoles(realmId, client);
        List<RoleRepresentation> toAdd = clientRoles.stream().filter(clientRole -> roles.contains(clientRole.getName())).collect(Collectors.toList());
        KeyCloakUtil.assignClientLevelRolesToUser(realmId, client, uId, toAdd);
    }

    public static void updateRolesForUser(String uId, List<String> roles) {
        List<RoleRepresentation> clientRoles = KeyCloakUtil.getClientRoles(realmId, client);
        List<String> ownedRoles = KeyCloakUtil.getClientRolesByUId(realmId, client, uId).stream()
                .map(roleRep -> roleRep.getName()).collect(Collectors.toList());

        List<RoleRepresentation> rolesToAdd = clientRoles.stream().filter(
                clientRole -> roles.contains(clientRole.getName()) && !ownedRoles.contains(clientRole.getName())).collect(Collectors.toList());
        List<RoleRepresentation> rolesToRemove = clientRoles.stream().filter(
                clientRole -> ownedRoles.contains(clientRole.getName()) && !roles.contains(clientRole.getName())).collect(Collectors.toList());

        KeyCloakUtil.assignClientLevelRolesToUser(realmId, client, uId, rolesToAdd);
        KeyCloakUtil.removeClientLevelRolesFromUser(realmId, client, uId, rolesToRemove);
    }
}
