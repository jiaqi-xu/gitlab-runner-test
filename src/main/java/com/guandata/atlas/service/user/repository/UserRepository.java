package com.guandata.atlas.service.user.repository;

import com.alibaba.fastjson.JSONObject;
import com.guandata.atlas.common.utils.KeyCloakUtil;
import com.guandata.atlas.enums.ErrorCode;
import com.guandata.atlas.exception.AtlasException;
import com.guandata.atlas.service.user.model.User;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

@Repository
public class UserRepository {
    private static final Logger logger = LoggerFactory.getLogger(UserRepository.class);

    private static String realmId;

    private static String client;

    @Value("${keycloak.realm}")
    public void setRealmId(String realm) {
        UserRepository.realmId = realm;
    }

    @Value("${keycloak.resource}")
    public void setClient(String client) {
        UserRepository.client = client;
    }

    public static String createUser(User user) {
        return KeyCloakUtil.createUser(realmId, user.transferToUserRep());
    }

    public static void deleteUser(String uId) {
        KeyCloakUtil.deleteUser(realmId, uId);
    }

    public static void updateUser(String uId, User user) {
        UserRepresentation userRep = KeyCloakUtil.getUserById(realmId, uId);

        if(!user.getUsername().equals(userRep.getFirstName())) {
            userRep.setFirstName(user.getUsername());
        }

        if(user.getEmail() != null && !user.getEmail().equals(userRep.getEmail())) {
            userRep.setEmail(user.getEmail());
        }

        KeyCloakUtil.updateUser(realmId, userRep);
    }

    public static void resetPassword(String uId, CredentialRepresentation credentialRepresentation) {
        KeyCloakUtil.resetPassword(realmId, uId, credentialRepresentation);
    }

    public static Boolean validateUserCredential(String username, String password) {
        JSONObject responseObj = KeyCloakUtil.loginIn(realmId, client, username, password);
        if (responseObj.containsKey("access_token")) {
            return true;
        } else {
            logger.warn("验证用户身份失败: {}", responseObj.getString("error_description"));
            throw new AtlasException(ErrorCode.KEYCLOAK_VALIDATE_USER_CREDENTIALS, "原密码输入错误");
        }
    }

    public static User getUserById(String uId) {
        UserRepresentation userRep = KeyCloakUtil.getUserById(realmId, uId);
        return new User(userRep);
    }

    public static List<User> getUserList(String search, Integer first, Integer max) {
        List<UserRepresentation> userRepList = KeyCloakUtil.getUsers(realmId, search, first, max);
        return userRepList.stream().map(userRep -> new User(userRep)).collect(Collectors.toList());
    }

}
