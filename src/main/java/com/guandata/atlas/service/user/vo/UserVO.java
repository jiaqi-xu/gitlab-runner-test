package com.guandata.atlas.service.user.vo;

import com.alibaba.fastjson.annotation.JSONType;
import lombok.Data;

import java.util.List;

@JSONType(orders = {"uId", "loginId", "username", "email", "roles", "groups"})
@Data
public class UserVO {
    /** 用户Id **/
    private String uId;

    /** 账号 **/
    private String loginId;

    /** 用户名称 **/
    private String username;

    /** 邮箱 **/
    private String email;

    /** 账号角色 **/
    private List<String> roles;

    /** 用户组 **/
    private List<String> groups;

}
