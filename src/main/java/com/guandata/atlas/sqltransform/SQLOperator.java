package com.guandata.atlas.sqltransform;

import com.guandata.atlas.dto.FilterCondition;
import com.guandata.atlas.dto.ResultCondition;
import org.jooq.Condition;
import org.jooq.impl.DSL;

import java.util.List;

//TODO：to abstract other sql operators
public class SQLOperator {
    public static Condition whereConditionTransform(List<ResultCondition> conditions) {
        Condition whereCondition = DSL.trueCondition();
        for(ResultCondition condition: conditions) {
            if (condition.getValues().size() == 1) {
                whereCondition = whereCondition.and(DSL.field(DSL.name(condition.getColumn().getColumnId())).eq(condition.getValues().get(0)));
            } else {
                whereCondition = whereCondition.and(DSL.field(DSL.name(condition.getColumn().getColumnId())).in(condition.getValues()));
            }
        }
        return whereCondition;
    }

    /**
     * where condition for filters
     *
     * @param conditions
     * @return
     */
    public static Condition filterConditionsTransForm(List<FilterCondition> conditions) {
        Condition filterConditions = DSL.trueCondition();
        for (FilterCondition condition: conditions) {
            if (condition.getValues().size() == 1 && condition.getFilterType().equals("EQ")) {
                filterConditions = filterConditions.and(DSL.field(DSL.name(condition.getName())).eq(condition.getValues().get(0)));
            } else if (condition.getFilterType().equals("IN")) {
                filterConditions = filterConditions.and(DSL.field(DSL.name(condition.getName())).in(condition.getValues()));
            } else if (condition.getValues().size() == 2 && condition.getFilterType().equals("BT") && condition.getFilterType().equals("DATE")) {
                filterConditions = filterConditions.and(DSL.field(DSL.name(condition.getName())).greaterThan(condition.getValues().get(0)))
                                                   .and(DSL.field(DSL.name(condition.getName())).lessThan(condition.getValues().get(1)));
            }
        }
        return filterConditions;
    }
}
