package com.guandata.atlas.task;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.guandata.atlas.dao.WorkFlowEntity;
import com.guandata.atlas.mapper.WorkFlowMapper;
import com.guandata.atlas.service.ActionButtonService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Component
public class ActionButtonTask {
    private static final Logger logger = LoggerFactory.getLogger(ActionButtonTask.class);

    @Autowired
    private WorkFlowMapper workFlowMapper;

    @Autowired
    private ActionButtonService actionButtonService;

    /**
     * scheduled task getting result for unfinished workflow per 5 seconds
     *
     * @throws Exception
     */
    @Scheduled(cron = "0/5 * * * * ?")
    public void getWorkFlowTaskState() throws Exception {
        logger.info("Scheduling Action Button Task- Get Work Flow State: {}, {}", new Date(), System.currentTimeMillis());
        // get unfinished workflow tasks
        QueryWrapper<WorkFlowEntity> workflowQueryWrapper = new QueryWrapper<>();
        workflowQueryWrapper.notIn("state", Arrays.asList(5, 6, 7, 9));
        List<WorkFlowEntity> workFlowEntityList = workFlowMapper.selectList(workflowQueryWrapper);
        for (WorkFlowEntity workFlowEntity : workFlowEntityList) {
            actionButtonService.getTaskState(workFlowEntity);
        }
        logger.info("Finished Action Button Task - Get Work Flow State: {}, {}", new Date(), System.currentTimeMillis());
    }
}
