package com.guandata.atlas.task;

import com.alibaba.fastjson.JSON;
import com.guandata.atlas.dao.DataSetDAO;
import com.guandata.atlas.dao.TaskDAO;
import com.guandata.atlas.dto.DataSetDTO;
import com.guandata.atlas.dto.dataset.DSExt;
import com.guandata.atlas.enums.TaskStatusEnum;
import com.guandata.atlas.service.disruptor.DisruptorTaskService;
import com.guandata.atlas.service.disruptor.EventTypeEnum;
import com.guandata.atlas.service.task.repository.TaskRepository;
import org.jooq.DSLContext;
import org.jooq.impl.DSL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class DataSetTask implements ApplicationListener<ContextRefreshedEvent> {
    private static final Logger logger = LoggerFactory.getLogger(DataSetTask.class);

    @Autowired
    private DSLContext dslContext;

    @Resource
    private DisruptorTaskService disruptorTaskService;

    private volatile boolean started;



    @Scheduled(cron = "0/10 * * * * ?")
    public boolean updateDataSetExt() {
        logger.info("Scheduling DataSet Task - Update DataSet Ext: {}, {}", new Date(), System.currentTimeMillis());
        List<DataSetDAO> datasets = dslContext.select(DSL.field("id"), DSL.field("name"), DSL.field("table_name"), DSL.field("definition"), DSL.field("ext"), DSL.field("update_time"))
                .from(DSL.table("dataset")).where(DSL.condition("is_del = 0"))
                .fetch(item -> item.into(DataSetDAO.class));
        List<DataSetDTO> dataSetDTOS = datasets.stream().map(item -> item.toDataSetDTO()).collect(Collectors.toList());
        dataSetDTOS.stream().forEach(dataset -> {
            try {
                int rowCount = dslContext.fetchCount(DSL.table(DSL.name(dataset.getTableName())));
                //TODO: get colCount if column size will change
                int colCount = dataset.getDefinition().size();
                dataset.setExt(new DSExt(rowCount, colCount));
                dslContext.update(DSL.table("dataset"))
                        .set(DSL.field("ext"), JSON.toJSONString(dataset.getExt()))
                        .where(DSL.condition("table_name = {0} and is_del = 0", dataset.getTableName())).execute();
            } catch (Exception e) {
                logger.error("Fail to update dataset {}: {}", dataset.getName(), e.getMessage());
            }
        });
        logger.info("Finished DataSet Task - Update DataSet Ext: {}, {}", new Date(), System.currentTimeMillis());
        return true;
    }

    @Scheduled(cron = "0 */5 * * * ?")
    public void updateExpireTask() {

        List<TaskDAO> tasksByStatus = TaskRepository.getTasksByStatus(
                new ArrayList<>(Arrays.asList(new Integer[]{TaskStatusEnum.INIT.getStatus(),
                        TaskStatusEnum.RUNNING.getStatus()})));
        if (CollectionUtils.isEmpty(tasksByStatus)) {
            return;
        }

        List<TaskDAO> toBeUpdateTasks = tasksByStatus.stream().filter(
                s->new Date().getTime() - s.getStartTime().getTime() > 600000
        ).collect(Collectors.toList());
        if (CollectionUtils.isEmpty(toBeUpdateTasks)) {
            return;
        }

        for (TaskDAO toBeUpdateTask : toBeUpdateTasks) {

            toBeUpdateTask.setEndTime(new Date());
            toBeUpdateTask.setStatus(TaskStatusEnum.TIME_OUT.getStatus());

            TaskRepository.updateTask(toBeUpdateTask);
        }

    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {

        //此处判断主要是用于解决web应用中的重复刷新问题，因为web应用中会存在父子容器，导致
        //事件ContextRefreshedEvent会被触发多次。通过限定只在父容器刷新事件触发时才进行
        //处理,从而解决多次触发问题。
        if(event.getApplicationContext().getParent()==null && !started){
            //初始化函数
            disruptorTaskService.sendNotify(EventTypeEnum.INIT_PAGE_SYNC, null);
            started = true;
        }
    }


}

