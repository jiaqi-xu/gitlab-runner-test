-- ----------------------------
-- Table structure for dataset
-- ----------------------------
CREATE TABLE IF NOT EXISTS `dataset` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '数据集名字',
  `table_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '数据集uuid',
  `definition` json DEFAULT NULL COMMENT '数据集定义，包括字段schema',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '数据集创建时间',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '数据集结构更新时间',
  `editor_id` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '最近一次修改者',
  `creator_id` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '数据集创建者',
  `is_del` tinyint(1) DEFAULT '0' COMMENT '是否被移除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for page
-- ----------------------------
CREATE TABLE IF NOT EXISTS `page` (
  `pg_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '页面uuid',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '页面名称',
  `ds_id` int(11) NOT NULL COMMENT '关联的数据源类型id',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'page的状态，e.g. 0代表正常状态，1表示删除状态，2表示初始化状态',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'page创建时间，即初始化时间',
  PRIMARY KEY (`pg_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for card
-- ----------------------------
CREATE TABLE IF NOT EXISTS `card` (
  `cd_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '卡片uuid',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '卡片名称',
  `pg_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '卡片所在的page id',
  `seq_no` int(11) DEFAULT NULL COMMENT '卡片顺序',
  `is_del` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否存在',
  PRIMARY KEY (`cd_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for field
-- ----------------------------
CREATE TABLE IF NOT EXISTS `field` (
  `fd_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'uuid',
  `fd_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '类型，e.g. DOUBLE, VARCHAR',
  `column_id` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '字段在数据集中的Id',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '数据集中字段的名称',
  `alias` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '字段别名',
  `meta_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '在planning table中的类型, e.g. METRIC, TEXT',
  `fd_property` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '在表格字段中的属性，e.g. DIM, METRIC',
  `is_aggregated` tinyint(1) NOT NULL COMMENT '是否是聚合主键',
  `aggr_type` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '聚合计算因子，e.g. SUM, AVG, MAX',
  `allow_filtrated` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否启用筛选',
  `allow_sorted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否启用排序',
  `allow_edited` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否启用编辑',
  `cd_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '关联的卡片类型',
  `seq_no` int(11) DEFAULT NULL COMMENT '在字段表格中的顺序',
  `in_default_view` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否为默认展示的column',
  `data_format` json DEFAULT NULL COMMENT '数据格式(数值)',
  PRIMARY KEY (`fd_id`,`cd_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- ----------------------------
-- Table structure for filter
-- ----------------------------
CREATE TABLE IF NOT EXISTS `filter` (
  `fd_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Field id',
  `column_id` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '数据集字段id',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'filter字段的名称，也是数据集字段名称',
  `alias` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '字段别名',
  `cd_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '关联的card Id',
  `pg_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '关联的page Id',
  `seq_no` int(11) DEFAULT NULL COMMENT '页面筛选器的顺序',
  PRIMARY KEY (`fd_id`,`pg_id`,`cd_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for initial_field_list
-- ----------------------------
CREATE TABLE IF NOT EXISTS `initial_field_list` (
  `fd_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'uuid',
  `column_id` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '字段在数据集中的Id',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '数据集中字段名称',
  `fd_type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '类型，e.g. DOUBLE, INT',
  `meta_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '在planning table中的类型, e.g. METRIC, TEXT',
  `pg_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '页面id',
  `seq_no` int(11) NOT NULL COMMENT '初始化字段的顺序',
  PRIMARY KEY (`fd_id`,`pg_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



