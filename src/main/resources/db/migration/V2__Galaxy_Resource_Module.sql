-- ----------------------------
-- Table structure for admin_module
-- ----------------------------
CREATE TABLE IF NOT EXISTS `admin_module` (
  `id` int(2) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '模块名',
  `alias` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '模块别名',
  `type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '模块类型，e.g. 数据看板管理PAGE, 数据收集管理FORM',
  `enabled` tinyint(1) DEFAULT '0' COMMENT '是否启用',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
INSERT INTO `admin_module` VALUES (1, '数据看板', '数据看板', 'PAGE', 1);
INSERT INTO `admin_module` VALUES (2, '促销维护', '数据收集', 'FORM', 1);
-- ----------------------------
-- Table structure for account
-- ----------------------------
CREATE TABLE IF NOT EXISTS `account` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '平台连接环境名称',
  `cn_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '连接器类型 e.g. universe, galaxy',
  `auth_config` json DEFAULT NULL COMMENT 'Json格式的连接相关的验证信息',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `is_del` tinyint(1) DEFAULT '0' COMMENT '是否被移除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
-- ----------------------------
-- Table structure for kanban
-- ----------------------------
CREATE TABLE IF NOT EXISTS `kanban` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `object_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'tab名称',
  `account_id` int(11) DEFAULT NULL COMMENT 'tab对应账户环境的id',
  `object_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '对象类型，e.g. PAGE, FORM',
  `object_id` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'tab对应的pageId',
  `seq_no` int(11) DEFAULT NULL COMMENT 'tab顺序',
  `editor_id` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '0' COMMENT '创建/更新/删除该条记录的userId',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `is_del` tinyint(1) DEFAULT '0' COMMENT '是否被移除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


