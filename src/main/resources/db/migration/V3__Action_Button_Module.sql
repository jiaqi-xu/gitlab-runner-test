-- ----------------------------
-- Table structure for action_button
-- ----------------------------
CREATE TABLE IF NOT EXISTS `action_button` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'button名字',
  `action_type` int(1) DEFAULT NULL COMMENT '1-工作流调度；2-跳转查看',
  `pg_id` varchar(255) DEFAULT NULL COMMENT 'button关联的pageId',
  `button_setting` json DEFAULT NULL COMMENT '按钮展示配置',
  `target_setting` json DEFAULT NULL COMMENT '按钮目标配置',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '按钮创建时间',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '按钮更新时间',
  `creator_id` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '按钮创建者',
  `editor_id` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '最近一次修改者',
  `is_del` tinyint(1) DEFAULT '0' COMMENT '是否被移除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for workflow
-- ----------------------------
CREATE TABLE IF NOT EXISTS `workflow` (
  `task_id` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '任务id',
  `button_id` bigint(11) NOT NULL COMMENT '按钮id',
  `state` int(1) DEFAULT '0' COMMENT '具体见WorkFlowTaskState.java',
  `start_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '工作流开始时间',
  `end_time` timestamp NULL DEFAULT NULL COMMENT '工作流结束时间',
  `editor_id` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '触发按钮的操作者',
  PRIMARY KEY (`task_id`,`button_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;