-- ----------------------------
-- Table structure for grant_users_resources
-- ----------------------------
CREATE TABLE IF NOT EXISTS `grant_users_resources` (
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增主键id',
  `resource_id` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '资源id',
  `resource_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '资源名称',
  `resource_type` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '资源类型，e.g. PT_PAGE, PAGE, FORM',
  `user_id` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '拥有该资源的用户id',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_resource_user` (`resource_id`,`user_id`) USING BTREE,
  KEY `idx_user_type` (`user_id`,`resource_type`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Add index for table kanban
-- ----------------------------
ALTER TABLE `kanban`
ADD INDEX `idx_object_id`(`object_id`) USING BTREE;