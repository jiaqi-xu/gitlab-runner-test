-- ----------------------------
-- Add columns for table dataset
-- ----------------------------
ALTER TABLE `dataset`
ADD COLUMN `source_type` varchar(64) NULL COMMENT '数据集来源，e.g. UNIVERSE, GALAXY' AFTER `name`,
ADD COLUMN `source_ds` json NULL COMMENT '源(参照)数据集信息' AFTER `source_type`,
ADD COLUMN `ext` json NULL COMMENT '数据集扩展信息，e.g. rowCount, colCount' AFTER `definition`,
MODIFY COLUMN `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '数据集名字'
AFTER `id`;

-- ----------------------------
-- Add index for table dataset
-- ----------------------------
ALTER TABLE `dataset`
ADD UNIQUE INDEX `uk_table_name`(`table_name`) USING BTREE;

-- -------------------------------------------------------
-- drp columns for table initial_field_list, filter, field
-- -------------------------------------------------------
ALTER TABLE `initial_field_list`
DROP COLUMN `name`,
DROP COLUMN `fd_type`;

ALTER TABLE `filter`
DROP COLUMN `name`;

ALTER TABLE `field`
DROP COLUMN `fd_type`,
DROP COLUMN `name`;

-- ----------------------------
-- Table structure for task
-- ----------------------------
CREATE TABLE IF NOT EXISTS `task` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `task_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '任务uuid',
  `task_type` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '任务类型',
  `object_id` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '任务业务对象id',
  `status` tinyint DEFAULT NULL COMMENT '任务运行状态',
  `failure_cause` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT '运行错误原因信息',
  `start_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '任务开始时间',
  `end_time` timestamp NULL DEFAULT NULL COMMENT '任务结束时间',
  `creator_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '任务触发者',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `uk_task_id` (`task_id`) USING BTREE,
  KEY `idx_object_id_type_status` (`object_id`,`task_type`,`status`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;