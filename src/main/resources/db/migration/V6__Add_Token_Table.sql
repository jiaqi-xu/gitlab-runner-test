-- ----------------------------
-- Table structure for token
-- ----------------------------
CREATE TABLE IF NOT EXISTS `token` (
  `id` int(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `object_type` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '业务类型',
  `object_id` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '业务对象id',
  `access_token` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '访问令牌',
  `effective_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '生效时间',
  `expired_time` timestamp NOT NULL DEFAULT '2037-12-31 23:59:59' COMMENT '失效时间',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_object_id_type` (`object_id`,`object_type`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;