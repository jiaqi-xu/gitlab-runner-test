-- ----------------------------
-- Table structure for grant_groups_resources
-- ----------------------------
CREATE TABLE IF NOT EXISTS `grant_groups_resources` (
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增主键id',
  `resource_id` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '资源id',
  `resource_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '资源名称',
  `resource_type` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '资源类型，e.g. PT_PAGE, PAGE, FORM',
  `group_id` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '拥有该资源的用户组id',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_resource_user` (`resource_id`,`group_id`) USING BTREE,
  KEY `idx_user_type` (`group_id`,`resource_type`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;