ALTER TABLE `filter` ADD COLUMN `type` varchar(32)  NOT NULL DEFAULT 'DS_ELEMENTS';
ALTER TABLE `filter` ADD COLUMN `ext` text;
ALTER TABLE `filter` ADD COLUMN `aggr_type` varchar(32);

update `filter` set ext ='{"subType":"MULTI","defaultType":"NONE","showAll":true,"sortType":"DEFAULT"}';