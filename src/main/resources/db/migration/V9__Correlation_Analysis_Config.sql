ALTER TABLE `field`
ADD COLUMN `correlation_analysis` json NULL COMMENT '关联分析配置' AFTER `data_format`,
ADD COLUMN `kanban_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci GENERATED ALWAYS AS (json_extract(`correlation_analysis`,'$.objectId')) VIRTUAL COMMENT '虚拟列：关联分析配置化' NULL AFTER `correlation_analysis`,
ADD INDEX `virtual_idx_kanbanid`(`kanban_id`) USING BTREE;