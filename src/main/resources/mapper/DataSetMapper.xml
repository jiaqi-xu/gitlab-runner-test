<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="com.guandata.atlas.mapper.DataSetMapper">
    <resultMap id="BaseResultMap" type="com.guandata.atlas.dao.DataSetDAO">
        <id column="id" jdbcType="INTEGER" property="id" />
        <result column="name" jdbcType="VARCHAR" property="name"/>
        <result column="table_name" jdbcType="VARCHAR" property="tableName" />
        <result column="definition" jdbcType="VARCHAR" property="definition"/>
    </resultMap>

    <sql id="BaseColumnList">
        id, name, table_name, definition
    </sql>

    <sql id="ModifyHistoryColumnList">
        id, cd_id, pg_id, user_id, user_name, modify_time, apply_status, data_object, column_id, modified_from, modified_to
    </sql>

    <select id="list" resultMap="BaseResultMap">
        select id, name from dataset
    </select>

    <select id="getById" resultMap="BaseResultMap">
        select
        <include refid="BaseColumnList"/>
        from dataset where id = #{Id}
    </select>

    <select id="getByName" resultMap="BaseResultMap">
        select
        <include refid="BaseColumnList"/>
        from dataset where name = #{name} and is_del = 0
    </select>

    <sql id="select_condition">
        <foreach collection="fields" index="index" item="f" separator=",">
                `${f.column.columnId}`
        </foreach>
    </sql>

    <sql id="select_groupBy_fields_condition">
        <foreach collection="fields" index="index" item="f" separator=",">
            <if test="f.isAggregated == true and f.fdProperty != null">
                `${f.column.columnId}`
            </if>

            <if test="f.isAggregated == false and f.aggrType == 'SUM'">
                SUM(`${f.column.columnId}`) as `${f.column.columnId}`
            </if>

            <if test="f.isAggregated == false and f.aggrType == 'MAX'">
                MAX(`${f.column.columnId}`) as `${f.column.columnId}`
            </if>
        </foreach>
    </sql>

    <sql id="where_condition">
        <if test="conditions != null">
            <foreach collection="conditions" index="index" item="c" separator=" ">
                <if test="c.values.size() == 1">
                    AND `${c.column.columnId}` = #{c.values[0]}
                </if>

                <if test="c.values.size() > 1">
                    AND `${c.column.columnId}` IN
                    <foreach collection="c.values" index="index" item="v" open="(" close=")" separator=",">
                        #{v}
                    </foreach>
                </if>
            </foreach>
        </if>
    </sql>

    <sql id="groupby_condition">
        AND
        <if test="groupByValues != null">
            <foreach collection="groupByFields" index="index" item="groupByField" open="(" close=")" separator=",">
                `${groupByField}`
            </foreach>
            IN
            <foreach collection="groupByValues" index="index1" item="v1" open="(" close=")" separator=",">
                <foreach collection="groupByValue" index="index2" item="v2" open="(" close=")" separator=",">
                    #{v2}
                </foreach>
            </foreach>
        </if>
    </sql>

    <sql id="group_by">
        <if test="groupByFields.isEmpty() == false">
            group by
            <foreach collection="groupByFields" index="index" item="groupByField" separator=",">
                `${groupByField}`
            </foreach>
        </if>
    </sql>
    
    <sql id="order_by">
        <if test="orderByFields != null">
            order by
            <foreach collection="orderByFields" index="index" item="orderByField" separator=",">
                `${orderByField.column.columnId}` ${orderByField.orderType}
            </foreach>
        </if>
    </sql>

    <sql id="exclude_modified_row">
        <if test="idList.size() > 0">
            AND id NOT IN
            <foreach collection="idList" item="item" open="(" close=")" separator=",">
                #{item}
            </foreach>
        </if>
    </sql>

    <sql id="filter_condition">
        <if test="filters.size() != 0">
            AND
            <foreach collection="filters" index="index" item="f" separator=" AND ">
                <choose>
                    <when test="f.filterType == 'IN'">
                        `${f.name}` IN (<foreach collection="f.values" index="index" item="value" separator=",">#{value}</foreach> )
                    </when>
                    <when test="f.filterType == 'BT' and f.values.size() == 2 and f.metaType == 'DATE'">
                        `${f.name}` &gt; '${f.values.get(0)}' AND `${f.name}` &lt; '${f.values.get(1)}'
                    </when>
                </choose>
            </foreach>
        </if>
    </sql>

    <select id="getModifiedRows" resultType="java.lang.Long">
        select id from `${tableName}_update` WHERE 1 = 1
        <include refid="where_condition"/>
    </select>

    <select id="getModifyActions" resultType="Map">
        SELECT <include refid="ModifyHistoryColumnList"/> FROM `${dsTable}_modify_history`
        WHERE 1=1
        <include refid="filter_condition"/>
        AND
        <choose>
            <when test="modifyIdList.size() != 0 and hasDetailFilter == false">
                ((apply_status = 0) OR (id IN
                <foreach collection="modifyIdList" item="item" open="(" close=")" separator=",">
                    #{item}
                </foreach>) OR
                (cd_id is NULL)
                )
            </when>
            <when test="modifyIdList.size() != 0 and hasDetailFilter == true">
                ((id IN
                <foreach collection="modifyIdList" item="item" open="(" close=")" separator=",">
                    #{item}
                </foreach>) OR
                (cd_id is NULL))
            </when>
            <when test="modifyIdList.size() == 0 and hasDetailFilter == false">
                id IS NOT NULL
            </when>
            <when test="modifyIdList.size() == 0 and hasDetailFilter == true">
                cd_id is NULL
            </when>
        </choose>
        ORDER BY modify_time DESC
    </select>

    <select id="getColumns" resultType="java.lang.String">
        SELECT COLUMN_NAME FROM information_schema.COLUMNS
        WHERE TABLE_SCHEMA='${dbname}' and table_name='${dsTable}_modify_history_detail';
    </select>

    <select id="getDetails" resultType="Map">
        SELECT
        <foreach collection="columns" item="column" separator=",">
            `${column}`
        </foreach>
        FROM `${dsTable}_modify_history_detail`
        WHERE
        1=1
        <include refid="filter_condition"/>
        AND modify_id=#{modifyId}
    </select>

    <select id="getExceptionById" resultType="Map">
        SELECT id AS modify_id, message FROM `${dsTable}_modify_history` WHERE id=#{modifyId}
    </select>
</mapper>