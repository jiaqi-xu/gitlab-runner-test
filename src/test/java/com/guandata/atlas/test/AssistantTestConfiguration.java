package com.guandata.atlas.test;

import com.guandata.atlas.AtlasServiceWalmartApplication;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.autoconfigure.AutoConfigurationExcludeFilter;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.TypeExcludeFilter;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.FilterType;


@SpringBootApplication
@ComponentScan(basePackages = "com.guandata.atlas", excludeFilters = {
        @ComponentScan.Filter(type = FilterType.REGEX, pattern = "com.guandata.atlas.controller.*"),
        @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = AtlasServiceWalmartApplication.class),
        @ComponentScan.Filter(type = FilterType.CUSTOM, classes = {TypeExcludeFilter.class}),
        @ComponentScan.Filter(type = FilterType.CUSTOM, classes = {AutoConfigurationExcludeFilter.class})
})
@MapperScan("com.guandata.atlas.mapper")
@EnableAspectJAutoProxy
//@EnableScheduling 依赖定时任务的时候需要放开
public class AssistantTestConfiguration {
}
