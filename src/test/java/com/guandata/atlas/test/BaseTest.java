package com.guandata.atlas.test;

import com.guandata.atlas.test.testHelper.service.DatasetTestService;
import com.guandata.atlas.test.testHelper.service.PlanningTableTestService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = AssistantTestConfiguration.class, webEnvironment = SpringBootTest.WebEnvironment.NONE)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Slf4j
public class BaseTest {
    @Autowired
    protected DatasetTestService datasetTestService;

    @Autowired
    protected PlanningTableTestService planningTableTestService;


}
