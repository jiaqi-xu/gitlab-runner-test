package com.guandata.atlas.test.dataSetServiceTest;

import com.guandata.atlas.test.BaseTest;
import com.guandata.atlas.test.testHelper.constant.PlanningTableConstant;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;

@Slf4j
public class DataSetBaseTest extends BaseTest {
    protected int dsId;

    protected static final String dataSetDataFileName = "module-dataset/dataset-data.txt";
    protected static final String dataSetUpdateDataFileName = "module-dataset/dataset-update-data.txt";
    protected static final String dataSetDefinitionFileName = "module-dataset/dataset-definition.txt";
    protected static final String planningTableFileName = "module-dataset/planningTable.txt";

    @BeforeAll
    public void mockData() {
        log.info("----正在初始化数据集----");
        dsId = datasetTestService.createDataSet(dataSetDefinitionFileName, dataSetDataFileName, dataSetUpdateDataFileName);
        planningTableTestService.createPlanningTable(dsId, planningTableFileName);
        log.info("----初始化数据集完毕，dsId:{}----", dsId);
    }

    @AfterAll
    public void clearData() {
        log.info("----正在清理mock数据----");
        planningTableTestService.removePlanningTable(PlanningTableConstant.PG_ID_LIST, PlanningTableConstant.CD_ID_LIST);
        datasetTestService.removeDataSet(dsId);
        log.info("----清理mock数据完毕----");

    }
}
