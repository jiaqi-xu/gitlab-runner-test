package com.guandata.atlas.test.dataSetServiceTest;

import com.alibaba.fastjson.JSONObject;
import com.guandata.atlas.common.constants.Constants;
import com.guandata.atlas.common.constants.OpenAPIConstants;
import com.guandata.atlas.controller.request.FilterDataGetRequest;
import com.guandata.atlas.dao.FieldInfoDO;
import com.guandata.atlas.dto.DSDataPreviewDTO;
import com.guandata.atlas.dto.DataSetDTO;
import com.guandata.atlas.dto.ResultCondition;
import com.guandata.atlas.dto.TableSchema;
import com.guandata.atlas.dto.dataset.DSColumn;
import com.guandata.atlas.dto.dataset.DSExt;
import com.guandata.atlas.pojo.CardInfo;
import com.guandata.atlas.pojo.FilterInfo;
import com.guandata.atlas.pojo.PTFilterInfo;
import com.guandata.atlas.pojo.PageInfo;
import com.guandata.atlas.service.DataSetService;
import com.guandata.atlas.test.BaseTest;
import com.guandata.atlas.test.testHelper.constant.DataSetConstant;
import com.guandata.atlas.test.testHelper.constant.PlanningTableConstant;
import com.guandata.atlas.test.testHelper.service.DatasetTestService;
import com.guandata.atlas.test.testHelper.service.PlanningTableTestService;
import com.guandata.atlas.test.testHelper.utils.DataSetUtil;
import com.guandata.atlas.test.testHelper.utils.JsonFileParser;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.*;
import java.util.stream.Collectors;


@Slf4j
public class TestDataSetService extends DataSetBaseTest {
    @Autowired
    private DataSetService dataSetService;

    private static final DataSetDTO dataSetDTO;
    private static final PageInfo pageInfo;
    static {
        dataSetDTO = new JsonFileParser<>(DataSetDTO.class).objectFromFile(dataSetDefinitionFileName);
        pageInfo = new JsonFileParser<>(PageInfo.class).objectFromFile(planningTableFileName);
    }

    @Test
    @Transactional
    public void testPreviewDsData() {
        DSDataPreviewDTO dsDataPreviewDTOExpected = new DSDataPreviewDTO();
        dsDataPreviewDTOExpected.setColumns(dataSetDTO.getDefinition().stream().peek(dsColumn -> dsColumn.setOriginName(null))
                .collect(Collectors.toList()));
        dsDataPreviewDTOExpected.setPreview(DataSetUtil.getDataFromFile(dataSetUpdateDataFileName).stream().map(rowData -> {
            ArrayList<Object> returnedRowData = new ArrayList<>();
            // 抽取String类型比较是否一致
            returnedRowData.add(rowData.get(0));
            // 抽取浮点类型（保留2位小数）比较是否一致
            returnedRowData.add(new BigDecimal(rowData.get(4).toString()).setScale(2, BigDecimal.ROUND_HALF_UP).toString());
            return returnedRowData;
        }).collect(Collectors.toList()));

        DSDataPreviewDTO dsDataPreviewDTOActual = dataSetService.previewDSData(dsId, null);
        dsDataPreviewDTOActual.setPreview(dsDataPreviewDTOActual.getPreview().stream().map(rowData -> {
            List<Object> returnedRowData = new ArrayList<>();
            returnedRowData.add(rowData.get(0));
            returnedRowData.add(rowData.get(4).toString());
            return returnedRowData;
        }).collect(Collectors.toList()));

        log.info("testPreviewDsData, dsDataPreviewDTOActual:[{}]", JSONObject.toJSONString(dsDataPreviewDTOActual));
        Assertions.assertEquals(dsDataPreviewDTOExpected, dsDataPreviewDTOActual);
    }

    @Test
    @Transactional
    public void testGetDataSetSchemaInfoWithNullPageId() {
        Set<String> MetricDataType = new HashSet<String>() {{
            add("DOUBLE");
            add("FLOAT");
            add("LONG");
            add("INTEGER");
        }};
        List<FieldInfoDO> fields = new ArrayList<>();
        for (DSColumn col : dataSetDTO.getDefinition()) {
            FieldInfoDO field = new FieldInfoDO();
            field.setColumn(col);
            field.setSeqNo(col.getSeqNo());
            if (MetricDataType.contains(field.getColumn().getType().toUpperCase())) {
                field.setMetaType(Constants.METRIC);
            } else {
                field.setMetaType(Constants.TEXT);
            }
            fields.add(field);
        }
        TableSchema dataSetSchemaInfoExpected = new TableSchema(
                dataSetDTO.getName(), fields, dataSetDTO.getDefinition().size(), DataSetConstant.ROW_COUNT, null);

        TableSchema dataSetSchemaInfoActual = dataSetService.getDataSetSchemaInfo(dsId, null);
        dataSetSchemaInfoActual.getColumns().stream().forEach(fieldInfoDO -> fieldInfoDO.setFdId(null));
        dataSetSchemaInfoActual.setLastUpdate(null);

        Assertions.assertEquals(dataSetSchemaInfoExpected, dataSetSchemaInfoActual);

    }

    @Test
    @Transactional
    public void testGetDataSetSchemaInfoWithPageId() {
        List<FieldInfoDO> fields = pageInfo.getInitialFieldList().stream().map(fieldInfo -> {
            FieldInfoDO fieldInfoDO = new FieldInfoDO();
            BeanUtils.copyProperties(fieldInfo, fieldInfoDO);
            fieldInfoDO.setPgId(null);
            return fieldInfoDO;
        }).collect(Collectors.toList());
        TableSchema dataSetSchemaInfoExpected = new TableSchema(
                dataSetDTO.getName(), fields, fields.size(), DataSetConstant.ROW_COUNT, null);

        TableSchema dataSetSchemaInfoActual = dataSetService
                .getDataSetSchemaInfoByExistingPage(PlanningTableConstant.PG_ID_LIST.get(0));
        dataSetSchemaInfoActual.setLastUpdate(null);

        Assertions.assertEquals(dataSetSchemaInfoExpected, dataSetSchemaInfoActual);

    }

    @Test
    @Transactional
    public void testGetFilterData() {
        FilterDataGetRequest filterDataGetRequest = new FilterDataGetRequest();
        // filter
        FilterInfo filterInfo = pageInfo.getPgFilters().get(0);
        filterInfo.setSearch("a");
        List<FilterInfo> filterInfos = Collections.singletonList(filterInfo);
        filterDataGetRequest.setFilters(filterInfos);
        // dsId
        filterDataGetRequest.setDsId(dsId);
        // conditions
        /*List<ResultCondition> resultConditions = pageInfo.getCards().get(0).getCdFilters().stream().map(cdFilterInfo -> {
            ResultCondition resultCondition = new ResultCondition();
            resultCondition.setColumn(cdFilterInfo.getColumn());
            resultCondition.setFilterInfo(cdFilterInfo);
            return resultCondition;
        }).collect(Collectors.toList());*/
        List<FilterInfo> cdFilters = pageInfo.getCards().get(0).getCdFilters();
        ResultCondition resultCondition1 = new ResultCondition();
        resultCondition1.setColumn(cdFilters.get(3).getColumn());
        resultCondition1.setValues(Collections.singletonList("2003-06-13"));
        resultCondition1.setFilterInfo(cdFilters.get(3));
        filterDataGetRequest.setConditions(Collections.singletonList(resultCondition1));

        List<PTFilterInfo> filterData = dataSetService.getFilterData(filterDataGetRequest);

        Assertions.assertArrayEquals(new String[]{"a6880", "a059f", "a7a19"}, filterData.get(0).getChoice().toArray());

    }

    @Test
    @Transactional
    public void testGetDataSetInfo() {
        DataSetDTO dataSetDTOExpected = new DataSetDTO();
        dataSetDTOExpected.setId(dataSetDTO.getId());
        dataSetDTOExpected.setName(dataSetDTO.getName());
        dataSetDTOExpected.setTableName(dataSetDTO.getTableName());
        dataSetDTOExpected.setDefinition(dataSetDTO.getDefinition());

        DataSetDTO dataSetDTOActual = dataSetService.getDataSetInfo(dsId);

        Assertions.assertEquals(dataSetDTOExpected, dataSetDTOActual);
    }

    @Test
    @Transactional
    public void testGetDataSetMeta() {
        List<List<Object>> dataFromFile = datasetTestService.getDataFromFile(dataSetDataFileName);
        DataSetDTO dataSetDTOExpected = new DataSetDTO();
        dataSetDTOExpected.setId(dataSetDTO.getId());
        dataSetDTOExpected.setName(dataSetDTO.getName());
        dataSetDTOExpected.setSourceType(dataSetDTO.getSourceType());
        dataSetDTOExpected.setSourceDS(dataSetDTO.getSourceDS());
        dataSetDTOExpected.setTableName(dataSetDTO.getTableName());
        dataSetDTOExpected.setDefinition(dataSetDTO.getDefinition());
        dataSetDTOExpected.setExt(new DSExt(dataFromFile.size(), dataSetDTO.getDefinition().size()));
        dataSetDTOExpected.setDataSyncOpenAPI(new MessageFormat(OpenAPIConstants.LIST_DATASET_URL)
                .format(new String[]{String.valueOf(dataSetDTO.getTableName()), DataSetConstant.ACCESS_TOKEN}));
        dataSetDTOExpected.setBackFlowSQL("select \n" +
                "  `STRING-1` as `STRING-字符串1`, \n" +
                "  `STRING-2` as `STRING-字符串2`, \n" +
                "  round(`INTEGER-1`) as `INTEGER-整型1`, \n" +
                "  round(`INTEGER-2`) as `INTEGER-整型2`, \n" +
                "  round(\n" +
                "    `DOUBLE-1`, \n" +
                "    2\n" +
                "  ) as `DOUBLE-双精度1`, \n" +
                "  round(\n" +
                "    `DOUBLE-2`, \n" +
                "    2\n" +
                "  ) as `DOUBLE-双精度2`, \n" +
                "  round(`LONG-1`) as `LONG-长精度1`, \n" +
                "  round(`LONG-2`) as `LONG-长精度2`, \n" +
                "  round(\n" +
                "    `FLOAT-1`, \n" +
                "    2\n" +
                "  ) as `FLOAT-浮点1`, \n" +
                "  round(\n" +
                "    `FLOAT-2`, \n" +
                "    2\n" +
                "  ) as `FLOAT-浮点2`, \n" +
                "  `DATE-1` as `DATE-日期1`, \n" +
                "  `DATE-2` as `DATE-日期2`, \n" +
                "  `DATETIME-1` as `DATETIME-日期时间1`, \n" +
                "  `DATETIME-2` as `DATETIME-日期时间2`, \n" +
                "  `BOOLEAN-1` as `BOOLEAN-布尔1`, \n" +
                "  `BOOLEAN-2` as `BOOLEAN-布尔2`\n" +
                "from `test-table_update`");

        DataSetDTO dataSetDTOActual = dataSetService.getDataSetMeta(dsId);
        dataSetDTOActual.setUpdateTime(null);

        Assertions.assertEquals(dataSetDTOExpected, dataSetDTOActual);
    }

}
