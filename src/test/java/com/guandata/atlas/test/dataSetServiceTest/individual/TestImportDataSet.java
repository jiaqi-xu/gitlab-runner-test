package com.guandata.atlas.test.dataSetServiceTest.individual;

import com.guandata.atlas.dto.DataSetDTO;
import com.guandata.atlas.dto.dataset.DSExt;
import com.guandata.atlas.pojo.PageInfo;
import com.guandata.atlas.service.DataSetService;
import com.guandata.atlas.test.BaseTest;
import com.guandata.atlas.test.dataSetServiceTest.DataSetBaseTest;
import com.guandata.atlas.test.dataSetServiceTest.TestDataSetService;
import com.guandata.atlas.test.testHelper.constant.Constant;
import com.guandata.atlas.test.testHelper.utils.JsonFileParser;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;



/**
 * 不需要预置数据集
 */
@Slf4j
public class TestImportDataSet extends DataSetBaseTest {
    @Autowired
    private DataSetService dataSetService;

    private static final DataSetDTO dataSetDTO;
    static {
        dataSetDTO = new JsonFileParser<>(DataSetDTO.class).objectFromFile(dataSetDefinitionFileName);
    }

    @BeforeAll
    public void init(){
        dataSetDTO.setId(null);
        dataSetDTO.setTableName("test-import-dataset-table");
        dataSetDTO.setName("test-import-dataset");
        // 删除可能残留的数据
        datasetTestService.removeDataSet(dataSetDTO.getName());
    }

    @AfterAll
    public void clear(){
        // 清理数据集
        datasetTestService.removeDataSet(dataSetDTO.getName());
    }

    @Test
    @Transactional
    public void testImportDataSet() {
        Integer dataSetId = dataSetService.importDataSet(dataSetDTO, Constant.USER_ID);
        DataSetDTO dataSetDTOExpected = new DataSetDTO();
        BeanUtils.copyProperties(dataSetDTO, dataSetDTOExpected);
        dataSetDTOExpected.setExt(new DSExt(0, dataSetDTO.getDefinition().size()));
        dataSetDTOExpected.setId(dataSetId);

        /**dataSetServiceToTest.getDataSetMeta 方法单独测试{@link TestDataSetService#testGetDataSetMeta()}*/
        DataSetDTO datasetDTOActual = dataSetService.getDataSetMeta(dataSetId);
        datasetDTOActual.setUpdateTime(null);
        datasetDTOActual.setBackFlowSQL(null);
        datasetDTOActual.setDataSyncOpenAPI(null);

        Assertions.assertEquals(dataSetDTOExpected, datasetDTOActual);
    }


}
