package com.guandata.atlas.test.dataSetServiceTest.individual;

import com.guandata.atlas.dto.DataSetDTO;
import com.guandata.atlas.dto.UserInfo;
import com.guandata.atlas.service.DataSetService;
import com.guandata.atlas.service.DataSyncService;
import com.guandata.atlas.service.dataset.model.DataSet;
import com.guandata.atlas.test.BaseTest;
import com.guandata.atlas.test.dataSetServiceTest.DataSetBaseTest;
import com.guandata.atlas.test.testHelper.constant.Constant;
import com.guandata.atlas.test.testHelper.mockService.MockAccountService;
import com.guandata.atlas.test.testHelper.mockService.MockDisruptorTaskService;
import com.guandata.atlas.test.testHelper.mockService.MockUniverseService;
import com.guandata.atlas.test.testHelper.utils.JsonFileParser;
import lombok.extern.slf4j.Slf4j;
import org.jooq.*;
import org.jooq.impl.DSL;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.powermock.reflect.Whitebox;
import org.springframework.aop.framework.Advised;
import org.springframework.aop.support.AopUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
public class TestSyncData extends DataSetBaseTest {

    @Autowired
    private DataSetService dataSetService;

    @Autowired
    private DataSyncService dataSyncService;

    @Autowired
    private DSLContext dslContext;

    private static final DataSetDTO dataSetDTO;
    static {
        dataSetDTO = new JsonFileParser<>(DataSetDTO.class).objectFromFile(dataSetDefinitionFileName);
    }

    @PostConstruct
    public void init() throws Exception {
        if (AopUtils.isAopProxy(dataSetService)) {
            Object target = ((Advised) dataSetService).getTargetSource().getTarget();
            Whitebox.setInternalState(target, "disruptorTaskService", new MockDisruptorTaskService());
        }
        if (AopUtils.isAopProxy(dataSyncService)) {
            Object target = ((Advised) dataSyncService).getTargetSource().getTarget();
            Whitebox.setInternalState(target, "universeService", new MockUniverseService());
            Whitebox.setInternalState(target, "accountService", new MockAccountService());
        }
    }

    @Test
    @Transactional
    public void testSyncData() {
        // 模拟发起同步数据请求
        UserInfo mockUserInfo = new UserInfo();
        mockUserInfo.setUserId(Constant.USER_ID);
        mockUserInfo.setUserName(Constant.USER_NAME);
        String taskId = dataSetService.syncData(dsId, mockUserInfo, null);
        // 模拟同步数据
        dataSyncService.syncUniverseData(dataSetDTO, taskId, mockUserInfo);
        // 测试同步数据集后数据是否和预期的一致
        String[] oriValuesExpected = {"ori4", "ori5", "uni1", "uni2", "uni3", "uni4", "uni5"};
        String[] upValuesExpected = {"up4", "uni1", "uni2", "uni3", "uni4", "uni5"};
        String tableName = dataSetDTO.getTableName();
        List<Field<Object>> fields = dataSetDTO.getDefinition().stream()
                .map(dsColumn -> DSL.field(DSL.name(dsColumn.getColumnId()))).collect(Collectors.toList());
        Condition condition = DSL.field(DSL.name("STRING-1")).in("b1111", "b2222", "b3333", "b4444", "b5555");
        // 1.originRecords
        log.info("-----originRecords-----");
        String queryOriginSql = dslContext.select(fields).from(DSL.table(DSL.name(tableName)))
                .where(condition).toString();
        Result<Record> originRecords = dslContext.fetch(queryOriginSql);
        String[] oriValuesActual = originRecords.map(record -> record.get(1).toString()).toArray(new String[0]);
        // 2.upRecords
        log.info("-----upRecords-----");
        String queryUpSql = dslContext.select(fields).from(DSL.table(DSL.name(tableName + "_update")))
                .where(condition).toString();
        Result<Record> upRecords = dslContext.fetch(queryUpSql);
        String[] upValuesActual = upRecords.map(record -> record.get(1).toString()).toArray(new String[0]);

        Assertions.assertArrayEquals(oriValuesExpected, oriValuesActual);
        Assertions.assertArrayEquals(upValuesExpected, upValuesActual);
    }
}
