package com.guandata.atlas.test.planningTableServiceTest;

import com.guandata.atlas.controller.request.FilterDataGetRequest;
import com.guandata.atlas.dto.ResultCondition;
import com.guandata.atlas.pojo.PTFilterInfo;
import com.guandata.atlas.pojo.PageInfo;
import com.guandata.atlas.service.FilterService;
import com.guandata.atlas.test.testHelper.utils.DataSetUtil;
import com.guandata.atlas.test.testHelper.utils.JsonFileParser;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Slf4j
public class FilterData extends PlanningTableBaseTest {
    @Resource
    private FilterService filterService;

    private static final PageInfo pageInfo;
    static {
        pageInfo = new JsonFileParser<>(PageInfo.class).objectFromFile(ptFileName);
    }

    /**
     * 页面筛选器测试
     */
    @Test
    public void testPageFilterData() {
        FilterDataGetRequest filterDataGetRequest = new FilterDataGetRequest();
        filterDataGetRequest.setFilters(Arrays.asList(pageInfo.getPgFilters().get(0)));
        filterDataGetRequest.setDsId(dsId);
        filterDataGetRequest.setPgId(pageInfo.getPgId());

        List<PTFilterInfo> filterInfo = filterService.getFilterData(filterDataGetRequest);
        List<String> actualRes = filterInfo.get(0).getChoice();

        String expectedResFile = "module-dataset/expectedResult/planningTable/filter-data.txt";
        List<Object> expectedRes = DataSetUtil.getDataFromFile(expectedResFile).get(0);

        Assertions.assertEquals(expectedRes, actualRes, "页面筛选器: 实际值与预期值不一致");
    }

    /**
     * card后置筛选器测试
     */
    @Test
    public void testCardFilterData() {
        // Parameters Preparation
        FilterDataGetRequest filterDataGetRequest = new FilterDataGetRequest();
        filterDataGetRequest.setFilters(Arrays.asList(pageInfo.getCards().get(0).getCdFilters().get(0)));
        filterDataGetRequest.setDsId(dsId);
        filterDataGetRequest.setPgId(pageInfo.getPgId());
        filterDataGetRequest.setFieldsInfo(pageInfo.getCards().get(0).getFields());

        List<ResultCondition> conditions = new ArrayList<>();
        pageInfo.getPgFilters().stream().forEach(pgFilter -> {
            ResultCondition resCondition = new ResultCondition();
            resCondition.setFilterInfo(pgFilter);
            resCondition.setColumn(pgFilter.getColumn());
            if (pgFilter.getColumn().getColumnId().equals("STRING-1")) {
                List<Object> values = Arrays.asList("猛虎出山", "虎踞龙盘");
                resCondition.setValues(values);
            }

            if (pgFilter.getColumn().getColumnId().equals("INTEGER-1")) {
                resCondition.setValues(Arrays.asList("10002"));
            }
            conditions.add(resCondition);
        });

        filterDataGetRequest.setConditions(conditions);

        List<PTFilterInfo> filterInfo = filterService.getFilterData(filterDataGetRequest);
        List<String> actualRes = filterInfo.get(0).getChoice();

        String expectedResFile = "module-dataset/expectedResult/planningTable/filter-data.txt";
        List<Object> expectedRes = DataSetUtil.getDataFromFile(expectedResFile).get(1);

        Assertions.assertEquals(expectedRes, actualRes, "卡片后置筛选器: 实际值与预期值不一致");
    }
}
