package com.guandata.atlas.test.planningTableServiceTest;

import com.guandata.atlas.common.utils.ResponseResult;
import com.guandata.atlas.dto.*;
import com.guandata.atlas.dto.dataset.DSColumn;
import com.guandata.atlas.pojo.PageInfo;
import com.guandata.atlas.service.CardService;
import com.guandata.atlas.service.DataUpdateService;
import com.guandata.atlas.service.disruptor.handle.BatchUpdatePTHandler;
import com.guandata.atlas.service.planningtable.enums.BatchModifyTypeEnum;
import com.guandata.atlas.test.testHelper.mockService.MockTaskService;
import com.guandata.atlas.test.testHelper.mockService.MockUser;
import com.guandata.atlas.test.testHelper.utils.DataSetUtil;
import com.guandata.atlas.test.testHelper.utils.JsonFileParser;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.powermock.reflect.Whitebox;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class ModifyData extends PlanningTableBaseTest {
    @Resource
    private DataUpdateService dataUpdateService;

    @Resource
    private BatchUpdatePTHandler batchUpdatePTHandler;

    @Resource
    private CardService cardService;

    private static final DataSetDTO dataSetDTO;
    private static final PageInfo pageInfo;

    static {
        dataSetDTO = new JsonFileParser<>(DataSetDTO.class).objectFromFile(dsDefineFileName);
        pageInfo = new JsonFileParser<>(PageInfo.class).objectFromFile(ptFileName);
    }

    @PostConstruct
    public void init() {
        Whitebox.setInternalState(batchUpdatePTHandler, "taskService", new MockTaskService());
    }

    /**
     * 单个修改测试
     */
    @Test
    public void testSingleModify() {
        DataUpdateParam dataUpdateParam = new DataUpdateParam();
        List<DSColumn> aggrPKs = pageInfo.getCards().get(0).getFields().stream().filter(field -> field.getIsAggregated())
                .map(field -> field.getColumn()).collect(Collectors.toList());
        dataUpdateParam.setAggrPK(aggrPKs);
        dataUpdateParam.setCdId(pageInfo.getCards().get(0).getCdId());

        List<ResultCondition> conditions = new ArrayList<>();
        pageInfo.getPgFilters().stream().forEach(pgFilter -> {
            ResultCondition resCondition = new ResultCondition();
            resCondition.setFilterInfo(pgFilter);
            resCondition.setColumn(pgFilter.getColumn());
            if (pgFilter.getColumn().getColumnId().equals("STRING-1")) {
                List<Object> values = Arrays.asList("猛虎出山");
                resCondition.setValues(values);
            }

            if (pgFilter.getColumn().getColumnId().equals("INTEGER-1")) {
                resCondition.setValues(Arrays.asList("10002"));
            }
            conditions.add(resCondition);
        });

        dataUpdateParam.setConditions(conditions);
        dataUpdateParam.setField(pageInfo.getCards().get(0).getFields().get(2).getColumn());
        dataUpdateParam.setModifiedFrom("47873.312313");
        dataUpdateParam.setModifiedTo("50000");

        Authentication mockUserAuth = MockUser.generateUserAuth();
        SecurityContextHolder.getContext().setAuthentication(mockUserAuth);
        ResponseResult result = new ResponseResult();
        dataUpdateService.doUpdate(dataUpdateParam, result);

        List<DataUpdateRes> updateRes = (List)(((HashMap)result.getData()).get("affectedFields"));;
        Object actualValue = updateRes.get(0).getValue();

        Assertions.assertEquals("50000.000000", actualValue, "单个修改: 实际值与预期值不一致");
    }

    /**
     * 倍数批量修改测试
     */
    @Test
    public void testTimesBatchModify() {
        BatchUpdateDataDTO batchUpdateDataDTO = new BatchUpdateDataDTO();

        batchUpdateDataDTO.setCdId(pageInfo.getCards().get(0).getCdId());
        batchUpdateDataDTO.setType(BatchModifyTypeEnum.TIMES.getType());
        batchUpdateDataDTO.setValue("2");
        batchUpdateDataDTO.setDsId(dsId);
        batchUpdateDataDTO.setFdId(pageInfo.getCards().get(0).getFields().get(2).getFdId());

        List<ResultCondition> conditions = new ArrayList<>();
        pageInfo.getPgFilters().stream().forEach(pgFilter -> {
            ResultCondition resCondition = new ResultCondition();
            resCondition.setFilterInfo(pgFilter);
            resCondition.setColumn(pgFilter.getColumn());
            if (pgFilter.getColumn().getColumnId().equals("STRING-1")) {
                List<Object> values = Arrays.asList("猛虎出山", "虎踞龙盘");
                resCondition.setValues(values);
            }

            if (pgFilter.getColumn().getColumnId().equals("INTEGER-1")) {
                resCondition.setValues(Arrays.asList("10002"));
            }
            conditions.add(resCondition);
        });

        batchUpdateDataDTO.setConditions(conditions);
        batchUpdateDataDTO.setTableName(dataSetDTO.getTableName());
        Authentication mockUserAuth = MockUser.generateUserAuth();
        batchUpdateDataDTO.setAuthentication(mockUserAuth);

        batchUpdatePTHandler.handle(batchUpdateDataDTO);

        /***************** get result after modify ****************/
        TableFilter tableFilter = new TableFilter();
        tableFilter.setDsId(dsId);
        tableFilter.setPgId(pageInfo.getPgId());
        tableFilter.setFieldsInfo(pageInfo.getCards().get(0).getFields());
        tableFilter.setConditions(conditions);

        List<List<Object>> actualRes = getActualResultAfterModify(tableFilter);
        String expectedResFile = "module-dataset/expectedResult/planningTable/times-modify-data.txt";
        List<List<Object>> expectedRes = DataSetUtil.getDataFromFile(expectedResFile);
        Assertions.assertEquals(expectedRes, actualRes);
    }

    /**
     *  固定值批量修改测试
     */
    @Test
    public void testFixedValueBatchModify() {
        BatchUpdateDataDTO batchUpdateDataDTO = new BatchUpdateDataDTO();

        batchUpdateDataDTO.setCdId(pageInfo.getCards().get(0).getCdId());
        batchUpdateDataDTO.setType(BatchModifyTypeEnum.FIX_VALUE.getType());
        batchUpdateDataDTO.setValue("50000");
        batchUpdateDataDTO.setDsId(dsId);
        batchUpdateDataDTO.setFdId(pageInfo.getCards().get(0).getFields().get(2).getFdId());

        List<ResultCondition> conditions = new ArrayList<>();
        pageInfo.getPgFilters().stream().forEach(pgFilter -> {
            ResultCondition resCondition = new ResultCondition();
            resCondition.setFilterInfo(pgFilter);
            resCondition.setColumn(pgFilter.getColumn());
            if (pgFilter.getColumn().getColumnId().equals("STRING-1")) {
                List<Object> values = Arrays.asList("猛虎出山", "虎踞龙盘");
                resCondition.setValues(values);
            }

            if (pgFilter.getColumn().getColumnId().equals("INTEGER-1")) {
                resCondition.setValues(Arrays.asList("10002"));
            }
            conditions.add(resCondition);
        });

        batchUpdateDataDTO.setConditions(conditions);
        batchUpdateDataDTO.setTableName(dataSetDTO.getTableName());
        Authentication mockUserAuth = MockUser.generateUserAuth();
        batchUpdateDataDTO.setAuthentication(mockUserAuth);

        batchUpdatePTHandler.handle(batchUpdateDataDTO);

        /***************** get result after modify ****************/
        TableFilter tableFilter = new TableFilter();
        tableFilter.setDsId(dsId);
        tableFilter.setPgId(pageInfo.getPgId());
        tableFilter.setFieldsInfo(pageInfo.getCards().get(0).getFields());
        tableFilter.setConditions(conditions);

        List<List<Object>> actualRes = getActualResultAfterModify(tableFilter);
        String expectedResFile = "module-dataset/expectedResult/planningTable/fixed-value-modify-data.txt";
        List<List<Object>> expectedRes = DataSetUtil.getDataFromFile(expectedResFile);
        Assertions.assertEquals(expectedRes, actualRes);
    }

    /**
     * 替换批量修改测试(按被替换列比例拆)
     */
    @Test
    public void testReplaceBatchModify() {
        BatchUpdateDataDTO batchUpdateDataDTO = new BatchUpdateDataDTO();

        batchUpdateDataDTO.setCdId(pageInfo.getCards().get(0).getCdId());
        batchUpdateDataDTO.setType(BatchModifyTypeEnum.REPLACE.getType());
        batchUpdateDataDTO.setValue(pageInfo.getCards().get(0).getFields().get(3).getFdId());
        batchUpdateDataDTO.setDsId(dsId);
        batchUpdateDataDTO.setFdId(pageInfo.getCards().get(0).getFields().get(2).getFdId());

        List<ResultCondition> conditions = new ArrayList<>();
        pageInfo.getPgFilters().stream().forEach(pgFilter -> {
            ResultCondition resCondition = new ResultCondition();
            resCondition.setFilterInfo(pgFilter);
            resCondition.setColumn(pgFilter.getColumn());
            if (pgFilter.getColumn().getColumnId().equals("STRING-1")) {
                List<Object> values = Arrays.asList("猛虎出山", "虎踞龙盘");
                resCondition.setValues(values);
            }

            if (pgFilter.getColumn().getColumnId().equals("INTEGER-1")) {
                resCondition.setValues(Arrays.asList("10002"));
            }
            conditions.add(resCondition);
        });

        batchUpdateDataDTO.setConditions(conditions);
        batchUpdateDataDTO.setTableName(dataSetDTO.getTableName());
        Authentication mockUserAuth = MockUser.generateUserAuth();
        batchUpdateDataDTO.setAuthentication(mockUserAuth);

        batchUpdatePTHandler.handle(batchUpdateDataDTO);

        /***************** get result after modify ****************/
        TableFilter tableFilter = new TableFilter();
        tableFilter.setDsId(dsId);
        tableFilter.setPgId(pageInfo.getPgId());
        tableFilter.setFieldsInfo(pageInfo.getCards().get(0).getFields());
        tableFilter.setConditions(conditions);

        List<List<Object>> actualRes = getActualResultAfterModify(tableFilter);
        String expectedResFile = "module-dataset/expectedResult/planningTable/replace-modify-data.txt";
        List<List<Object>> expectedRes = DataSetUtil.getDataFromFile(expectedResFile);
        Assertions.assertEquals(expectedRes, actualRes);
    }

    /**
     * 引用填充批量修改(数据集层面直接替换成替换列)
     */
    @Test
    public void testImportPaddingBatchModify() {
        BatchUpdateDataDTO batchUpdateDataDTO = new BatchUpdateDataDTO();

        batchUpdateDataDTO.setCdId(pageInfo.getCards().get(0).getCdId());
        batchUpdateDataDTO.setType(BatchModifyTypeEnum.IMPORT_PADDING.getType());
        batchUpdateDataDTO.setValue(pageInfo.getCards().get(0).getFields().get(3).getFdId());
        batchUpdateDataDTO.setDsId(dsId);
        batchUpdateDataDTO.setFdId(pageInfo.getCards().get(0).getFields().get(2).getFdId());

        List<ResultCondition> conditions = new ArrayList<>();
        pageInfo.getPgFilters().stream().forEach(pgFilter -> {
            ResultCondition resCondition = new ResultCondition();
            resCondition.setFilterInfo(pgFilter);
            resCondition.setColumn(pgFilter.getColumn());
            if (pgFilter.getColumn().getColumnId().equals("STRING-1")) {
                List<Object> values = Arrays.asList("猛虎出山", "虎踞龙盘");
                resCondition.setValues(values);
            }

            if (pgFilter.getColumn().getColumnId().equals("INTEGER-1")) {
                resCondition.setValues(Arrays.asList("10002"));
            }
            conditions.add(resCondition);
        });

        batchUpdateDataDTO.setConditions(conditions);
        batchUpdateDataDTO.setTableName(dataSetDTO.getTableName());
        Authentication mockUserAuth = MockUser.generateUserAuth();
        batchUpdateDataDTO.setAuthentication(mockUserAuth);

        batchUpdatePTHandler.handle(batchUpdateDataDTO);

        /***************** get result after modify ****************/
        TableFilter tableFilter = new TableFilter();
        tableFilter.setDsId(dsId);
        tableFilter.setPgId(pageInfo.getPgId());
        tableFilter.setFieldsInfo(pageInfo.getCards().get(0).getFields());
        tableFilter.setConditions(conditions);

        List<List<Object>> actualRes = getActualResultAfterModify(tableFilter);
        String expectedResFile = "module-dataset/expectedResult/planningTable/import-padding-modify-data.txt";
        List<List<Object>> expectedRes = DataSetUtil.getDataFromFile(expectedResFile);
        Assertions.assertEquals(expectedRes, actualRes);
    }



    public List<List<Object>> getActualResultAfterModify(TableFilter tableFilter) {
        TableResult actualTableRes = cardService.getPreviewResultDataList(tableFilter, 1L, Long.MAX_VALUE);
        List<List<Object>> actualRes = new ArrayList<>();
        actualTableRes.getResultData().stream().forEach(row ->
                actualRes.add(row.stream().map(resultData -> resultData.getV()).collect(Collectors.toList()))
        );
        return actualRes;
    }
}
