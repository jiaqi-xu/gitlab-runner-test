package com.guandata.atlas.test.planningTableServiceTest;

import com.guandata.atlas.test.BaseTest;
import com.guandata.atlas.test.testHelper.constant.PlanningTableConstant;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;

@Slf4j
public class PlanningTableBaseTest extends BaseTest {
    protected int dsId;
    protected final static String dsDataFileName = "module-dataset/dataset-data.txt";
    protected static final String dsUpdateDataFileName = "module-dataset/dataset-update-data.txt";
    protected final static String dsDefineFileName = "module-dataset/dataset-definition.txt";
    protected final static String ptFileName = "module-dataset/planningTable.txt";

    @BeforeEach
    public void mockData() {
        log.info("----正在初始化数据集----");
        dsId = datasetTestService.createDataSet(dsDefineFileName, dsDataFileName, dsUpdateDataFileName);
        planningTableTestService.createPlanningTable(dsId, ptFileName);
        log.info("----初始化数据集完毕，dsId:{}----", dsId);
    }

    @AfterEach
    public void clearData() {
        log.info("----正在清理mock数据----");
        planningTableTestService.removePlanningTable(PlanningTableConstant.PG_ID_LIST, PlanningTableConstant.CD_ID_LIST);
        datasetTestService.removeDataSet(dsId);
        log.info("----清理mock数据完毕----");
    }
}
