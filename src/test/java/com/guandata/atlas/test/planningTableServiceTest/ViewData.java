package com.guandata.atlas.test.planningTableServiceTest;

import com.guandata.atlas.dto.ResultCondition;
import com.guandata.atlas.dto.TableFilter;
import com.guandata.atlas.dto.TableResult;
import com.guandata.atlas.pojo.PageInfo;
import com.guandata.atlas.service.CardService;
import com.guandata.atlas.test.testHelper.utils.DataSetUtil;
import com.guandata.atlas.test.testHelper.utils.JsonFileParser;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
public class ViewData extends PlanningTableBaseTest {
    @Resource
    private CardService cardService;

    private static final PageInfo pageInfo;
    static {
        pageInfo = new JsonFileParser<>(PageInfo.class).objectFromFile(ptFileName);
    }

    /**
     * Card数据预览
     */
    @Test
    public void testCardDataPreview() {
        // Parameters Preparation
        TableFilter tableFilter = new TableFilter();
        tableFilter.setDsId(dsId);
        tableFilter.setPgId(pageInfo.getPgId());
        tableFilter.setFieldsInfo(pageInfo.getCards().get(0).getFields());

        List<ResultCondition> conditions = new ArrayList<>();
        pageInfo.getPgFilters().stream().forEach(pgFilter -> {
            ResultCondition resCondition = new ResultCondition();
            resCondition.setFilterInfo(pgFilter);
            resCondition.setColumn(pgFilter.getColumn());
            if (pgFilter.getColumn().getColumnId().equals("STRING-1")) {
                List<Object> values = Arrays.asList("猛虎出山", "虎踞龙盘");
                resCondition.setValues(values);
            }

            if (pgFilter.getColumn().getColumnId().equals("INTEGER-1")) {
                resCondition.setValues(Arrays.asList("10002"));
            }
            conditions.add(resCondition);
        });

        tableFilter.setConditions(conditions);

        TableResult actualTableRes = cardService.getPreviewResultDataList(tableFilter, 1L, Long.MAX_VALUE);
        List<List<Object>> actualRes = new ArrayList<>();
        actualTableRes.getResultData().stream().forEach(row ->
                actualRes.add(row.stream().map(resultData -> resultData.getV()).collect(Collectors.toList()))
        );
        String expectedResFile = "module-dataset/expectedResult/planningTable/data-preview.txt";
        List<List<Object>> expectedRes = DataSetUtil.getDataFromFile(expectedResFile);

        Assertions.assertEquals(expectedRes, actualRes, "数据预览: 实际值与预期值不一致");
    }

}
