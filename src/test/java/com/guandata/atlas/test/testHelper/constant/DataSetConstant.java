package com.guandata.atlas.test.testHelper.constant;

public class DataSetConstant {
    @Deprecated
    public static final String DATA_SET_TABLE_FILE_NAME="dataset-table.txt";
    @Deprecated
    public static final String DATA_SET_DATA_FILE_NAME= "module-dataset/dataset-data.txt";

    public static final String DATA_TYPE_STRING="STRING";
    public static final String DATA_TYPE_INTEGER="INTEGER";
    public static final String DATA_TYPE_DOUBLE="DOUBLE";
    public static final String DATA_TYPE_LONG="LONG";
    public static final String DATA_TYPE_FLOAT="FLOAT";
    public static final String DATA_TYPE_DATE="DATE";
    public static final String DATA_TYPE_DATETIME="DATETIME";
    public static final String DATA_TYPE_BOOLEAN="BOOLEAN";

    public static final String ACCESS_TOKEN="test-access-token";

    public static final Integer ROW_COUNT=150;

}
