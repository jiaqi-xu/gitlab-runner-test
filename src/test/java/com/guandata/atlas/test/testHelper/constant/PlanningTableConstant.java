package com.guandata.atlas.test.testHelper.constant;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class PlanningTableConstant {
    @Deprecated
    public static final String PLANNING_TABLE_FILE_NAME= "module-dataset/planningTable.txt";

    public static final List<String> PG_ID_LIST= Collections.singletonList("PG_TEST");

    public static final List<String> CD_ID_LIST = Arrays.asList("cd1", "cd2");
}
