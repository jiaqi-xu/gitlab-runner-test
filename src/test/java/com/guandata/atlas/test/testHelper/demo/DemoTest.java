package com.guandata.atlas.test.testHelper.demo;

import com.guandata.atlas.dto.DataSetDTO;
import com.guandata.atlas.test.testHelper.utils.DataGeneratorUtil;
import com.guandata.atlas.test.testHelper.utils.DataSetUtil;
import com.guandata.atlas.test.testHelper.utils.JsonFileParser;
import com.guandata.atlas.test.testHelper.utils.dataGenerator.DataGeneratorFactory;
import com.guandata.atlas.test.testHelper.utils.dataGenerator.generator.AbstractDataGenerator;
import com.guandata.atlas.test.testHelper.utils.dataGenerator.generator.IntegerDataGenerator;
import com.guandata.atlas.test.testHelper.utils.dataGenerator.generator.StringDataGenerator;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Disabled
@Slf4j
public class DemoTest {
    String[] stringsToGenerate = {"虎踞龙盘", "猛虎出山", "如虎添翼", "初生牛犊不怕虎"};
    Integer[] integersToGenerate = {10001, 10002, 10003, 10004};

    @Test
    public void test() {
        String datasetDefinitionFileName = "module-dataset/dataset-definition.txt";
        DataSetDTO dataSetDTO = new JsonFileParser<>(DataSetDTO.class).objectFromFile(datasetDefinitionFileName);
        Map<String, AbstractDataGenerator<?>> generatorMap = DataGeneratorFactory.getGeneratorMap();
        AtomicInteger i = new AtomicInteger();
        List<AbstractDataGenerator<?>> dataGenerators = dataSetDTO.getDefinition().stream().map(dsColumn -> {
            AbstractDataGenerator<?> dataGenerator;
            if (i.get() == 0) {
                dataGenerator = new StringDataGenerator() {
                    @Override
                    public String generate() {
                        return stringsToGenerate[(int) (Math.random() * (3 + 1))];
                    }
                };
            } else if (i.get() == 2) {
                dataGenerator = new IntegerDataGenerator() {
                    @Override
                    public Integer generate() {
                        return integersToGenerate[(int) (Math.random() * (3 + 1))];
                    }
                };
            } else {
                dataGenerator = generatorMap.get(dsColumn.getType());
            }
            i.getAndIncrement();
            return dataGenerator;
        }).collect(Collectors.toList());
        DataGeneratorUtil.generateRandomRowDataAndDump(140, String.class, datasetDefinitionFileName,
                dataGenerators, "module-dataset", "限制了版本主键的值的范围的随机数据.txt");
    }

}
