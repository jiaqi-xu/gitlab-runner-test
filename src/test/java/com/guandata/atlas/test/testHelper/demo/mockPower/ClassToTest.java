package com.guandata.atlas.test.testHelper.demo.mockPower;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ClassToTest {
    @Autowired
    private ServiceInClassToTest serviceInClassToTest;


    public void testSay(){
        serviceInClassToTest.say();
    }
}
