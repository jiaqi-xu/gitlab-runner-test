package com.guandata.atlas.test.testHelper.demo.mockPower;

import com.guandata.atlas.AtlasServiceWalmartApplication;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.powermock.reflect.Whitebox;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@Disabled
@SpringBootTest(classes = AtlasServiceWalmartApplication.class)
public class TestMockPower {
    @Autowired
    private ClassToTest classToTest;

    @Test
    public void testMockPower(){
        Whitebox.setInternalState(classToTest, "serviceInClassToTest", new MockServiceInClassToTest());
        classToTest.testSay();
    }

}
