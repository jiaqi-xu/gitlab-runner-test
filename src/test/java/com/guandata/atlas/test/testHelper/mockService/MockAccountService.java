package com.guandata.atlas.test.testHelper.mockService;

import com.guandata.atlas.dao.AccountEntity;
import com.guandata.atlas.exception.AtlasException;
import com.guandata.atlas.service.AccountService;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

public class MockAccountService extends AccountService {
    @Override
    public List<AccountEntity> list(String cnType) {
        return new ArrayList<>();
    }

    @Override
    public AccountEntity get(int id) {
        AccountEntity accountEntity = new AccountEntity();
        accountEntity.setAuthConfig("{\"address\": \"https://test.guandata.com\", \"domainId\": \"test\", \"username\": \"test@guandata.com\", \"privateKey\": \"MIICdgIBADAN\"}");
        return accountEntity;
    }

    @Override
    public int insert(AccountEntity account) {
        return 1;
    }

    @Override
    public Boolean isNameExisted(AccountEntity account) {
        return false;
    }

    @Override
    public int updateById(AccountEntity account) {
        return 1;
    }

    @Override
    public int softDelete(int id) throws AtlasException {
        return 1;
    }

    @Override
    public boolean testConnectivity(AccountEntity account) {
        return true;
    }
}
