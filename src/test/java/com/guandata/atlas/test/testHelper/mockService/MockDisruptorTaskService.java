package com.guandata.atlas.test.testHelper.mockService;

import com.guandata.atlas.service.disruptor.DisruptorTaskService;
import com.guandata.atlas.service.disruptor.EventTypeEnum;
import org.springframework.stereotype.Component;

public class MockDisruptorTaskService extends DisruptorTaskService {
    @Override
    public void init() {
        System.out.println("overrideInit");
    }

    @Override
    public <T> void sendNotify(EventTypeEnum eventType, T message) {
        System.out.println("overrideSendNotify");
    }
}
