package com.guandata.atlas.test.testHelper.mockService;

import com.guandata.atlas.dao.TaskDAO;
import com.guandata.atlas.service.TaskService;

public class MockTaskService extends TaskService {
    @Override
    public int updateTask(TaskDAO taskDAO) {
        return 1;
    }
}
