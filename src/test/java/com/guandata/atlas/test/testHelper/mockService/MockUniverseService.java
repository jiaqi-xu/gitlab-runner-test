package com.guandata.atlas.test.testHelper.mockService;

import com.guandata.atlas.dto.BaseDSDTO;
import com.guandata.atlas.dto.DSDataPreviewDTO;
import com.guandata.atlas.dto.DataSetDTO;
import com.guandata.atlas.dto.SqlParamDTO;
import com.guandata.atlas.dto.universe.UniverseLoginInDTO;
import com.guandata.atlas.dto.universe.UniverseProjectDTO;
import com.guandata.atlas.service.UniverseService;
import com.guandata.atlas.test.testHelper.constant.DataSetConstant;
import com.guandata.atlas.test.testHelper.utils.DataSetUtil;
import com.guandata.atlas.test.testHelper.utils.JsonFileParser;
import org.springframework.stereotype.Component;

import java.util.List;

public class MockUniverseService extends UniverseService {
    private static final String dataSetUniverseDataFileName = "module-dataset/dataset-universe-data.txt";
    private static final String dataSetDefinitionFileName = "module-dataset/dataset-definition.txt";

    @Override
    public String getLoginToken(UniverseLoginInDTO universeLoginInDTO) {
        return null;
    }

    @Override
    public List<UniverseProjectDTO> getProjects(UniverseLoginInDTO universeLoginInDTO) {
        return null;
    }

    @Override
    public List<BaseDSDTO> getDatasets(UniverseLoginInDTO universeLoginInDTO, Integer projectId) {
        return null;
    }

    @Override
    public BaseDSDTO getDatasetInfo(UniverseLoginInDTO universeLoginInDTO, Integer projectId, Integer datasetId) {
        return null;
    }

    @Override
    public DSDataPreviewDTO exportDataSetData(UniverseLoginInDTO universeLoginInDTO, Integer datasetId, SqlParamDTO sqlParam) {
        return exportDataSetDataByPagination(null,null, null,null,0, DataSetConstant.ROW_COUNT);
    }

    @Override
    public String getDataSetDataSnapShotPath(UniverseLoginInDTO universeLoginInDTO, Integer projectId, Integer datasetId, SqlParamDTO sqlParam) {
        return "";
    }

    @Override
    public DSDataPreviewDTO exportDataSetDataByPagination(UniverseLoginInDTO universeLoginInDTO, String filePath, Integer projectId, Integer datasetId, Integer offset, Integer limit) {
        DataSetDTO dataSetDTO = new JsonFileParser<>(DataSetDTO.class).objectFromFile(dataSetDefinitionFileName);
        List<List<Object>> dataFromFile = DataSetUtil.getDataFromFile(dataSetUniverseDataFileName);
        DSDataPreviewDTO dsDataPreviewDTO = new DSDataPreviewDTO();

        boolean hasMore = offset + limit < dataFromFile.size();
        dsDataPreviewDTO.setHasMore(hasMore);
        dsDataPreviewDTO.setPreview(dataFromFile.subList(offset, hasMore ? offset + limit : dataFromFile.size()));
        dsDataPreviewDTO.setColumns(dataSetDTO.getDefinition());
        return dsDataPreviewDTO;
    }

    @Override
    public String generateSql(SqlParamDTO params) {
        return super.generateSql(params);
    }

    @Override
    public List<Object> processBooleanColumn(List<Integer> indexes, List<Object> preview) {
        return super.processBooleanColumn(indexes, preview);
    }
}
