package com.guandata.atlas.test.testHelper.mockService;

import org.keycloak.KeycloakPrincipal;
import org.keycloak.KeycloakSecurityContext;
import org.keycloak.adapters.RefreshableKeycloakSecurityContext;
import org.keycloak.adapters.springsecurity.account.SimpleKeycloakAccount;
import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.keycloak.representations.AccessToken;

import java.security.Principal;
import java.util.Arrays;
import java.util.HashSet;

public class MockUser {
    public static KeycloakAuthenticationToken generateUserAuth() {
        AccessToken token = new AccessToken();
        token.setSubject("test-user-id");
        token.setName("mock-user");

        KeycloakSecurityContext keycloakSecurityContext = new KeycloakSecurityContext(null, token, null, null);
        KeycloakPrincipal<RefreshableKeycloakSecurityContext> keycloakPrincipal = new KeycloakPrincipal("mock-principal", keycloakSecurityContext);

        SimpleKeycloakAccount simpleKeycloakAccount = new SimpleKeycloakAccount(
                keycloakPrincipal, new HashSet<>(Arrays.asList("Admin")), null) {

            @Override
            public Principal getPrincipal() {
                AccessToken token = new AccessToken();
                token.setSubject("test-user-id");
                token.setName("mock-user");
                KeycloakSecurityContext keycloakSecurityContext = new KeycloakSecurityContext(null, token, null, null);
                return new KeycloakPrincipal("test-mock-user", keycloakSecurityContext);
            }
        };

        KeycloakAuthenticationToken authentication = new KeycloakAuthenticationToken(
                simpleKeycloakAccount, false) {
        };

        return authentication;
    }
}
