package com.guandata.atlas.test.testHelper.service;

import java.util.List;

public interface DatasetTestService {
    int createDataSet(String dataSetDefinitionFileName, String dataSetDataFileName, String dataSetUpdateDataFileName);

    int removeDataSet(int dsId);

    int removeDataSet(String dsName);

    List<Integer> removeDataSet(int dsId, String dsName);

    List<List<Object>> getDataFromFile(String fileName);

    void importData(int dsId, String dataSetDataFileName);

    void importData(int dsId, String dataFileName, String tableName);

    void importData(int dsId, String dataFileName, String tableName, boolean withId);

    void importOriginDataToUpdateTable(int dsId);

}
