package com.guandata.atlas.test.testHelper.service;

import java.util.List;

public interface PlanningTableTestService {

    void createPlanningTable(int dsId, String planningTableFileName);

    void removePlanningTable(List<String> pgIdList, List<String> cdIdList);

    @Deprecated
    void removePlanningTable(int dsId);


}
