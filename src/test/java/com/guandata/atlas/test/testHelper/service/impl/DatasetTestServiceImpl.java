package com.guandata.atlas.test.testHelper.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.guandata.atlas.dao.DataSetDAO;
import com.guandata.atlas.dao.TokenDAO;
import com.guandata.atlas.dto.DataSetDTO;
import com.guandata.atlas.enums.TaskTypeEnum;
import com.guandata.atlas.mapper.DataSetMapper;
import com.guandata.atlas.mapper.TokenMapper;
import com.guandata.atlas.service.DataSetService;
import com.guandata.atlas.service.UniverseService;
import com.guandata.atlas.service.account.repository.TokenRepository;
import com.guandata.atlas.test.testHelper.constant.Constant;
import com.guandata.atlas.test.testHelper.constant.DataSetConstant;
import com.guandata.atlas.test.testHelper.service.DatasetTestService;
import com.guandata.atlas.test.testHelper.utils.DataSetUtil;
import com.guandata.atlas.test.testHelper.utils.JsonFileParser;
import lombok.extern.slf4j.Slf4j;
import org.jooq.DSLContext;
import org.jooq.Field;
import org.jooq.InsertValuesStepN;
import org.jooq.Record;
import org.jooq.impl.DSL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;


@Service
@Slf4j
public class DatasetTestServiceImpl implements DatasetTestService {
    @Autowired
    private DataSetMapper dataSetMapper;

    @Autowired
    private TokenMapper tokenMapper;

    @Autowired
    private DSLContext dslContext;

    @Autowired
    private DataSetService dataSetService;

    @Autowired
    private UniverseService universeService;

    public int createDataSet(String dataSetDefinitionFileName, String dataSetDataFileName, String dataSetUpdateDataFileName) {
        DataSetDTO dataSetDTO = new JsonFileParser<>(DataSetDTO.class)
                .objectFromFile(dataSetDefinitionFileName);
        // 清理可能残留的数据
        removeDataSet(dataSetDTO.getId(), dataSetDTO.getName());
        // 创建数据集
        return doCreateDataSet(dataSetDTO, dataSetDataFileName, dataSetUpdateDataFileName);
    }

    private int doCreateDataSet(DataSetDTO dataSetDTO, String dataSetDataFileName, String dataSetUpdateDataFileName) {
        // 引入数据集
        int dsId = importDataSet(dataSetDTO);
        // 向数据集导入数据
        importData(dsId, dataSetDataFileName);
        importData(dsId, dataSetUpdateDataFileName, dataSetDTO.getTableName() + "_update");
        return dsId;
    }

    public int removeDataSet(int dsId) {
        // 删除数据集相关的表
        DataSetDAO dataSetDAO = dataSetMapper.getById(dsId);
        return doRemoveDataSet(dataSetDAO);
    }

    public int removeDataSet(String dsName) {
        DataSetDAO dataSetDAO = dataSetMapper.getByName(dsName);
        return doRemoveDataSet(dataSetDAO);
    }

    public List<Integer> removeDataSet(int dsId, String dsName) {
        return Arrays.asList(removeDataSet(dsId), removeDataSet(dsName));
    }

    private int doRemoveDataSet(DataSetDAO dataSetDAO) {
        if (dataSetDAO != null) {
            String tableName = dataSetDAO.getTableName();
            dslContext.dropTableIfExists(tableName).execute();
            dslContext.dropTableIfExists(tableName + "_update").execute();
            dslContext.dropTableIfExists(tableName + "_modify_history").execute();
            dslContext.dropTableIfExists(tableName + "_modify_history_detail").execute();
            // 删除数据集
            dslContext.deleteFrom(DSL.table("dataset")).where(DSL.field("id").eq(dataSetDAO.getId())).execute();
            // 删除token
            tokenMapper.delete(new QueryWrapper<TokenDAO>().lambda()
                    .eq(TokenDAO::getAccessToken, DataSetConstant.ACCESS_TOKEN)
                    .or(q -> q.eq(TokenDAO::getObjectId, dataSetDAO.getTableName())));
            return dataSetDAO.getId();
        }
        return -1;
    }

    /**
     * 引入数据集
     *
     * @param dataSetDTO 数据集数据传输对象
     */
    private int importDataSet(DataSetDTO dataSetDTO) {
        // 添加数据集
        int dsId = saveWithDsId(dataSetDTO.toDataSet(), Constant.USER_ID);
        // 创建数据集相关表
        dataSetService.createAssociatedTable(dataSetDTO.getTableName(), dataSetDTO.getDefinition());
        // 数据集记录
        createAssociatedRecord(dataSetDTO);
        return dsId;
    }

    /**
     * 插入数据集记录(指定数据集id)
     *
     * @param dataSet 数据集对象
     * @param userId  创建数据集用户
     */
    private int saveWithDsId(DataSetDAO dataSet, String userId) {
        dslContext.insertInto(DSL.table("dataset")).columns(DSL.field("id"), DSL.field("name"), DSL.field("source_type"), DSL.field("source_ds"), DSL.field("table_name"), DSL.field("definition"), DSL.field("ext"), DSL.field("editor_id"), DSL.field("creator_id"))
                .values(dataSet.getId(), dataSet.getName(), dataSet.getSourceType(), dataSet.getSourceDS(), dataSet.getTableName(), dataSet.getDefinition(), dataSet.getExt(), userId, userId).execute();
        return dataSet.getId();
    }

    /**
     * 新建/插入数据集相关记录至相关表
     */
    private void createAssociatedRecord(DataSetDTO dataSetDTO) {
        TokenDAO tokenDAO = new TokenDAO(
                TaskTypeEnum.SYNC_DATASET_DATA.getType(), dataSetDTO.getTableName(), DataSetConstant.ACCESS_TOKEN);
        TokenRepository.saveToken(tokenDAO);
    }

    /**
     * 向数据集导入数据
     *
     * @param dsId 数据集id
     */
    public void importData(int dsId, String dataSetDataFileName) {
        importData(dsId, dataSetDataFileName, null, false);
    }

    public void importData(int dsId, String dataFileName, String tableName) {
        importData(dsId, dataFileName, tableName, true);
    }

    public void importData(int dsId, String dataFileName, String tableName, boolean withId) {
        List<List<Object>> dataFromFile = getDataFromFile(dataFileName);
        // 获取数据集信息
        DataSetDTO dataSetDTO = dataSetMapper.getById(dsId).toDataSetDTO();
        List<Field<Object>> columns = dataSetDTO.getDefinition().stream()
                .map(dsColumn -> DSL.field("`" + dsColumn.getColumnId() + "`")).collect(Collectors.toList());
        // 数据加工
        resolveData(dataFromFile, dataSetDTO);
        // 确定tableName
        tableName = StringUtils.isEmpty(tableName) ? dataSetDTO.getTableName() : tableName;
        // 导入数据
        log.info("开始导入数据到[{}]，共[{}]条数据", tableName, dataFromFile.size());
        if (withId) {
            columns.add(DSL.field(DSL.name("id")));
            // id从1开始自增
            for (int i = 0; i < dataFromFile.size(); i++) {
                List<Object> rowValues = dataFromFile.get(i);
                rowValues.add(i + 1);
            }
        }
        InsertValuesStepN<Record> insertValuesStepN =
                dslContext.insertInto(DSL.table(DSL.name(tableName))).columns(columns);
        dataFromFile.forEach(insertValuesStepN::values);
        insertValuesStepN.execute();
        log.info("导入完毕");
    }

    public void importOriginDataToUpdateTable(int dsId) {
        // 获取数据集信息
        DataSetDTO dataSetDTO = dataSetMapper.getById(dsId).toDataSetDTO();
        List<Field<Object>> columnsWithId = dataSetDTO.getDefinition().stream()
                .map(dsColumn -> DSL.field("`" + dsColumn.getColumnId() + "`")).collect(Collectors.toList());
        columnsWithId.add(DSL.field("id"));
        // 向数据集导入数据
        InsertValuesStepN<Record> insertValuesStepN = dslContext
                .insertInto(DSL.table(DSL.name(dataSetDTO.getTableName() + "_update")))
                .columns(columnsWithId);
        insertValuesStepN.select(dslContext.select(columnsWithId)
                .from(DSL.table(DSL.name(dataSetDTO.getTableName())))
                .orderBy(DSL.field("id").asc()));
        insertValuesStepN.execute();
    }

    public List<List<Object>> getDataFromFile(String fileName) {
        return DataSetUtil.getDataFromFile(fileName);
    }

    private void resolveData(List<List<Object>> dataFromFile, DataSetDTO dataSetDTO) {
        // 处理boolean类型的数据
        List<Integer> booleanIndexes = dataSetDTO.getDefinition().stream().filter(
                f -> f.getType().equals("BOOLEAN")).map(c -> (c.getSeqNo() - 1)).collect(Collectors.toList());
        dataFromFile.forEach(rowData -> universeService.processBooleanColumn(booleanIndexes, rowData));
    }

}
