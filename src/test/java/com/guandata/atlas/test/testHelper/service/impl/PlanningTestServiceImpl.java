package com.guandata.atlas.test.testHelper.service.impl;

import com.guandata.atlas.dao.BaseFilter;
import com.guandata.atlas.dao.CardInfoDO;
import com.guandata.atlas.dao.FieldInfoDO;
import com.guandata.atlas.dao.PageInfoDO;
import com.guandata.atlas.mapper.CardMapper;
import com.guandata.atlas.mapper.FieldMapper;
import com.guandata.atlas.mapper.FilterMapper;
import com.guandata.atlas.mapper.PageMapper;
import com.guandata.atlas.pojo.CardInfo;
import com.guandata.atlas.pojo.FieldInfo;
import com.guandata.atlas.pojo.FilterInfo;
import com.guandata.atlas.pojo.PageInfo;
import com.guandata.atlas.service.planningtable.enums.PageStatusEnum;
import com.guandata.atlas.service.planningtable.model.card.entity.Card;
import com.guandata.atlas.service.planningtable.model.field.entity.BaseField;
import com.guandata.atlas.service.planningtable.model.filter.entity.Filter;
import com.guandata.atlas.service.planningtable.model.page.entity.Page;
import com.guandata.atlas.service.planningtable.repository.FilterRepository;
import com.guandata.atlas.test.testHelper.constant.PlanningTableConstant;
import com.guandata.atlas.test.testHelper.service.PlanningTableTestService;
import com.guandata.atlas.test.testHelper.utils.JsonFileParser;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class PlanningTestServiceImpl implements PlanningTableTestService {
    @Autowired
    private PageMapper pageMapper;

    @Autowired
    private CardMapper cardMapper;

    @Autowired
    private FilterMapper filterMapper;

    @Autowired
    private FieldMapper fieldMapper;

    /**
     * @param dsId 数据集id
     */
    public void createPlanningTable(int dsId, String planningTableFileName) {
        PageInfo pageInfo = new JsonFileParser<>(PageInfo.class)
                .objectFromFile(planningTableFileName);
        pageInfo.setDsId(dsId);
        // 清理可能残留的数据
        removePlanningTable(PlanningTableConstant.PG_ID_LIST, PlanningTableConstant.CD_ID_LIST);

        // 创建page
        createPage(pageInfo);
        // 创建page filter
        createPageFilter(pageInfo);
        // 创建card
        createCard(pageInfo);
        // 创建card filter
        createCardFilter(pageInfo);
        // 创建card field
        createField(pageInfo.getCards());
        // 创建initialFieldList
        createInitialFields(pageInfo);
    }

    public void removePlanningTable(List<String> pgIdList, List<String> cdIdList) {
        doRemovePlanningTable(pgIdList, cdIdList);
    }

    public void removePlanningTable(int dsId) {
        // 查询数据集下有哪些page
        List<PageInfoDO> pagesByDsId = pageMapper.getPageByDsId(dsId);
        if (pagesByDsId == null || pagesByDsId.isEmpty()) {
            return;
        }
        List<String> pgIdList = pagesByDsId.stream().map(PageInfoDO::getPgId).collect(Collectors.toList());
        // 查询数据集下有哪些card
        List<CardInfoDO> cardsByPages = cardMapper.getCardByPages(pgIdList);
        List<String> cdIdList = new ArrayList<>();
        if (cardsByPages != null && !cardsByPages.isEmpty()) {
            cdIdList.addAll(cardsByPages.stream().map(CardInfoDO::getCdId).collect(Collectors.toList()));
        }
        // 删除planningTable
        doRemovePlanningTable(pgIdList, cdIdList);
    }

    private void doRemovePlanningTable(List<String> pgIdList, List<String> cdIdList) {
        // 删除card中的field
        if (cdIdList != null && !cdIdList.isEmpty()) {
            removeCardFields(cdIdList);
        }
        if (pgIdList != null && !pgIdList.isEmpty()) {
            // 删除card和page的filter
            removeFilters(pgIdList);
            // 删除initialFieldList
            removeInitialFields(pgIdList);
            // 删除card
            removeCards(pgIdList);
            // 删除page
            removePages(pgIdList);
        }
    }

    private void removePages(List<String> pgIdList) {
        pgIdList.forEach(pgId -> pageMapper.hardDeletePage(pgId));
    }

    private void removeCards(List<String> pgIdList) {
        pgIdList.forEach(pgId -> cardMapper.hardDeleteCardsByPgId(pgId));
    }

    private void removeInitialFields(List<String> pgIdList) {
        pgIdList.forEach(pgId -> fieldMapper.deleteInitialFieldListByPgId(pgId));
    }

    private void removeFilters(List<String> pgIdList) {
        pgIdList.forEach(pgId -> filterMapper.deleteFiltersByPgId(pgId));
    }

    private void removeCardFields(List<String> cdIdList) {
        cdIdList.forEach(cdId -> fieldMapper.deleteFieldsByCdId(cdId));
    }

    private void createPage(PageInfo pageInfo) {
        pageInfo.setStatus(PageStatusEnum.NORMAL.getStatus());
        pageMapper.insertPage(new PageInfoDO(new Page(pageInfo)));
    }

    private void createCard(PageInfo pageInfo) {
        List<CardInfoDO> cardInfoDOS = pageInfo.getCards().stream().map(cardInfo -> {
            /*CardInfoDO cardInfoDO = new CardInfoDO();
            BeanUtils.copyProperties(cardInfo, cardInfoDO);
            return cardInfoDO;*/
            return new CardInfoDO(new Card(cardInfo, new Page(pageInfo)));
        }).collect(Collectors.toList());
        cardMapper.insertCards(cardInfoDOS);
    }

    private void createPageFilter(PageInfo pageInfo) {
        createFilter(pageInfo.getPgFilters(), pageInfo, null);
    }

    private void createCardFilter(PageInfo pageInfo) {
        pageInfo.getCards().forEach(cardInfo -> {
            List<FilterInfo> filterInfos = cardInfo.getCdFilters();
            createFilter(filterInfos, pageInfo, cardInfo);
        });
    }

    private void createFilter(List<FilterInfo> filterInfos, PageInfo pageInfo, CardInfo cardInfo) {
        /*List<BaseFilter> baseFilters = filterInfos.stream().map(filterInfo -> {
            BaseFilter baseFilter = new BaseFilter();
            BeanUtils.copyProperties(filterInfo, baseFilter);
            baseFilter.setType(filterInfo.getSelectorType());
            return baseFilter;
        }).collect(Collectors.toList());*/
        List<Filter> filters = FilterRepository.transFilters(filterInfos,
                cardInfo == null ? null : new Card(cardInfo, new Page(pageInfo)));
        List<BaseFilter> baseFilters = filters.stream().map(BaseFilter::new).collect(Collectors.toList());
        filterMapper.insertFilters(baseFilters);
    }

    private void createField(List<CardInfo> cardInfos) {
        ArrayList<FieldInfo> fieldInfos = new ArrayList<>();
        cardInfos.forEach(cardInfo -> fieldInfos.addAll(cardInfo.getFields()));
        List<FieldInfoDO> fieldInfoDOS = fieldInfos.stream().map(fieldInfo -> {
            FieldInfoDO fieldInfoDO = new FieldInfoDO();
            BeanUtils.copyProperties(fieldInfo, fieldInfoDO);
            return fieldInfoDO;
        }).collect(Collectors.toList());
        fieldMapper.insertFields(fieldInfoDOS);
    }

    private void createInitialFields(PageInfo pageInfo) {
        List<FieldInfoDO> fieldInfoDOS = pageInfo.getInitialFieldList().stream().map(fieldInfo -> {
            FieldInfoDO fieldInfoDO = new FieldInfoDO();
            BeanUtils.copyProperties(fieldInfo, fieldInfoDO);
            return fieldInfoDO;
        }).collect(Collectors.toList());
        fieldMapper.insertInitialFields(fieldInfoDOS);
    }
}
