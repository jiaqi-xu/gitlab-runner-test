package com.guandata.atlas.test.testHelper.utils;

import com.guandata.atlas.dto.DataSetDTO;
import com.guandata.atlas.dto.dataset.DSColumn;
import com.guandata.atlas.test.testHelper.constant.Constant;
import com.guandata.atlas.test.testHelper.utils.dataGenerator.DataGeneratorFactory;
import com.guandata.atlas.test.testHelper.utils.dataGenerator.generator.AbstractDataGenerator;
import lombok.extern.slf4j.Slf4j;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
public class DataGeneratorUtil {

    /**
     * @param rowCount                  生成数据行数
     * @param clazz                     生成数据类型（Object/String）
     * @param dataSetDefinitionFileName 数据集定义文件名
     */
    public static <T> List<List<T>> generateRandomRowData(int rowCount, Class<T> clazz, String dataSetDefinitionFileName,
                                                          List<AbstractDataGenerator<?>> dataGenerators) {
        // 获取数据集定义
        JsonFileParser<DataSetDTO> jsonFileParser = new JsonFileParser<>(DataSetDTO.class);
        DataSetDTO dataSetDTO = jsonFileParser.objectFromFile(dataSetDefinitionFileName);
        List<String> definitionTypes = dataSetDTO.getDefinition().stream()
                .map(DSColumn::getType).collect(Collectors.toList());
        // 根据 定义中每一列的类型 获取 对应的随机数据生成器
        if (dataGenerators == null) {
            dataGenerators = definitionTypes.stream().map(DataGeneratorFactory::getGenerator).collect(Collectors.toList());
        } else if (dataGenerators.isEmpty() || dataGenerators.size() != definitionTypes.size()) {
            throw new InvalidParameterException("传入的dataGenerators无法解析");
        }

        List<List<T>> ret = new ArrayList<>();
        while ((rowCount--) > 0) {
            List<T> row = new ArrayList<>();
            for (int i = 0; i < definitionTypes.size(); i++) {
                AbstractDataGenerator<?> dataGenerator = dataGenerators.get(i);
                if (String.class == clazz) {
                    row.add(clazz.cast(dataGenerator.generateString()));
                } else {
                    row.add(clazz.cast(dataGenerator.generate()));
                }
            }
            ret.add(row);
        }
        return ret;
    }

    /**
     * @param rowCount                  生成数据行数
     * @param clazz                     生成的数据类型（Object/String）
     * @param dataSetDefinitionFileName 数据集定义文件名
     * @param dumpDirName               导出文件所在目录名
     * @param dumpFileName              导出的文件名
     */
    public static <T> List<List<T>> generateRandomRowDataAndDump(int rowCount, Class<T> clazz, String dataSetDefinitionFileName,
                                                                 List<AbstractDataGenerator<?>> dataGenerators,
                                                                 String dumpDirName, String dumpFileName) {
        List<List<T>> lists = generateRandomRowData(rowCount, clazz, dataSetDefinitionFileName, dataGenerators);
        File dir = new File(dumpDirName);
        if (!dir.isDirectory()) {
            dir.mkdirs();
        }
        try {
            File file = new File(dir, dumpFileName);
            if (!file.isFile()) {
                file.createNewFile();
            }
        } catch (IOException e) {
            log.error("创建数据集数据备份文件失败，fileName：{}", dumpFileName);
        }
        try (
                FileOutputStream fileOutputStream = new FileOutputStream(dumpDirName + "/" + dumpFileName);
                OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream, StandardCharsets.UTF_8);
                BufferedWriter bufferedWriter = new BufferedWriter(outputStreamWriter);
        ) {
            for (List<T> list : lists) {
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < list.size(); i++) {
                    sb.append(list.get(i).toString());
                    if (i != list.size() - 1) {
                        sb.append(", ");
                    }
                }
                bufferedWriter.write(sb.toString());
                bufferedWriter.newLine();
            }
        } catch (Exception e) {
            log.error("数据集数据写入文件失败", e);
        }
        return lists;
    }

}
