package com.guandata.atlas.test.testHelper.utils;

import lombok.extern.slf4j.Slf4j;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
public class DataSetUtil {
    public static List<List<Object>> getDataFromFile(String fileName) {
        List<List<Object>> dataList = new ArrayList<>();
        try (
                InputStream isToCheck = JsonFileParser.class.getClassLoader().getResourceAsStream(fileName);
                InputStream is = (isToCheck == null) ? new FileInputStream(fileName) : isToCheck;
                InputStreamReader inputStreamReader = new InputStreamReader(is, StandardCharsets.UTF_8);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
        ) {

            String bufLine;
            while ((bufLine = bufferedReader.readLine()) != null) {
                if (!"".equals(bufLine)) {
                    List<Object> columnValues = Arrays.asList(bufLine.split(","));
                    // 去除两端空格
                    columnValues = columnValues.stream().map(o -> o.toString().trim()).collect(Collectors.toList());
                    dataList.add(columnValues);
                }
            }
        } catch (Exception e) {
            log.error("数据集数据文件获取失败", e);
        }
        return dataList;
    }
}
