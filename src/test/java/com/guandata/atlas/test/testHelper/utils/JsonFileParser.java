package com.guandata.atlas.test.testHelper.utils;

import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.charset.StandardCharsets;

public class JsonFileParser<T> {
    private static Logger log = LoggerFactory.getLogger(JsonFileParser.class);

    private final Class<T> clazz;

    public JsonFileParser(Class<T> c) {
        this.clazz = c;
    }

    public T objectFromFile(String fileName) {
        return objectFromJsonString(jsonStringFromFile(fileName));
    }

    public static String jsonStringFromFile(String fileName) {
        StringBuilder sb = new StringBuilder();
        try (
                InputStream isToCheck = JsonFileParser.class.getClassLoader().getResourceAsStream(fileName);
                InputStream is = (isToCheck == null) ? new FileInputStream(fileName) : isToCheck;
                InputStreamReader inputStreamReader = new InputStreamReader(is, StandardCharsets.UTF_8);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
        ) {
            String bufLine;
            while ((bufLine = bufferedReader.readLine()) != null) {
                sb.append(bufLine);
            }
        } catch (Exception e) {
            log.error("数据集文件获取失败", e);
        }

        return sb.toString();
    }

    public T objectFromJsonString(String jsonString) {
        return JSONObject.parseObject(jsonString, clazz);
    }
}
