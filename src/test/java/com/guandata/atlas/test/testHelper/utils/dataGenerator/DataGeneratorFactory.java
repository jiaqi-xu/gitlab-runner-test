package com.guandata.atlas.test.testHelper.utils.dataGenerator;

import com.guandata.atlas.test.testHelper.constant.DataSetConstant;
import com.guandata.atlas.test.testHelper.utils.dataGenerator.generator.*;

import java.util.HashMap;
import java.util.Map;

public class DataGeneratorFactory {
    private static final Map<String, AbstractDataGenerator<?>> generatorMap = new HashMap<>();
    static{
        generatorMap.put(DataSetConstant.DATA_TYPE_BOOLEAN, new BooleanDataGenerator());
        generatorMap.put(DataSetConstant.DATA_TYPE_DATE, new DateDataGenerator(2000, 2021));
        generatorMap.put(DataSetConstant.DATA_TYPE_DATETIME, new DateTimeDataGenerator(2000, 2021));
        generatorMap.put(DataSetConstant.DATA_TYPE_DOUBLE, new DoubleDataGenerator(0.0, 9999.0));
        generatorMap.put(DataSetConstant.DATA_TYPE_FLOAT, new FloatDataGenerator(0.0f, 9999.0f));
        generatorMap.put(DataSetConstant.DATA_TYPE_INTEGER, new IntegerDataGenerator(0, 9999));
        generatorMap.put(DataSetConstant.DATA_TYPE_LONG, new LongDataGenerator(0L, 9999L));
        generatorMap.put(DataSetConstant.DATA_TYPE_STRING, new StringDataGenerator(5));
    }

    public static AbstractDataGenerator<?> getGenerator(String name){
        return generatorMap.get(name);
    }

    public static Map<String,AbstractDataGenerator<?>> getGeneratorMap(){
        return generatorMap;
    }
}
