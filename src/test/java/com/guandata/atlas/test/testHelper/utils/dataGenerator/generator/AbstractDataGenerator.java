package com.guandata.atlas.test.testHelper.utils.dataGenerator.generator;

public abstract class AbstractDataGenerator<T> {
    public abstract T generate();

    public String generateString(){
        return generate().toString();
    }
}
