package com.guandata.atlas.test.testHelper.utils.dataGenerator.generator;

import java.util.Random;

public class BooleanDataGenerator extends AbstractDataGenerator<Boolean> {
    @Override
    public Boolean generate() {
        int i = new Random().nextInt(2);
        return i == 0;
    }

}
