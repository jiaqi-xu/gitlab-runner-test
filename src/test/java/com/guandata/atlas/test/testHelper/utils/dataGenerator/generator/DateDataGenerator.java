package com.guandata.atlas.test.testHelper.utils.dataGenerator.generator;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Random;

public class DateDataGenerator extends AbstractDataGenerator<LocalDateTime> {
    private int startYearInclusive;

    private int endYearExclusive;

    public DateDataGenerator(int startYearInclusive, int endYearExclusive) {
        this.startYearInclusive = startYearInclusive;
        this.endYearExclusive = endYearExclusive;
    }

    @Override
    public LocalDateTime generate() {
        long startEpochDayInclusive = LocalDate.of(startYearInclusive, 1, 1).toEpochDay();
        long endEpochDayExclusive = LocalDate.of(endYearExclusive, 1, 1).toEpochDay();
        long randomEpochDay = startEpochDayInclusive + new Random().nextInt((int) (endEpochDayExclusive - startEpochDayInclusive));
        return LocalDateTime.of(LocalDate.ofEpochDay(randomEpochDay), LocalTime.MIN);
    }

    @Override
    public String generateString() {
        return DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(generate());
    }
}
    
