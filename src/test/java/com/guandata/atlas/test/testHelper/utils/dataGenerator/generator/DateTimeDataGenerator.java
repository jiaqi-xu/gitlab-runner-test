package com.guandata.atlas.test.testHelper.utils.dataGenerator.generator;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Random;

public class DateTimeDataGenerator extends DateDataGenerator {
    public DateTimeDataGenerator(int startYearInclusive, int endYearExclusive) {
        super(startYearInclusive, endYearExclusive);
    }

    @Override
    public LocalDateTime generate() {
        LocalDateTime localDateTimeWithMinTime = super.generate();
        return LocalDateTime.of(localDateTimeWithMinTime.toLocalDate(), randomLocalTime());
    }

    private LocalTime randomLocalTime() {
        return LocalTime.ofSecondOfDay(LocalTime.MIN.toSecondOfDay() +
                new Random().nextInt(LocalTime.MAX.toSecondOfDay() - LocalTime.MIN.toSecondOfDay() + 1));
    }
}
