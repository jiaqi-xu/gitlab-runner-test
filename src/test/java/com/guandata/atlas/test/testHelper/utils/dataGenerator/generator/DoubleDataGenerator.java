package com.guandata.atlas.test.testHelper.utils.dataGenerator.generator;

import org.apache.commons.lang3.RandomUtils;

public class DoubleDataGenerator extends AbstractDataGenerator<Double> {
    private double startInclusive;

    private double endInclusive;

    public DoubleDataGenerator(double startInclusive, double endInclusive){
        this.startInclusive=startInclusive;
        this.endInclusive=endInclusive;
    }

    @Override
    public Double generate() {
        return RandomUtils.nextDouble(startInclusive, endInclusive);
    }

}
