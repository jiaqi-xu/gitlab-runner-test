package com.guandata.atlas.test.testHelper.utils.dataGenerator.generator;

import org.apache.commons.lang3.RandomUtils;

public class FloatDataGenerator extends AbstractDataGenerator<Float> {
    private float startInclusive;

    private float endInclusive;

    public FloatDataGenerator(float startInclusive, float endInclusive){
        this.startInclusive=startInclusive;
        this.endInclusive=endInclusive;
    }

    @Override
    public Float generate() {
        return RandomUtils.nextFloat(startInclusive, endInclusive);
    }

}
