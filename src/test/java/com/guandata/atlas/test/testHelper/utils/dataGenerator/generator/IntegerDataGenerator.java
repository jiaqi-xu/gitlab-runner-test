package com.guandata.atlas.test.testHelper.utils.dataGenerator.generator;

public class IntegerDataGenerator extends AbstractDataGenerator<Integer> {
    private int minInclusive;

    private int maxInclusive;

    public IntegerDataGenerator(int minInclusive, int maxInclusive) {
        this.minInclusive = minInclusive;
        this.maxInclusive = maxInclusive;
    }

    public IntegerDataGenerator() {
        this(Integer.MIN_VALUE, Integer.MAX_VALUE);
    }

    @Override
    public Integer generate() {
        return minInclusive + (int) (Math.random() * (maxInclusive - minInclusive + 1));
    }

}
