package com.guandata.atlas.test.testHelper.utils.dataGenerator.generator;

import org.apache.commons.lang3.RandomUtils;

public class LongDataGenerator extends AbstractDataGenerator<Long> {
    private long startInclusive;

    private long endExclusive;

    public LongDataGenerator(long startInclusive, long endExclusive) {
        this.startInclusive = startInclusive;
        this.endExclusive = endExclusive;
    }

    @Override
    public Long generate() {
        return RandomUtils.nextLong(startInclusive, endExclusive);
    }

}
