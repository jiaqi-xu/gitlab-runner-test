package com.guandata.atlas.test.testHelper.utils.dataGenerator.generator;

import java.util.UUID;

public class StringDataGenerator extends AbstractDataGenerator<String> {

    private int strLen;

    public StringDataGenerator(int stringLength) {
        this.strLen = stringLength;
    }

    public StringDataGenerator() {
        this(4);
    }

    @Override
    public String generate() {
        checkLen();
        String optionalLetterString = "abcdefghijklmnopqrstuvwxyz";
        char firstLetter = optionalLetterString.charAt((int) (Math.random() * optionalLetterString.length()));
        String nextString = UUID.randomUUID().toString().substring(0, strLen-1);
        return firstLetter + nextString;
    }

    @Override
    public String generateString() {
        return this.generate();
    }

    private void checkLen() {
        if (strLen <= 0) {
            throw new RuntimeException("随机字符串长度必须大于0！");
        }
    }
}
